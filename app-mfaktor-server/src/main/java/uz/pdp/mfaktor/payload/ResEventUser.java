package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResEventUser {
    private UUID userId;
    private UUID id;
    private String photoUrl;
    private String fullName;
    private String phoneNumber;
    private String telegramUsername;
    private double balance;
    private String payType;
    private String seatName;
    private Timestamp payTime;
    private Timestamp targetTime;
    private Boolean participated;
    private String comment;
}
