package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResUserOutcome {
    private Timestamp eventDate;
    private String eventName;
    private String seatName;
    private double expenseSum;
    private double saleSum;
    private double seatPrice;
}
