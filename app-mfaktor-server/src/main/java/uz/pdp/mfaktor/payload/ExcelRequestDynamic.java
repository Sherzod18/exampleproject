package uz.pdp.mfaktor.payload;

import lombok.Data;

import java.util.List;

@Data
public class ExcelRequestDynamic {
    private List<Object> objects;

    private List<String> columns;

    private String title;
}
