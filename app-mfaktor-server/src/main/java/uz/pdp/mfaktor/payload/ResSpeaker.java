package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResSpeaker {
    private UUID id;
    private String photoUrl;
    private List<String> logosUrl;
    private String fullName;
    private String name;
    private String lastName;
    private String company;
    private String positionUz;
    private String positionRu;
    private String phoneNumber;
    private Integer positionId;
    private int countEvent;
    private String about;

}
