package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.enums.PriceType;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqEvent {
    private UUID id;
    private String title;
    private String serialNumber;
    private List<UUID> speakersId;
    private UUID eventBannerId;
    private String more;
    private Timestamp startTime;
    private Timestamp registerTime;
    private Integer languageId;
    private double fixedPrice;
    private Integer eventTypeId;
    private UUID placeCapacityId;
    private Boolean isTop;




}
