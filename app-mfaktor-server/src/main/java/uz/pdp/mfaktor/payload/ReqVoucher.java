package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqVoucher {
    private UUID id;
    private String voucherId;
    private UUID ownerId;
    private double price;
    Timestamp deadline;
    private String status;
}
