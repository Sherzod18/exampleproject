package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.Article;

import java.util.List;

/**
 * Created by Pinup on 26.07.2019.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResArticle {
    List<Article> articles;
    Integer totalPages;
}
