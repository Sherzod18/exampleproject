package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResUserIncomeAndOutcome {
    private List<ResUserIncome>incomes;
    private List<ResUserOutcome>outcomes;
}
