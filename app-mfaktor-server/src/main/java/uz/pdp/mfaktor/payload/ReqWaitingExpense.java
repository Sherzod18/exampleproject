package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqWaitingExpense {
    private UUID id;
    private Integer expenseTypeId;
    private double sum;
    private Timestamp deadline;
    private String comment;
}
