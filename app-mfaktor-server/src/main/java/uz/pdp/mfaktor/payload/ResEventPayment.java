package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResEventPayment {
    private String payTypeUz;
    private String payTypeRu;
    private double sum;
    private double paymentToday;
    private double weeklyIncome;
}
