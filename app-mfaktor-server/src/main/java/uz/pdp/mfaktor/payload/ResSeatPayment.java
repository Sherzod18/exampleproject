package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.PayType;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResSeatPayment {
    private String fullName;
    private String phoneNumber;
    private String seatName;
    private Timestamp payDate;
    private double sum;
    private String payType;

}
