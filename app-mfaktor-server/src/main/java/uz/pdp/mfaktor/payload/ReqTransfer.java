package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqTransfer {
    private Integer fromCashId;
    private Integer toCashId;
    private double sum;
    private double usdSum;
    private Timestamp time;
    private String details;
    private boolean toUsd;
    private boolean fromUsd;
}
