package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResDashboard {
    private int dailyIncomeCount;
    private double dailyIncome;
    private int weeklyIncomeCount;
    private double weeklyIncome;
    private int monthlyIncomeCount;
    private double monthlyIncome;
    private double dailyExpense;
    private double weeklyExpense;
    private double monthlyExpense;
    private double dailyProfit;
    private double weeklyProfit;
    private double monthlyProfit;
    private int countClients;
    private int countNewUserWeekly;
    private int countEvents;
    private int countEventMonthly;
    private double dailyWaitingPayment;
    private double weeklyWaitingPayment;
    private double monthlyWaitingPayment;
    private double currentAllBalance;
    private List<ResBalanceByPayType> balanceByPayTypes;
    private List<ResBalanceByCashType> balanceByCashTypes;
    private double allIncome;
    private double allOutcome;
    private double balance;
}
