package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqCashType {
    private Integer id;
    private String nameUz;
    private String nameRu;
    private String color;
    private List<Integer> payTypeIds;
    private String payTypes;
    private String description;
}
