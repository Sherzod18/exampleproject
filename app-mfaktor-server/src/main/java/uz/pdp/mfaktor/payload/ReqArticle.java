package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.Attachment;
import uz.pdp.mfaktor.entity.Category;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqArticle {
    private UUID id;
    private UUID photoId;
    @NotBlank
    private String title;
    private String author;
    @NotBlank
    private String categoryId;
    @NotBlank
    private String description;

}
