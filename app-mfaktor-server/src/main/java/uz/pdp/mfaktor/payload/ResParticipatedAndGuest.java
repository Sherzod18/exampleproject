package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResParticipatedAndGuest {
    private Integer registeredCount;
    private Integer participatedCount;
    private Integer notParticipatedCount;
    private Integer guestCount;

}
