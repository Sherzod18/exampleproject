package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqExpense {
    private UUID id;
    private Integer expenseTypeId;
    private double sum;
    private Timestamp date;
    private Integer cashTypeId;
    private String comment;
    private UUID eventId;
    private UUID waitingExpenseId;
}
