package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResBalanceByPayType {
    private String payTypeUz;
    private String payTypeRu;
    private double dailyIncome;
    private double weeklyIncome;
    private double monthlyIncome;
    private double dailyOutcome;
    private double weeklyOutcome;
    private double monthlyOutcome;
    private double balance;
}
