package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResUser {
    private UUID id;
    private String photoUrl;
    private String fullName;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Date birthDate;
    private String company;
    private String positionUz;
    private String positionRu;
    private Integer positionId;
    private String awareUz;
    private String awareRu;
    private Integer awareId;
    private int countVisit;
    private String addressUz;
    private String addressRu;
    private Integer regionId;
    private double balance;
    private double sale;
}
