package uz.pdp.mfaktor.payload;

import lombok.Data;

@Data
public class ReqPassword {
    private String oldPassword;
    private String newPassword;
}
