package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.User;

import java.util.List;

/**
 * Created by Pinup on 06.08.2019.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResClient {
    private List<ResUser> users;
    private Integer totalPages;
    private Integer totalElements;
}
