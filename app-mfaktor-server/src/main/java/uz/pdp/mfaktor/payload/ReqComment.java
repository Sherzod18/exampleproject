package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqComment {
    private UUID id;
    private String text;
    private UUID articleId;
}
