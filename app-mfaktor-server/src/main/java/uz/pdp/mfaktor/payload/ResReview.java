package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResReview {
    private double avgStar;
    private Integer countStars;
    private int one;
    private int two;
    private int three;
    private int four;
    private int five;
    List<ResReviewList> reviewList;

    public ResReview(int one, int two, int three, int four, int five, List<ResReviewList> reviewList) {
        this.one = one;
        this.two = two;
        this.three = three;
        this.four = four;
        this.five = five;
        this.reviewList = reviewList;
    }
}
