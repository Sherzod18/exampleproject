package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResUserIncome {
    private Timestamp payDate;
    private double sum;
    private String payTypeUz;
    private String payTypeRu;
    private String details;
}
