package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.enums.PlaceStatus;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqSeat {
    private UUID userId;
    private PlaceStatus placeStatus;
    private UUID seatId;
    private Timestamp bookedExpireDate;
    private UUID eventId;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String company;
    private Integer positionId;
    private Integer regionId;
    List<UUID> seatIds;
    private String comment;

}
