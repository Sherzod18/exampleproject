package uz.pdp.mfaktor.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ReqExcel{

    private UUID fileId;

    private Double cost;

    private Double customCost;

    private Double fareCost;

    private Double retailPrice;

    private Double wholesalePrice;


}
