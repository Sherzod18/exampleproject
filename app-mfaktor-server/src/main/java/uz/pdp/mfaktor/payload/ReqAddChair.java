package uz.pdp.mfaktor.payload;

import lombok.Data;
import java.util.UUID;

@Data
public class ReqAddChair {
    private UUID eventId;
    private String name;
    private Double price;
    private Integer row;
}
