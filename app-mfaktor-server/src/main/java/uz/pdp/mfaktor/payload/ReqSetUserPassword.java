package uz.pdp.mfaktor.payload;

import lombok.Data;

/**
 * Created by Pinup on 01.06.2019.
 */
@Data
public class ReqSetUserPassword {
    private String phoneNumber;
    private String password;
    private String prePassword;
}
