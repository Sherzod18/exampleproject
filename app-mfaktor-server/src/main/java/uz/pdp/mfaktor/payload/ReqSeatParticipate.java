package uz.pdp.mfaktor.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
public class ReqSeatParticipate {
    private UUID seatId;
    private boolean participated;
}
