package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResEvent {
    private UUID id;
    private String serialNumber;
    private String photoUrl;
    private String title;
    private List<UUID> speakersId;
    private String speakers;
    private Integer eventTypeId;
    private String eventType;
    private String status;
    private Timestamp startTime;
    private Timestamp registerTime;
    private Integer languageId;
    private String language;
    private Boolean isOpen;
    private Boolean isTop;
    private int allSeat;
    private int bookedAndPartly;
    private int emptySeat;
    private int soldSeat;
    private int reservedSeat;
    private int vipSeat;
    private double rating;
    private int countReview;
    private double minPrice;
    private double maxPrice;
    private double sum;
    private double paidSum; //user joy uchun to'lagan pul
    private String place;
    private String placeStatus;
    private String aboutEvent;
    private String aboutSpeaker;

    public ResEvent(UUID id, String serialNumber, String photoUrl, String title, List<UUID> speakersId,String speakers,
                    Integer eventTypeId,String eventType, String status, Timestamp startTime,Timestamp registerTime,Integer languageId,String language,Boolean isOpen,Boolean isTop,
                    int allSeat, int bookedAndPartly, int emptySeat,int soldSeat,int reservedSeat,
                    int vipSeat, double rating, int countReview, double minPrice, double maxPrice, double sum, String aboutEvent, String aboutSpeaker) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.photoUrl = photoUrl;
        this.title = title;
        this.speakersId=speakersId;
        this.speakers = speakers;
        this.eventTypeId=eventTypeId;
        this.eventType = eventType;
        this.status = status;
        this.startTime = startTime;
        this.registerTime=registerTime;
        this.languageId=languageId;
        this.language=language;
        this.isOpen=isOpen;
        this.isTop=isTop;
        this.allSeat = allSeat;
        this.bookedAndPartly= bookedAndPartly;
        this.emptySeat = emptySeat;
        this.soldSeat=soldSeat;
        this.reservedSeat=reservedSeat;
        this.vipSeat=vipSeat;
        this.rating = rating;
        this.countReview = countReview;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.sum = sum;
        this.aboutEvent = aboutEvent;
        this.aboutSpeaker = aboutSpeaker;
    }

    public ResEvent(UUID id, String serialNumber, String title) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.title = title;
    }
}
