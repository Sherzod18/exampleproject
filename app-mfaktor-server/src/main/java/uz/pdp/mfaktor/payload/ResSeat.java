package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.enums.PlaceStatus;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResSeat {
    private UUID seatId;
    private String name;
    private PlaceStatus placeStatus;
    private Integer row;
    private double price;
    private String user;
    private String phoneNumber;
}
