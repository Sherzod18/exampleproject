package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class PaymentPageableResponse {

    private List<ResPayment> resPayments;

    private int total;

    private int page;

    private int size;
}
