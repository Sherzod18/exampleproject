package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResExpense {
    private UUID id;
    private Integer expenseTypeId;
    private String expenseTypeNameUz;
    private String expenseTypeNameRu;
    private double sum;
    private Timestamp date;
    private Integer cashTypeId;
    private String cashTypeUz;
    private String cashTypeRu;
    private String comment;
    private UUID eventId;
    private String eventName;
    private boolean canEdit;
}
