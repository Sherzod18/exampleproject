package uz.pdp.mfaktor.payload;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
public class ReqUser extends ReqSeat {

    private UUID id;
    @Pattern(regexp = "^[+][9][9][8][0-9]{9}$", message = "Phone number must be 13 digits.")
    private String phoneNumber;

    private String password;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    private Integer regionId;
    private Date birthDate;
    private Integer activityId;
    private Integer positionId;
    private String telegramUsername;
    private Integer telegramId;
    private String company;
    private double sale;
    private String about;
    private Integer awareId;
    private List<Integer> roleIds;
    private UUID photoId;


}
