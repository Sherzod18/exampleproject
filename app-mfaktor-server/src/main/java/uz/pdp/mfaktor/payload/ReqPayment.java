package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqPayment extends ReqUser {
    private double paySum;
    private double sumUsd;
    private Integer payTypeId;
    private Timestamp payDate;
    private String details;
    private boolean fromTelegram;
}
