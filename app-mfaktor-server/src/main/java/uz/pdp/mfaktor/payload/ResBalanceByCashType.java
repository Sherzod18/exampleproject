package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResBalanceByCashType {
    private String nameUz;
    private String name;
    private double allIncome;
    private double allOutcome;
    private double balance;
}
