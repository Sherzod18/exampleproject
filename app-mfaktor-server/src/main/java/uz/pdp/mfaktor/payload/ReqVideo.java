package uz.pdp.mfaktor.payload;

import lombok.Data;

import java.util.UUID;

@Data
public class ReqVideo {
    private String title;
    private String videoUrl;
    private UUID videoTypeId;

}
