package uz.pdp.mfaktor.payload;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
public class ReqSpeaker {
    private UUID id;
    @NotBlank
    private String phoneNumber;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;


    private Integer positionId;
    private String company;
    @NotNull
    private UUID photoId;
    private List<UUID> logoIds;
    private String about;

}
