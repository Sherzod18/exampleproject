package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResEventPaymentAndBalance {
    private List<ResEventPayment> resEventPayments;
    private double allSum;
    private double debtSum;
    private double weekSum;
    private double todaySum;
    private double outcome;
    private double income;
}
