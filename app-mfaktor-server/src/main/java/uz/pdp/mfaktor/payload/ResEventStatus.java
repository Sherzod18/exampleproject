package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.enums.EventStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResEventStatus {
    private String name;
    private String value;
}
