package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResReviewList {
    private UUID id;
    private UUID userId;
    private String photoUrl;
    private String firstName;
    private String lastName;
    private int star;
    private String text;
    private Timestamp date;
    private boolean isPublished;
    private String company;

}
