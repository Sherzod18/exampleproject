package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResPayment {
    private UUID id;
    private String photoUrl;
    private String firstName;
    private String lastName;
    private String fullName;
    private String phoneNumber;
    private String telegramUsername;
    private double paySum;
    private String payTypeUz;
    private String payTypeRu;
    private Timestamp payTime;

}
