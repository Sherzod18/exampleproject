package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResWaitingExpense {
    private UUID id;
    private Integer expenseTypeId;
    private String expenseTypeNameUz;
    private String expenseTypeNameRu;
    private double sum;
    private double leftover;
    private Timestamp deadline;
    private String comment;
    private boolean paid;
}
