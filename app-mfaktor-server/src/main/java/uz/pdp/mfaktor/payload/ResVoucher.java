package uz.pdp.mfaktor.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResVoucher {
    private UUID id;
    private String voucherId;
    private UUID ownerId;
    private String firstName;
    private String lastName;
    private double price;
    private double residue;
    private Timestamp deadline;
    private String status;

}
