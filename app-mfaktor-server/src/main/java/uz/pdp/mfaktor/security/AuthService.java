package uz.pdp.mfaktor.security;

import com.google.api.client.http.HttpStatusCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.entity.enums.RoleName;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqSignUp;
import uz.pdp.mfaktor.payload.ReqUser;
import uz.pdp.mfaktor.repository.*;

import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    PositionRepository positionRepository;
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    ActivityRepository activityRepository;
    @Autowired
    MessageSource messageSource;

    @Override
    public UserDetails loadUserByUsername(String phoneNumber) throws UsernameNotFoundException {
        if (getPhoneChecked(phoneNumber)) {
            return userRepository.findByPhoneNumber(phoneNumber).orElseThrow(() -> new UsernameNotFoundException(phoneNumber));
        }
            throw new UsernameNotFoundException(messageSource.getMessage("error.in.checking", null, LocaleContextHolder.getLocale()));
        }

    public UserDetails loadUserById(UUID userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("Animal id not found: " + userId));
    }


    public ApiResponse register(ReqSignUp reqSignUp) {
        if (getPhoneCheckedSignUp(reqSignUp.getPhoneNumber())){
            Optional<User> optionalUser = userRepository.findByPhoneNumber(reqSignUp.getPhoneNumber());
            if (optionalUser.isPresent()) {
                return new ApiResponse(messageSource.getMessage("phone.number.exist", null, LocaleContextHolder.getLocale()), false);
            } else {
                User user = new User(reqSignUp.getPhoneNumber(),
                        passwordEncoder.encode(reqSignUp.getPassword()),
                        reqSignUp.getFirstName(),
                        reqSignUp.getLastName(),
                        roleRepository.findAllByName(RoleName.ROLE_USER));
                userRepository.save(user);
                return new ApiResponse(messageSource.getMessage("user.created", null, LocaleContextHolder.getLocale()), true);
            }
        }else{
            return new ApiResponse(messageSource.getMessage("phone.number.exist", null, LocaleContextHolder.getLocale()), false);
        }

    }

    public Boolean getPhoneCheckedSignUp(String phoneNumber) {
        try {
            UserRecord userRecord = FirebaseAuth.getInstance().getUserByPhoneNumber(phoneNumber);
            if (userRecord == null) {
                throw new UsernameNotFoundException(messageSource.getMessage("user.notfound", null, LocaleContextHolder.getLocale()));
            } else {
                if (!userRepository.existsByPhoneNumber(userRecord.getPhoneNumber())) {
//                    FirebaseAuth.getInstance().deleteUser(userRecord.getUid());
                    return true;
                }
            }
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
            throw new UsernameNotFoundException(messageSource.getMessage("error.in.checking", null, LocaleContextHolder.getLocale()));
        }
        return false;
    }
    public Boolean getPhoneChecked(String phoneNumber) {
        try {
            UserRecord userRecord = FirebaseAuth.getInstance().getUserByPhoneNumber(phoneNumber);
            if (userRecord == null) {
                throw new UsernameNotFoundException(messageSource.getMessage("user.notfound", null, LocaleContextHolder.getLocale()));
            } else {
                if (userRepository.existsByPhoneNumber(userRecord.getPhoneNumber())) {
                    FirebaseAuth.getInstance().deleteUser(userRecord.getUid());
                    return true;
                }
            }
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
            throw new UsernameNotFoundException(messageSource.getMessage("error.in.checking", null, LocaleContextHolder.getLocale()));
        }
        return false;
    }


}
