package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.mfaktor.entity.Seat;
import uz.pdp.mfaktor.entity.SeatPayment;

import java.sql.Timestamp;
import java.util.UUID;

public interface SeatPaymentRepository extends JpaRepository<SeatPayment, UUID> {

    @Query(value = "SELECT COALESCE (sum(amount),0) FROM seat_payment t WHERE t.created_at >= date_trunc('week', current_date)", nativeQuery = true)
    double incomeWeeklySum();

    @Query(value = "SELECT count (*) FROM seat_payment t WHERE t.created_at >= date_trunc('week', current_date)", nativeQuery = true)
    int incomeWeeklyCount();

    @Query(value = "SELECT COALESCE (sum(amount),0) FROM seat_payment t WHERE t.created_at >= date_trunc('month', current_date)", nativeQuery = true)
    double incomeMonthlySum();

    @Query(value = "SELECT count (*) FROM seat_payment t WHERE t.created_at >= date_trunc('month', current_date)", nativeQuery = true)
    int incomeMonthlyCount();

    @Query(value = "SELECT COALESCE (sum(amount),0) FROM seat_payment t WHERE t.created_at >= date_trunc('day', current_date)", nativeQuery = true)
    double incomeDailySum();

    @Query(value = "SELECT count (*) FROM seat_payment t WHERE t.created_at >= date_trunc('day', current_date)", nativeQuery = true)
    int incomeDailyCount();

    @Query(value = "select coalesce (sum(amount),0) from seat_payment t where t.payment_id in (select id from payment where pay_type_id=:payTypeId) and t.created_at >= date_trunc('day', current_date)", nativeQuery = true)
    double incomeDailySumByPayType(@Param(value = "payTypeId") Integer payTypeId);

    @Query(value = "select coalesce (sum(amount),0) from seat_payment t where t.payment_id in (select id from payment where pay_type_id=:payTypeId) and t.created_at >= date_trunc('week', current_date)", nativeQuery = true)
    double incomeWeeklySumByPayType(@Param(value = "payTypeId") Integer payTypeId);

    @Query(value = "select coalesce (sum(amount),0) from seat_payment t where t.payment_id in (select id from payment where pay_type_id=:payTypeId) and t.created_at >= date_trunc('month', current_date)", nativeQuery = true)
    double incomeMonthlySumByPayType(@Param(value = "payTypeId") Integer payTypeId);

    @Query(value = "select coalesce (sum(amount),0) from seat_payment t where t.payment_id in (select id from payment where pay_type_id in(select pay_types_id from cash_type_pay_types where cash_type_id=:cashTypeId))", nativeQuery = true)
    double incomeAllByCashType(@Param(value = "cashTypeId") Integer cashTypeId);

    @Query(value = "select coalesce (sum(amount),0) from seat_payment t where t.payment_id in (select id from payment where pay_type_id=:payTypeId)", nativeQuery = true)
    double incomeAllByPayType(@Param(value = "payTypeId") Integer payTypeId);

    @Query(value = "select coalesce (sum(amount),0) from seat_payment", nativeQuery = true)
    double incomeAll();
}

