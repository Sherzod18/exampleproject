package uz.pdp.mfaktor.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.Comment;

import java.util.List;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    List<Comment> findAllByArticleId(UUID id);
}
