package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.EventType;
import uz.pdp.mfaktor.entity.Ticket;
import uz.pdp.mfaktor.entity.User;

import java.util.List;
import java.util.UUID;

public interface TicketRepository extends JpaRepository<Ticket, UUID> {
    List<Ticket>findAllByUserAndActiveAndEventType(User user, boolean active, EventType eventType);
}
