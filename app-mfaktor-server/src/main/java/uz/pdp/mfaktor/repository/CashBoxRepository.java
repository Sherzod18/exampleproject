package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.mfaktor.entity.CashBox;
import uz.pdp.mfaktor.entity.CashType;

import java.util.Optional;
import java.util.UUID;

public interface CashBoxRepository extends JpaRepository<CashBox, UUID> {
    Optional<CashBox> findByToCash(CashType toCash);

    @Query(value = "select coalesce (sum (cb.sum),0) from cash_box cb where cb.to_cash_id=:cashTypeId", nativeQuery = true)
    double getSumToByCashType(@Param("cashTypeId") Integer cashTypeId);

    @Query(value = "select coalesce (sum (cb.sum),0) from cash_box cb where cb.from_cash_id=:cashTypeId", nativeQuery = true)
    double getSumFromByCashType(@Param("cashTypeId") Integer cashTypeId);
}
