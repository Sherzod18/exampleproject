package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.Chair;

import java.util.UUID;

public interface ChairRepository extends JpaRepository<Chair, UUID> {
}
