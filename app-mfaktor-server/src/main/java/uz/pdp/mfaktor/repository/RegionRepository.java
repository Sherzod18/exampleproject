package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.mfaktor.entity.Region;
import uz.pdp.mfaktor.projection.CustomRegion;

import java.util.UUID;
@RepositoryRestResource(path = "/region",collectionResourceRel = "list",excerptProjection = CustomRegion.class)
public interface RegionRepository extends JpaRepository<Region, Integer> {

}
