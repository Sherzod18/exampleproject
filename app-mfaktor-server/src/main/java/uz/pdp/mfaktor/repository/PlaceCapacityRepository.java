package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.PlaceCapacity;

import java.util.List;
import java.util.UUID;

public interface PlaceCapacityRepository extends JpaRepository<PlaceCapacity, UUID> {
}

