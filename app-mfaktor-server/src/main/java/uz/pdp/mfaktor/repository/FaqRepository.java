package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.mfaktor.entity.Faq;
import uz.pdp.mfaktor.projection.CustomFaq;

import java.util.UUID;

@RepositoryRestResource(path = "faq", collectionResourceRel = "list", excerptProjection = CustomFaq.class)
public interface FaqRepository extends JpaRepository<Faq, UUID> {
}
