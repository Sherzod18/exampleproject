package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.mfaktor.entity.Aware;
import uz.pdp.mfaktor.projection.CustomAware;

import java.util.UUID;
@RepositoryRestResource(path = "/aware",excerptProjection = CustomAware.class)
public interface AwareRepository extends JpaRepository<Aware, Integer> {

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends Aware> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(Integer integer);
}

