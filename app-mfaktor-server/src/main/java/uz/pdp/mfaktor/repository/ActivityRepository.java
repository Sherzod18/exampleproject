package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.mfaktor.entity.Activity;
import uz.pdp.mfaktor.projection.CustomActivity;

@RepositoryRestResource(path = "/activity",collectionResourceRel = "list",excerptProjection = CustomActivity.class)
public interface ActivityRepository extends JpaRepository<Activity,Integer> {
}
