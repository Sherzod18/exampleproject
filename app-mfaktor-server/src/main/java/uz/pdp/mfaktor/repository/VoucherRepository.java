package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.Voucher;

import java.util.UUID;

public interface VoucherRepository extends JpaRepository<Voucher, UUID> {

}
