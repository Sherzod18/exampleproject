package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.mfaktor.entity.WaitingExpense;

import java.util.List;
import java.util.UUID;

public interface WaitingExpenseRepository extends JpaRepository<WaitingExpense, UUID> {


    @Query(value = "SELECT COALESCE (sum(sum),0) FROM waiting_expense t WHERE t.paid=false", nativeQuery = true)
    double weDebt();
//
//    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.date >= date_trunc('day', current_date)",nativeQuery = true)
//    double expenseDailySum();
//
//    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.date >= date_trunc('week', current_date)",nativeQuery = true)
//    double expenseWeeklySum();
//
//    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.date >= date_trunc('month', current_date)",nativeQuery = true)
//    double expenseMonthlySum();

    List<WaitingExpense> findAllByPaidOrderByDeadline(boolean paid);

}
