package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.mfaktor.entity.CashType;
import uz.pdp.mfaktor.entity.PayType;

import java.util.Collection;
import java.util.List;

public interface CashTypeRepository extends JpaRepository<CashType, Integer> {
    CashType findByDescription(String description);



    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends CashType> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(Integer integer);

    CashType findByNameUz(String text);

    CashType findByPayTypesIn(List<PayType> payTypes);
}

