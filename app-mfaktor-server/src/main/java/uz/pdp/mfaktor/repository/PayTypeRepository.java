package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.mfaktor.entity.PayType;
import uz.pdp.mfaktor.projection.CustomPayType;

import java.util.UUID;

@RepositoryRestResource(path = "payType",collectionResourceRel = "list",excerptProjection = CustomPayType.class)
public interface PayTypeRepository extends JpaRepository<PayType, Integer> {

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends PayType> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(Integer integer);

    PayType findByNameUz(String text);
}

