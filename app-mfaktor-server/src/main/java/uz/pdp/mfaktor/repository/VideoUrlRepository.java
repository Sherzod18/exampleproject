package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.VideoType;
import uz.pdp.mfaktor.entity.VideoUrl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

public interface VideoUrlRepository extends JpaRepository<VideoUrl, UUID> {
    Page<VideoUrl> findAllByVideoType(VideoType videoType, Pageable pageable);
}
