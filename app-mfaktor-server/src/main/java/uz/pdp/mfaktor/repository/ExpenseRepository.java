package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.mfaktor.entity.Expense;

import java.util.UUID;

public interface ExpenseRepository extends JpaRepository<Expense, UUID> {
    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense", nativeQuery = true)
    double expenseAllSum();

    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.date >= date_trunc('day', current_date)", nativeQuery = true)
    double expenseDailySum();

    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.date >= date_trunc('week', current_date)", nativeQuery = true)
    double expenseWeeklySum();

    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.date >= date_trunc('month', current_date)", nativeQuery = true)
    double expenseMonthlySum();

    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.cash_type_id=:cashTypeId and t.date >= date_trunc('day', current_date)", nativeQuery = true)
    double expenseDailySumByCashType(@Param(value = "cashTypeId") Integer cashTypeId);

    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.cash_type_id=:cashTypeId and t.date >= date_trunc('week', current_date)", nativeQuery = true)
    double expenseWeeklySumByCashType(@Param(value = "cashTypeId") Integer cashTypeId);

    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.cash_type_id=:cashTypeId and t.date >= date_trunc('month', current_date)", nativeQuery = true)
    double expenseMonthlySumByCashType(@Param(value = "cashTypeId") Integer cashTypeId);

    @Query(value = "SELECT COALESCE (sum(sum),0) FROM expense t WHERE t.cash_type_id=:cashTypeId", nativeQuery = true)
    double expenseAllByCashType(@Param(value = "cashTypeId") Integer cashTypeId);

    @Query(value = "select coalesce(sum(sum),0) from expense t where event_id=:eventId", nativeQuery = true)
    double expenseByEvent(@Param(value = "eventId") UUID eventId);

}
