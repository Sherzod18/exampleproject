package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.Attachment;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
}
