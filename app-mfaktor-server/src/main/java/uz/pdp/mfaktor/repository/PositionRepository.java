package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.mfaktor.entity.Position;
import uz.pdp.mfaktor.projection.CustomPosition;

import java.util.UUID;
@RepositoryRestResource(path = "/position",collectionResourceRel = "list",excerptProjection = CustomPosition.class)
public interface PositionRepository extends JpaRepository<Position, Integer> {

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends Position> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(Integer integer);
}
