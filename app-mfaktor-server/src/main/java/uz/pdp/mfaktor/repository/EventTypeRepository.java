package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.mfaktor.entity.Aware;
import uz.pdp.mfaktor.entity.EventType;
import uz.pdp.mfaktor.projection.CustomAware;
import uz.pdp.mfaktor.projection.CustomEventType;

import java.util.UUID;

@RepositoryRestResource(path = "eventType",excerptProjection = CustomEventType.class)
public interface EventTypeRepository extends JpaRepository<EventType, Integer> {

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends EventType> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(Integer integer);
}

