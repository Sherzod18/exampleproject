package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.mfaktor.entity.Payment;
import uz.pdp.mfaktor.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {
    List<Payment> findAllByUserAndLeftoverGreaterThanOrderByCreatedAt(User user, double a);

    @Query(value = "SELECT coalesce(SUM(leftover),0) FROM payment where user_id=:userId", nativeQuery = true)
    double sumOfLeftover(@Param(value = "userId") UUID userId);

    List<Payment> findAllByUserOrderByCreatedAt(User user);

    @Query(value = "SELECT COALESCE (sum(pay_sum),0) FROM payment t WHERE t.pay_type_id=:payTypeId and t.pay_date >= date_trunc('day', current_date)", nativeQuery = true)
    double paymentDailySumByPayType(@Param(value = "payTypeId") Integer payTypeId);

    @Query(value = "SELECT COALESCE (sum(pay_sum),0) FROM payment t WHERE t.pay_type_id=:payTypeId and t.pay_date >= date_trunc('week', current_date)", nativeQuery = true)
    double paymentWeeklySumByPayType(@Param(value = "payTypeId") Integer payTypeId);

    @Query(value = "SELECT COALESCE (sum(pay_sum),0) FROM payment t WHERE t.pay_type_id=:payTypeId and t.pay_date >= date_trunc('month', current_date)", nativeQuery = true)
    double paymentMonthlySumByPayType(@Param(value = "payTypeId") Integer payTypeId);


}

