package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.Message;

import java.util.UUID;

public interface MessageRepository extends JpaRepository<Message, UUID> {

}

