package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.mfaktor.entity.Language;
import uz.pdp.mfaktor.entity.PayType;
import uz.pdp.mfaktor.projection.CustomLanguage;

@RepositoryRestResource(path = "language",collectionResourceRel = "list",excerptProjection = CustomLanguage.class)
public interface LanguageRepository extends JpaRepository<Language,Integer> {

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends Language> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(Integer integer);

    PayType findByNameUz(String text);
}
