package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.Reklama;

import java.util.UUID;

public interface ReklamaRepository extends JpaRepository<Reklama, UUID> {


}
