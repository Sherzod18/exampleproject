package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.mfaktor.entity.TgChatsId;

@RepositoryRestResource(path = "/tgChatsId",collectionResourceRel = "list")
public interface TgChatsIdRepository extends JpaRepository<TgChatsId, Integer> {
}
