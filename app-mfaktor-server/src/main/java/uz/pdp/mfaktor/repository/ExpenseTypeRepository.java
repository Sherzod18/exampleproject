package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.mfaktor.entity.ExpenseType;
import uz.pdp.mfaktor.projection.CustomExpenseType;

import java.util.UUID;

@RepositoryRestResource(path = "/expenseType", itemResourceRel = "expenseType", collectionResourceRel = "list", excerptProjection = CustomExpenseType.class)
public interface ExpenseTypeRepository extends JpaRepository<ExpenseType, Integer> {

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends ExpenseType> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(Integer integer);
}
