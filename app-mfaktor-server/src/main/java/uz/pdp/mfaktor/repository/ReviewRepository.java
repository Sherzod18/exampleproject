package uz.pdp.mfaktor.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import uz.pdp.mfaktor.entity.Review;

import java.util.List;
import java.util.UUID;

public interface ReviewRepository extends PagingAndSortingRepository<Review, UUID> {
    Integer countReviewByEventId(UUID eventId);

    @Query(value = "SELECT COALESCE(SUM(star_count),0) FROM review t WHERE t.event_id=:eventId", nativeQuery = true)
    Integer sumOfStarEvent(@Param(value = "eventId") UUID eventId);

    @Query(value = "SELECT COUNT(star_count) FROM review WHERE star_count=:star AND event_id=:eventId OR speaker_id=:speakerId", nativeQuery = true)
    Integer countOfGivenValue(@Param(value = "star") int star, @Param(value = "eventId") UUID eventId, @Param(value = "speakerId") UUID speakerId);

    List<Review> findAllByEventIdOrSpeakerId(UUID eventId, UUID speakerId, Pageable pageable);

    List<Review> findAllByIsPublished(Boolean bool);

    List<Review> findAllByCreatedByAndEventIdOrSpeakerId(UUID userId, UUID eventId, UUID speakerId);


}
