package uz.pdp.mfaktor.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mfaktor.entity.Article;
import uz.pdp.mfaktor.entity.Category;

import java.util.List;
import java.util.UUID;

public interface ArticleRepository extends JpaRepository<Article, UUID> {
    Page<Article> findAllByActive(Boolean active, Pageable pageable);

    Page<Article> findAllByActiveAndCategory(Boolean active, Category category, Pageable pageable);

    Article getByUrlAndActive(String url, Boolean active);

    List<Article> findAllByTitleContainingIgnoreCase(String word);
}
