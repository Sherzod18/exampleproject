package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.mfaktor.entity.VideoType;
import uz.pdp.mfaktor.entity.VideoUrl;
import uz.pdp.mfaktor.projection.CustomCategory;
import uz.pdp.mfaktor.projection.CustomVideoType;

import java.util.List;
import java.util.UUID;
@RepositoryRestResource(path = "videoType",collectionResourceRel = "list",excerptProjection = CustomVideoType.class)
public interface VideoTypeRepository extends JpaRepository<VideoType, UUID> {
VideoType findByName(String name);
}
