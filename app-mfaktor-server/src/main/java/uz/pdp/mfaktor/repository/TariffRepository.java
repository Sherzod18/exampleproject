package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.mfaktor.entity.Tariff;
import uz.pdp.mfaktor.projection.CustomTariff;

@RepositoryRestResource(path = "/tariff",collectionResourceRel = "list",excerptProjection = CustomTariff.class)
public interface TariffRepository extends JpaRepository<Tariff,Integer> {

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends Tariff> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(Integer integer);
}
