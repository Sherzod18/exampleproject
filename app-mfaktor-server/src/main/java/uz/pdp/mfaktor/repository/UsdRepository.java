package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import uz.pdp.mfaktor.entity.Aware;
import uz.pdp.mfaktor.entity.Usd;
import uz.pdp.mfaktor.projection.CustomAware;

import java.util.List;
import java.util.UUID;

public interface UsdRepository extends JpaRepository<Usd, UUID> {

    @Query(value = "SELECT coalesce (rate,0) from usd t where t.created_at=(select max(created_at) FROM usd)", nativeQuery = true)
    double maxUsd();



}

