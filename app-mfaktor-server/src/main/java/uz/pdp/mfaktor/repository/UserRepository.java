package uz.pdp.mfaktor.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import uz.pdp.mfaktor.entity.Role;
import uz.pdp.mfaktor.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

//@RepositoryRestResource(path = "users")
public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByPhoneNumber(String phoneNumber);

    Optional<User> findFirstByTelegramId(Integer phoneNumber);

    Optional<User> findByTelegramId(Integer phoneNumber);

    Boolean existsByPhoneNumber(String phoneNumber);

    List<User> findFirst15ByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrPhoneNumberContaining(@Param("word") String firstName, @Param("word") String lastName, @Param("word") String phoneNumber);

    @Query(value = "SELECT sale FROM users where id=:userId", nativeQuery = true)
    double getSale(@Param(value = "userId") UUID userId);

    boolean existByPassportSerialAndIdNot(String passportSerial,Integer id);

    boolean existByPassportSerial(String passportSerial);

    Page<User> findAllByRolesIn(Role role, Pageable pageable);

    List<User> findAllByRolesIn(Role role);

    Optional<User> findByChatId(Long chatId);

    Page<User> findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrPhoneNumberContainingOrTelegramUsernameContainingIgnoreCaseOrCompanyContainingIgnoreCase(@Param("word") String firstName, @Param("word") String lastName, @Param("word") String phoneNumber, @Param("word") String telegram, @Param("word") String company, Pageable pageable);

    @Query(value = "SELECT * FROM users t WHERE t.created_at>= date_trunc('week', current_date)", nativeQuery = true)
    List<User> listNewUserWeekly();

    @Query(value = "select count (id) from users", nativeQuery = true)
    int countUsers();

    @Query(value = "select count (id) from users t WHERE t.created_at>= date_trunc('week', current_date)", nativeQuery = true)
    int countWeeklyUsers();
//
//    @RestResource(path = "count")
//    @Query(value = "select cast(u.id as varchar),u.first_name,u.last_name,u.phone_number,(select sum(p.leftover) from payment p where p.user_id=u.id),u.company, (select a.name_uz from aware a where a.id=u.aware_id),count(all) as soni from seat s inner join users u on s.user_id=u.id where participated=true group by u.id order by soni desc", nativeQuery = true)
//    Page<Object[]> getUsersByCountVisit(Pageable pageable);

    @Query(value = "select cast(u.id as varchar),cast(u.photo_id as varchar),u.first_name,u.last_name,u.phone_number, (select coalesce(sum(p.leftover),0) from payment p where p.user_id=u.id),u.company,(select a.name_uz from aware a where a.id=u.aware_id),count(s.id) as soni from seat s right join users u on s.user_id=u.id and participated=true group by u.id order by soni desc offset :page limit :size", nativeQuery = true)
    List<Object[]> getUsersByCountVisit(@Param("page") Integer page, @Param("size") Integer size);


}
