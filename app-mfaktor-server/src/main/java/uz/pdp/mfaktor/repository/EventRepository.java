package uz.pdp.mfaktor.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.mfaktor.entity.Event;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.entity.enums.EventStatus;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EventRepository extends JpaRepository<Event, UUID> {
    List<Event> findAllBySpeakers(User user);

    Page<Event> findAllByStatusInOrderByStartTime(List<EventStatus> eventStatuses, Pageable pageable);

    Optional<Event> findByTitle(String text);

    Optional<Event> findBySerialNumber(String text);

    List<Event> findAllByStatusInAndStartTimeIsAfterOrderByStartTime(List<EventStatus> eventStatuses, Timestamp time,Pageable pageable);

    List<Event> findAllByIsTopAndStatusOrderByStartTime(boolean a, EventStatus eventStatus);

    List<Event> findAllByEventTypeIdAndStatusIn(Integer id,List<EventStatus> statuses);

    List<Event> findAllByTitleContainingIgnoreCaseAndStatusIn(String word,List<EventStatus> statuses);

    List<Event> findAllByStatusOrderByStartTime(EventStatus eventStatus);
    List<Event> findAllByStatusInOrderByStartTime(List<EventStatus> eventStatuses);

    @Query(value = "SELECT count (*) FROM event t WHERE t.start_time>= date_trunc('month', current_date)",nativeQuery = true)
    int countEventMonthly();

    @Query(value = "SELECT count (*) FROM event t WHERE t.status<>'CANCELED' and t.status<>'DRAFT'",nativeQuery = true)
    int countEvent();

    @Query(value = "SELECT * FROM event t WHERE t.start_time>= date_trunc('month', current_date) and t.status<>'CANCELED' and t.status<>'DRAFT'",nativeQuery = true)
    List<Event>getMonthlyEvent();

    @Query(value = "SELECT * FROM event t WHERE t.start_time>= date_trunc('week', current_date)",nativeQuery = true)
    List<Event>getWeeklyEvent();

    @Query(value = "SELECT * FROM event t WHERE t.start_time>= date_trunc('day', current_date) and t.start_time<date_trunc('day', current_date+interval '1' day)",nativeQuery = true)
    List<Event>getDailyEvent();



}

