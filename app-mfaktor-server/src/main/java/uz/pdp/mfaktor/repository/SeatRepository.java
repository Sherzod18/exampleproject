package uz.pdp.mfaktor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.mfaktor.entity.Event;
import uz.pdp.mfaktor.entity.Seat;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.entity.enums.PlaceStatus;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SeatRepository extends JpaRepository<Seat, UUID> {
    List<Seat> findAllByUserAndParticipated(User user, boolean participated);

    List<Seat> findAllByEventOrderByRowAscNameAsc(Event event);

    List<Seat> findAllByEvent(Event event);

    List<Seat> findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(User user, PlaceStatus placeStatus,Boolean participated);
    List<Seat> findAllByUserAndPlaceStatusOrderByCreatedAt(User user, PlaceStatus placeStatus);

    List<Seat> findAllByEventAndPlaceStatusOrderByCreatedAt(Event event, PlaceStatus placeStatus);

    List<Seat> findAllByUserAndParticipatedAndPlaceStatusIn(User user, Boolean participated, List<PlaceStatus> placeStatus);

    List<Seat> findAllByEventAndUser(Event event, User user);

    List<Seat> findAllBy();

    @Query(value = "SELECT max (row) FROM seat where event_id=:eventId", nativeQuery = true)
    int maxRow(@Param(value = "eventId") UUID eventId);

    List<Seat> findAllByUserAndParticipated(User user,Boolean a);

    List<Seat> findAllByUser(User user);
    List<Seat> findAllByUserAndParticipatedOrderByCreatedAt(User user,Boolean participated);

}
