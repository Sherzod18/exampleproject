package uz.pdp.mfaktor.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.mfaktor.entity.*;
import uz.pdp.mfaktor.entity.enums.AttachmentTypeEnum;
import uz.pdp.mfaktor.entity.enums.RoleName;
import uz.pdp.mfaktor.repository.*;

import java.util.Arrays;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AttachmentTypeRepository attachmentTypeRepository;
    @Autowired
    VideoUrlRepository videoUrlRepository;
    @Autowired
    VideoTypeRepository videoTypeRepository;
    @Autowired
    FaqRepository faqRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(new User(

                    "+998905012651",

                    passwordEncoder.encode("root123"),
                    "Sanjar",
                    "Suvonov",

                    roleRepository.findAllByNameIn(
                            Arrays.asList(RoleName.ROLE_ADMIN,
                                    RoleName.ROLE_CASHIER,
                                    RoleName.ROLE_DIRECTOR,
                                    RoleName.ROLE_MANAGER,
                                    RoleName.ROLE_USER)
                    )));
            attachmentTypeRepository.save(new AttachmentType("image/jpeg,image/png,image/jpg", 1280, 858, AttachmentTypeEnum.EVENT_BANNER));
            attachmentTypeRepository.save(new AttachmentType("image/jpeg,image/png,image/jpg", 256, 256, AttachmentTypeEnum.COMPANY_LOGO));
            attachmentTypeRepository.save(new AttachmentType("image/jpeg,image/png,image/jpg", 350, 275, AttachmentTypeEnum.SPEAKER_AVATAR));
            attachmentTypeRepository.save(new AttachmentType("image/jpeg,image/png,image/jpg", AttachmentTypeEnum.USER_AVATAR, (long) 1024000));
            videoTypeRepository.save(new VideoType("Praktikum"));
            videoTypeRepository.save(new VideoType("Trip"));
            videoTypeRepository.save(new VideoType("Anons"));
            videoTypeRepository.save(new VideoType("MFaktor"));

            faqRepository.save(new Faq("Тадбирлар учун чегирмалар мавжудми?", null, "Тадбирлар учун чегирмалар ҳозирча мавжуд эмас."));
        }
    }
}
