package uz.pdp.mfaktor.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import uz.pdp.mfaktor.entity.Event;
import uz.pdp.mfaktor.entity.enums.EventStatus;
import uz.pdp.mfaktor.repository.EventRepository;
import uz.pdp.mfaktor.repository.UserRepository;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class EventListener {


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");


    @Autowired
    EventRepository eventRepository;

    @Autowired
    UserRepository userRepository;

    @Scheduled(fixedRate = 1000000)
    public void setStatusArchive() {
        List<Event> events = eventRepository.findAllByStatusInOrderByStartTime(Arrays.asList(EventStatus.OPEN, EventStatus.REGISTRATION_COMPLETED));
        for (Event event : events) {
            if ((event.getStartTime().getTime() + 9000000) <= new Date().getTime()) {
                event.setStatus(EventStatus.ARCHIVE);
                eventRepository.save(event);
            }
        }
    }
}