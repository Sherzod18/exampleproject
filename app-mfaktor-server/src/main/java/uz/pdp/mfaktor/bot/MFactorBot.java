package uz.pdp.mfaktor.bot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerPreCheckoutQuery;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendInvoice;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.payments.PreCheckoutQuery;
import org.telegram.telegrambots.meta.api.objects.payments.SuccessfulPayment;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.mfaktor.entity.Payment;
import uz.pdp.mfaktor.entity.Role;
import uz.pdp.mfaktor.entity.Seat;
import uz.pdp.mfaktor.entity.enums.PlaceStatus;
import uz.pdp.mfaktor.entity.enums.RoleName;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqPayment;
import uz.pdp.mfaktor.repository.*;
import uz.pdp.mfaktor.service.PaymentService;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

//@Component
public class MFactorBot{

//    private T t;
//
//    @Value("${bot.token}")
//    private String botToken;
//    @Value("${bot.username}")
//    private String botUsername;
//
//    @Autowired
//    TelegramStateRepository telegramStateRepository;
//
//    TelegramBotService telegramBotService;
//    @Autowired
//    TgChatsIdRepository tgChatsIdRepository;
//    @Autowired
//    AttachmentRepository attachmentRepository;
//    @Autowired
//    AttachmentContentRepository attachmentContentRepository;
//    @Autowired
//    EventRepository eventRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    UserRepository userRepository;
//    @Autowired
//    SeatRepository seatRepository;
//    @Autowired
//    PayTypeRepository payTypeRepository;
//    @Autowired
//    PaymentRepository paymentRepository;
//    @Autowired
//    SeatPaymentRepository seatPaymentRepository;
    @Autowired
    PaymentService paymentService;

//    public MFactorBot(@Lazy PaymentService paymentService, @Lazy TelegramBotService telegramBotService) {
//        this.paymentService = paymentService;
//        this.telegramBotService = telegramBotService;
//    }
//
////    public MFactorBot(T t) {
////        this.t = t;
////        telegramBotService.setTelegramServiceInterface(this);
////    }
//
//    @Override
//    public void onUpdateReceived(Update update) {
////        List<TgChatsId> allChatIds = tgChatsIdRepository.findAll();
////        long chatId = update.getMessage() != null
////                ? update.getMessage().getChatId():update.getChannelPost() != null
////                ? update.getChannelPost().getChatId() : 0;
////        boolean bool = true;
////        if (allChatIds.size() > 0) {
////            for (int i = 0; i < allChatIds.size(); ++i) {
////                if (allChatIds.get(i).getChatId() == chatId) {
////                    bool = false;
////                }
////            }
////            if (bool) {
////                allChatIds.add(new TgChatsId(chatId));
////                bool = true;
////            }
////        } else {
////            allChatIds.add(new TgChatsId(chatId));
////        }
////        tgChatsIdRepository.saveAll(allChatIds);
//        if (update.hasMessage()) {
//
////            if (update.getMessage().getText().equals("a")) {
////                List<Event> all = eventRepository.findAll();
////                Event event = all.get(all.size()-1);
////                for (int i = 0; i < allChatIds.size(); i++) {
////                    SendPhoto photo = new SendPhoto();
////                    photo.setCaption(event.getTitle());
////                    photo.setChatId(allChatIds.get(i).getChatId());
////                    AttachmentContent byAttachment = attachmentContentRepository.findByAttachment(
////                            event.getPhoto()).orElseThrow(() -> new ResourceNotFoundException
////                            ("get/event", "photoId", event.getPhoto()));
////                    photo.setPhoto(event.getTitle(), new ByteArrayInputStream(byAttachment.getContent()));
////                    try { this.execute(photo); }
////                    catch (TelegramApiException var21) { var21.printStackTrace(); }
////                }
////            }
//            Animal user = update.getMessage().getFrom();
//            if (update.getMessage().hasText()) {
//                TelegramState lastState = telegramBotService.getLastState(user.getId());
//                String text = update.getMessage().getText();
//                int i = text.indexOf('-');
//                if (text.equals("/start")) {
//                    try {
//                        execute(telegramBotService.chooseLang(update));
//                        } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.REGISTRATION_CUSTOMER_KR)) {
//                    try {
//                        execute(telegramBotService.shareContact(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.REGISTRATION_CUSTOMER_RU)) {
//                    try {
//                        execute(telegramBotService.shareContact(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.LANG_KR)) {
//                    if (!lastState.getState().equals(BotState.CHANGE_LANGUAGE_DATA)) {
//                        telegramBotService.setMainState(user, BotConstants.KR, BotState.LANG);
//                        try {
//                            execute(telegramBotService.registrationCustomer(update));
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        telegramBotService.setMainState(user, BotConstants.KR, BotState.SETTINGS);
//                        try {
//                            execute(telegramBotService.settingsMenu(update));
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                } else if (text.equals(BotConstants.LANG_RU)) {
//                    if (!lastState.getState().equals(BotState.CHANGE_LANGUAGE_DATA)) {
//                        telegramBotService.setMainState(user, BotConstants.RU, BotState.LANG);
//                        try {
//                            execute(telegramBotService.registrationCustomer(update));
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        telegramBotService.setMainState(user, BotConstants.RU, BotState.SETTINGS);
//                        try {
//                            execute(telegramBotService.settingsMenu(update));
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                } else if (text.equals(BotConstants.BACK_KR)) {
//                    try {
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BACK_RU)) {
//                    try {
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MONEY_KR)) {
//                    try {
//                        execute(telegramBotService.registerUserBySeatTypeMoney(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MONEY_RU)) {
//                    try {
//                        execute(telegramBotService.registerUserBySeatTypeMoney(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.SETTINGS_KR)) {
//                    try {
//                        execute(telegramBotService.settingsMenu(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.SETTINGS_RU)) {
//                    try {
//                        execute(telegramBotService.settingsMenu(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CHANGE_PROFILE_DATA_KR)) {
//                    try {
//                        execute(telegramBotService.changeProfileData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CHANGE_PROFILE_DATA_RU)) {
//                    try {
//                        execute(telegramBotService.changeProfileData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CHANGE_LANGUAGE_KR)) {
//                    try {
//                        execute(telegramBotService.changeLanguageData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CHANGE_LANGUAGE_RU)) {
//                    try {
//                        execute(telegramBotService.changeLanguageData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CURRENT_PROFILE_DATA_KR)) {
//                    try {
//                        execute(telegramBotService.getCurrentProfileData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CURRENT_PROFILE_DATA_RU)) {
//                    try {
//                        execute(telegramBotService.getCurrentProfileData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CONNECT_KR)) {
//                    try {
//                        execute(telegramBotService.connectMenu(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CONNECT_RU)) {
//                    try {
//                        execute(telegramBotService.connectMenu(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.EVENTS_KR)) {
//                    try {
//                        execute(telegramBotService.allEventsData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.EVENTS_RU)) {
//                    try {
//                        execute(telegramBotService.allEventsData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.REGISTERED_EVENT_KR)) {
//                    try {
//                        Optional<uz.pdp.mfaktor.entity.Animal> user1 = userRepository.findById(lastState.getUser().getId());
//                        List<Seat> seats = seatRepository.findAllByUserAndPlaceStatusOrderByCreatedAt(user1.get(), PlaceStatus.BOOKED);
//                        SendMessage sendMessage = new SendMessage().setChatId(update.getMessage().getChatId())
//                                .setParseMode(ParseMode.MARKDOWN);
//                        if (seats.size() > 0) {
//                            for (int j = 0; j < seats.size(); j++) {
//                                execute(telegramBotService.showRegisteredDataForEvent(update, seats.get(j)));
//                            }
//                        } else {
//                            sendMessage.setText(BotConstants.NOT_FOUND_BY_USER_FOR_EVENT_KR);
//                            execute(sendMessage);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.REGISTERED_EVENT_RU)) {
//                    try {
//                        Optional<uz.pdp.mfaktor.entity.Animal> user1 = userRepository.findById(lastState.getUser().getId());
//                        List<Seat> seats = seatRepository.findAllByUserAndPlaceStatusOrderByCreatedAt(user1.get(), PlaceStatus.BOOKED);
//                        SendMessage sendMessage = new SendMessage().setChatId(update.getMessage().getChatId())
//                                .setParseMode(ParseMode.MARKDOWN);
//                        if (seats.size() > 0) {
//                            for (int j = 0; j < seats.size(); j++) {
//                                execute(telegramBotService.showRegisteredDataForEvent(update, seats.get(j)));
//                            }
//                        } else {
//                            sendMessage.setText(BotConstants.NOT_FOUND_BY_USER_FOR_EVENT_RU);
//                            execute(sendMessage);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MANY_QUESTION_KR)) {
//                    try {
//                        execute(telegramBotService.manyQuestions(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MANY_QUESTION_RU)) {
//                    try {
//                        execute(telegramBotService.manyQuestions(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.ADDRESS_US_KR)) {
//                    try {
//                        execute(telegramBotService.showImage(update));
//                        execute(telegramBotService.showAddress(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.ADDRESS_US_RU)) {
//                    try {
//                        execute(telegramBotService.showImage(update));
//                        execute(telegramBotService.showAddress(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BACK_TO_HOME_MENU_KR)) {
//                    try {
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BACK_TO_HOME_MENU_RU)) {
//                    try {
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.SHOW_US_ADDRESS_KR)) {
//                    try {
//                        execute(telegramBotService.showImage(update));
//                        execute(telegramBotService.showAddress(update));
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.SHOW_US_ADDRESS_RU)) {
//                    try {
//                        execute(telegramBotService.showImage(update));
//                        execute(telegramBotService.showAddress(update));
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_FIRST)) {
//                    try {
//                        execute(telegramBotService.getMyFirstName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_LAST)) {
//                    try {
//                        execute(telegramBotService.getMyLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.COMPANY)) {
//                    try {
//                        execute(telegramBotService.saveUserCompany(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.SHARE_CONTACT)) {
//                    try {
//                        execute(telegramBotService.getMyPhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MONEY_KR)) {
//
//                } else if (text.equals(BotConstants.MONEY_RU)) {
//
//                } else if (text.equals(BotConstants.PLASTIC_KR)) {
//                    try {
//                        t = telegramBotService.payByPlastic(update);
//                        if (t instanceof SendInvoice) {
//                            execute((SendInvoice) t);
//                        } else {
//                            execute((SendMessage) t);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.PLASTIC_RU)) {
//                    try {
//                        t = telegramBotService.payByPlastic(update);
//                        if (t instanceof SendInvoice) {
//                            execute((SendInvoice) t);
//                        } else {
//                            execute((SendMessage) t);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_PASSWORD)) {
//                    try {
//                        execute(telegramBotService.controlPasswordAdmin(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
//                else if (text.equals(BotConstants.BRON_CURRENT_KR)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_CURRENT_RU)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_PARTNER_KR)) {
//                    try {
//                        execute(telegramBotService.otherPartnerData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_PARTNER_RU)) {
//                    try {
//                        execute(telegramBotService.otherPartnerData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_CURRENT_KR)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_CURRENT_RU)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (eventRepository.findByTitle(text.substring(text.indexOf(BotConstants.SPLITTER) + 4, text.indexOf(BotConstants.TIME)-1)).isPresent()) {
//                    try {
//                        t = telegramBotService.showSeatsByEvent(update);
//                        if (t instanceof SendPhoto) {
//                            execute((SendPhoto) t);
//                        } else {
//                            execute((SendMessage) t);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
//                else if (text.equals(BotConstants.CONNECT_TO_ADMIN_KR)) {
//                    try {
//                        execute(telegramBotService.writeToAdmin(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CONNECT_TO_ADMIN_RU)) {
//                    try {
//                        execute(telegramBotService.writeToAdmin(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
////                else if (eventRepository.findByTitle(update.getMessage().getText()).isPresent()) {
////                    try {
////                        execute(telegramBotService.showSeatsByEvent(update));
////                    } catch (TelegramApiException e) {
////                        e.printStackTrace();
////                    }
////                }
//                 else if (lastState.getState().equals(BotState.CHANGE_PHONE_NUMBER)) {
//                    try {
//                        execute(telegramBotService.saveChangePhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_FIRST_NAME)) {
//                    try {
//                        execute(telegramBotService.saveFirstNameAndWriteLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_LAST_NAME)) {
//                    try {
//                        execute(telegramBotService.saveChangeLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_COMPANY_NAME)) {
//                    try {
//                        execute(telegramBotService.saveChangeCompanyName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveChangeBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.SHARE_CONTACT_OTHER)) {
//                    try {
//                        execute(telegramBotService.getOtherPhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_OTHER_USER_BY_FIRST)) {
//                    try {
//                        execute(telegramBotService.getOtherFirstName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_OTHER_USER_BY_LAST)) {
//                    try {
//                        execute(telegramBotService.getOtherLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.OTHER_COMPANY)) {
//                    try {
//                        execute(telegramBotService.saveOtherUserCompany(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.OTHER_BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveOtherBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } else if (update.getMessage().hasSuccessfulPayment()) {
//                SuccessfulPayment payment = update.getMessage().getSuccessfulPayment();
//                successfulPayment(payment, update);
//            } else {
//                TelegramState lastState = telegramBotService.getLastState(user.getId());
//                if (lastState.getState().equals(BotState.SHARE_CONTACT)) {
//                    try {
//                        execute(telegramBotService.getMyPhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_FIRST)) {
//                    try {
//                        execute(telegramBotService.getMyFirstName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_LAST)) {
//                    try {
//                        execute(telegramBotService.getMyLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.COMPANY)) {
//                    try {
//                        execute(telegramBotService.saveUserCompany(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_PHONE_NUMBER)) {
//                    try {
//                        execute(telegramBotService.saveChangePhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.OTHER_COMPANY)) {
//                    try {
//                        execute(telegramBotService.saveOtherUserCompany(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.OTHER_BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveOtherBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.PAY_PARTNER)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        } else if (update.hasCallbackQuery()) {
//
//            String data = update.getCallbackQuery().getData();
//            if (data.startsWith("RegionId#")) {
//                try {
//                    execute(telegramBotService.putRegion(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("ActivityId#")) {
//                try {
//                    execute(telegramBotService.putActivity(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PositionId#")) {
//                try {
//                    execute(telegramBotService.putPosition(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("AwareId#")) {
//                try {
//                    execute(telegramBotService.saveUserAware(update));
//                    execute(telegramBotService.showMainMenu(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("SeatId#")) {
//                try {
//                    execute(telegramBotService.chooseProfileForRegistration(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PayMoney#")) {
//                try {
//                    execute(telegramBotService.saveUserBySeatForMoney(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Phone_number")) {
//                try {
//                    execute(telegramBotService.changePhoneNumber(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("FIO")) {
//                try {
//                    execute(telegramBotService.changeFioData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Region")) {
//                try {
//                    execute(telegramBotService.changeRegionData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("ChangeRegionId#")) {
//                try {
//                    execute(telegramBotService.saveChangeRegion(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Activity")) {
//                try {
//                    execute(telegramBotService.changeActivityData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();

//    @Override
//    public void onUpdateReceived(Update update) {
//        List<TgChatsId> allChatIds = tgChatsIdRepository.findAll();
//        long chatId = update.getMessage() != null
//                ? update.getMessage().getChatId():update.getChannelPost() != null
//                ? update.getChannelPost().getChatId() : 0;
//        boolean bool = true;
//        if (allChatIds.size() > 0) {
//            for (int i = 0; i < allChatIds.size(); ++i) {
//                if (allChatIds.get(i).getChatId() == chatId) {
//                    bool = false;
//                }
//            }
//            if (bool) {
//                allChatIds.add(new TgChatsId(chatId));
//                bool = true;
//            }
//        } else {
//            allChatIds.add(new TgChatsId(chatId));
//        }
//        tgChatsIdRepository.saveAll(allChatIds);
    //    if (update.hasMessage()) {

//            if (update.getMessage().getText().equals("a")) {
//                List<Event> all = eventRepository.findAll();
//                Event event = all.get(all.size()-1);
//                for (int i = 0; i < allChatIds.size(); i++) {
//                    SendPhoto photo = new SendPhoto();
//                    photo.setCaption(event.getTitle());
//                    photo.setChatId(allChatIds.get(i).getChatId());
//                    AttachmentContent byAttachment = attachmentContentRepository.findByAttachment(
//                            event.getPhoto()).orElseThrow(() -> new ResourceNotFoundException
//                            ("get/event", "photoId", event.getPhoto()));
//                    photo.setPhoto(event.getTitle(), new ByteArrayInputStream(byAttachment.getContent()));
//                    try { this.execute(photo); }
//                    catch (TelegramApiException var21) { var21.printStackTrace(); }
//                }
//            }
//            User user = update.getMessage().getFrom();
//            if (update.getMessage().hasText()) {
//                TelegramState lastState = telegramBotService.getLastState(user.getId());
//                String text = update.getMessage().getText();
//                int i = text.indexOf('-');
//                if (text.equals("/start")) {
//                    try {
//                        execute(telegramBotService.chooseLang(update));
//                        } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.REGISTRATION_CUSTOMER_KR)) {
//                    try {
//                        execute(telegramBotService.shareContact(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.REGISTRATION_CUSTOMER_RU)) {
//                    try {
//                        execute(telegramBotService.shareContact(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.LANG_KR)) {
//                    if (!lastState.getState().equals(BotState.CHANGE_LANGUAGE_DATA)) {
//                        telegramBotService.setMainState(user, BotConstants.KR, BotState.LANG);
//                        try {
//                            execute(telegramBotService.registrationCustomer(update));
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        telegramBotService.setMainState(user, BotConstants.KR, BotState.SETTINGS);
//                        try {
//                            execute(telegramBotService.settingsMenu(update));
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                } else if (text.equals(BotConstants.LANG_RU)) {
//                    if (!lastState.getState().equals(BotState.CHANGE_LANGUAGE_DATA)) {
//                        telegramBotService.setMainState(user, BotConstants.RU, BotState.LANG);
//                        try {
//                            execute(telegramBotService.registrationCustomer(update));
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        telegramBotService.setMainState(user, BotConstants.RU, BotState.SETTINGS);
//                        try {
//                            execute(telegramBotService.settingsMenu(update));
//                        } catch (TelegramApiException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                } else if (text.equals(BotConstants.BACK_KR)) {
//                    try {
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BACK_RU)) {
//                    try {
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MONEY_KR)) {
//                    try {
//                        execute(telegramBotService.registerUserBySeatTypeMoney(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MONEY_RU)) {
//                    try {
//                        execute(telegramBotService.registerUserBySeatTypeMoney(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.SETTINGS_KR)) {
//                    try {
//                        execute(telegramBotService.settingsMenu(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.SETTINGS_RU)) {
//                    try {
//                        execute(telegramBotService.settingsMenu(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CHANGE_PROFILE_DATA_KR)) {
//                    try {
//                        execute(telegramBotService.changeProfileData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CHANGE_PROFILE_DATA_RU)) {
//                    try {
//                        execute(telegramBotService.changeProfileData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CHANGE_LANGUAGE_KR)) {
//                    try {
//                        execute(telegramBotService.changeLanguageData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CHANGE_LANGUAGE_RU)) {
//                    try {
//                        execute(telegramBotService.changeLanguageData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CURRENT_PROFILE_DATA_KR)) {
//                    try {
//                        execute(telegramBotService.getCurrentProfileData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CURRENT_PROFILE_DATA_RU)) {
//                    try {
//                        execute(telegramBotService.getCurrentProfileData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CONNECT_KR)) {
//                    try {
//                        execute(telegramBotService.connectMenu(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CONNECT_RU)) {
//                    try {
//                        execute(telegramBotService.connectMenu(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.EVENTS_KR)) {
//                    try {
//                        execute(telegramBotService.allEventsData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.EVENTS_RU)) {
//                    try {
//                        execute(telegramBotService.allEventsData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.REGISTERED_EVENT_KR)) {
//                    try {
//                        Optional<uz.pdp.mfaktor.entity.User> user1 = userRepository.findById(lastState.getUser().getId());
//                        List<Seat> seats = seatRepository.findAllByUserAndPlaceStatusOrderByCreatedAt(user1.get(), PlaceStatus.BOOKED);
//                        SendMessage sendMessage = new SendMessage().setChatId(update.getMessage().getChatId())
//                                .setParseMode(ParseMode.MARKDOWN);
//                        if (seats.size() > 0) {
//                            for (int j = 0; j < seats.size(); j++) {
//                                execute(telegramBotService.showRegisteredDataForEvent(update, seats.get(j)));
//                            }
//                        } else {
//                            sendMessage.setText(BotConstants.NOT_FOUND_BY_USER_FOR_EVENT_KR);
//                            execute(sendMessage);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.REGISTERED_EVENT_RU)) {
//                    try {
//                        Optional<uz.pdp.mfaktor.entity.User> user1 = userRepository.findById(lastState.getUser().getId());
//                        List<Seat> seats = seatRepository.findAllByUserAndPlaceStatusOrderByCreatedAt(user1.get(), PlaceStatus.BOOKED);
//                        SendMessage sendMessage = new SendMessage().setChatId(update.getMessage().getChatId())
//                                .setParseMode(ParseMode.MARKDOWN);
//                        if (seats.size() > 0) {
//                            for (int j = 0; j < seats.size(); j++) {
//                                execute(telegramBotService.showRegisteredDataForEvent(update, seats.get(j)));
//                            }
//                        } else {
//                            sendMessage.setText(BotConstants.NOT_FOUND_BY_USER_FOR_EVENT_RU);
//                            execute(sendMessage);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MANY_QUESTION_KR)) {
//                    try {
//                        execute(telegramBotService.manyQuestions(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MANY_QUESTION_RU)) {
//                    try {
//                        execute(telegramBotService.manyQuestions(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.ADDRESS_US_KR)) {
//                    try {
//                        execute(telegramBotService.showImage(update));
//                        execute(telegramBotService.showAddress(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.ADDRESS_US_RU)) {
//                    try {
//                        execute(telegramBotService.showImage(update));
//                        execute(telegramBotService.showAddress(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BACK_TO_HOME_MENU_KR)) {
//                    try {
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BACK_TO_HOME_MENU_RU)) {
//                    try {
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.SHOW_US_ADDRESS_KR)) {
//                    try {
//                        execute(telegramBotService.showImage(update));
//                        execute(telegramBotService.showAddress(update));
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.SHOW_US_ADDRESS_RU)) {
//                    try {
//                        execute(telegramBotService.showImage(update));
//                        execute(telegramBotService.showAddress(update));
//                        execute(telegramBotService.showMainRoute(update, 1));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_FIRST)) {
//                    try {
//                        execute(telegramBotService.getMyFirstName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_LAST)) {
//                    try {
//                        execute(telegramBotService.getMyLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.COMPANY)) {
//                    try {
//                        execute(telegramBotService.saveUserCompany(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.SHARE_CONTACT)) {
//                    try {
//                        execute(telegramBotService.getMyPhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.MONEY_KR)) {
//
//                } else if (text.equals(BotConstants.MONEY_RU)) {
//
//                } else if (text.equals(BotConstants.PLASTIC_KR)) {
//                    try {
//                        t = telegramBotService.payByPlastic(update);
//                        if (t instanceof SendInvoice) {
//                            execute((SendInvoice) t);
//                        } else {
//                            execute((SendMessage) t);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.PLASTIC_RU)) {
//                    try {
//                        t = telegramBotService.payByPlastic(update);
//                        if (t instanceof SendInvoice) {
//                            execute((SendInvoice) t);
//                        } else {
//                            execute((SendMessage) t);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_PASSWORD)) {
//                    try {
//                        execute(telegramBotService.controlPasswordAdmin(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
//                else if (text.equals(BotConstants.BRON_CURRENT_KR)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_CURRENT_RU)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_PARTNER_KR)) {
//                    try {
//                        execute(telegramBotService.otherPartnerData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_PARTNER_RU)) {
//                    try {
//                        execute(telegramBotService.otherPartnerData(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_CURRENT_KR)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.BRON_CURRENT_RU)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
//                else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_PASSWORD)) {
//                    try {
//                        execute(telegramBotService.controlPasswordAdmin(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                else if (text.equals(BotConstants.CONNECT_TO_ADMIN_KR)) {
//                    try {
//                        execute(telegramBotService.writeToAdmin(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (text.equals(BotConstants.CONNECT_TO_ADMIN_RU)) {
//                    try {
//                        execute(telegramBotService.writeToAdmin(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
////                else if (eventRepository.findByTitle(update.getMessage().getText()).isPresent()) {
////                    try {
////                        execute(telegramBotService.showSeatsByEvent(update));
////                    } catch (TelegramApiException e) {
////                        e.printStackTrace();
////                    }
////                }
//                 else if (lastState.getState().equals(BotState.CHANGE_PHONE_NUMBER)) {
//                    try {
//                        execute(telegramBotService.saveChangePhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_FIRST_NAME)) {
//                    try {
//                        execute(telegramBotService.saveFirstNameAndWriteLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_LAST_NAME)) {
//                    try {
//                        execute(telegramBotService.saveChangeLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_COMPANY_NAME)) {
//                    try {
//                        execute(telegramBotService.saveChangeCompanyName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveChangeBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.SHARE_CONTACT_OTHER)) {
//                    try {
//                        execute(telegramBotService.getOtherPhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_OTHER_USER_BY_FIRST)) {
//                    try {
//                        execute(telegramBotService.getOtherFirstName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_OTHER_USER_BY_LAST)) {
//                    try {
//                        execute(telegramBotService.getOtherLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.OTHER_COMPANY)) {
//                    try {
//                        execute(telegramBotService.saveOtherUserCompany(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.OTHER_BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveOtherBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (eventRepository.findByTitle(text.substring(text.indexOf(BotConstants.SPLITTER) + 4, text.indexOf(BotConstants.TIME)-1)).isPresent()) {
//                    try {
//                        t = telegramBotService.showSeatsByEvent(update);
//                        if (t instanceof SendPhoto) {
//                            execute((SendPhoto) t);
//                        } else {
//                            execute((SendMessage) t);
//                        }
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
//            } else if (update.getMessage().hasSuccessfulPayment()) {
//                SuccessfulPayment payment = update.getMessage().getSuccessfulPayment();
//                successfulPayment(payment, update);
//            } else {
//                TelegramState lastState = telegramBotService.getLastState(user.getId());
//                if (lastState.getState().equals(BotState.SHARE_CONTACT)) {
//                    try {
//                        execute(telegramBotService.getMyPhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_FIRST)) {
//                    try {
//                        execute(telegramBotService.getMyFirstName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.REGISTRATION_USER_BY_LAST)) {
//                    try {
//                        execute(telegramBotService.getMyLastName(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.COMPANY)) {
//                    try {
//                        execute(telegramBotService.saveUserCompany(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.CHANGE_PHONE_NUMBER)) {
//                    try {
//                        execute(telegramBotService.saveChangePhoneNumber(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.OTHER_COMPANY)) {
//                    try {
//                        execute(telegramBotService.saveOtherUserCompany(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.OTHER_BIRTH_DATE)) {
//                    try {
//                        execute(telegramBotService.saveOtherBirthDate(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                } else if (lastState.getState().equals(BotState.PAY_PARTNER)) {
//                    try {
//                        execute(telegramBotService.payUserForSeat(update));
//                    } catch (TelegramApiException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        } else if (update.hasCallbackQuery()) {
//
//            String data = update.getCallbackQuery().getData();
//            if (data.startsWith("RegionId#")) {
//                try {
//                    execute(telegramBotService.putRegion(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("ActivityId#")) {
//                try {
//                    execute(telegramBotService.putActivity(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PositionId#")) {
//                try {
//                    execute(telegramBotService.putPosition(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("AwareId#")) {
//                try {
//                    execute(telegramBotService.saveUserAware(update));
//                    execute(telegramBotService.showMainMenu(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("SeatId#")) {
//                try {
//                    execute(telegramBotService.chooseProfileForRegistration(update));
//                    execute(telegramBotService.deleteMessage(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PayMoney#")) {
//                try {
//                    execute(telegramBotService.saveUserBySeatForMoney(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Phone_number")) {
//                try {
//                    execute(telegramBotService.changePhoneNumber(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("FIO")) {
//                try {
//                    execute(telegramBotService.changeFioData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Region")) {
//                try {
//                    execute(telegramBotService.changeRegionData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("ChangeRegionId#")) {
//                try {
//                    execute(telegramBotService.saveChangeRegion(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Activity")) {
//                try {
//                    execute(telegramBotService.changeActivityData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("ChangeActivityId#")) {
//                try {
//                    execute(telegramBotService.saveChangeActivity(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Company")) {
//                try {
//                    execute(telegramBotService.changeCompanyName(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Position")) {
//                try {
//                    execute(telegramBotService.changePositionData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("ChangePositionId#")) {
//                try {
//                    execute(telegramBotService.saveChangePosition(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Birth")) {
//                try {
//                    execute(telegramBotService.changeBirthData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PartnerRegionId#")) {
//                try {
//                    execute(telegramBotService.putOtherRegion(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PartnerActivityId#")) {
//                try {
//                    execute(telegramBotService.putOtherActivity(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PartnerPositionId#")) {
//                try {
//                    execute(telegramBotService.putOtherPosition(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PartnerAwareId#")) {
//                try {
//                    execute(telegramBotService.saveOtherUserAware(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("CloseBron#")) {
//                try {
//                    execute(telegramBotService.closeBroneForSeat(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("Faq#")) {
//                try {
//                    execute(telegramBotService.showOneQuestionAnswer(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("AcceptBron")) {
//                try {
//                    execute(telegramBotService.afterSaveShowAddres(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("NoAcceptBron")) {
//                try {
//                    execute(telegramBotService.showMainRoute(update, 1));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            }
//        } else if (update.hasPreCheckoutQuery()) {
//            PreCheckoutQuery preCheckoutQuery = update.getPreCheckoutQuery();
//            AnswerPreCheckoutQuery answerPreCheckoutQuery = new AnswerPreCheckoutQuery(preCheckoutQuery.getId(), true);
//            try {
//                execute(answerPreCheckoutQuery);
//            } catch (TelegramApiException e) {
//                e.printStackTrace();
//            }
//        } else if (update.hasInlineQuery()) {
//            String a = "";
//        } else if (update.hasChosenInlineQuery()) {
//            String b = "";
//        }
//    }

//    private void successfulPayment(SuccessfulPayment payment, Update update) {
//        try {
//            TelegramState lastState = getLastState(update.getMessage().getFrom().getId());
//            ReqPayment reqPayment = new ReqPayment();
//            reqPayment.setDetails("Payme orqali tushdi");
//            reqPayment.setPayDate(new Timestamp(System.currentTimeMillis()));
//            reqPayment.setPaySum(((double) payment.getTotalAmount() / 100));
//            reqPayment.setId(userRepository.findByPhoneNumber(lastState.getUser().getPhoneNumber()).get().getId());
//            reqPayment.setUserId(userRepository.findByPhoneNumber(lastState.getUser().getPhoneNumber()).get().getId());
//            reqPayment.setPayTypeId(4);
//            reqPayment.setSeatId(lastState.getSeatId());
//            reqPayment.setPlaceStatus(PlaceStatus.SOLD);
//            reqPayment.setFromTelegram(true);
//            ApiResponse apiResponse = paymentService.addPayment(reqPayment, lastState.getUser());
//
//            if (apiResponse.isSuccess()) {
//
//                execute(telegramBotService.showMainRoute(update, 2));
//                execute(telegramBotService.successFullPayTypePlastic(update));
//            } else if (data.startsWith("ChangeActivityId#")) {
//                try {
//                    execute(telegramBotService.saveChangeActivity(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Company")) {
//                try {
//                    execute(telegramBotService.changeCompanyName(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Position")) {
//                try {
//                    execute(telegramBotService.changePositionData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("ChangePositionId#")) {
//                try {
//                    execute(telegramBotService.saveChangePosition(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("Birth")) {
//                try {
//                    execute(telegramBotService.changeBirthData(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PartnerRegionId#")) {
//                try {
//                    execute(telegramBotService.putOtherRegion(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PartnerActivityId#")) {
//                try {
//                    execute(telegramBotService.putOtherActivity(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PartnerPositionId#")) {
//                try {
//                    execute(telegramBotService.putOtherPosition(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("PartnerAwareId#")) {
//                try {
//                    execute(telegramBotService.saveOtherUserAware(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("CloseBron#")) {
//                try {
//                    execute(telegramBotService.closeBroneForSeat(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.startsWith("Faq#")) {
//                try {
//                    execute(telegramBotService.showOneQuestionAnswer(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("AcceptBron")) {
//                try {
//                    execute(telegramBotService.afterSaveShowAddres(update));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            } else if (data.equals("NoAcceptBron")) {
//                try {
//                    execute(telegramBotService.showMainRoute(update, 1));
//                } catch (TelegramApiException e) {
//                    e.printStackTrace();
//                }
//            }
//        } else if (update.hasPreCheckoutQuery()) {
//            PreCheckoutQuery preCheckoutQuery = update.getPreCheckoutQuery();
//            AnswerPreCheckoutQuery answerPreCheckoutQuery = new AnswerPreCheckoutQuery(preCheckoutQuery.getId(), true);
//            try {
//                execute(answerPreCheckoutQuery);
//            } catch (TelegramApiException e) {
//                e.printStackTrace();
//            }
//        } else if (update.hasInlineQuery()) {
//            String a = "";
//        } else if (update.hasChosenInlineQuery()) {
//            String b = "";
//        }
//    }
//
//    private void successfulPayment(SuccessfulPayment payment, Update update) {
//        try {
//            TelegramState lastState = getLastState(update.getMessage().getFrom().getId());
//            ReqPayment reqPayment = new ReqPayment();
//            reqPayment.setDetails("Payme orqali tushdi");
//            reqPayment.setPayDate(new Timestamp(System.currentTimeMillis()));
//            reqPayment.setPaySum(((double) payment.getTotalAmount() / 100));
//            reqPayment.setId(userRepository.findByPhoneNumber(lastState.getUser().getPhoneNumber()).get().getId());
//            reqPayment.setPayTypeId(4);
//            reqPayment.setSeatId(lastState.getSeatId());
//            reqPayment.setPlaceStatus(PlaceStatus.SOLD);
//            ApiResponse apiResponse = paymentService.addPayment(reqPayment, lastState.getUser());
//
//            if (apiResponse.isSuccess()) {
//
//                execute(telegramBotService.showMainRoute(update, 2));
//                execute(telegramBotService.successFullPayTypePlastic(update));
////
////                Role roleManager = roleRepository.findByName(RoleName.ROLE_MANAGER).get();
////                List<uz.pdp.mfaktor.entity.Animal> allManagers = userRepository.findAllByRolesIn(roleManager);
////                for (int i = 0; i < allManagers.size(); i++) {
////                    execute(telegramBotService.sendInformationToManager(update, allManagers.get(i).getChatId(), 2));
////                }
//                Role roleDirector = roleRepository.findByName(RoleName.ROLE_DIRECTOR).get();
//                List<uz.pdp.mfaktor.entity.Animal> allAdmins = userRepository.findAllByRolesIn(roleDirector);
//                for (int i = 0; i < allAdmins.size(); i++) {
//                    execute(telegramBotService.sendInformationToAdmin(update, allAdmins.get(i).getChatId(), 2));
//                }
//            } else {
//                execute(telegramBotService.sendErrorMessage(update));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public TelegramState getLastState(Integer tgUserId) {
//        return telegramStateRepository.findByTgUserId(tgUserId).orElse(new TelegramState(tgUserId, BotConstants.KR, BotState.LANG));
//    }
//
//    @Override
//    public String getBotUsername() {
//        return botUsername;
//    }
//
//    @Override
//    public String getBotToken() {
//        return botToken;
//    }
//

//
////    @Override
////    public void executeMessage(SendMessage message) {
////        try {
////            execute(message);
////        } catch (TelegramApiException e) {
////            e.printStackTrace();
////        }
////    }
}
//pdp\npersonal\ndevelopment\nprocess\nuzbek\nbootcamp\ndesign\ndasturlash\nakademiya\nIT\ndastur\nprogram\nprogramming\nweb\nandroid\nios\nmern\nnodejs\nreactjs\nexpressjs\nhtml\ncss\njavascript\nbootstrap\nsass\nless\nspring\njava\noracle\npostgresql\njsp\nboot\nkotlin\nphotoshop\nillustrator\nfigma\nsketch\nxd\nrxjava\nfirebase\ndagger\nbot\ntelegram\napp\napplication\nui\nux\nmobile\nsoftware