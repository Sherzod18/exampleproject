package uz.pdp.mfaktor.bot;

public interface BotState {

    String LANG = "LANG";
    String SHARE_CONTACT = "SHARE_CONTACT";
    String REGISTRATION_USER_BY_FIRST = "REGISTRATION_USER_BY_FIRST";
    String REGISTRATION_USER_BY_LAST = "REGISTRATION_USER_BY_LAST";
    String REGION = "REGION";
    String ACTIVITY = "ACTIVITY";
    String COMPANY = "COMPANY";
    String POSITION = "POSITION";
    String BIRTH_DATE = "BIRTH_DATE";
    String AWARE = "AWARE";
    String MAIN_MENU = "MAIN_MENU";
    String PAY_MENU = "PAY_MENU";
    String SEATS = "SEATS";
    String CHANGE_PHONE_NUMBER = "CHANGE_PHONE_NUMBER";
    String SETTINGS = "SETTINGS";
    String CHANGE_FIRST_NAME = "CHANGE_FIRST_NAME";
    String CHANGE_LAST_NAME = "CHANGE_LAST_NAME";
    String CHANGE_COMPANY_NAME = "CHANGE_COMPANY_NAME";
    String CHANGE_BIRTH_DATE = "CHANGE_BIRTH_DATE";
    String CHANGE_LANGUAGE_DATA = "CHANGE_LANGUAGE_DATA";
    String CONNECT_MENU = "CONNECT_MENU";
    String ALL_EVENTS = "ALL_EVENTS";
    String ALL_REGISTERED_EVENTS = "ALL_REGISTERED_EVENTS";
    String CHOOSE_PROFILE_FOR_REGISTRATION = "CHOOSE_PROFILE_FOR_REGISTRATION";
    String SHARE_CONTACT_OTHER = "SHARE_CONTACT_OTHER";
    String REGISTRATION_OTHER_USER_BY_FIRST = "REGISTRATION_OTHER_USER_BY_FIRST";
    String REGISTRATION_OTHER_USER_BY_LAST = "REGISTRATION_OTHER_USER_BY_LAST";
    String PARTNER_REGION = "PARTNER_REGION";
    String OTHER_ACTIVITY = "OTHER_ACTIVITY";
    String OTHER_COMPANY = "OTHER_COMPANY";
    String OTHER_POSITION = "OTHER_POSITION";
    String OTHER_BIRTH_DATE = "OTHER_BIRTH_DATE";
    String OTHER_AWARE = "OTHER_AWARE";
    String PAY_PARTNER = "PAY_PARTNER";
    String WRITE_TO_ADMIN = "WRITE_TO_ADMIN";
    String ANSWER_TO_USER = "ANSWER_TO_USER";
    String REGISTRATION_USER_BY_PASSWORD = "REGISTRATION_USER_BY_PASSWORD";
}
