package uz.pdp.mfaktor.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.mfaktor.entity.Event;
import uz.pdp.mfaktor.entity.Seat;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.repository.EventRepository;
import uz.pdp.mfaktor.repository.SeatRepository;
import uz.pdp.mfaktor.repository.UserRepository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class ScheduledTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    @Autowired
    SeatRepository seatRepository;

    @Autowired
    EventRepository eventRepository;

//    @Autowired
//    MFactorBot mFactorBot;

    @Autowired
    UserRepository userRepository;


    @Scheduled(fixedRate = 10000000)
    public void reportCurrentTime() {
//        List<Event> allEvents = eventRepository.findAll();
//
//        allEvents.forEach(event -> {
//            String format = dateFormat.format(event.getStartTime());
//            if (format.equals(dateFormat.format(new Date()))) {
//                List<Seat> seats = event.getSeats();
//                seats.forEach(seat -> {
//                    if (seat.getUser() != null) {
//                        Animal user = seat.getUser();
//                        if (user.getChatId() != null) {
//                            SendMessage sendMessage = new SendMessage().setChatId(seat.getUser().getChatId()).setParseMode(ParseMode.MARKDOWN);
//                            Optional<TelegramState> tgUser = telegramStateRepository.findByTgUserId(seat.getUser().getTelegramId());
//                            tgUser.ifPresent(telegramState -> {
//                                if (telegramState.getLang().equals(BotConstants.KR)) {
//                                    sendMessage.setText("Брон қилинган жой учун тўловни амалга ошириш вақти келди.");
//                                } else {
//                                    sendMessage.setText("Пришло время оплатить бронирование.");
//                                }
//                                try {
//                                    mFactorBot.execute(sendMessage);
//                                } catch (TelegramApiException e) {
//                                    e.printStackTrace();
//                                }
//                            });
//                        }
//                    }
//                });
//            }
//        });
    }
}