package uz.pdp.mfaktor.bot;

public interface BotConstants {

    String CHOOSE_LANG_TEXT = "\uD83C\uDDFA\uD83C\uDDFF *Тилни танланг* \n\uD83C\uDDF7\uD83C\uDDFA *Выбрать язык*";

    String LANG_KR = "\uD83C\uDDFA\uD83C\uDDFF Ўзбекча";

    String LANG_RU = "\uD83C\uDDF7\uD83C\uDDFA Русский";

    String KR = "KR";

    String RU = "RU";

    String SHARE_CONTACT_KR = "\uD83D\uDCDE Телефон номерни юбориш";

    String SHARE_CONTACT_RU = "\uD83D\uDCDE Отправка номера телефона";

    String SHARE_CONTACT_DESCRIPTION_KR = "\uD83D\uDCDE Телефон номерни *+998XX xxx xx xx* шаклида киритинг ёки *телефон номерни юбориш* имкониятидан фойдаланинг.";

    String SHARE_CONTACT_DESCRIPTION_RU = "\uD83D\uDCDE Введите номер телефона в форме * 998XX xxx xx xx * или используйте * номер телефона *.";

    String REG_WRITE_FIO_KR = "\uD83D\uDCCC Рўйхатдан ўтиш учун фамилия, исм ва шарифингизни киритинг. *Масалан: Сувонов Санжар Шухрат ўғли*: ";

    String REG_WRITE_FIO_RU = "\uD83D\uDCCC Введите свою фамилию, имя и отчества для регистрации. * Например: Сувонов Санжар Шухрат ўғли *:";

    String WRITE_FULL_DATA_KR = "❗️Маълумотларни тўлиқ киритинг. Қайтадан уриниб кўринг.";

    String WRITE_FULL_DATA_RU = "❗️Введите полные данные. Пожалуйста, попробуйте еще раз.";

    String CHOOSE_REGION_KR = "✅ ҚАЙСИ ВИЛОЯТДАНСИЗ ? \uD83D\uDC47";

    String CHOOSE_REGION_RU = "✅ Вы с какой области? \uD83D\uDC47";

    String CHOOSE_ACTIVITY_KR = "\uD83D\uDD39ҚАЙСИ СОҲАДА ФАОЛИЯТ ЮРИТАСИЗ? \uD83D\uDC47";

    String CHOOSE_ACTIVITY_RU = "\uD83D\uDD39В какой сфере вы работаете? \uD83D\uDC47";

    String WRITE_COMPANY_NAME_KR = "\uD83D\uDD38ФИРМАНГИЗ НОМИ ЁКИ КОМПАНИЯНГИЗ БРЕНДИНИ КИРИТИНГ \uD83D\uDC47";

    String WRITE_COMPANY_NAME_RU = "\uD83D\uDD38Введите название вашей компании или название бренда \uD83D\uDC47";

    String POSITION_TEXT_KR = "\uD83D\uDCBC ЛАВОЗИМИНГИЗНИ ТАНЛАНГ:\uD83D\uDC47";

    String POSITION_TEXT_RU = "\uD83D\uDCBC Выберите свою должность:\uD83D\uDC47";

    String BIRTH_DATE_KR = "\uD83D\uDCCC Туғилган йилингиз ва кунингизни киритинг *Масалан: 06.05.1996*: \uD83D\uDC47";

    String BIRTH_DATE_RU = "\uD83D\uDCCC Введите год и день рождения * Например: 06.05.1996 *: \uD83D\uDC47";

    String AWARE_TEXT_KR = "✅ \"MFAKTOR PRAKTIKUM\" НИНГ УШБУ ТРЕНИНГИ ҲАҚИДА ҚАЕРДАН БИЛДИНГИЗ ? \uD83D\uDC47";

    String AWARE_TEXT_RU = "✅ Где вы узнали об этом тренинге на \"MFAKTOR\" ? \uD83D\uDC47";

    String SUCCESSFULLY_REGISTERED_KR = "✅ Муваффақиятли рўйхатга олинди.";

    String SUCCESSFULLY_REGISTERED_RU = "✅ Успешно зарегистрирован.";

    String REGISTRATION_CUSTOMER_KR = "Рўйхатдан ўтиш";

    String REGISTRATION_CUSTOMER_RU = "Зарегистрироваться";

    String REGISTRATION_DESC_KR = "#Mfaktor тадбирларидан рўйхатга ўтиш учун кўрсатилган кетма-кетликдаги маълумотларни тўлдиринг. Бу маълумотлар асосида бошқа тадбирларга ҳам рўйхатга ўтишингиз мумкин.\n*Рўйхатдан ўтиш* тугмасини босинг\uD83D\uDC47";

    String REGISTRATION_DESC_RU = "Для регистрации заполните анкету в указанной последовательности, чтобы участвовать в мероприятиях #MFaktor. На основании этой информации вы также можете зарегистрироваться для участия в других мероприятиях. Для регистрации нажмите кнопку *Зарегистрироваться*.\uD83D\uDC47";

    String TEL_KR = "Телефон номери:\n";

    String TEL_RU = "Номер телефона:\n";

    String FIO_KR = "ФИШ:\n";

    String FIO_RU = "ФИО:\n";

    String REGION_KR = "Вилояти:\n";

    String REGION_RU = "Область:\n";

    String ACTIVITY_KR = "Фаолият тури:\n";

    String ACTIVITY_RU = "Тип деятельности:\n";

    String COMPANY_KR = "Компания номи:\n";

    String COMPANY_RU = "Название компании:\n";

    String POSITION_KR = "Лавозими:\n";

    String POSITION_RU = "Позиция:\n";

    String BIRTH_KR = "Туғилган вақти:\n";

    String BIRTH_RU = "Дата рождения:\n";

    String BEST_NEAR_EVENTS_KR = "Энг яқин тадбирлар";

    String BEST_NEAR_EVENTS_RU = "Предстоящие события";

    String EVENTS_KR = "Навбатдаги тадбирлар";

    String EVENTS_RU = "Предстоящие события";

    String SETTINGS_KR = "Созламалар";

    String SETTINGS_RU = "Настройки";

    String CONNECT_KR = "Боғланиш";

    String CONNECT_RU = "Связаться с нами";

    String HOME_KR = "Бош сахифа";

    String HOME_RU = "Домашняя страница";

    String MONEY_KR = "Нақд пул";

    String MONEY_RU = "Наличные деньги";

    String PLASTIC_KR = "Тўлов картаси";

    String PLASTIC_RU = "Платежная карта";

    String PAY_TYPE_KR = "Тўлов турини танланг";

    String PAY_TYPE_RU = "Пожалуйста, выберите тип оплаты";

    String REG_WRITE_FIRST_KR = "Исмингизни киритинг:";

    String REG_WRITE_FIRST_RU = "Введите ваше имя:";

    String REG_WRITE_LAST_KR = "Фамилиянгизни киритинг:";

    String REG_WRITE_LAST_RU = "Введите свою фамилию:";

    String BACK_KR = "◀️Орқага";

    String BACK_RU = "◀️Назад";

    String FREE_PLACE_KR = "тадбири учун бўш ўринлар:";

    String FREE_PLACE_RU = "место на мероприятие:";

    String TODAY_KR = "Бугун";

    String MONDAY_KR = "Душанба";

    String THURSDAY_KR = "Сешанба";

    String TOMORROW_KR = "Эртага";

    String TODAY_RU = "Сегодня";

    String MONDAY_RU = "Пониделник";

    String THURSDAY_RU = "Вторник";

    String TOMORROW_RU = "Завтра";

    String SUCCESSFULLY_BRONED_KR = "Муваффақиятли брон қилинди.";

    String SUCCESSFULLY_BRONED_RU = "Успешное бронирование.";

    String CHANGE_PROFILE_DATA_KR = "Профил маълумотларини ўзгартириш";

    String CHANGE_LANGUAGE_KR = "Тилни ўзгартириш";

    String CHANGE_PROFILE_DATA_RU = "Изменить информацию профиля";

    String CHANGE_LANGUAGE_RU = "Изменить язык";

    String CHANGE_PHONE_NUMBER_KR = "Телефон номерни ўзгартириш";

    String CHANGE_PHONE_NUMBER_RU = "Изменить номер телефона";

    String CHANGE_FIO_KR = "Исм ва фамилияни ўзгартириш";

    String CHANGE_FIO_RU = "Изменить имя и фамилию";

    String CHANGE_REGION_KR = "Вилоятни ўзгартириш";

    String CHANGE_REGION_RU = "Изменить регион";

    String CHANGE_ACTIVITY_KR = "Фаолият турини ўзгартириш";

    String CHANGE_ACTIVITY_RU = "Изменить вид деятельности";

    String CHANGE_COMPANY_KR = "Компания номини ўзгартириш";

    String CHANGE_COMPANY_RU = "Изменить название компании";

    String CHANGE_POSITION_KR = "Лавозимни ўзгартириш";

    String CHANGE_POSITION_RU = "Изменить позиция";

    String CHANGE_BIRTH_KR = "Туғилган кунни ўзгартириш";

    String CHANGE_BIRTH_RU = "Изменить дату рождения";

    String ALREADY_REGISTRATION_KR = "Бу номер олдин рўйхатга олинган.";

    String ALREADY_REGISTRATION_RU = "Этот номер уже зарегистрирован.";

    String SUCCESSFULLY_CHANGED_KR = "Муваффақиятли ўзгартирилди.";

    String SUCCESSFULLY_CHANGED_RU = "Изменено успешно.";

    String CURRENT_PROFILE_DATA_KR = "Жорий профил маълумотлари";

    String CURRENT_PROFILE_DATA_RU = "Информация о текущем профиле";

    String MANY_QUESTION_KR = "Энг кўп бериладиган саволлар";

    String MANY_QUESTION_RU = "Часто задаваемые вопросы";

    String ABOUT_US_KR = "Биз хақимизда";

    String ABOUT_US_RU = "О нас";

    String REGISTERED_EVENT_KR = "Рўйхатдан ўтилган тадбирлар";

    String REGISTERED_EVENT_RU = "Зарегистрированные события";

    String ADDRESS_US_KR = "Бизнинг манзил";

    String CONNECT_TO_ADMIN_KR = "Админ билан боғланиш";

    String ADDRESS_US_RU = "Наш адрес";

    String CONNECT_TO_ADMIN_RU = "Связаться с администратором";

    String BRON_CURRENT_KR = "Жорий профил орқали банд қилиш(брон)";

    String BRON_PARTNER_KR = "Бошқа мижозга банд қилиш(брон)";

    String DOING_BRON_KR = "\uD83D\uDCCC Агар тадбирга ўзингиз учун ўрин банд қилмоқчи бўлсангиз, \"Жорий профил орқали банд қилиш\" бандини танланг. Бошқа мижоз учун банд қилмоқчи бўлсангиз, \"Бошқа мижозга банд қилиш\" бандини танланг.\n";

    String BRON_CURRENT_RU = "Бронировать для текущего профиля";

    String BRON_PARTNER_RU = "Бронировать для другого клиента";

    String DOING_BRON_RU = "Если вы хотите сделать предварительный заказ на ваше мероприятие, выберите «Бронировать для текущего профиля». Если вы хотите забронировать другого клиента, выберите «Бронировать для другого клиента».";

    String SHOW_US_ADDRESS_KR = "Бизнинг манзилни кўриш";

    String BACK_TO_HOME_MENU_KR = "Бош сахифага қайтиш";

    String SHOW_US_ADDRESS_RU = "Посмотрите наш адрес";

    String BACK_TO_HOME_MENU_RU = "Вернуться на главную страницу";

    String ABOUT_SEND_MESSAGE_TO_ADMIN_KR = "Админга ўз хабарингизни тўлиқ, ифодали ва лўнда ёзиб юборинг";

    String ABOUT_SEND_MESSAGE_TO_ADMIN_RU = "Напишите ваше сообщение вашему администратору, полное, наглядное и встроенное";

    String CLOSE_BRON_KR = "Бекор қилиш";

    String CLOSE_BRON_RU = "Отменить заказ";

    String CLOSE_BRON_TEXT_KR = " тадбирига банд қилинган жойларни бекор қилдингиз.";

    String CLOSE_BRON_TEXT_RU = "Вы отменили событие бронирования для ";

    String EVENT_NAME_KR = "Тадбир номи: ";

    String BRON_TIME_KR = "Тадбир ўтказиладиган сана: ";

    String BRON_PLACE_KR = "Банд қилинган жой: ";

    String PLACE_PRICE_KR = "Жой нархи: ";

    String PAY_TIME_KR = "Тўлов муддати: ";

    String EVENT_NAME_RU = "Название мероприятия: ";

    String BRON_TIME_RU = "Дата мероприяти: ";

    String BRON_PLACE_RU = "Занято место: ";

    String PLACE_PRICE_RU = "Цена места: ";

    String PAY_TIME_RU = "Срок оплаты: ";

    String SUCCESSFULLY_PAID_KR = "Муваффақиятли тўланди.";

    String SUCCESSFULLY_PAID_RU = "Успешно оплачено.";

    String ERROR_PAID_KR = "Тўлов бажарилмади.";

    String ERROR_PAID_RU = "Оплата не удалась.";

    String ANSWER_FROM_ADMIN_KR = "Админ томондан юбориладиган жавобни кутинг.";

    String ANSWER_FROM_ADMIN_RU = "Ждите ответа администратора.";

    String NOT_FOUND_BY_USER_FOR_EVENT_KR = "Рўйхатдан ўтган тадбирлар топилмади.";

    String NOT_FOUND_BY_USER_FOR_EVENT_RU = "События не найдены.";

    String NOT_FOUND_SEAT_KR = "Бу жой банд қилинган. Қайтадан уриниб кўринг.\n\"*Орқага*\" тугмасини босинг";

    String NOT_FOUND_SEAT_RU = "Это место забронировано. Пожалуйста, попробуйте еще раз.\nНажмите кнопку \"*Назад*\"";

    String BRON_USER_KR = "Мижоз: ";

    String BRON_USER_RU = "Клиент: ";

    String SPLITTER = " \uD83D\uDD39 ";

    String TIME = "\uD83D\uDD54";

    String BRON_SPIKER_KR = "Спикер: ";

    String BRON_SPIKER_RU = "Спикер: ";

    String TYPE_PAY_KR = "Тўлов тури: ";

    String ACCEPTED_KR = "Тасдиқлаш";

    String ACCEPTED_RU = "Подтвердить";

    String ACCEPTED_REAL_KR = "✅Тасдиқланди.";

    String ACCEPTED_REAL_RU = "✅Потверждена.";

    String REG_WRITE_PASSWORD_KR = "Паролни киритинг:";

    String REG_WRITE_PASSWORD_RU = "Введите ваш пароль:";

    String ERROR_PASSWORD_KR = "Парол хато";

    String ERROR_PASSWORD_RU = "Неверный пароль";

    String GO_TO_PAY_KR= "Борганда тўлов қиламан.";

    String GO_TO_PAY_RU= "Я заплачу по прибытии.";

    String MFAKTOR_TIME = " 18:00";

    String IMPULSE_TIME = " 09:00";
}
