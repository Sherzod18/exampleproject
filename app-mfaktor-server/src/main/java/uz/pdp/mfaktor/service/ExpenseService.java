package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.Expense;
import uz.pdp.mfaktor.entity.WaitingExpense;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqExpense;
import uz.pdp.mfaktor.payload.ResExpense;
import uz.pdp.mfaktor.payload.ResExpensePageable;
import uz.pdp.mfaktor.repository.*;
import uz.pdp.mfaktor.utils.CommonUtils;

import java.util.stream.Collectors;

@Service
public class ExpenseService {
    @Autowired
    ExpenseRepository expenseRepository;
    @Autowired
    ExpenseTypeRepository expenseTypeRepository;
    @Autowired
    PayTypeRepository payTypeRepository;
    @Autowired
    EventService eventService;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    WaitingExpenseRepository waitingExpenseRepository;
    @Autowired
    SeatPaymentRepository seatPaymentRepository;
    @Autowired
    CashTypeRepository cashTypeRepository;

    public ApiResponse addExpense(ReqExpense request) {
        Expense expense = new Expense();
        WaitingExpense waitingExpense = new WaitingExpense();
        if (request.getId() != null) {
            expense = expenseRepository.findById(request.getId()).orElseThrow(() -> new ResourceNotFoundException("getExpense", "expenseId", request.getId()));
            if (!eventService.isToday(expense.getDate())) {
                return new ApiResponse("Tahrirlash imkoni yo'q", false);
            }
        }
        double income = seatPaymentRepository.incomeAllByCashType(request.getCashTypeId());
        double outcome = expenseRepository.expenseAllByCashType(request.getCashTypeId());
        if (request.getSum() <= (income - outcome)) {
            if (request.getWaitingExpenseId() != null) {
                waitingExpense = waitingExpenseRepository.findById(request.getWaitingExpenseId()).orElseThrow(() -> new ResourceNotFoundException("getWaitingExpense", "waitingExpense", request.getId()));
                waitingExpense.setComment(request.getComment());
                if (request.getSum() >= waitingExpense.getLeftover()) {
                    waitingExpense.setPaid(true);
                    expense.setSum(waitingExpense.getLeftover());
                    waitingExpense.setLeftover(0D);
                } else {
                    waitingExpense.setLeftover(waitingExpense.getLeftover() - request.getSum());
                    expense.setSum(request.getSum());
                }
                expense.setExpenseType(waitingExpense.getExpenseType());
            }
            if (request.getExpenseTypeId() != null) {
                expense.setExpenseType(expenseTypeRepository.findById(request.getExpenseTypeId()).orElseThrow(() -> new ResourceNotFoundException("getExpenseType", "expenseTypeId", request.getExpenseTypeId())));
                expense.setSum(request.getSum());
            }
            expense.setComment(request.getComment());
            expense.setEventId(request.getEventId());
            expense.setDate(request.getDate());
            expense.setCashType(cashTypeRepository.findById(request.getCashTypeId()).orElseThrow(() -> new ResourceNotFoundException("getPayType", "payTypeId", request.getCashTypeId())));
            expenseRepository.save(expense);
            waitingExpenseRepository.save(waitingExpense);
            return new ApiResponse(request.getId() == null ? request.getWaitingExpenseId() != null ? "Kutilayotgan to'lov uchun mablag' to'landi" : "Harajat  qo'shildi" : "Harajat  tahrirlandi", true);
        }
        return new ApiResponse("Ushbu to'lov turi orqali to'lash uchun mablag' yetarli emas", false);
    }

    public ResExpensePageable getExpenses(int page, int size) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
        Page<Expense> res = expenseRepository.findAll(pageable);
        return new ResExpensePageable(res.getContent().stream().map(this::getExpense).collect(Collectors.toList()), res.getTotalPages());
    }

    public ResExpense getExpense(Expense expense) {
        return new ResExpense(
                expense.getId(),
                expense.getExpenseType().getId(),
                expense.getExpenseType().getNameUz(),
                expense.getExpenseType().getNameRu(),
                expense.getSum(),
                expense.getDate(),
                expense.getCashType().getId(),
                expense.getCashType().getNameUz(),
                expense.getCashType().getNameRu(),
                expense.getComment(),
                expense.getEventId(),
                expense.getEventId() == null ? null : eventRepository.findById(expense.getEventId()).orElseThrow(() -> new ResourceNotFoundException("getEvent", "eventId", expense.getEventId())).getTitle(),
                expense.getDate() != null && eventService.isToday(expense.getDate())
        );
    }

}
