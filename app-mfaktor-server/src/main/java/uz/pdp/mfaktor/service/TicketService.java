package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.*;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqTicket;
import uz.pdp.mfaktor.repository.PaymentRepository;
import uz.pdp.mfaktor.repository.TariffRepository;
import uz.pdp.mfaktor.repository.TicketRepository;
import uz.pdp.mfaktor.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class TicketService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    UserService userService;
    @Autowired
    TariffRepository tariffRepository;
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    MessageSource messageSource;

    public ApiResponse addTicket(ReqTicket reqTicket) {
        User user = userRepository.findById(reqTicket.getUserId())
                .orElseThrow(() -> new ResourceNotFoundException
                        ("getUser", "userId",
                                reqTicket.getUserId()));
        List<Ticket> tickets = new ArrayList<>();
        Tariff tariff=tariffRepository.findById(reqTicket.getTariffId()).
                orElseThrow(() -> new ResourceNotFoundException("getTarif","tarifId",reqTicket.getTariffId()));
        List<Payment> payments = paymentRepository.findAllByUserAndLeftoverGreaterThanOrderByCreatedAt(user, 0);
        for (int i = 0; i < tariff.getTicketCount(); i++) {
            Ticket ticket = new Ticket();
            ticket.setUser(user);
            double ticketPrice=tariff.getPrice()/tariff.getTicketCount();

            double amount=0;
            for (Payment payment:payments) {
                List<SeatPayment> seatPayments= new ArrayList<>();
                if (amount+payment.getLeftover()>ticketPrice){
                        SeatPayment seatPayment = new SeatPayment();
                        seatPayment.setPayment(payment);
                        seatPayment.setAmount(ticketPrice-amount);
                        payment.setLeftover(payment.getLeftover()-(ticketPrice-amount));
                        seatPayments.add(seatPayment);
                        ticket.setSeatPayments(seatPayments);
                        break;
                }else {
                    amount = payment.getLeftover();
                    SeatPayment seatPayment = new SeatPayment();
                    seatPayment.setPayment(payment);
                    seatPayment.setAmount(amount);
                    payment.setLeftover(0);
                    seatPayments.add(seatPayment);
                }
            }
            ticket.setDeadline(reqTicket.getDeadline());
            ticket.setPrice(ticketPrice);
            tickets.add(ticket);
        }
        ticketRepository.saveAll(tickets);
        paymentRepository.saveAll(payments);
        return new ApiResponse(messageSource.getMessage("addTicked",null, LocaleContextHolder.getLocale()), true);

    }
}
