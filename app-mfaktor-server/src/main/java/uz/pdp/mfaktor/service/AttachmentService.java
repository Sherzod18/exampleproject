package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.pdp.mfaktor.entity.Attachment;
import uz.pdp.mfaktor.entity.AttachmentContent;
import uz.pdp.mfaktor.entity.AttachmentType;
import uz.pdp.mfaktor.entity.enums.AttachmentTypeEnum;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ResUploadFile;
import uz.pdp.mfaktor.repository.AttachmentContentRepository;
import uz.pdp.mfaktor.repository.AttachmentRepository;
import uz.pdp.mfaktor.repository.AttachmentTypeRepository;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    AttachmentTypeRepository attachmentTypeRepository;

    @Transactional
    public ApiResponseModel uploadFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> itr = request.getFileNames();
        MultipartFile file;
        List<ResUploadFile> resUploadFiles = new ArrayList<>();
        while (itr.hasNext()) {
            file = request.getFile(itr.next());
            if ((file.getContentType().equals("image/jpeg") || file.getContentType().equals("image/png") || file.getContentType().equals("image/jpg")) && file.getSize() > 2048000) {
                return new ApiResponseModel(false, "Rasmning hajmi 2 MB dan ortiq.", resUploadFiles);
            }
            if (!request.getParameter("type").equals("undefined")) {
                AttachmentType type = attachmentTypeRepository.findByType(AttachmentTypeEnum.valueOf(request.getParameter("type")));
                if (!type.getContentTypes().contains(file.getContentType())) {
                    return new ApiResponseModel(false, "Rasm formati *.jpeg,*.png,*.jpg bo'lishi zarur", resUploadFiles);
                }
                if (request.getParameter("type").equals(attachmentTypeRepository.findByType(AttachmentTypeEnum.USER_AVATAR))) {
                    if (file.getSize() > attachmentTypeRepository.findByType(AttachmentTypeEnum.USER_AVATAR).getSize()) {
                        return new ApiResponseModel(false, "Rasmning hajmi 1 MB dan ortiq.", resUploadFiles);
                    }
                }
                BufferedImage image = ImageIO.read(file.getInputStream());
                if (type.equals(attachmentTypeRepository.findByType(AttachmentTypeEnum.valueOf(request.getParameter("type")))) && !(image.getWidth() == type.getWidth() && image.getHeight() == type.getHeight())) {
                    return new ApiResponseModel(false, "Rasmning o'lchovi mos emas. Tavsiya qilingan o'lcham: " + type.getWidth() + " x " + type.getHeight(), resUploadFiles);
                }
            }
            Attachment attachment = attachmentRepository.save(new Attachment(file.getOriginalFilename(), file.getContentType(), file.getSize()));
            try {
                attachmentContentRepository.save(new AttachmentContent(attachment, file.getBytes()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            resUploadFiles.add(new ResUploadFile(attachment.getId(),
                    attachment.getName(),
                    ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/").path(attachment.getId().toString()).toUriString(),
                    attachment.getContentType(),
                    attachment.getSize()));

        }
        return new ApiResponseModel(true, "", resUploadFiles);
    }

    public HttpEntity<?> getAttachmentContent(UUID attachmentId) {
        Attachment attachment = attachmentRepository.findById(attachmentId).orElseThrow(() -> new ResourceNotFoundException("Attachment", "id", attachmentId));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachment(attachment).orElseThrow(() -> new ResourceNotFoundException("Attachment content", "attachment id", attachmentId));

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getContent());
    }

}
