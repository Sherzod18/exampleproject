package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.pdp.mfaktor.entity.Event;
import uz.pdp.mfaktor.entity.Review;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.*;
import uz.pdp.mfaktor.repository.EventRepository;
import uz.pdp.mfaktor.repository.ReviewRepository;
import uz.pdp.mfaktor.repository.UserRepository;
import uz.pdp.mfaktor.utils.CommonUtils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ReviewService {

    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    MessageSource messageSource;

    public ApiResponseModel getOwnReview(UUID userId, UUID eventOrSpeakerId) {
        List<ResReviewList> reviewLists = reviewRepository.findAllByCreatedByAndEventIdOrSpeakerId(userId, eventOrSpeakerId, eventOrSpeakerId)
                .stream().map(this::getReviewList).collect(Collectors.toList());
        if (reviewLists.size() == 1)
            return new ApiResponseModel(true, "Animal sharxi", reviewLists.get(0));

        return new ApiResponseModel(false, "Animal sharx qoldirmagan", null);
    }

    public ResReview getEventReviews(UUID eventOrSpeakerId, int page, int size) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
        List<Review> reviews = reviewRepository.findAllByEventIdOrSpeakerId(eventOrSpeakerId, eventOrSpeakerId, pageable);
        ResReview resReview = new ResReview(
                reviewRepository.countOfGivenValue(1,eventOrSpeakerId,eventOrSpeakerId),
                reviewRepository.countOfGivenValue(2,eventOrSpeakerId,eventOrSpeakerId),
                reviewRepository.countOfGivenValue(3,eventOrSpeakerId,eventOrSpeakerId),
                reviewRepository.countOfGivenValue(4,eventOrSpeakerId,eventOrSpeakerId),
                reviewRepository.countOfGivenValue(5,eventOrSpeakerId,eventOrSpeakerId),
                reviews.stream().map(this::getReviewList).collect(Collectors.toList())
        );
        resReview.setCountStars(reviews.size());
        eventRepository.findById(eventOrSpeakerId).ifPresent(event -> resReview.setAvgStar(event.getAvgStar()));
        userRepository.findById(eventOrSpeakerId).ifPresent(speaker -> resReview.setAvgStar(speaker.getAvgStar()));
        return resReview;
    }

    public ApiResponse addReview(ReqReview reqReview) {
        Review review = new Review();
        review.setText(reqReview.getText());
        review.setStarCount(reqReview.getStarCount());
        NumberFormat formatter = new DecimalFormat("#0.0");
        if (eventRepository.findById(reqReview.getEventOrSpeakerId()).isPresent()) {
            Event event = eventRepository.findById(reqReview.getEventOrSpeakerId()).orElseThrow(() -> new ResourceNotFoundException("Event", "id", reqReview.getEventOrSpeakerId()));
            review.setEvent(event);
            review.setPublished(false);
            reviewRepository.save(review);
            Integer amountStar = reviewRepository.sumOfStarEvent(event.getId());
            Integer amountReview = reviewRepository.countReviewByEventId(event.getId());
            event.setAvgStar(amountStar == 0 ? 0 : Double.valueOf(amountStar / amountReview));
            eventRepository.save(event);
        }

        if (userRepository.findById(reqReview.getEventOrSpeakerId()).isPresent()) {
            User speaker = userRepository.findById(reqReview.getEventOrSpeakerId()).orElseThrow(() -> new ResourceNotFoundException("Speaker", "id", reqReview.getEventOrSpeakerId()));
            review.setSpeaker(speaker);
            review.setPublished(false);
            reviewRepository.save(review);
            double amountReview = reviewRepository.countReviewByEventId(speaker.getId());
            double amountStar = reviewRepository.sumOfStarEvent(speaker.getId());
            speaker.setAvgStar(amountStar == 0 ? 0 : Double.valueOf(formatter.format(amountStar / amountReview)));
            userRepository.save(speaker);
        }
        return new ApiResponse(messageSource.getMessage("review.added", null, LocaleContextHolder.getLocale()), true);
    }

    public ResReviewList getReviewList(Review review) {
        User createdBy = userRepository.findById(review.getCreatedBy()).orElseThrow(() -> new ResourceNotFoundException("Animal", "id", review.getCreatedBy()));
        return new ResReviewList(review.getId(), review.getCreatedBy(), createdBy.getPhoto() != null ? ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/file/").path(createdBy.getPhoto().getId().toString()).toUriString() : "", createdBy.getFirstName(),
                createdBy.getLastName(), review.getStarCount(), review.getText(), review.getCreatedAt(),review.isPublished(),createdBy.getCompany());

    }

    public List<ResReviewList> getReviews(){
        List<Review> reviews = reviewRepository.findAllByIsPublished(true);
        return reviews.stream().map(this::getReviewList).collect(Collectors.toList());
    }
}
