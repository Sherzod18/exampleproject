package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.*;
import uz.pdp.mfaktor.entity.enums.PlaceStatus;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.*;
import uz.pdp.mfaktor.repository.*;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SeatService {
    @Autowired
    SeatRepository seatRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    SeatPaymentRepository seatPaymentRepository;
    @Autowired
    MessageSource messageSource;
    @Autowired
    UserService userService;
    @Autowired
    PositionRepository positionRepository;
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    PaymentService paymentService;
    @Autowired
    CashTypeRepository cashTypeRepository;

    public ApiResponse seatOrder(ReqUser request, UUID seatId, User currentUser) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(true);
        Seat seat = seatRepository.findById(seatId).orElseThrow(() -> new ResourceNotFoundException("getPlace", "placeId", seatId));
        if (!request.getPlaceStatus().equals(PlaceStatus.RESERVED)) {
            User user = createOrGetUser(request);
            boolean userHave = false;
            if (seat.getUser() != null) {
                if (seat.getUser().getId().equals(user.getId())) {
                    userHave = true;
                }
            } else {
                userHave = true;
            }
            if ((user.getPhoneNumber() != null) && seat.getPlaceStatus().equals(PlaceStatus.EMPTY) || (seat.getPlaceStatus().equals(PlaceStatus.VIP))
                    || seat.getPlaceStatus().equals(PlaceStatus.RESERVED) || userHave) {
                seat.setComment(request.getComment());
                double residue = paymentRepository.sumOfLeftover(user.getId());
                double price = seatPriceWithSale(seat, user);
                if (request.getPlaceStatus().equals(PlaceStatus.SOLD)) {
                    List<Ticket> tickets = ticketRepository.findAllByUserAndActiveAndEventType(user, true, seat.getEvent().getEventType());
                    if (tickets.size() > 0) {
                        seat.setUser(user);
                        seat.setPlaceStatus(request.getPlaceStatus());
                        seat.setSeatPayments(tickets.get(0).getSeatPayments());
                        tickets.get(0).setActive(false);
                        ticketRepository.saveAll(tickets);
                        apiResponse.setMessage("Siz bilet orqali joyni oldingiz");
                    } else {
                        if (residue >= price) {
                            seat.setSeatPayments(getSeatPayments(user, seat));
                            seat.setPlaceStatus(request.getPlaceStatus());
                            seat.setUser(user);
                            apiResponse.setMessage("Joy xarid qildingiz");
                        } else if (residue > 0) {
                            seat.setSeatPayments(getSeatPayments(user, seat));
                            seat.setPlaceStatus(request.getPlaceStatus());
                            seat.setUser(user);
                            seat.setPlaceStatus(PlaceStatus.PARTLY_PAID);
                            apiResponse.setMessage("Joy uchun qisman to'lov amalga oshirdingiz");
                        }
                    }
                } else if (request.getPlaceStatus().equals(PlaceStatus.BOOKED)) {
                    if (residue == 0) {
                        seat.setBookedExpireDate(isSaturday() ? Timestamp.valueOf(LocalDate.now().plusDays(2).atStartOfDay()) : Timestamp.valueOf(LocalDate.now().plusDays(1).atStartOfDay()));
                        seat.setUser(user);
                        seat.setPlaceStatus(PlaceStatus.BOOKED);
                        apiResponse.setMessage("Joy band(bron) qildingiz");
                    } else if (residue > 0) {
                        seat.setUser(user);
                        seat.setSeatPayments(getSeatPayments(user, seat));
                        if (residue < price) {
                            seat.setPlaceStatus(PlaceStatus.PARTLY_PAID);
                            apiResponse.setMessage("Joy uchun qisman to'lov amalga oshirdingiz");
                        } else {
                            seat.setPlaceStatus(PlaceStatus.SOLD);
                            apiResponse.setMessage("Joy xarid qildingiz");
                        }
                    }
                } else {
                    seat.setUser(user);
                    seat.setPlaceStatus(PlaceStatus.VIP);
                    apiResponse.setMessage("Joy mehmon uchun band qilindi");
                }
            } else {
                apiResponse.setMessage(user.getPhoneNumber() == null ? "Bunday telefon raqamli foydalanuvchi mavjud" : "Bu joy allaqachon band qilingan");
                apiResponse.setSuccess(false);
            }
        } else {
            if (request.getPlaceStatus().equals(PlaceStatus.RESERVED) && currentUser.getRoles().size() > 1
                    && (seat.getPlaceStatus().equals(PlaceStatus.EMPTY))) {
                seat.setPlaceStatus(PlaceStatus.RESERVED);
                seat.setComment(request.getComment());
                apiResponse.setMessage("Joy zahiraga olindi");
            }
        }
        if (apiResponse.isSuccess()) {
            seat.setTargetTime(new Timestamp(new Date().getTime()));
            seatRepository.save(seat);
        }
        return apiResponse;
    }

    public ApiResponse bookSeats(ReqSeat request) {
        User user = userRepository.findById(request.getUserId()).orElseThrow(() -> new ResourceNotFoundException("Animal", "id", request.getUserId()));
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setBirthDate(request.getBirthDate());
        user.setCompany(request.getCompany());
        user.setPosition(request.getPositionId() != null ? positionRepository.findById(request.getPositionId())
                .orElseThrow(() -> new ResourceNotFoundException("Position", "id", request.getPositionId())) : null);
        user.setRegion(request.getRegionId() != null ? regionRepository.findById(request.getRegionId())
                .orElseThrow(() -> new ResourceNotFoundException("Region", "id", request.getRegionId())) : null);
        user = userRepository.save(user);
        ReqUser reqUser = new ReqUser();
        reqUser.setUserId(user.getId());
        reqUser.setPhoneNumber(user.getPhoneNumber());
        reqUser.setPlaceStatus(request.getPlaceStatus());

        List<Seat> seats = seatRepository.findAllById(request.getSeatIds());
        List<Seat> bookedSeats = new ArrayList<>();
        for (Seat seat : seats) {
            if (seatOrder(reqUser, seat.getId(), user).isSuccess()) {
                bookedSeats.add(seat);
            }
        }
        return new ApiResponse(bookedSeats.size() > 0 ? "Sizga " + bookedSeats.size() + " ta (" + bookedSeats.stream().map(Seat::getName).collect(Collectors.joining(", ")) +
                (bookedSeats.size() > 1 ? ") joy" : ") joylar") + " band qilindi" : "Siz tanlagan o'rinlar barchasi band", bookedSeats.size() > 0);
    }

    public ApiResponse cancelBooked(UUID seatId) {
        ApiResponse response = new ApiResponse();
        if (seatRepository.findById(seatId).isPresent()) {
            Seat seat = seatRepository.findById(seatId).orElseThrow(() -> new ResourceNotFoundException("getSeat", "seatid", seatId));
            if (seat.getPlaceStatus().equals(PlaceStatus.BOOKED) || seat.getPlaceStatus().equals(PlaceStatus.RESERVED) ||
                    (seat.getPlaceStatus().equals(PlaceStatus.VIP))) {
                seat.setPlaceStatus(PlaceStatus.EMPTY);
                seat.setUser(null);
                seatRepository.save(seat);
                response.setMessage("Joyni bekor qildingiz");
                response.setSuccess(true);
            } else {
                response.setMessage("Bu joy bekor qilish imkoni yo'q");
                response.setSuccess(false);
            }
        } else {
            response.setMessage("Bunday joy mavjud emas");
            response.setSuccess(false);
        }
        return response;
    }

    public List<SeatPayment> getSeatPayments(User user, Seat seat) {
        double seatPrice = seatPriceWithSale(seat, user);
        List<SeatPayment> seatPayments = new ArrayList<>();
        List<Payment> payments = paymentRepository.findAllByUserAndLeftoverGreaterThanOrderByCreatedAt(user, 0);
        double amount = seat.getSeatPayments().stream().mapToDouble(SeatPayment::getAmount).sum();
        for (Payment payment : payments) {
            if ((amount + payment.getLeftover()) >= seatPrice) {
                SeatPayment seatPayment = new SeatPayment();
                seatPayment.setAmount(seatPrice - amount);
                seatPayment.setPayment(payment);
                payment.setLeftover(payment.getLeftover() - (seatPrice - amount));
                seatPayments.add(seatPayment);
                break;
            } else {
                SeatPayment seatPayment = new SeatPayment();
                seatPayment.setPayment(payment);
                seatPayment.setAmount(payment.getLeftover());
                amount += payment.getLeftover();
                payment.setLeftover(0);
                seatPayments.add(seatPayment);
            }
        }
        paymentRepository.saveAll(payments);
        seatPayments = seatPaymentRepository.saveAll(seatPayments);
        return seatPayments;
    }

    public CashType getCashType(PayType payType) {
        return cashTypeRepository.findByPayTypesIn(Collections.singletonList(payType));
    }

    public User createOrGetUser(ReqUser request) {
        User user = new User();
        if (request.getId() != null) {
            request.setUserId(request.getId());
        }
        if (request.getUserId() != null) {
            user = userRepository.findById(request.getUserId()).orElseThrow(() -> new ResourceNotFoundException("getUser", "userId", request.getUserId()));

        } else {
            ApiResponse response = userService.addUser(request);
            if (response.isSuccess()) {
                user = userRepository.findByPhoneNumber(request.getPhoneNumber()).orElseThrow(() -> new ResourceNotFoundException("getUser", "phoneNumber", request.getPhoneNumber()));
            }
        }
        return user;
    }

    public List<SeatPayment> getSeatPayments(User user, Seat seat, List<SeatPayment> seatPayments) {
        seatPayments.addAll(getSeatPayments(user, seat));
        seatPayments = seatPaymentRepository.saveAll(seatPayments);
        return seatPayments;
    }

    public List<ResSeat> eventSeats(UUID eventId) {
        return seatRepository.findAllByEventOrderByRowAscNameAsc(eventRepository.findById(eventId).
                orElseThrow(() -> new ResourceNotFoundException("getEvent", "eventId", eventId))).stream().
                map(this::getSeat).collect(Collectors.toList());
    }

    public ResSeat getSeat(Seat seat) {
        return new ResSeat(
                seat.getId(),
                seat.getName(),
                seat.getPlaceStatus(),
                seat.getRow(),
                seat.getPrice(),
                seat.getUser() != null ? userService.getFullName(seat.getUser()) : "",
                seat.getUser() != null ? seat.getUser().getPhoneNumber() : "");
    }

    public boolean isSaturday() {
        Date date = new Date();
        return date.toString().contains("Sa");
    }

    public ApiResponse setParticipated(ReqSeatParticipate request) {
        Seat seat = seatRepository.findById(request.getSeatId()).orElseThrow(() -> new ResourceNotFoundException("getSeat", "seatId", request.getSeatId()));
        if (seat.getParticipated() == null || seat.getParticipated()) {
            if (request.isParticipated()) {
                seat.setParticipated(true);
                seatRepository.save(seat);
                return new ApiResponse("Tasdiqlandi", true);
            } else if (!request.isParticipated()) {
                List<SeatPayment> seatPayments = seat.getSeatPayments();
                List<Payment> payments = new ArrayList<>();
                for (SeatPayment seatPayment : seatPayments) {
                    Payment payment = seatPayment.getPayment();
                    payment.setLeftover(payment.getLeftover() + seatPayment.getAmount());
                    payments.add(payment);
                }
                paymentRepository.saveAll(payments);
                seat.setSeatPayments(null);
                seatPaymentRepository.deleteAll(seatPayments);
                seat.setParticipated(false);
                seatRepository.save(seat);
                List<Seat> partlyPaidSeats = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(seat.getUser(), PlaceStatus.PARTLY_PAID, null);
                List<Seat> partlyPaidSeats2 = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(seat.getUser(), PlaceStatus.PARTLY_PAID, true);
                if (partlyPaidSeats2.size() > 0) {
                    paymentService.changeSeatStatus(partlyPaidSeats2);
                }
                if (partlyPaidSeats.size() > 0) {
                    paymentService.changeSeatStatus(partlyPaidSeats);
                }
                List<Seat> bookedSeats = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(seat.getUser(), PlaceStatus.BOOKED, null);
                List<Seat> bookedSeats2 = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(seat.getUser(), PlaceStatus.BOOKED, true);
                if (bookedSeats2.size() > 0) {
                    paymentService.changeSeatStatus(bookedSeats2);
                }
                if (bookedSeats.size() > 0) {
                    paymentService.changeSeatStatus(bookedSeats);
                }
                return new ApiResponse("Tasdiqlandi", true);
            }
        }
        return new ApiResponse("Tadiqlash imkoni yo'q. Chunki ushbu foydalanuvchi mablag'lari qaytarilgan", false);
    }

    public ApiResponse cancelSold(UUID seatId) {
        Seat seat = seatRepository.findById(seatId).orElseThrow(() -> new ResourceNotFoundException("getSeat", "seatId", seatId));
        User user = seat.getUser();
        if (seat.getUser() != null) {
            List<SeatPayment> seatPayments = seat.getSeatPayments();
            List<Payment> payments = new ArrayList<>();
            for (SeatPayment seatPayment : seatPayments) {
                Payment payment = seatPayment.getPayment();
                payment.setLeftover(payment.getLeftover() + seatPayment.getAmount());
                payments.add(payment);
            }
            paymentRepository.saveAll(payments);
            seat.setSeatPayments(null);
            seatPaymentRepository.deleteAll(seatPayments);
            seat.setPlaceStatus(PlaceStatus.EMPTY);
            seat.setUser(null);
            seatRepository.save(seat);
            List<Seat> partlyPaidSeats = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.PARTLY_PAID, null);
            List<Seat> partlyPaidSeats2 = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.PARTLY_PAID, true);
            if (partlyPaidSeats2.size() > 0) {
                paymentService.changeSeatStatus(partlyPaidSeats2);
            }
            if (partlyPaidSeats.size() > 0) {
                paymentService.changeSeatStatus(partlyPaidSeats);
            }
            List<Seat> bookedSeats = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.BOOKED, null);
            List<Seat> bookedSeats2 = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.BOOKED, true);
            if (bookedSeats2.size() > 0) {
                paymentService.changeSeatStatus(bookedSeats2);
            }
            if (bookedSeats.size() > 0) {
                paymentService.changeSeatStatus(bookedSeats);
            }
            return new ApiResponse("Tasdiqlandi", true);
        }
        return new ApiResponse("Tadiqlash imkoni yo'q. Chunki ushbu foydalanuvchi mablag'lari qaytarilgan", false);
    }

    public double seatPriceWithSale(Seat seat, User user) {
        double sale = userRepository.getSale(user.getId()) / 100;
        return seat.getPrice() - (seat.getPrice() * sale);
    }

    public ApiResponse addSeat(ReqAddChair reqAddChair) {
        Event event = eventRepository.findById(reqAddChair.getEventId())
                .orElseThrow(() ->
                        new ResourceNotFoundException("getEvent", "eventId", reqAddChair.getEventId()));
        Seat seat = new Seat();
        seat.setEvent(event);
        seat.setName(reqAddChair.getName());
        seat.setPlaceStatus(PlaceStatus.EMPTY);
        seat.setPrice(reqAddChair.getPrice());
        seat.setRow(reqAddChair.getRow());
        seat.setEventStartTime(event.getStartTime());
        seatRepository.save(seat);
        return new ApiResponse("Joy qo'shildi", true);
    }

    public ApiResponse addRow(List<ReqAddChair> reqAddChairs) {
        List<Seat> seats = new ArrayList<>();
        Event event = eventRepository.findById(reqAddChairs.get(0).getEventId())
                .orElseThrow(() -> new ResourceNotFoundException("getEvent", "eventId", reqAddChairs.get(0).getEventId()));
        reqAddChairs.forEach(reqAddChair ->
                seats.add(new Seat(reqAddChair.getName(),
                        reqAddChair.getPrice(),
                        PlaceStatus.EMPTY,
                        event,
                        reqAddChair.getRow(),
                        event.getStartTime()
                ))
        );
        seatRepository.saveAll(seats);
        return new ApiResponse("Qator qoshildi", true);
    }
}

