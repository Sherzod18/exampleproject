package uz.pdp.mfaktor.service;

import com.google.protobuf.Enum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.Voucher;
import uz.pdp.mfaktor.entity.enums.VoucherStatus;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqVoucher;
import uz.pdp.mfaktor.payload.ResVoucher;
import uz.pdp.mfaktor.repository.UserRepository;
import uz.pdp.mfaktor.repository.VoucherRepository;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class VoucherService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    VoucherRepository voucherRepository;
    @Autowired
    MessageSource messageSource;


    public ResVoucher getVoucher(UUID id) {
        ResVoucher resVoucher = new ResVoucher();
        voucherRepository.findById(id).ifPresent(this::getVoucher);
        return resVoucher;
    }

    public List<ResVoucher> getAllVouchers() {
        return voucherRepository.findAll().stream().map(this::getVoucher).collect(Collectors.toList());
    }

    public ApiResponse addVoucher(ReqVoucher reqVoucher) {
        Voucher voucher = new Voucher(reqVoucher.getVoucherId(),
                userRepository.findById(reqVoucher.getOwnerId()).orElseThrow(() -> new ResourceNotFoundException("Animal", "id", reqVoucher.getOwnerId())),
                reqVoucher.getPrice(), reqVoucher.getPrice(), reqVoucher.getDeadline(), VoucherStatus.ACTIVE
        );
        voucherRepository.save(voucher);
        return new ApiResponse(messageSource.getMessage("voucher.created", null, LocaleContextHolder.getLocale()), true);
    }

    public ApiResponse editVoucher(ReqVoucher reqVoucher) {
        voucherRepository.findById(reqVoucher.getId()).ifPresent(voucher -> {
            voucher.setVoucherId(reqVoucher.getVoucherId());
            voucher.setPrice(reqVoucher.getPrice());
            voucher.setDeadline(reqVoucher.getDeadline());
            voucher.setStatus(VoucherStatus.valueOf(reqVoucher.getStatus()));
            userRepository.findById(reqVoucher.getOwnerId()).orElseThrow(() -> new ResourceNotFoundException("Animal", "id", reqVoucher.getOwnerId()));
            voucherRepository.save(voucher);
        });


        return new ApiResponse(messageSource.getMessage("voucher.edited", null, LocaleContextHolder.getLocale()), true);
    }

    public ApiResponse deleteVoucher(UUID id) {
        if (voucherRepository.findById(id).isPresent()) {
            voucherRepository.deleteById(id);
            return new ApiResponse(messageSource.getMessage("voucher.deleted", null, LocaleContextHolder.getLocale()), true);
        }
        return new ApiResponse(messageSource.getMessage("voucher.notfound", null, LocaleContextHolder.getLocale()), false);


    }

    private ResVoucher getVoucher(Voucher voucher) {
        return new ResVoucher(
                voucher.getId(),
                voucher.getVoucherId(),
                voucher.getOwner().getId(),
                voucher.getOwner().getFirstName(),
                voucher.getOwner().getLastName(),
                voucher.getPrice(),
                voucher.getResidue(),
                voucher.getDeadline(),
                voucher.getStatus().toString());
    }


}
