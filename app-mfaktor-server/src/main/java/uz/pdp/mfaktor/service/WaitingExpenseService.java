package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.Expense;
import uz.pdp.mfaktor.entity.WaitingExpense;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.*;
import uz.pdp.mfaktor.repository.ExpenseRepository;
import uz.pdp.mfaktor.repository.ExpenseTypeRepository;
import uz.pdp.mfaktor.repository.PayTypeRepository;
import uz.pdp.mfaktor.repository.WaitingExpenseRepository;
import uz.pdp.mfaktor.utils.CommonUtils;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class WaitingExpenseService {
    @Autowired
    ExpenseRepository expenseRepository;
    @Autowired
    ExpenseTypeRepository expenseTypeRepository;
    @Autowired
    PayTypeRepository payTypeRepository;
    @Autowired
    EventService eventService;
    @Autowired
    WaitingExpenseRepository waitingExpenseRepository;


    public ApiResponse addWaitingExpense(ReqWaitingExpense request) {
        try {
            WaitingExpense waitingExpense = new WaitingExpense();
            waitingExpense.setComment(request.getComment());
            waitingExpense.setDeadline(request.getDeadline());
            waitingExpense.setExpenseType(expenseTypeRepository.findById(request.getExpenseTypeId()).orElseThrow(() -> new ResourceNotFoundException("getExpenseType", "expensTypeId", request.getExpenseTypeId())));
            waitingExpense.setSum(request.getSum());
            waitingExpense.setLeftover(request.getSum());
            waitingExpenseRepository.save(waitingExpense);
            return new ApiResponse("Kutilayotgan xarajat qo'shildi", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Kutilayotgan xarajat qo'shishda xatolik", false);
        }
    }

    public ApiResponse editWaitingExpense(ReqWaitingExpense request) {
        try {
            WaitingExpense waitingExpense = waitingExpenseRepository.findById(request.getId()).orElseThrow(() -> new ResourceNotFoundException("getWaitingExpense", "waitingExpense", request.getId()));
            if (waitingExpense.getSum() == waitingExpense.getLeftover()) {
                waitingExpense.setComment(request.getComment());
                waitingExpense.setExpenseType(expenseTypeRepository.findById(request.getExpenseTypeId()).orElseThrow(() -> new ResourceNotFoundException("getExpenseType", "expensTypeId", request.getExpenseTypeId())));
                waitingExpense.setSum(request.getSum());
                waitingExpense.setDeadline(request.getDeadline());
                waitingExpense.setLeftover(request.getSum());
                waitingExpenseRepository.save(waitingExpense);
                return new ApiResponse("Kutilayotgan xarajat tahrirlandi", true);
            }
            return new ApiResponse("Kutilayotgan xarajat tahrirlash mumkin emas", false);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Kutilayotgan xarajat tahrirlashda xatolik", false);
        }
    }


    public ApiResponse deleteWaitingExpense(UUID id) {
        WaitingExpense waitingExpense = waitingExpenseRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getWaitingExpense", "waitingExpense", id));
        if (waitingExpense.getSum() == waitingExpense.getLeftover()) {
            waitingExpenseRepository.delete(waitingExpense);
            return new ApiResponse("Kutilayotgan xarajat o'chirildi", true);
        }
        return new ApiResponse("Kutilayotgan xarajatni o'chirish imkoni yo'q ", false);
    }

    public List<ResWaitingExpense> getWaitingExpenses() {
        return waitingExpenseRepository.findAllByPaidOrderByDeadline(false).stream().map(this::getWaitingExpense).collect(Collectors.toList());
    }

    public ResWaitingExpense getWaitingExpense(WaitingExpense waitingExpense) {
        return new ResWaitingExpense(
                waitingExpense.getId(),
                waitingExpense.getExpenseType().getId(),
                waitingExpense.getExpenseType().getNameUz(),
                waitingExpense.getExpenseType().getNameRu(),
                waitingExpense.getSum(),
                waitingExpense.getLeftover(),
                waitingExpense.getDeadline(),
                waitingExpense.getComment(),
                waitingExpense.isPaid()
        );
    }
}
