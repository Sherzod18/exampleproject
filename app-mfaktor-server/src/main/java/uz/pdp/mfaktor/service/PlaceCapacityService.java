package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.Chair;
import uz.pdp.mfaktor.entity.PlaceCapacity;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqChair;
import uz.pdp.mfaktor.payload.ReqTemplatePlace;
import uz.pdp.mfaktor.repository.ChairRepository;
import uz.pdp.mfaktor.repository.PlaceCapacityRepository;
import uz.pdp.mfaktor.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PlaceCapacityService {
    @Autowired
    ChairRepository chairRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PlaceCapacityRepository placeCapacityRepository;
    @Autowired
    MessageSource messageSource;

    public ApiResponse savePlace(ReqTemplatePlace reqTemplatePlace) {
        List<Chair> chairs = new ArrayList<>();
        for (ReqChair reqPlace : reqTemplatePlace.getReqChairs()) {
            Chair chair = new Chair();
            try {
                chair = chairRepository.findById(UUID.fromString(reqPlace.getId())).orElseGet(Chair::new);
            } catch (Exception ignored) {
            }
            chair.setRow(reqPlace.getRow());
            chair.setName(reqPlace.getName());
            chair.setPrice(reqPlace.getPrice());
            chairs.add(chair);
        }
        List<Chair> savedSeat = chairRepository.saveAll(chairs);
        PlaceCapacity placeCapacity = new PlaceCapacity();
        try {
            placeCapacity = placeCapacityRepository.findById(reqTemplatePlace.getId()).orElseGet(PlaceCapacity::new);
        } catch (Exception e) {
        }
        placeCapacity.setChairs(savedSeat);
        placeCapacity.setName(reqTemplatePlace.getName());
        placeCapacityRepository.save(placeCapacity);
        return new ApiResponse(messageSource.getMessage("templatePlace.saved", null, LocaleContextHolder.getLocale()), true);
    }

    @Transactional
    public ApiResponse deletePlace(UUID id) {
        Optional<PlaceCapacity> pl = placeCapacityRepository.findById(id);
        if (pl.isPresent()) {
            PlaceCapacity place = pl.get();
            placeCapacityRepository.delete(place);
            List<Chair> chairs = place.getChairs();
            chairRepository.deleteAll(chairs);
            return new ApiResponse("O'chirildi", true);
        }
        return new ApiResponse("Xatolik", false);
    }


}
