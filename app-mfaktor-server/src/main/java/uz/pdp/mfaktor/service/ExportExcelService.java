package uz.pdp.mfaktor.service;


import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.pdp.mfaktor.entity.Attachment;
import uz.pdp.mfaktor.entity.AttachmentContent;
import uz.pdp.mfaktor.entity.Event;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ResEventUser;
import uz.pdp.mfaktor.repository.AttachmentContentRepository;
import uz.pdp.mfaktor.repository.AttachmentRepository;
import uz.pdp.mfaktor.repository.EventRepository;

import java.io.*;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Service
public class ExportExcelService {

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    EventService eventService;
    @Autowired
    EventRepository eventRepository;

    public ApiResponseModel exportDataToExcel(UUID eventId) {
        Event event = eventRepository.findById(eventId).orElseThrow(() ->
                new ResourceNotFoundException("add/event", "eventId", eventId));
        String title = event.getTitle() + "ga qatnashuvchilar ro'yxati. (" + LocalDate.now() + ")";
        List<ResEventUser> eventUsers = eventService.getEventUsers(eventId);
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(title);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 8));

        XSSFRow rowHead = sheet.createRow((short) 0);
        rowHead.setHeightInPoints(30);
        XSSFCell cellHead = rowHead.createCell(0);
        cellHead.setCellValue(title);
        XSSFFont font = workbook.createFont();
        font.setColor((short) 10);
        font.setFontHeightInPoints((short) 22);
        font.setColor(IndexedColors.WHITE.getIndex());
        XSSFCellStyle xssfCellStyle = workbook.createCellStyle();
        xssfCellStyle.setFont(font);
        rowHead.setRowStyle(xssfCellStyle);

        CellStyle styleHead = workbook.createCellStyle();
        styleHead.setBorderBottom(BorderStyle.THIN);
        styleHead.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        styleHead.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHead.setAlignment(HorizontalAlignment.CENTER);
        styleHead.setFont(font);
        cellHead.setCellStyle(styleHead);
        CellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setAlignment(HorizontalAlignment.CENTER);
        XSSFFont font1 = workbook.createFont();
        font1.setFontHeightInPoints((short) 14);
        style.setFont(font1);
        CellStyle styleCol = workbook.createCellStyle();
        styleCol.setAlignment(HorizontalAlignment.CENTER);
        XSSFFont fontCol = workbook.createFont();
        styleCol.setFont(fontCol);

        XSSFRow rowHeader = sheet.createRow((short) 1);
        Cell cell = rowHeader.createCell(0);
        cell.setCellValue("T/r");
        cell = rowHeader.createCell(1);
        cell.setCellValue("FISH");
        cell = rowHeader.createCell(2);
        cell.setCellValue("telefon raqami");
        cell = rowHeader.createCell(3);
        cell.setCellValue("Balansi");
        cell = rowHeader.createCell(4);
        cell.setCellValue("Joyi");
        cell = rowHeader.createCell(5);
        cell.setCellValue("Band qilgan vaqti");
        cell = rowHeader.createCell(6);
        cell.setCellValue("To'lov vaqti");
        cell = rowHeader.createCell(7);
        cell.setCellValue("Keldi");
        cell = rowHeader.createCell(8);
        cell.setCellValue("Izoh");


        for (int i = 0; i < eventUsers.size(); i++) {
            XSSFRow row = sheet.createRow((short) i + 2);
            cell = row.createCell(0);
            cell.setCellValue(i + 1);
            cell = row.createCell(1);
            cell.setCellValue(eventUsers.get(i).getFullName());
            cell = row.createCell(2);
            cell.setCellValue(eventUsers.get(i).getPhoneNumber());
            cell = row.createCell(3);
            cell.setCellValue(eventUsers.get(i).getBalance());
            cell = row.createCell(4);
            cell.setCellValue(eventUsers.get(i).getSeatName());
            cell = row.createCell(5);
            cell.setCellValue(eventUsers.get(i).getTargetTime());
            cell = row.createCell(6);
            cell.setCellValue(eventUsers.get(i).getPayTime());
            cell = row.createCell(7);
            cell.setCellValue(eventUsers.get(i).getParticipated() == null ? "Belgilanmagan" : eventUsers.get(i).getParticipated() ? "Keldi" : "Kelmadi");
            cell = row.createCell(8);
            cell.setCellValue(eventUsers.get(i).getComment());
        }
        try {
            File file = new File("report3.xlsx");
            OutputStream outputStream = new FileOutputStream("report3.xlsx");
            InputStream inputStream = new FileInputStream("report3.xlsx");
            Attachment attachment = new Attachment();
            attachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            attachment.setName(file.getName());
            workbook.write(outputStream);
            workbook.close();
            attachment.setSize(file.length());
            Attachment savedAttachment = attachmentRepository.save(attachment);
            AttachmentContent attachmentContent = new AttachmentContent();
            attachmentContent.setAttachment(savedAttachment);
            byte[] bytes = IOUtils.toByteArray(inputStream);
            attachmentContent.setContent(bytes);
            attachmentContentRepository.save(attachmentContent);
            return new ApiResponseModel(true, "Mana file id", ServletUriComponentsBuilder.fromCurrentContextPath().
                    path("/api/file/").path(String.valueOf(savedAttachment.getId())).toUriString());
        } catch (
                IOException e) {
            e.printStackTrace();
            return new ApiResponseModel(false, "Fayl yaratishda xatolik", "Xato");
        }
    }
}
