package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.mfaktor.bot.BotConstants;
import uz.pdp.mfaktor.bot2.BotUser;
import uz.pdp.mfaktor.bot2.BotUserRepository;
import uz.pdp.mfaktor.bot2.RealMFaktorBot;
import uz.pdp.mfaktor.entity.*;
import uz.pdp.mfaktor.entity.enums.EventStatus;
import uz.pdp.mfaktor.entity.enums.PlaceStatus;
import uz.pdp.mfaktor.entity.enums.PriceType;
import uz.pdp.mfaktor.entity.template.AbsEntity;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.*;
import uz.pdp.mfaktor.repository.*;
import uz.pdp.mfaktor.utils.CommonUtils;

import java.io.ByteArrayInputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EventService {
    @Autowired
    WaitingExpenseRepository waitingExpenseRepository;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    EventTypeRepository eventTypeRepository;
    @Autowired
    PlaceCapacityRepository placeCapacityRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    SeatRepository seatRepository;
    @Autowired
    UserService userService;
    @Autowired
    PayTypeRepository payTypeRepository;
    @Autowired
    MessageSource messageSource;
    @Autowired
    RealMFaktorBot mFactorBot;
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    LanguageRepository languageRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    SeatService seatService;
    @Autowired
    SeatPaymentRepository seatPaymentRepository;
    @Autowired
    ExpenseRepository expenseRepository;
    @Autowired
    BotUserRepository botUserRepository;
    @Autowired
    CashTypeRepository cashTypeRepository;
    @Autowired
    CashBoxRepository cashBoxRepository;
    @Autowired
    CashTypeService cashTypeService;

    public ApiResponse addEvent(ReqEvent reqEvent) {
        Event event = new Event(
                reqEvent.getSerialNumber(),
                reqEvent.getTitle(),
                userRepository.findAllById(reqEvent.getSpeakersId()),
                reqEvent.getMore(),
                reqEvent.getStartTime(),
                reqEvent.getRegisterTime(),
                attachmentRepository.findById(reqEvent.getEventBannerId()).orElseThrow(() -> new ResourceNotFoundException("add/event", "photoId", reqEvent.getEventBannerId())),
                eventTypeRepository.findById(reqEvent.getEventTypeId()).orElseThrow(() -> new ResourceNotFoundException("add/event", "eventTypeId", reqEvent.getEventTypeId())),
                reqEvent.getFixedPrice() > 0 ? PriceType.FIXED : PriceType.BY_SEAT,
                languageRepository.findById(reqEvent.getLanguageId()).orElseThrow(() -> new ResourceNotFoundException("getLanguage", "languageId", reqEvent.getLanguageId())),
                EventStatus.DRAFT,
                new Timestamp(new Date().getTime()),
                reqEvent.getIsTop()
        );
        event.setAvgStar(0.0);
        Event savedEvent = eventRepository.save(event);
        seatRepository.saveAll(placeCapacityRepository.findById(reqEvent.getPlaceCapacityId()).orElseThrow(() ->
                new ResourceNotFoundException("add/event", "placeTemplateId", reqEvent.getPlaceCapacityId())).
                getChairs().stream().map(tempChair ->
                new Seat(
                        tempChair.getName(),
                        reqEvent.getFixedPrice() == 0 ? tempChair.getPrice() : reqEvent.getFixedPrice(),
                        PlaceStatus.EMPTY,
                        savedEvent,
                        tempChair.getRow(),
                        savedEvent.getStartTime()
                )).collect(Collectors.toList()));
        return new ApiResponse(messageSource.getMessage("event.created", null, LocaleContextHolder.getLocale()), true);
    }

    public ApiResponse editEvent(ReqEvent reqEvent, UUID eventId) {
        Event event = eventRepository.findById(eventId).orElseThrow(() ->
                new ResourceNotFoundException("add/event", "eventId", eventId));
        event.setEventType(eventTypeRepository.findById(reqEvent.getEventTypeId()).orElseThrow(() ->
                new ResourceNotFoundException("add/event", "eventTypeId", reqEvent.getEventTypeId())));
        event.setMore(reqEvent.getMore());
        event.setPhoto(attachmentRepository.findById(reqEvent.getEventBannerId()).orElseThrow(() ->
                new ResourceNotFoundException("add/event", "photoId", reqEvent.getEventBannerId())));
        event.setSerialNumber(reqEvent.getSerialNumber());
        event.setSpeakers(userRepository.findAllById(reqEvent.getSpeakersId()));
        event.setStartTime(reqEvent.getStartTime());
        event.setRegisterTime(reqEvent.getRegisterTime());
        event.setTitle(reqEvent.getTitle());
        event.setIsTop(reqEvent.getIsTop());
        eventRepository.save(event);
        return new ApiResponse("Muvaffaqiyatli tahrirlandi", true);
    }

    public ApiResponse changeStatusEventOpen(ReqEventStatus reqEventStatus) {
        Event event = eventRepository.findById(reqEventStatus.getEventId()).orElseThrow(() ->
                new ResourceNotFoundException("add/event", "eventId", reqEventStatus.getEventId()));
        if (!event.getStatus().equals(EventStatus.ARCHIVE)) {
            event.setStatus(EventStatus.valueOf(reqEventStatus.getEventStatus()));
            if (reqEventStatus.getEventStatus().equals("OPEN")) {
                event.setOpenTime(new Timestamp(new Date().getTime()));
            }
            Event saveEvent = eventRepository.save(event);
            if (saveEvent.getStatus().equals(EventStatus.OPEN)) {
                List<User> allUser = userRepository.findAll();
                for (User user : allUser) {
                    try {
                        if (user.getChatId() != null) {
                            mFactorBot.execute(showMainRoute(saveEvent, user));
                        }
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            }
            return new ApiResponse("Tadbir tahrirlandi", true);
        }
        return new ApiResponse("Tadbir arxiv holatida shu sababli uni o'zgartirib bo'lmaydi", false);
    }

    public ResEventPageable getEvents(String status, int page, int size) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "startTime");
        List<EventStatus> list = new ArrayList<>();
        if (status.equals("all")) {
            list.add(EventStatus.DRAFT);
            list.add(EventStatus.OPEN);
            list.add(EventStatus.REGISTRATION_COMPLETED);
        }
        if (status.equals("draft")) {
            list.add(EventStatus.DRAFT);
        }
        if (status.equals("open")) {
            list.add(EventStatus.OPEN);
        }
        if (status.equals("registered")) {
            list.add(EventStatus.REGISTRATION_COMPLETED);
        }
        Page<Event> events = eventRepository.findAllByStatusInOrderByStartTime(list, pageable);
        return new ResEventPageable(events.getContent().stream().map(this::getEvent).collect(Collectors.toList()), events.getTotalPages());
    }

    public ResEventPageable getArchiveAndCancelEvents(String status, int page, int size) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "startTime");
        List<EventStatus> list = new ArrayList<>();
        if (status.equals("all")) {
            list.add(EventStatus.ARCHIVE);
            list.add(EventStatus.CANCELED);
        }
        if (status.equals("archive")) {
            list.add(EventStatus.ARCHIVE);
        }
        if (status.equals("cancel")) {
            list.add(EventStatus.CANCELED);
        }
        Page<Event> events = eventRepository.findAllByStatusInOrderByStartTime(list, pageable);
        return new ResEventPageable(events.getContent().stream().map(this::getEvent).collect(Collectors.toList()), events.getTotalPages());
    }

    public ResEventPageable getEventsForUser(int page, int size) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "createdAt");
        Page<Event> events = eventRepository.findAllByStatusInOrderByStartTime(Arrays.asList(EventStatus.OPEN, EventStatus.REGISTRATION_COMPLETED), pageable);
        return new ResEventPageable(events.getContent().stream().map(this::getEvent).collect(Collectors.toList()), events.getTotalPages());
    }

    public List<ResEvent> getEventsByType(Integer id) {
        return eventRepository.findAllByEventTypeIdAndStatusIn(id, Arrays.asList(EventStatus.OPEN, EventStatus.REGISTRATION_COMPLETED)).stream().map(this::getEvent).collect(Collectors.toList());
    }

    public List<ResEvent> searchEvent(String word) {
        return eventRepository.findAllByTitleContainingIgnoreCaseAndStatusIn(word, Arrays.asList(
                EventStatus.ARCHIVE,
                EventStatus.OPEN,
                EventStatus.REGISTRATION_COMPLETED
        )).stream().map(this::getEvent).collect(Collectors.toList());
    }

    public List<ResEvent> getEventsByUser(UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("user", "id", id));
        List<Seat> seatsOfUpcomingEvents = seatRepository.findAllByUserAndParticipatedAndPlaceStatusIn(user, null,
                Arrays.asList(PlaceStatus.BOOKED, PlaceStatus.PARTLY_PAID, PlaceStatus.SOLD)).stream()
                .filter(seat -> seat.getEvent().getStartTime().after(new Timestamp(new Date().getTime()))).collect(Collectors.toList());
        return seatsOfUpcomingEvents.stream().map(this::getUserEvent).collect(Collectors.toList());
    }

    public List<ResEvent> getParticipatedEvent(UUID id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("user", "id", id));
        List<Seat> participatedSeats = seatRepository.findAllByUserAndParticipated(user, true);
        List<ResEvent> eventsOfUser = new ArrayList<>();
        for (Seat seat : participatedSeats) {
            eventsOfUser.add(getUserEvent(seat));
        }
        return eventsOfUser;
    }

    public List<ResEvent> getUpcomingEvents(int page, int size) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "createdAt");
        return eventRepository.findAllByStatusInAndStartTimeIsAfterOrderByStartTime(
                Arrays.asList(EventStatus.OPEN, EventStatus.REGISTRATION_COMPLETED), new Timestamp(new Date().getTime()), pageable)
                .stream().map(this::getEvent).collect(Collectors.toList());
    }

    public ResEventPaymentAndBalance getBalanceByEvent(UUID eventId) {
        Event event = eventRepository.findById(eventId).orElseThrow(() -> new ResourceNotFoundException("getEvent", "eventId", eventId));
        double allSum = getSum(getEventSumByPayType(event));
        double expenseSum = expenseRepository.expenseByEvent(eventId);
        return new ResEventPaymentAndBalance(
                getEventSumByPayType(event),
                allSum,
                Math.abs(getDebtSum(getEventUsers(event.getId()))),
                getWeekly(getEventSumByPayType(event)),
                getTodaySum(getEventSumByPayType(event)),
                expenseSum,
                (allSum - expenseSum)
        );
    }

    public List<ResEventPayment> getEventSumByPayType(Event event) {
        List<ResEventPayment> resEventPaymentList = new ArrayList<>();
        List<Seat> seats = seatRepository.findAllByEventOrderByRowAscNameAsc(event)
                .stream().filter(seat -> seat.getPlaceStatus().equals(PlaceStatus.SOLD)
                        || seat.getPlaceStatus().equals(PlaceStatus.PARTLY_PAID)
                        && (seat.getParticipated() == null || seat.getParticipated())).collect(Collectors.toList());

        List<SeatPayment> seatPayments = new ArrayList<>();
        for (Seat seat : seats) {
            seatPayments.addAll(seat.getSeatPayments());
        }
        List<PayType> payTypes = payTypeRepository.findAll();
        for (PayType payType : payTypes) {
            ResEventPayment resEventPayment = new ResEventPayment();
            double allSum = 0;
            double todaySum = 0;
            double weeklySum = 0;
            resEventPayment.setPayTypeUz(payType.getNameUz());
            resEventPayment.setPayTypeRu(payType.getNameRu());
            for (SeatPayment seatPayment : seatPayments) {
                if (payType.equals(seatPayment.getPayment().getPayType())) {
                    allSum += seatPayment.getAmount();
                    if (isWeekly(seatPayment.getPayment(), event.getOpenTime())) {
                        weeklySum += seatPayment.getAmount();
                    }
                    if (isToday(seatPayment.getPayment().getPayDate())) {
                        todaySum += seatPayment.getAmount();
                    }
                }
            }
            resEventPayment.setSum(allSum);
            resEventPayment.setPaymentToday(todaySum);
            resEventPayment.setWeeklyIncome(weeklySum);
            resEventPaymentList.add(resEventPayment);
        }
        return resEventPaymentList;
    }

    private ResEvent getUserEvent(Seat seat) {
        Event event = seat.getEvent();
        ResEvent resEvent = getEvent(event);
        resEvent.setPaidSum(seat.getPlaceStatus().equals(PlaceStatus.BOOKED) ? 0 : getSumBySeatPayments(seat));
        resEvent.setPlaceStatus(seat.getPlaceStatus().toString());
        resEvent.setPlace(seat.getName());
        return resEvent;
    }

    public List<ResEventUser> getEventUsers(UUID eventId) {
        return seatRepository.findAllByEventOrderByRowAscNameAsc(eventRepository.findById(eventId)
                .orElseThrow(() ->
                        new ResourceNotFoundException
                                ("getEvent", "eventId", eventId)))
                .stream().filter(seat -> seat.getUser() != null)
                .map(this::getEventUser).collect(Collectors.toList());


    }

    public ResEvent getEvent(Event event) {
        return new ResEvent(
                event.getId(),
                event.getSerialNumber(),
                ServletUriComponentsBuilder.fromCurrentContextPath().
                        path("/api/file/").path(event.getPhoto().getId().toString()).toUriString(),
                event.getTitle(),
                event.getSpeakers().stream().map(AbsEntity::getId).collect(Collectors.toList()),
                getSpeakerName(event),
                event.getEventType().getId(),
                event.getEventType().getNameUz(),
                event.getStatus().toString(),
                event.getStartTime(),
                event.getRegisterTime(),
                event.getLanguage().getId(),
                event.getLanguage().getNameUz(),
                event.getOpenTime() != null,
                event.getIsTop(),
                event.getSeats().size(),
                (int) event.getSeats().stream().filter(seat ->
                        seat.getPlaceStatus().equals(PlaceStatus.BOOKED) || seat.getPlaceStatus().equals(PlaceStatus.PARTLY_PAID)).count(),
                (int) event.getSeats().stream().filter(seat -> seat.getPlaceStatus().equals(PlaceStatus.EMPTY)).count(),
                (int) event.getSeats().stream().filter(seat -> seat.getPlaceStatus().equals(PlaceStatus.SOLD)).count(),
                (int) event.getSeats().stream().filter(seat -> seat.getPlaceStatus().equals(PlaceStatus.RESERVED)).count(),
                (int) event.getSeats().stream().filter(seat -> seat.getPlaceStatus().equals(PlaceStatus.VIP)).count(),
                event.getAvgStar(),
                event.getReviews().size(),
                getMinAndMaxValue(event, false),
                getMinAndMaxValue(event, true),
                event.getOpenTime() != null ? getSum(getEventSumByPayType(event)) : 0,
                event.getMore(),
                getSpeakersAbout(event));
    }

    public List<ResSeatPayment> getSeatPaymentsByEvent(UUID eventId) {
        List<Seat> seats = seatRepository.findAllByEventOrderByRowAscNameAsc(eventRepository.findById(eventId)
                .orElseThrow(() ->
                        new ResourceNotFoundException
                                ("getEvent", "eventId", eventId)))
                .stream().filter(seat -> seat.getSeatPayments().size() > 0).collect(Collectors.toList());
        List<ResSeatPayment> seatPayments = new ArrayList<>();
        for (Seat seat : seats) {
            List<SeatPayment> payments = seat.getSeatPayments();
            for (SeatPayment seatPayment : payments) {
                seatPayments.add(new ResSeatPayment(
                        userService.getFullName(seat.getUser()),
                        seat.getUser().getPhoneNumber(),
                        seat.getName(),
                        seatPayment.getPayment().getPayDate(),
                        seatPayment.getAmount(),
                        seatPayment.getPayment().getPayType().getNameUz()));
            }
        }
        return seatPayments;
    }

    private double getMinAndMaxValue(Event event, boolean isMax) {
        double minPrice = Double.MAX_VALUE;
        double maxPrice = 0;
        for (Seat seat : event.getSeats()) {
            if (isMax)
                maxPrice = Math.max(maxPrice, seat.getPrice());

            else
                minPrice = Math.min(minPrice, seat.getPrice());

        }
        return isMax ? maxPrice : minPrice;
    }

    private ResEventUser getEventUser(Seat seat) {
        return new ResEventUser(
                seat.getUser().getId(),
                seat.getId(),
                seat.getUser().getPhoto() != null ?
                        ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/").
                                path(seat.getUser().getPhoto().getId().toString()).toUriString() : "",
                userService.getFullName(seat.getUser()),
                seat.getUser().getPhoneNumber(),
                seat.getUser().getTelegramUsername() != null
                        ?
                        seat.getUser().getTelegramUsername() : "",
                seat.getPlaceStatus().equals(PlaceStatus.VIP)
                        ?
                        0 :
                        seat.getPlaceStatus().equals(PlaceStatus.BOOKED) &&
                                (seat.getParticipated() == null || seat.getParticipated())
                                ?
                                0 - seatService.seatPriceWithSale(seat, seat.getUser())
                                :
                                seat.getPlaceStatus().equals(PlaceStatus.PARTLY_PAID) &&
                                        (seat.getParticipated() == null || seat.getParticipated())
                                        ?
                                        getSumBySeatPayments(seat) - seatService.seatPriceWithSale(seat, seat.getUser())
                                        :
                                        paymentRepository.sumOfLeftover(seat.getUser().getId()),
                seat.getPlaceStatus().equals(PlaceStatus.BOOKED) ||
                        seat.getPlaceStatus().equals(PlaceStatus.VIP) || seat.getSeatPayments().size() == 0 ? "" :
                        getSeatPayment(seat).getPayment().getPayType().getNameUz(),
                seat.getName(),
                seat.getPlaceStatus().equals(PlaceStatus.BOOKED)
                        || seat.getPlaceStatus().equals(PlaceStatus.VIP) || seat.getSeatPayments().size() == 0 ? null
                        : getSeatPayment(seat).getPayment().getPayDate(),
                seat.getTargetTime(),
                seat.getParticipated(),
                seat.getComment()
        );
    }

    private SeatPayment getSeatPayment(Seat seat) {
        SeatPayment seatPayment = new SeatPayment();
        double sum = 0;
        for (SeatPayment seatPay : seat.getSeatPayments()) {
            seatPayment.setPayment(seatPay.getPayment());
            sum += seatPay.getAmount();
        }
        seatPayment.setAmount(sum);
        return seatPayment;
    }

    public ResParticipatedAndGuest getParticipatedAndGuest(UUID eventId) {
        List<Seat> seats = seatRepository.findAllByEventOrderByRowAscNameAsc(eventRepository.findById(eventId).
                orElseThrow(() -> new ResourceNotFoundException("event/{eventId}", "eventId", eventId)));
        return new ResParticipatedAndGuest(
                (int) seats.stream().filter(seat -> !seat.getPlaceStatus().equals(PlaceStatus.EMPTY) &&
                        !seat.getPlaceStatus().equals(PlaceStatus.RESERVED)).count(),
                (int) seats.stream().filter(seat -> seat.getParticipated() != null && seat.getParticipated()).count(),
                (int) seats.stream().filter(seat -> seat.getParticipated() != null && !seat.getParticipated()).count(),
                (int) seats.stream().filter(seat -> seat.getPlaceStatus().equals(PlaceStatus.VIP)).count()
        );
    }

    public List<ResEventStatus> getEventStatuses() {
        List<ResEventStatus> resEventStatuses = new ArrayList<>();
        resEventStatuses.add(new ResEventStatus(EventStatus.DRAFT.toString(), "Qoralama"));
        resEventStatuses.add(new ResEventStatus(EventStatus.OPEN.toString(), "Ochiq"));
        resEventStatuses.add(new ResEventStatus(EventStatus.REGISTRATION_COMPLETED.toString(), "Ro'yxatga olish yakunlangan"));
        resEventStatuses.add(new ResEventStatus(EventStatus.CANCELED.toString(), "Bekor qilingan"));
        resEventStatuses.add(new ResEventStatus(EventStatus.ARCHIVE.toString(), "Arxiv"));
        return resEventStatuses;
    }

    public boolean isToday(Timestamp timestamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String today = dateFormat.format(new Date());
        String isCheck = dateFormat.format(new Date(timestamp.getTime()));
        return today.equals(isCheck);
    }

    public double getSumBySeatPayments(Seat seat) {
        double sum = 0;
        for (SeatPayment seatPayment : seat.getSeatPayments()) {
            sum += seatPayment.getAmount();
        }
        return sum;
    }

    private double getDebtSum(List<ResEventUser> resEventUsers) {
        double debt = 0;
        for (ResEventUser resEventUser : resEventUsers) {
            if (resEventUser.getBalance() < 0) {
                debt += resEventUser.getBalance();
            }
        }
        return debt;
    }

    private double getSum(List<ResEventPayment> resEventPayments) {
        double sum = 0;
        for (ResEventPayment res : resEventPayments) {
            sum += res.getSum();
        }
        return sum;
    }

    private double getWeekly(List<ResEventPayment> resEventPayments) {
        double sum = 0;
        for (ResEventPayment res : resEventPayments) {
            sum += res.getWeeklyIncome();
        }
        return sum;
    }

    private double getTodaySum(List<ResEventPayment> resEventPayments) {
        double sum = 0;
        for (ResEventPayment res : resEventPayments) {
            sum += res.getPaymentToday();
        }
        return sum;
    }

    private String getSpeakerName(Event event) {
        return event.getSpeakers().stream().map(user ->
                userService.getFullName(user)).
                collect(Collectors.joining(","));
    }

    private String getSpeakersAbout(Event event) {
        return event.getSpeakers().stream().map(User::getAbout).
                collect(Collectors.joining("\n\n"));
    }

    private boolean isWeekly(Payment payment, Timestamp openTime) {
        return payment.getPayDate().after(openTime);
    }

    public SendPhoto showMainRoute(Event event, User user) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(user.getTelegramId());
        try {
            SendPhoto sendPhoto = new SendPhoto();
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rows = new ArrayList<>();
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            List<InlineKeyboardButton> row2 = new ArrayList<>();

            InlineKeyboardButton button1 = new InlineKeyboardButton();
            InlineKeyboardButton button2 = new InlineKeyboardButton();

            sendPhoto.setChatId(user.getChatId())
                    .setParseMode(ParseMode.MARKDOWN);
            List<Event> allTopEvents = eventRepository.findAllByIsTopAndStatusOrderByStartTime(true, EventStatus.OPEN);
            StringBuilder stringBuilder1;
            for (int i = 0; i < allTopEvents.size(); i++) {
                List<uz.pdp.mfaktor.entity.User> speakers = allTopEvents.get(i).getSpeakers();
                stringBuilder1 = new StringBuilder();
                for (int j = 0; j < speakers.size(); j++) {
                    stringBuilder1.append(speakers.get(j).getFirstName()).append(" ").append(speakers.get(j).getLastName()).append("\n");
                }
            }

            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                button1.setText("Банд қилиш");
                button2.setText(BotConstants.BACK_KR);
            } else {
                button1.setText("Бронирование");
                button2.setText(BotConstants.BACK_RU);
            }
            button1.setCallbackData("EventId#" + event.getId());
            button2.setCallbackData("BackMain2");
            StringBuilder stringBuilder = new StringBuilder();
            event.getSpeakers().forEach(user1 -> {
                stringBuilder.append(user1.getFirstName()).append(" ").append(user1.getLastName()).append("\n");
            });
            Attachment photo = event.getPhoto();
            Optional<AttachmentContent> attachmentContent = attachmentContentRepository.findByAttachment(photo);
            sendPhoto.setPhoto(event.getPhoto().getName(), new ByteArrayInputStream(attachmentContent.get().getContent()));
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                sendPhoto.setCaption("Ассалому алайкум ҳурматли *" + user.getFirstName() + " " + user.getLastName() + "*.\n" +
                        "\n" +
                        "Сизга *" + event.getRegisterTime().toString().substring(0, event.getRegisterTime().toString().length() - 5) + "* да бўлиб ўтадиган тадбирда иштирок этишни тавсия этамиз.\n" +
                        "\n" +
                        "Мавзу: *" + event.getTitle() + "*\n" +
                        "Спикер: *" + stringBuilder.toString() + "*");
            } else {
                sendPhoto.setCaption("Здравствуйте, *" + user.getFirstName() + " " + user.getLastName() + "*.\n" +
                        "\n" +
                        "Советуем участвовать на мероприятии, которое состоится *" + event.getRegisterTime().toString().substring(0, event.getRegisterTime().toString().length() - 5) + "*.\n" +
                        "\n" +
                        "Тема: *" + event.getTitle() + "*\n" +
                        "Cпикер: *" + stringBuilder.toString() + "*");
            }
            row1.add(button1);
            row2.add(button2);
            rows.add(row1);
            rows.add(row2);
            inlineKeyboardMarkup.setKeyboard(rows);
            sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
            return sendPhoto;
        } catch (Exception e) {
            return null;
        }
    }

    public ResDashboard getInfo() {
        double allCurrentBalance = 0;
        double dailyDebt = 0;
        double weeklyDebt = 0;
        double monthlyDebt = 0;
        List<Event> dailyEvent = eventRepository.getDailyEvent();
        for (Event event : dailyEvent) {
            dailyDebt += Math.abs(getDebtSum(getEventUsers(event.getId())));
        }
        List<Event> weeklyEvent = eventRepository.getWeeklyEvent();
        for (Event event : weeklyEvent) {
            weeklyDebt += Math.abs(getDebtSum(getEventUsers(event.getId())));
        }
        List<Event> monthlyEvent = eventRepository.getMonthlyEvent();
        for (Event event : monthlyEvent) {
            monthlyDebt += Math.abs(getDebtSum(getEventUsers(event.getId())));
        }
        for (ResBalanceByPayType resBalanceByPayType : resBalanceByPayTypes()) {
            allCurrentBalance += resBalanceByPayType.getBalance();
        }
        double expenseDailySum = expenseRepository.expenseDailySum();
        double expenseWeeklySum = expenseRepository.expenseWeeklySum();
        double expenseMonthlySum = expenseRepository.expenseMonthlySum();
        double incomeDailySum = seatPaymentRepository.incomeDailySum();
        double incomeWeeklySum = seatPaymentRepository.incomeWeeklySum();
        double incomeMonthlySum = seatPaymentRepository.incomeMonthlySum();
        double incomeAll = seatPaymentRepository.incomeAll();
        double expenseAllSum = expenseRepository.expenseAllSum();
        return new ResDashboard(
                seatPaymentRepository.incomeDailyCount(),
                incomeDailySum,
                seatPaymentRepository.incomeWeeklyCount(),
                incomeWeeklySum,
                seatPaymentRepository.incomeMonthlyCount(),
                incomeMonthlySum,
                expenseDailySum,
                expenseWeeklySum,
                expenseMonthlySum,
                (incomeDailySum - expenseDailySum),
                (incomeWeeklySum - expenseWeeklySum),
                (incomeMonthlySum - expenseMonthlySum),
                userRepository.countUsers(),
                userRepository.countWeeklyUsers(),
                eventRepository.countEvent(),
                eventRepository.countEventMonthly(),
                dailyDebt,
                weeklyDebt,
                monthlyDebt,
                allCurrentBalance,
                resBalanceByPayTypes(),
                cashTypeRepository.findAll().stream().map(this::getBalanceByCashType).collect(Collectors.toList()),
                incomeAll,
                expenseAllSum,
                incomeAll - expenseAllSum

        );
    }

    public List<ResBalanceByPayType> resBalanceByPayTypes() {
        List<PayType> payTypes = payTypeRepository.findAll();
        return payTypes.stream().map(this::resBalanceByPayType).collect(Collectors.toList());
    }

    public ResBalanceByPayType resBalanceByPayType(PayType payType) {
        return new ResBalanceByPayType(
                payType.getNameUz(),
                payType.getNameRu(),
                seatPaymentRepository.incomeDailySumByPayType(payType.getId()),
                seatPaymentRepository.incomeWeeklySumByPayType(payType.getId()),
                seatPaymentRepository.incomeMonthlySumByPayType(payType.getId()),
                expenseRepository.expenseDailySumByCashType(payType.getId()),
                expenseRepository.expenseWeeklySumByCashType(payType.getId()),
                expenseRepository.expenseMonthlySumByCashType(payType.getId()),
                balanceByPayType(payType)
        );
    }

    private double balanceByPayType(PayType payType) {
        return seatPaymentRepository.incomeAllByPayType(payType.getId());
    }

    public ResBalanceByCashType getBalanceByCashType(CashType cashType) {
        double incomePayTypes = getIncomeByCashType(cashType);
        double sumToCashBoxByCashType = cashTypeService.getSumToCashBoxByCashType(cashType);
        double sumFromCashBoxByCashType = cashTypeService.getSumFromCashBoxByCashType(cashType);
        double expense = expenseRepository.expenseAllByCashType(cashType.getId());
        return new ResBalanceByCashType(
                cashType.getNameUz(),
                cashType.getDescription(),
                incomePayTypes + sumToCashBoxByCashType,
                expense + sumFromCashBoxByCashType,
                getSumByCashType(cashType)
        );
    }

    public double getIncomeByCashType(CashType cashType) {
        return cashType.getPayTypes().stream().mapToDouble(this::balanceByPayType).sum();
    }

    public double getSumByCashType(CashType cashType) {

        double incomePayTypes = getIncomeByCashType(cashType);
        double expense = expenseRepository.expenseAllByCashType(cashType.getId());
        double sumToCashBoxByCashType = cashTypeService.getSumToCashBoxByCashType(cashType);
        double sumFromCashBoxByCashType = cashTypeService.getSumFromCashBoxByCashType(cashType);
        return (incomePayTypes - expense - sumFromCashBoxByCashType) + sumToCashBoxByCashType;
    }

    public double debtToUs() {
        List<Event> events = eventRepository.findAllByStatusInOrderByStartTime(Arrays.asList(EventStatus.OPEN, EventStatus.REGISTRATION_COMPLETED, EventStatus.ARCHIVE));
        double sum = 0;
        for (Event event : events) {
            sum += getDebtSum(getEventUsers(event.getId()));
        }
        return Math.abs(sum);
    }

    public double weDebt() {
        double a = waitingExpenseRepository.weDebt();
        return waitingExpenseRepository.weDebt();
    }

    public List<ResEvent> getOpenEvents() {
        return eventRepository.findAllByStatusOrderByStartTime(EventStatus.OPEN).stream().map(event -> new ResEvent(event.getId(), event.getSerialNumber(), event.getTitle())).collect(Collectors.toList());
    }
}
