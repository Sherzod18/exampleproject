package uz.pdp.mfaktor.service;

import com.google.protobuf.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.VideoType;
import uz.pdp.mfaktor.entity.VideoUrl;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqVideo;
import uz.pdp.mfaktor.repository.VideoTypeRepository;
import uz.pdp.mfaktor.repository.VideoUrlRepository;
import uz.pdp.mfaktor.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class VideoService {
    @Autowired
    VideoUrlRepository videoUrlRepository;
    @Autowired
    VideoTypeRepository videoTypeRepository;

    public List<VideoUrl> getAll(int page, int size) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "createdAt");
        return  videoUrlRepository.findAll(pageable).getContent();

    }

    public ApiResponseModel saveVideo(ReqVideo reqVideo) {
        String start = "<iframe width=\"100%\" height=\"100%\" src=\"";
        String end = "\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>";
        String httpStart = reqVideo.getVideoUrl().substring(0, 24);
        String httpend = reqVideo.getVideoUrl().substring(32);
        VideoUrl videoUrl = new VideoUrl();
        videoUrl.setTitle(reqVideo.getTitle());
        videoUrl.setVideoType(videoTypeRepository
                .findById(reqVideo.getVideoTypeId())
                .orElseThrow(() ->
                        new ResourceNotFoundException
                                ("getVideoType", "videoTypeId",
                                        reqVideo.getVideoTypeId())));
        videoUrl.setVideoUrl(start + httpStart + "embed/" + httpend + end);
        videoUrl.setYoutubeId(httpend);
        videoUrl = videoUrlRepository.save(videoUrl);
        return new ApiResponseModel(true, "Video muvoffaqiyatli yaratildi.", videoUrl);
    }

    public ApiResponse deleteVideo(UUID videoId) {
        videoUrlRepository.deleteById(videoId);
        return new ApiResponse("Video deleted successfully", true);
    }

    public List<VideoUrl> getLast5Videos() {
        List<VideoUrl> videoUrls = videoUrlRepository.findAll();

        List<VideoUrl> last5VideoUrls = new ArrayList<>();
if(videoUrls.size()>=5){
        for (int i = (videoUrls.size() - 1); i >= (videoUrls.size() - 5); i--) {
            last5VideoUrls.add(videoUrls.get(i));
        }
}else{
    last5VideoUrls=videoUrls;
}
        return last5VideoUrls;
    }

    public List<VideoUrl> getPraktikums(String videoTypeNam, int page, int size) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "createdAt");
        return  videoUrlRepository
                .findAllByVideoType(videoTypeRepository
                                .findByName(videoTypeNam),pageable).getContent();

    }

    public List<VideoUrl> searchByTitle(String title){
        List<VideoUrl> videoUrls=videoUrlRepository.findAll();
        List<VideoUrl> videoUrls1=new ArrayList<>();
        for (VideoUrl video:videoUrls) {
            if (video.getTitle().toLowerCase().contains(title.toLowerCase())){
                videoUrls1.add(video);
            }
        }
        return videoUrls1;
    }

    public VideoType getAnonsType(){
        String str="anons";
        VideoType videoType=new VideoType();
        List<VideoType> videoTypes=videoTypeRepository.findAll();
        for (VideoType vt:videoTypes) {
            if (vt.getName().toLowerCase().contains(str)){
                videoType=vt;
            }
        }
        return videoType;
    }
}
