package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.pdp.mfaktor.entity.Article;
import uz.pdp.mfaktor.entity.Comment;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqComment;
import uz.pdp.mfaktor.payload.ResComment;
import uz.pdp.mfaktor.repository.ArticleRepository;
import uz.pdp.mfaktor.repository.CommentRepository;
import uz.pdp.mfaktor.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    UserService userService;
    @Autowired
    ArticleRepository articleRepository;

    public List<ResComment> getCommentsByArticle(UUID articleId) {
        return commentRepository.findAllByArticleId(articleId).stream().map(this::getComment).collect(Collectors.toList());
    }

    public ResComment getComment(Comment comment) {
        User user = userRepository.findById(comment.getCreatedBy()).
                orElseThrow(() -> new ResourceNotFoundException("getUser", "userId", comment.getCreatedBy()));
        return new ResComment(
                user.getPhoto() != null ? ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/").
                        path(user.getPhoto().getId().toString()).toUriString() : "",
                comment.getText(),
                userService.getFullName(user),
                comment.getCreatedAt()
        );
    }

    public ApiResponse addComment(ReqComment reqComment) {
        Optional<Article> article = articleRepository.findById(reqComment.getArticleId());
        if (article.isPresent()) {
            Comment comment = new Comment();
            if (reqComment.getId() != null) {
                comment.setId(reqComment.getId());
            }
            comment.setText(reqComment.getText());
            comment.setArticle(article.get());
            commentRepository.save(comment);
            return new ApiResponse(reqComment.getId() != null ? "Izoh tahrirlandi" : "Izoh qo'shildi", true);
        } else {
            return new ApiResponse("Maqola topilmadi", false);
        }
    }

    public ApiResponse deleteComment(UUID commentId) {
        if (commentRepository.findById(commentId).isPresent()) {
            commentRepository.deleteById(commentId);
            return new ApiResponse("Izoh o'chirildi", true);
        }
        return new ApiResponse("Izoh topilmadi", false);
    }

}
