package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.CashBox;
import uz.pdp.mfaktor.entity.CashType;
import uz.pdp.mfaktor.entity.template.AbsNameEntity;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqCashType;
import uz.pdp.mfaktor.payload.ReqTransfer;
import uz.pdp.mfaktor.repository.CashBoxRepository;
import uz.pdp.mfaktor.repository.CashTypeRepository;
import uz.pdp.mfaktor.repository.PayTypeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CashTypeService {
    @Autowired
    CashTypeRepository cashTypeRepository;
    @Autowired
    PayTypeRepository payTypeRepository;
    @Autowired
    CashBoxRepository cashBoxRepository;
    @Autowired
    EventService eventService;

    public ApiResponse addCashType(ReqCashType reqCashType) {
        CashType cashType = new CashType();
        if (reqCashType.getId() != null) {
            cashType = cashTypeRepository.findById(reqCashType.getId()).orElseThrow(() -> new ResourceNotFoundException("getcashType", "cashType", reqCashType.getId()));
        }
        cashType.setColor(reqCashType.getColor());
        cashType.setNameUz(reqCashType.getNameUz());
        cashType.setNameRu(reqCashType.getNameRu());
        if (reqCashType.getPayTypeIds().size() > 0) {
            cashType.setPayTypes(payTypeRepository.findAllById(reqCashType.getPayTypeIds()));
        }
        cashType.setDescription(reqCashType.getDescription());
        cashTypeRepository.save(cashType);
        return new ApiResponse("Saqlandi", true);
    }

    public List<ReqCashType> getCashTypes() {
        return cashTypeRepository.findAll().stream().map(this::getCashType).collect(Collectors.toList());
    }

    public ReqCashType getCashType(CashType cashType) {
        return new ReqCashType(
                cashType.getId(),
                cashType.getNameUz(),
                cashType.getNameRu(),
                cashType.getColor(),
                cashType.getPayTypes().stream().map(AbsNameEntity::getId).collect(Collectors.toList()),
                cashType.getPayTypes().stream().map(AbsNameEntity::getNameUz).collect(Collectors.joining(",")),
                cashType.getDescription()
        );
    }

    public ApiResponse transfer(ReqTransfer reqTransfer) {
        try {
            CashBox cashBox = new CashBox();
            CashType fromCash;
            CashType toCash = null;
            if (reqTransfer.isFromUsd()) {
                fromCash = cashTypeRepository.findByDescription("usd");
                toCash = cashTypeRepository.findByDescription("cash");
                cashBox.setFromUsd(true);
            } else if (reqTransfer.isToUsd()) {
                fromCash = cashTypeRepository.findByDescription("cash");
                toCash = cashTypeRepository.findByDescription("usd");
                cashBox.setToUsd(true);
            } else {
                fromCash = cashTypeRepository.findById(reqTransfer.getFromCashId()).orElseThrow(() -> new ResourceNotFoundException("getCashType", "cashType", reqTransfer.getFromCashId()));
            }
            if (eventService.getSumByCashType(fromCash) >= reqTransfer.getSum()) {
                cashBox.setFromCash(fromCash);
                if (!reqTransfer.isToUsd() && !reqTransfer.isFromUsd()) {
                    cashBox.setToCash(cashTypeRepository.findById(reqTransfer.getToCashId()).orElseThrow(() -> new ResourceNotFoundException("getCashType", "cashType", reqTransfer.getFromCashId())));
                } else {
                    cashBox.setToCash(toCash);
                }
                if (reqTransfer.isFromUsd()) {
                    cashBox.setToSum(reqTransfer.getSum() * reqTransfer.getUsdSum());
                } else if (reqTransfer.isToUsd()) {
                    cashBox.setToSum(reqTransfer.getSum() / reqTransfer.getUsdSum());
                } else {
                    cashBox.setToSum(reqTransfer.getSum());
                }
                cashBox.setTime(reqTransfer.getTime());
                cashBox.setDetails(reqTransfer.getDetails());
                cashBoxRepository.save(cashBox);
                return new ApiResponse("Saqlandi", true);
            } else {
                return new ApiResponse("Mablag' yetarli emas", false);
            }
        } catch (Exception e) {
            return new ApiResponse("O'tkazishda xatolik", false);
        }
    }

    public double getSumToCashBoxByCashType(CashType cashType) {
        return cashBoxRepository.getSumToByCashType(cashType.getId());
    }

    public double getSumFromCashBoxByCashType(CashType cashType) {
        return cashBoxRepository.getSumFromByCashType(cashType.getId());
    }


}
