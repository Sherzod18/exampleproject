package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.pdp.mfaktor.bot2.RealMFaktorBot;
import uz.pdp.mfaktor.entity.Payment;
import uz.pdp.mfaktor.entity.Seat;
import uz.pdp.mfaktor.entity.SeatPayment;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.entity.enums.PlaceStatus;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.PaymentPageableResponse;
import uz.pdp.mfaktor.payload.ReqPayment;
import uz.pdp.mfaktor.payload.ResPayment;
import uz.pdp.mfaktor.repository.*;
import uz.pdp.mfaktor.utils.CommonUtils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PaymentService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    PayTypeRepository payTypeRepository;
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    UserService userService;
    @Autowired
    SeatRepository seatRepository;
    @Autowired
    SeatService seatService;
    @Autowired
    MessageSource messageSource;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    ActivityRepository activityRepository;
    @Autowired
    AwareRepository awareRepository;
    @Autowired
    PositionRepository positionRepository;
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    UsdRepository usdRepository;
    @Autowired
    RealMFaktorBot mFactorBot;

    public ApiResponse addPayment(ReqPayment request, User currentUser) {
        ApiResponse apiResponse = new ApiResponse();
        User user = seatService.createOrGetUser(request);
        if (user.getPhoneNumber() != null) {
            Payment payment = new Payment();
            payment.setUser(user);
            payment.setPayType(payTypeRepository.
                    findById(request.getPayTypeId())
                    .orElseThrow(() ->
                            new ResourceNotFoundException(
                                    "api/payment",
                                    "payTypeId",
                                    request.getPayTypeId())));
            if (request.getSumUsd() > 0) {
                payment.setSumUsd(request.getPaySum());
                request.setPaySum(exchangeUsd(request.getPaySum()));
            }
            payment.setPaySum(request.getPaySum());
            payment.setPayDate(request.getPayDate());
            payment.setDetails(request.getDetails());
            payment.setLeftover(request.getPaySum());
            paymentRepository.save(payment);
            if (request.isFromTelegram() || request.getSeatId() != null) {
                if (!request.isFromTelegram()) {
                    request.setUserId(user.getId());
                    request.setId(user.getId());
                }
                ApiResponse response = seatService.seatOrder(request, request.getSeatId(), currentUser);
                if (response.isSuccess()) {
                    apiResponse.setSuccess(true);
                    apiResponse.setMessage(response.getMessage());
                }
            } else {
                List<Seat> partlyPaidSeats2 = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.PARTLY_PAID, true);
                List<Seat> partlyPaidSeats = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.PARTLY_PAID, null);
                if (partlyPaidSeats2.size() > 0) {
                    changeSeatStatus(partlyPaidSeats2);
                }
                if (partlyPaidSeats.size() > 0) {
                    changeSeatStatus(partlyPaidSeats);
                }
                List<Seat> bookedSeats2 = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.BOOKED, true);
                List<Seat> bookedSeats = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.BOOKED, null);
                if (bookedSeats2.size() > 0) {
                    changeSeatStatus(bookedSeats2);
                }
                if (bookedSeats.size() > 0) {
                    changeSeatStatus(bookedSeats);
                }
                if (request.getSeatId() == null && !apiResponse.isSuccess()) {
                    apiResponse.setSuccess(true);
                }
            }
            mFactorBot.sendNotification(payment);
            apiResponse.setMessage("To'lov qabul qilindi");
//            new Thread(() -> {
//                if (request.isFromTelegram() || request.getSeatId() != null) {
//                    if (!request.isFromTelegram()) {
//                        request.setUserId(user.getId());
//                        request.setId(user.getId());
//                    }
//                    ApiResponse response = seatService.seatOrder(request, request.getSeatId(), currentUser);
//                    if (response.isSuccess()) {
//                        apiResponse.setSuccess(true);
//                        apiResponse.setMessage(response.getMessage());
//                    }
//                } else {
//                    List<Seat> partlyPaidSeats2 = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.PARTLY_PAID, true);
//                    List<Seat> partlyPaidSeats = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.PARTLY_PAID, null);
//                    if (partlyPaidSeats2.size() > 0) {
//                        changeSeatStatus(partlyPaidSeats2);
//                    }
//                    if (partlyPaidSeats.size() > 0) {
//                        changeSeatStatus(partlyPaidSeats);
//                    }
//                    List<Seat> bookedSeats2 = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.BOOKED, true);
//                    List<Seat> bookedSeats = seatRepository.findAllByUserAndPlaceStatusAndParticipatedOrderByEventStartTime(user, PlaceStatus.BOOKED, null);
//                    if (bookedSeats2.size() > 0) {
//                        changeSeatStatus(bookedSeats2);
//                    }
//                    if (bookedSeats.size() > 0) {
//                        changeSeatStatus(bookedSeats);
//                    }
//                    if (request.getSeatId() == null && !apiResponse.isSuccess()) {
//                        apiResponse.setSuccess(true);
//                    }
//                }
//
//                mFactorBot.sendNotification(payment);
//
//                apiResponse.setMessage("To'lov qabul qilindi");
//            }).start();
            return apiResponse;
        }
        return new ApiResponse("Bunday telefon raqamli foydalanuvchi mavjud", false);
    }

    public PaymentPageableResponse getPayments(int page, int size) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
        Page<Payment> paymentPage = paymentRepository.findAll(pageable);
        return new PaymentPageableResponse(paymentRepository.findAll(pageable).getContent().stream().map(payment ->
                new ResPayment(
                        payment.getId(),
                        ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/").
                                path(payment.getUser().getPhoto() != null ? payment.getUser().getPhoto().getId().toString() : "").toUriString(),
                        payment.getUser().getFirstName(),
                        payment.getUser().getLastName(),
                        userService.getFullName(payment.getUser()),
                        payment.getUser().getPhoneNumber(),
                        payment.getUser().getTelegramUsername() != null ?
                                payment.getUser().getTelegramUsername() : "",
                        payment.getPaySum(),
                        payment.getPayType().getNameUz(),
                        payment.getPayType().getNameRu(),
                        payment.getPayDate()
                )).collect(Collectors.toList()), paymentPage.getTotalPages(),
                paymentPage.getPageable().getPageNumber(),
                paymentPage.getPageable().getPageSize()
        );


    }

    public void changeSeatStatus(List<Seat> seats) {
        double sale = userRepository.getSale(seats.get(0).getUser().getId()) / 100;
        for (Seat seat : seats) {
            double balance = paymentRepository.sumOfLeftover(seat.getUser().getId());
            double price = seat.getPrice();
            if (balance > 0) {
                double paid = seat.getSeatPayments().stream().mapToDouble(SeatPayment::getAmount).sum();
                if (balance >= ((price - (price * sale)) - paid)) {
                    seat.setSeatPayments(seatService.getSeatPayments(seat.getUser(), seat, seat.getSeatPayments()));
                    seat.setPlaceStatus(PlaceStatus.SOLD);
                    seat.setTargetTime(new Timestamp(new Date().getTime()));
                    seatRepository.save(seat);
                } else {
                    if (balance > 0) {
                        seat.setSeatPayments(seatService.getSeatPayments(seat.getUser(), seat, seat.getSeatPayments()));
                        seat.setPlaceStatus(PlaceStatus.PARTLY_PAID);
                        seat.setTargetTime(new Timestamp(new Date().getTime()));
                        seatRepository.save(seat);
                    }
                }
            } else {
                break;
            }
        }
    }

    private double exchangeUsd(double sum) {
        return (usdRepository.maxUsd() * sum);
    }
}
