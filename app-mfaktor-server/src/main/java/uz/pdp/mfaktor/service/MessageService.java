package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.mfaktor.bot.BotConstants;
import uz.pdp.mfaktor.bot2.BotUser;
import uz.pdp.mfaktor.bot2.BotUserRepository;
import uz.pdp.mfaktor.bot2.RealMFaktorBot;
import uz.pdp.mfaktor.entity.*;
import uz.pdp.mfaktor.entity.enums.EventStatus;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqMessage;
import uz.pdp.mfaktor.repository.AttachmentContentRepository;
import uz.pdp.mfaktor.repository.EventRepository;
import uz.pdp.mfaktor.repository.MessageRepository;
import uz.pdp.mfaktor.repository.UserRepository;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
public class MessageService {
    @Autowired
    EventRepository eventRepository;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    BotUserRepository botUserRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    RealMFaktorBot mFactorBot;
    public ApiResponse addMessage(ReqMessage reqMessage) {
        try {
            Message message = new Message();
            message.setText(reqMessage.getText());
            if (reqMessage.getEventId() != null) {
                message.setEvent(eventRepository.findById(reqMessage.getEventId()).orElseThrow(() -> new ResourceNotFoundException("getEvent", "event", reqMessage.getEventId())));
            }
            messageRepository.save(message);
            //SANJAR BOTGA JUNAT
            List<User> users = null;
            if (reqMessage.getEventId() == null) {
                users = userRepository.findAll().stream().filter(user -> user.getTelegramId() != null).collect(Collectors.toList());
                //TODO SANJAR USHBU USERLARGA reqMessage.getText() NI JUNATING ILTIMOS
            } else {
                Event event = eventRepository.findById(reqMessage.getEventId()).orElseThrow(() ->
                        new ResourceNotFoundException("getEvent", "event", reqMessage.getEventId()));
                List<Seat> seatList = event.getSeats().stream().
                        filter(seat -> seat.getUser() != null).collect(Collectors.toList());
                ///TODO SANJAR BOTGA XABAR JUNATING JOYI VA AGAR QATNASHMASA ADMINGA (BOTDAGI ADMINGA) BOG'LANISHI HAQIDA
                seatList.forEach(seat -> {
                    try {
                        mFactorBot.execute(sendUserByEvent(event,seat));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                });
            }

            //usersda faqat telegram id si borlar.
            return new ApiResponse("Barchaga xabar yuborildi", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Yuborishda xatolik", false);
        }
    }

    public List<ReqMessage> getMessages() {
        return messageRepository.findAll(Sort.by(Sort.Direction.DESC, "createdAt")).stream().map(message -> new ReqMessage(message.getText(), message.getEvent() == null ? null : message.getEvent().getId(), message.getEvent() == null ? null : message.getEvent().getTitle())).collect(Collectors.toList());
    }

    public SendPhoto sendUserByEvent(Event event, Seat seat) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(seat.getUser().getTelegramId());
        try {
            SendPhoto sendPhoto = new SendPhoto();
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rows = new ArrayList<>();
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            List<InlineKeyboardButton> row2 = new ArrayList<>();

            InlineKeyboardButton button1 = new InlineKeyboardButton();
            InlineKeyboardButton button2 = new InlineKeyboardButton();

            if(seat.getUser().getChatId()!=null) {
                sendPhoto.setChatId(seat.getUser().getChatId())
                        .setParseMode(ParseMode.MARKDOWN);
            }else{
                sendPhoto.setChatId(String.valueOf(seat.getUser().getTelegramId()))
                        .setParseMode(ParseMode.MARKDOWN);
            }
            List<Event> allTopEvents = eventRepository.findAllByIsTopAndStatusOrderByStartTime(true, EventStatus.OPEN);
            StringBuilder stringBuilder1;
            for (int i = 0; i < allTopEvents.size(); i++) {
                List<uz.pdp.mfaktor.entity.User> speakers = allTopEvents.get(i).getSpeakers();
                stringBuilder1 = new StringBuilder();
                for (int j = 0; j < speakers.size(); j++) {
                    stringBuilder1.append(speakers.get(j).getFirstName()).append(" ").append(speakers.get(j).getLastName()).append("\n");
                }
            }
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                button1.setText(BotConstants.CLOSE_BRON_KR);
                button2.setText(BotConstants.HOME_KR);
            } else {
                button1.setText(BotConstants.CLOSE_BRON_KR);
                button2.setText(BotConstants.HOME_RU);
            }
            button1.setCallbackData("CloseBron#"+seat.getId());
            button2.setCallbackData("MyProfile");
            StringBuilder stringBuilder = new StringBuilder();
            event.getSpeakers().forEach(user1 -> {
                stringBuilder.append(user1.getFirstName()).append(" ").append(user1.getLastName()).append("\n");
            });
            Attachment photo = event.getPhoto();
            Optional<AttachmentContent> attachmentContent = attachmentContentRepository.findByAttachment(photo);
            sendPhoto.setPhoto(event.getPhoto().getName(), new ByteArrayInputStream(attachmentContent.get().getContent()));
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                sendPhoto.setCaption("Ассалому алайкум ҳурматли *" + seat.getUser().getFirstName() + " " + seat.getUser().getLastName() + "*.\n" +
                        "\n" +
                        "Сизни *" + event.getRegisterTime().toString().substring(0, event.getRegisterTime().toString().length() - 5) + "* да бўлиб ўтадиган тадбирда кутиб қоламиз.\n" +
                        "\n" +
                        "Мавзу: *" + event.getTitle() + "*\n" +
                        "Спикер: *" + stringBuilder.toString() + "*\n" +
                        "Банд қилинган жой: *"+seat.getName()+"*\n" +
                        "Манзил: *Тошкент ш., Ёшлар Ижод Саройи*\n" +
                        "Рўйхатга олиш вакти: *13:00*\n" +
                        "Тадбир бошланиш вакти: *14:00*\n" +
                        "Локация: *https://goo.gl/maps/aW96k3E5qDdWu9297*\n\n" +
                        "Мабодо бирор сабабга кўра бугунги тадбирга кела олмaсангиз, БАНД қилинган жойингизни бекор қилишингизни тавсия этамиз. Раҳмат!");
            } else {
                sendPhoto.setCaption("Здравствуйте, *" + seat.getUser().getFirstName() + " " + seat.getUser().getLastName() + "*.\n" +
                        "\n" +
                        "Ждем вас на мероприятии, которое состоится *" + event.getRegisterTime().toString().substring(0, event.getRegisterTime().toString().length() - 5) + "*.\n" +
                        "\n" +
                        "Тема: *" + event.getTitle() + "*\n" +
                        "Cпикер: *" + stringBuilder.toString() + "*\n" +
                        "Занято место: *"+seat.getName()+"*\n" +
                        "Адрес: *Дворец творчества молодежи*\n" +
                        "Время регистрации:*13:00*\n" +
                        "Время начала" +
                        " мероприятия: *14:00*\n" +
                        "Локация: *https://goo.gl/maps/aW96k3E5qDdWu9297*\n\n" +
                        "Если вы по какой-либо причине не можете присутствовать на сегодняшнем мероприятии, мы рекомендуем отменить бронирование. Спасибо!");
            }
            row1.add(button1);
            row2.add(button2);
            rows.add(row1);
            rows.add(row2);
            inlineKeyboardMarkup.setKeyboard(rows);
            sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
            return sendPhoto;
        } catch (Exception e) {
            return null;
        }
    }
}
