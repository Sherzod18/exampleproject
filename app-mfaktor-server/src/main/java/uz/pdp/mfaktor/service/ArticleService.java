package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import uz.pdp.mfaktor.entity.Article;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqArticle;
import uz.pdp.mfaktor.repository.ArticleRepository;
import uz.pdp.mfaktor.repository.AttachmentRepository;
import uz.pdp.mfaktor.repository.CategoryRepository;

import java.awt.print.Pageable;
import java.util.List;

@Service
public class ArticleService {
    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    MessageSource messageSource;

    public ApiResponse createArticle(ReqArticle reqArticle) {
        Article article = new Article();
        try {
            article = articleRepository.findById(reqArticle.getId()).orElseGet(Article::new);
        } catch (Exception ignored) {
        }
        article.setPhoto(attachmentRepository.findById(reqArticle.getPhotoId()).orElseThrow(() -> new ResourceNotFoundException("getAttachment", "attachmentId", reqArticle.getPhotoId())));
        article.setCategory(categoryRepository.findById(Integer.valueOf(reqArticle.getCategoryId())).orElseThrow(() -> new ResourceNotFoundException("getCategory", "categoryId", reqArticle.getCategoryId())));
        article.setTitle(reqArticle.getTitle());
        article.setAuthor(reqArticle.getAuthor());
        article.setDescription(reqArticle.getDescription());
        articleRepository.save(article);
        return new ApiResponse(messageSource.getMessage("article.saved", null, LocaleContextHolder.getLocale()), true);
    }

    public List<Article> getArticle(int size, int page) {
        Page<Article> articles = articleRepository.findAll(PageRequest.of(page, size));
        return articles.getContent();
    }
}
