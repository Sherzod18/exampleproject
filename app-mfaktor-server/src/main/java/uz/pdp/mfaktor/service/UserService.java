package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.pdp.mfaktor.entity.Payment;
import uz.pdp.mfaktor.entity.Role;
import uz.pdp.mfaktor.entity.Seat;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.entity.enums.RoleName;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.*;
import uz.pdp.mfaktor.repository.*;
import uz.pdp.mfaktor.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    AwareRepository awareRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    PaymentRepository paymentRepository;
    @Autowired
    SeatRepository seatRepository;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    TicketRepository ticketRepository;
    @Autowired
    PositionRepository positionRepository;
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    ActivityRepository activityRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    MessageSource messageSource;
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    EventService eventService;

    public ApiResponse addUser(ReqUser reqUser) {
        if (!userRepository.findByPhoneNumber(reqUser.getPhoneNumber()).isPresent()) {
            User user = new User(
                    reqUser.getPhoneNumber(),
                    null,
                    reqUser.getFirstName(),
                    reqUser.getLastName()
            );
            user.setCompany(reqUser.getCompany());
            user.setPosition(reqUser.getPositionId() != null ? positionRepository.findById(reqUser.getPositionId()).orElseThrow(() -> new ResourceNotFoundException("getPosition", "positionId", reqUser.getPositionId())) : null);
            user.setAware(reqUser.getAwareId() != null ? awareRepository.findById(reqUser.getAwareId()).orElseThrow(() -> new ResourceNotFoundException("getAware", "awareId", reqUser.getAwareId())) : null);
            user.setAbout(reqUser.getAbout());
            user.setBirthDate(reqUser.getBirthDate());
            user.setActivity(reqUser.getActivityId() != null ? activityRepository.findById(reqUser.getActivityId()).orElseThrow(() -> new ResourceNotFoundException("getActivity", "activityId", reqUser.getActivityId())) : null);
            user.setRegion(reqUser.getRegionId() != null ? regionRepository.findById(reqUser.getRegionId()).orElseThrow(() -> new ResourceNotFoundException("getRegion", "regionId", reqUser.getRegionId())) : null);
            user.setTelegramUsername(reqUser.getTelegramUsername());
            user.setTelegramId(reqUser.getTelegramId());
            user.setRoles(roleRepository.findAllByName(RoleName.ROLE_USER));
            user.setPhoto(reqUser.getPhotoId() != null ? attachmentRepository.findById(reqUser.getPhotoId()).orElseThrow(() -> new ResourceNotFoundException("getPhoto", "photoId", reqUser.getPhotoId())) : null);
            user.setSale(reqUser.getSale());
            userRepository.save(user);
            return new ApiResponse(messageSource.getMessage("user.created", null, LocaleContextHolder.getLocale()), true);
        } else {
            return new ApiResponse(messageSource.getMessage("phone.number.exist", null, LocaleContextHolder.getLocale()), false);
        }
    }

    public ApiResponse editUser(ReqUser reqUser, UUID id) {
        ApiResponse response = new ApiResponse();
        response.setSuccess(true);
        if (userRepository.findById(id).isPresent()) {
            User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("user/edit", "id", id));
            user.setFirstName(reqUser.getFirstName());
            user.setLastName(reqUser.getLastName());
            user.setBirthDate(reqUser.getBirthDate());
            user.setRegion(reqUser.getRegionId() != null ? regionRepository.findById(reqUser.getRegionId()).orElseThrow(() -> new ResourceNotFoundException("getRegion", "regionId", reqUser.getRegionId())) : null);
            user.setPosition(reqUser.getPositionId() != null ? positionRepository.findById(reqUser.getPositionId()).orElseThrow(() -> new ResourceNotFoundException("getPosition", "positionId", reqUser.getPositionId())) : null);
            user.setActivity(reqUser.getActivityId() != null ? activityRepository.findById(reqUser.getActivityId()).orElseThrow(() -> new ResourceNotFoundException("getActivity", "activityId", reqUser.getActivityId())) : null);
            user.setCompany(reqUser.getCompany());
            user.setTelegramUsername(reqUser.getTelegramUsername());
            user.setAbout(reqUser.getAbout());
            user.setTelegramUsername(reqUser.getTelegramUsername());
            user.setAware(reqUser.getAwareId() != null ? awareRepository.findById(reqUser.getAwareId()).orElseThrow(() -> new ResourceNotFoundException("user/edit", "id", id)) : null);
            user.setSale(reqUser.getSale());
            if (reqUser.getPhotoId() != null) {
                user.setPhoto(attachmentRepository.findById(reqUser.getPhotoId()).orElseThrow(() -> new ResourceNotFoundException("user/edit", "id", id)));
            }
            user.setPosition(reqUser.getPositionId() != null ? positionRepository.findById(reqUser.getPositionId()).orElseThrow(() -> new ResourceNotFoundException("getPosition", "positionId", reqUser.getPositionId())) : null);
            if (!user.getPhoneNumber().equals(reqUser.getPhoneNumber())) {
                if (!userRepository.findByPhoneNumber(reqUser.getPhoneNumber()).isPresent()) {
                    user.setPhoneNumber(reqUser.getPhoneNumber());
                } else {
                    response.setSuccess(false);
                    response.setMessage("Phone number is already exist");
                }
            }
            if (response.isSuccess()) {
                response.setMessage(messageSource.getMessage("succesfully.edited", null, LocaleContextHolder.getLocale()));
                userRepository.save(user);
            }
            return response;
        }
        return new ApiResponse(messageSource.getMessage("user.notfound", null, LocaleContextHolder.getLocale()), false);
    }

    public HttpEntity editPassword(ReqPassword reqPassword, User user) {
        if (passwordEncoder.matches(reqPassword.getOldPassword(), user.getPassword())) {
            user.setPassword(passwordEncoder.encode(reqPassword.getNewPassword()));
            userRepository.save(user);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse(messageSource.getMessage("password.edited", null, LocaleContextHolder.getLocale()), true));
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(messageSource.getMessage("password.incorrect", null, LocaleContextHolder.getLocale()), false));
        }
    }

    public ResClient getUsersByCountVisit(int page, int size) {
        page = page * size;
        List<Object[]> users = userRepository.getUsersByCountVisit(page, size);
        List<ResUser> userList = new ArrayList<>();
        for (Object[] user : users) {
            ResUser resUser = new ResUser();
            if (user[1] != null) {
                resUser.setPhotoUrl(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/").
                        path(user[1].toString()).toUriString());
            }
            resUser.setFirstName(user[2] == null ? "" : user[2].toString());
            resUser.setLastName(user[3] == null ? "" : user[3].toString());
            resUser.setFullName(user[2] == null ? "" : user[2].toString() + " " + user[3] == null ? "" : user[3].toString());
            resUser.setPhoneNumber(user[4] == null ? "" : user[4].toString());
            resUser.setBalance(user[5] == null ? 0 : Double.valueOf(user[5].toString()));
            resUser.setCompany(user[6] == null ? "" : user[6].toString());
            resUser.setAwareUz(user[7] == null ? "Mavjud emas" : user[7].toString());
            resUser.setCountVisit(user[8] == null ? 0 : Integer.parseInt(user[8].toString()));
            userList.add(resUser);
        }
        int countUsers = userRepository.countUsers();
        return new ResClient(
                userList, (countUsers / size) + 1, countUsers
        );

    }

    public ResClient getUsers(int page, int size, String search) {
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "lastName");
        Page<User> u = null;
        if (search.isEmpty()) {
            Role role = roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(() -> new ResourceNotFoundException("getRole", "roleName", RoleName.ROLE_USER));
            u = userRepository.findAllByRolesIn(role, pageable);
        } else {
            u = userRepository.findAllByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrPhoneNumberContainingOrTelegramUsernameContainingIgnoreCaseOrCompanyContainingIgnoreCase(search, search, search, search, search, pageable);
        }
        int countUsers = userRepository.countUsers();
        List<User> users = u.getContent().stream().filter(user -> user.getRoles().size() < 2).collect(Collectors.toList());
        return new ResClient(users.stream().map(this::getUser)
                .collect(Collectors.toList()), u.getTotalPages(), countUsers);
    }

    public ResUser getUser(User user) {
        return new ResUser(user.getId(),
                user.getPhoto() != null ? ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/").
                        path(user.getPhoto().getId().toString()).toUriString() : "",
                getFullName(user),
                user.getFirstName(),
                user.getLastName(),
                user.getPhoneNumber(),
                user.getBirthDate(),
                user.getCompany(),
                user.getPosition() != null ? user.getPosition().getNameUz() : "",
                user.getPosition() != null ? user.getPosition().getNameRu() : "",
                user.getPosition() != null ? user.getPosition().getId() : 0,
                user.getAware() != null ? user.getAware().getNameUz() : "",
                user.getAware() != null ? user.getAware().getNameRu() : "",
                user.getAware() != null ? user.getAware().getId() : 0,
                seatRepository.findAllByUserAndParticipated(user, true).size(),
                user.getRegion() != null ? user.getRegion().getNameUz() : "",
                user.getRegion() != null ? user.getRegion().getNameRu() : "",
                user.getRegion() != null ? user.getRegion().getId() : 0,
                paymentRepository.sumOfLeftover(user.getId()),
                user.getSale()
        );
    }

    public String getFullName(User user) {
        return user.getFirstName() +
                " " + user.getLastName();
    }

    public List<ResUser> search(ReqSearch word) {
        return userRepository.findFirst15ByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrPhoneNumberContaining(word.getWord(), word.getWord(), word.getWord())
                .stream().filter(user -> user.getRoles().size() < 2).map(this::getUser).collect(Collectors.toList());


    }

    public ResUserIncomeAndOutcome getUserIncomesAndOutCome(UUID userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("getUser", "userId", userId));
        List<Payment> payments = paymentRepository.findAllByUserOrderByCreatedAt(user);
        List<ResUserIncome> incomes = payments.stream().map(this::getUserIncome).collect(Collectors.toList());
        List<Seat> userSeat = seatRepository.findAllByUserAndParticipatedOrderByCreatedAt(user, true);
        List<ResUserOutcome> outcomes = userSeat.stream().map(this::getUserOutcome).collect(Collectors.toList());
        return new ResUserIncomeAndOutcome(incomes, outcomes);
    }

    private ResUserIncome getUserIncome(Payment payment) {
        return new ResUserIncome(
                payment.getPayDate(),
                payment.getPaySum(),
                payment.getPayType().getNameUz(),
                payment.getPayType().getNameRu(),
                payment.getDetails()
        );
    }

    private ResUserOutcome getUserOutcome(Seat seat) {
        return new ResUserOutcome(
                seat.getEvent().getStartTime(),
                seat.getEvent().getTitle(),
                seat.getName(),
                eventService.getSumBySeatPayments(seat),
                (seat.getPrice() - eventService.getSumBySeatPayments(seat)),
                seat.getPrice()
        );
    }

}

