package uz.pdp.mfaktor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.pdp.mfaktor.entity.Role;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.entity.enums.RoleName;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqSpeaker;
import uz.pdp.mfaktor.payload.ResSpeaker;
import uz.pdp.mfaktor.repository.*;
import uz.pdp.mfaktor.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SpeakerService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    UserService userService;
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    MessageSource messageSource;

    @Autowired
    PositionRepository positionRepository;


    public ApiResponse addSpeaker(ReqSpeaker reqSpeaker) {
        if (!userRepository.findByPhoneNumber(reqSpeaker.getPhoneNumber()).isPresent()) {
            User speaker = createSpeaker(reqSpeaker);
            userRepository.save(speaker);
            return new ApiResponse(messageSource.getMessage("speaker.created", null, LocaleContextHolder.getLocale()), true);
        }
        return new ApiResponse(messageSource.getMessage("phone.number.exist", null, LocaleContextHolder.getLocale()), false);
    }

    public List<ResSpeaker> getSpeakers() {
        Role role = roleRepository.findByName(RoleName.ROLE_SPEAKER).orElseThrow(() -> new ResourceNotFoundException("Role", "name", RoleName.ROLE_SPEAKER));
        return userRepository.findAllByRolesIn(role).stream().map(this::getSpeaker).collect(Collectors.toList());
    }

    public List<ResSpeaker> getSpeakersPagination(Integer page, Integer size) {

        Role role = roleRepository.findByName(RoleName.ROLE_SPEAKER).orElseThrow(() -> new ResourceNotFoundException("Role", "name", RoleName.ROLE_SPEAKER));
        CommonUtils.validatePageNumberAndSize(page, size);
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "createdAt");
//        return null;
                return userRepository.findAllByRolesIn(role,pageable).stream().map(this::getSpeaker).collect(Collectors.toList());
    }

    public ResSpeaker getSpeaker(User user) {
        return new ResSpeaker(
                user.getId(),
                ServletUriComponentsBuilder.fromCurrentContextPath().
                        path("/api/file/").path(user.getPhoto() == null ? "" : user.getPhoto().getId().toString()).toUriString(),
                user.getLogos().stream().map(attachment ->
                        ServletUriComponentsBuilder.fromCurrentContextPath().
                                path("/api/file/").path(attachment != null ? String.valueOf(attachment.getId()) : "").toUriString()
                ).collect(Collectors.toList()),
                userService.getFullName(user),
                user.getFirstName(),
                user.getLastName(),
                user.getCompany(),
                user.getPosition() != null ? user.getPosition().getNameUz() : "",
                user.getPosition() != null ? user.getPosition().getNameRu() : "",
                user.getPhoneNumber(),
                user.getPosition().getId(),
                eventRepository.findAllBySpeakers(user).size(),
                user.getAbout()
        );
    }

    public ApiResponse editSpeaker(ReqSpeaker reqSpeaker) {
        ApiResponse response = new ApiResponse();
        if (userRepository.findById(reqSpeaker.getId()).isPresent()) {
            User editedSpeaker = createSpeaker(reqSpeaker);
            userRepository.save(editedSpeaker);
            response.setSuccess(true);
            response.setMessage(messageSource.getMessage("editedSpeaker", null, LocaleContextHolder.getLocale()));
        } else {
            response.setSuccess(false);
            response.setMessage(messageSource.getMessage("user.notfound", null, LocaleContextHolder.getLocale()));
        }
        return response;
    }

    public User createSpeaker(ReqSpeaker reqSpeaker) {
        User user = reqSpeaker.getId() != null ?
                userRepository.findById(reqSpeaker.getId()).orElseThrow(() -> new ResourceNotFoundException("getSpeaker", "id", reqSpeaker.getId())) :
                new User();
        user.setPhoneNumber(reqSpeaker.getPhoneNumber());
        user.setPassword(null);
        user.setFirstName(reqSpeaker.getFirstName());
        user.setLastName(reqSpeaker.getLastName());
        user.setRoles(roleRepository.findAllByName(RoleName.ROLE_SPEAKER));
        user.setCompany(reqSpeaker.getCompany());
        user.setAbout(reqSpeaker.getAbout());
        user.setPosition(positionRepository.findById(reqSpeaker.getPositionId()).orElseThrow(() -> new ResourceNotFoundException("getPosition", "positionId", reqSpeaker.getPositionId())));
        user.setLogos(reqSpeaker.getLogoIds() != null ? attachmentRepository.findAllById(reqSpeaker.getLogoIds()) : null);
        user.setPhoto(attachmentRepository.findById(reqSpeaker.getPhotoId()).orElseThrow(() -> new ResourceNotFoundException("getPhoto", "photoId", reqSpeaker.getPhotoId())));
        return user;
    }
}
