package uz.pdp.mfaktor.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints =
@UniqueConstraint(columnNames = {"event_id", "speaker_id", "created_by_id"})
)
public class Review extends AbsEntity {
    private String text;
    private Integer starCount;
    private boolean isPublished = false;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    private Event event;

    @ManyToOne(fetch = FetchType.LAZY)
    private User speaker;

}
