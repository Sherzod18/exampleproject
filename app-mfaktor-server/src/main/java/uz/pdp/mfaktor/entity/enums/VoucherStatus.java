package uz.pdp.mfaktor.entity.enums;

public enum VoucherStatus {
    ACTIVE,
    USED,
    EXPIRED,

}
