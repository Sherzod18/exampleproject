package uz.pdp.mfaktor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CashBox extends AbsEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    private CashType fromCash;
    @ManyToOne(fetch = FetchType.LAZY)
    private CashType toCash;
    private double fromSum;
    private double toSum;
    private boolean fromUsd;
    private boolean toUsd;
    private Timestamp time;
    private String details;
}
