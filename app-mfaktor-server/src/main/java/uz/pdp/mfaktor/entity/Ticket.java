package uz.pdp.mfaktor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Ticket extends AbsEntity {
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User user;

    private double price;
    private boolean active = true;
    private Timestamp deadline;

    @ManyToOne(fetch = FetchType.LAZY)
    private EventType eventType;

    @OneToMany(fetch = FetchType.LAZY)
    private List<SeatPayment> seatPayments;

    public Ticket(User user, double price, Timestamp deadline) {
        this.user = user;
        this.price = price;
        this.deadline = deadline;
    }

}
