package uz.pdp.mfaktor.entity.enums;

public enum EventStatus {
    DRAFT,
    OPEN,
    REGISTRATION_COMPLETED,
    ARCHIVE,
    CANCELED
}
