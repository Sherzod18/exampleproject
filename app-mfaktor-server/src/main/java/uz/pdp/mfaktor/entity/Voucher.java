package uz.pdp.mfaktor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.enums.VoucherStatus;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Voucher extends AbsEntity {

    private String voucherId;
    @ManyToOne(fetch = FetchType.LAZY)
    private User owner;

    private double price;

    private double residue;

    private Timestamp deadline;
    @Enumerated(EnumType.STRING)
    private VoucherStatus status;
}
