package uz.pdp.mfaktor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users")
@Where(clause = "enabled=true")
public class User extends AbsEntity implements UserDetails {

    @Column(unique = true, nullable = false)
    private String phoneNumber;

    private String password;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    private Integer telegramId;
    private Long chatId;
    private String telegramUsername;

    @OneToOne(fetch = FetchType.LAZY)
    private Attachment photo;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Attachment> logos;

    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;

    @ManyToOne(fetch = FetchType.LAZY)
    private Activity activity;

    private String company;

    @ManyToOne(fetch = FetchType.LAZY)
    private Position position;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
    List<Voucher> vouchers;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "speaker")
    private List<Review> reviews;
    private double avgStar;

    private String about;

    private Date birthDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Aware aware;

    private Double sale=0D;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles;

    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;


    public User(String phoneNumber, String password, String firstName, String lastName) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User(String phoneNumber, String password, String firstName, String lastName, List<Role> roles) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.roles = roles;
    }

    public User(String phoneNumber, String password, String firstName,
                String lastName, Integer telegramId,
                Attachment photo, String company, Position position,
                String about, Aware aware, List<Role> roles) {
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.telegramId = telegramId;
        this.photo = photo;
        this.company = company;
        this.position = position;
        this.about = about;
        this.aware = aware;
        this.roles = roles;
    }


    @Override
    public String getUsername() {
        return this.phoneNumber;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}
