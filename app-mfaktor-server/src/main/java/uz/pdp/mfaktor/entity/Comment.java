package uz.pdp.mfaktor.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Comment extends AbsEntity {
    @Column(columnDefinition = "text")
    private String text;
    private boolean isPublished = false;
    @ManyToOne(fetch = FetchType.LAZY)
    private Article article;


}
