package uz.pdp.mfaktor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.template.AbsEntity;
import uz.pdp.mfaktor.entity.template.AbsNameEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Tariff extends AbsNameEntity {
    private double price;
    private int ticketCount;
    @ManyToOne(fetch = FetchType.LAZY)
    private EventType eventType;
}
