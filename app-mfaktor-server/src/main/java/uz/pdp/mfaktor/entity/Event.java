package uz.pdp.mfaktor.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.enums.EventStatus;
import uz.pdp.mfaktor.entity.enums.PriceType;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Event extends AbsEntity {
    private String serialNumber;
    private String title;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<User> speakers;

    private String more;
    private Timestamp startTime;
    private Timestamp registerTime;

    @OneToOne(fetch = FetchType.LAZY)
    private Attachment photo;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private EventType eventType;

    @Enumerated(value = EnumType.STRING)
    private PriceType priceType;

    @ManyToOne(fetch = FetchType.LAZY)
    private Language language;

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.REMOVE)
    private List<Seat> seats;

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "event", cascade = CascadeType.REMOVE)
    private List<Review> reviews;

    private Double avgStar;

    @Enumerated(value = EnumType.STRING)
    private EventStatus status;

    private Timestamp openTime;

    private Boolean isTop;

    public Event(String serialNumber, String title, List<User> speakers,
                 String more, Timestamp startTime, Timestamp registerTime, Attachment photo,
                 EventType eventType,
                 PriceType priceType, Language language, EventStatus status, Timestamp openTime, Boolean isTop) {
        this.serialNumber = serialNumber;
        this.title = title;
        this.speakers = speakers;
        this.more = more;
        this.startTime = startTime;
        this.registerTime = registerTime;
        this.photo = photo;
        this.eventType = eventType;
        this.priceType = priceType;
        this.language = language;
        this.status = status;
        this.openTime = openTime;
        this.isTop = isTop;
    }
}
