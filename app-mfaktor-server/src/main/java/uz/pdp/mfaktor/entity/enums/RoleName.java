package uz.pdp.mfaktor.entity.enums;

public enum RoleName {
    ROLE_USER,
    ROLE_DIRECTOR,
    ROLE_ADMIN,
    ROLE_MANAGER,
    ROLE_CASHIER,
    ROLE_SPEAKER
}
