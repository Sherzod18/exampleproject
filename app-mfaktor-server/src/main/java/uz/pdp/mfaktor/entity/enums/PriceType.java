package uz.pdp.mfaktor.entity.enums;

public enum PriceType {
    FIXED,
    BY_SEAT
}
