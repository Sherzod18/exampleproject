package uz.pdp.mfaktor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Where;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.*;
import java.text.Normalizer;
import java.util.regex.Pattern;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@Where(clause = "deleted=false")
public class Article extends AbsEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Attachment photo;
    private String title;
    private String author;

    private boolean active = true;

    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;

    @Column(columnDefinition = "text")
    private String description;

    private int viewsCount = 0;

    @Column(unique = true)
    private String url;
    private boolean deleted = false;

    @PrePersist
    @PreUpdate
    public void setPostUrl() {
        this.url = toSlug(this.title);
    }

    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");
    private static final Pattern EDGESDHASHES = Pattern.compile("(^-|-$)");

    private String toSlug(String input) {
        String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        slug = EDGESDHASHES.matcher(slug).replaceAll("");
        return slug.toLowerCase();
    }

    public Article(Attachment photo, String title, String author, Category category, String description) {
        this.photo = photo;
        this.title = title;
        this.author = author;
        this.category = category;
        this.description = description;
    }
}
