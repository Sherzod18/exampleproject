package uz.pdp.mfaktor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Message extends AbsEntity {
    @Column(columnDefinition = "text", nullable = false)
    private String text;
    @ManyToOne(fetch = FetchType.LAZY)
    private Event event;
}
