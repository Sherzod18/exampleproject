package uz.pdp.mfaktor.entity.enums;

public enum PlaceStatus {
    EMPTY,
    BOOKED,
    PARTLY_PAID,
    SOLD,
    RESERVED,
    VIP
}
