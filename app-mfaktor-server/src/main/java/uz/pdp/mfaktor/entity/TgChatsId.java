package uz.pdp.mfaktor.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.template.AbsNameEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@NoArgsConstructor
public class TgChatsId extends AbsNameEntity {
    private Long chatId;
    public TgChatsId(long chatId){
        this.chatId = chatId;
    }
}
