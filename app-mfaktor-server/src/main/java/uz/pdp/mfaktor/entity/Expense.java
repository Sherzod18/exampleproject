package uz.pdp.mfaktor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Expense extends AbsEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    private ExpenseType expenseType;
    private double sum;
    private Timestamp date;
    private String comment;
    @ManyToOne(fetch = FetchType.LAZY)
    private CashType cashType;
    private UUID eventId;
}



