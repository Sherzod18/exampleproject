package uz.pdp.mfaktor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class WaitingExpense extends AbsEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    private ExpenseType expenseType;
    private double sum;
    private Double leftover;
    private Timestamp deadline;
    private String comment;
    private boolean paid;
}
