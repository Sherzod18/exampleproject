package uz.pdp.mfaktor.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.mfaktor.entity.template.AbsNameEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Activity extends AbsNameEntity {
}
