package uz.pdp.mfaktor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import uz.pdp.mfaktor.config.InitConfig;

@SpringBootApplication
@EnableScheduling
public class AppMfaktorServerApplication {


    public static void main(String[] args) {
        if (InitConfig.isStart()){
            SpringApplication.run(AppMfaktorServerApplication.class, args);
        }else {
            System.err.println("Boshlang'ich ma'lumotlar kiritilmadi.");
        }
    }
}
