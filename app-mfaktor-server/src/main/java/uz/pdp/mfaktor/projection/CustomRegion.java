package uz.pdp.mfaktor.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.Position;
import uz.pdp.mfaktor.entity.Region;

@Projection(name = "customRegion", types = {Region.class})
public interface CustomRegion {
    Integer getId();
    String getNameUz();
    String getNameRu();

}
