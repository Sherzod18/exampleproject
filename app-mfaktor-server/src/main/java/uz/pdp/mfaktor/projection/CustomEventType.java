package uz.pdp.mfaktor.projection;

import org.springframework.beans.factory.Aware;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.EventType;

import java.util.UUID;

@Projection(name = "customEventType", types = {EventType.class})
public interface CustomEventType {
    Integer getId();
    String getNameUz();
    String getNameRu();
    String getDescription();

}
