package uz.pdp.mfaktor.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.PayType;

import java.util.UUID;

@Projection(name = "customPayType", types = {PayType.class})
public interface CustomPayType {

    Integer getId();
    String getNameUz();
    String getNameRu();
    String getDescription();
    String getColor();

}
