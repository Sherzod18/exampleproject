package uz.pdp.mfaktor.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.Activity;
import uz.pdp.mfaktor.entity.EventType;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.UUID;

@Projection(name = "customEventType", types = {Activity.class})
public interface CustomActivity {
    Integer getId();
    String getNameUz();
    String getNameRu();

}
