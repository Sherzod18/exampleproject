package uz.pdp.mfaktor.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.Language;
import uz.pdp.mfaktor.entity.PayType;

@Projection(name = "customLanguage", types = {Language.class})
public interface CustomLanguage {
    Integer getId();
    String getNameUz();
    String getNameRu();
}
