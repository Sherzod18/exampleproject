package uz.pdp.mfaktor.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.ExpenseType;

import java.util.UUID;

@Projection(name = "customExpenseType",  types = {ExpenseType.class})
public interface CustomExpenseType {

    Integer getId();

    String getNameUz();

    String getNameRu();

    String getDescription();

}
