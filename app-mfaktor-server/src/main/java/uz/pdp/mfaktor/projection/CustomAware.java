package uz.pdp.mfaktor.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.Aware;

import java.sql.Timestamp;
import java.util.UUID;

@Projection(name = "customAware", types = Aware.class)
public interface CustomAware {

    Integer getId();
    String getNameUz();
    String getNameRu();
}
