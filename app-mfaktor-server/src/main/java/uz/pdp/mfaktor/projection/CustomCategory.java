package uz.pdp.mfaktor.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.Category;

@Projection(name = "customCategory", types = {Category.class})
public interface CustomCategory {
    Integer getId();
    String getNameUz();
    String getNameRu();

}
