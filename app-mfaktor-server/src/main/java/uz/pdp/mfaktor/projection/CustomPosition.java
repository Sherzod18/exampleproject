package uz.pdp.mfaktor.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.Activity;
import uz.pdp.mfaktor.entity.Position;

@Projection(name = "customPosition", types = {Position.class})
public interface CustomPosition {
    Integer getId();
    String getNameUz();
    String getNameRu();

}
