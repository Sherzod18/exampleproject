package uz.pdp.mfaktor.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.VideoType;

import java.util.UUID;

@Projection(name = "customVideoType", types = {VideoType.class})
public interface CustomVideoType {
    UUID getId();
    String getName();

}
