package uz.pdp.mfaktor.projection;

import org.springframework.beans.factory.Aware;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.Tariff;

import java.util.UUID;

@Projection(name = "customTariff", types = {Tariff.class})
public interface CustomTariff {
    Integer getId();
    String getNameRu();
    String getNameUz();
    Double getPrice();
    Integer getTicketCount();
    @Value("#{target.eventType.nameUz}")
    String getEventTypeUz();
    @Value("#{target.eventType.nameRu}")
    String getEventTypeRu();

    @Value("#{target.eventType.id}")
    Integer getEventTypeId();

}
