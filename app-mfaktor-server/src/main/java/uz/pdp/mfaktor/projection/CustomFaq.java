package uz.pdp.mfaktor.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.mfaktor.entity.Faq;

import java.util.UUID;

/**
 * Created by Pinup on 02.08.2019.
 */
@Projection(types = {Faq.class}, name = "customFaq")
public interface CustomFaq {
    UUID getId();

    String getQuestion();

    String getAnswer();

    UUID getAttachment();
}
