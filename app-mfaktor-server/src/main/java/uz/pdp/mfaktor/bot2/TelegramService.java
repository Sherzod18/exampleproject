package uz.pdp.mfaktor.bot2;

import org.telegram.telegrambots.meta.api.methods.send.SendAnimation;
import org.telegram.telegrambots.meta.api.methods.send.SendLocation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;

public interface TelegramService {

    SendMessage chooseLanguage(Update update);

    EditMessageText saveLanguage(Update update);

    EditMessageText backMain(Update update);

    SendMessage backMain2(Update update);

    EditMessageText myProfile(Update update);

    SendMessage showMyProfile(Update update);

    EditMessageText setFirstName(Update update);

    DeleteMessage deleteFirstNameMessage(Update update);

    EditMessageText setLastName(Update update);

    DeleteMessage deleteLastNameMessage(Update update);

    EditMessageText setPhoneNumber(Update update);

    EditMessageText setRegion(Update update);

    EditMessageText setActivity(Update update);

    EditMessageText setCompany(Update update);

    EditMessageText setPosition(Update update);

    EditMessageText setBirthDate(Update update);

    DeleteMessage deletePosition(Update update);

    DeleteMessage deletePhoneNumber(Update update);

    DeleteMessage deleteRegion(Update update);

    DeleteMessage deleteActivity(Update update);

    DeleteMessage deleteCompany(Update update);

    DeleteMessage deleteBirthDate(Update update);

    EditMessageText showAllEventData(Update update);

    EditMessageText notFullData(Update update);

    SendMessage notFullData2(Update update);

    <T> T allEvents(Update update);

    SendPhoto registerForByEvent(Update update);

    DeleteMessage deleteTopMessage(Update update);

    SendMessage showTypeForRegisterSeat(Update update);

    EditMessageText payByMoneyForSeat(Update update);

    <T> T payByPlasticForSeat(Update update);

    SendPhoto successFullPayTypePlastic(Update update);

    SendPhoto showAcceptedForSeat(Update update);

    SendMessage acceptBron(Update update);

    SendMessage noAcceptBron(Update update);

    SendMessage sendErrorMessage(Update update);

    SendMessage sendInformationToAdmin(Update update, Long chatId, int type);

    SendMessage allRegisterEvents(Update update);

    SendPhoto showOneRegisterEvent(Update update);

    SendMessage closeBron(Update update);

    SendPhoto referalLink(Update update);

    SendLocation showAddress(Update update);

    SendPhoto showImage(Update update);

    EditMessageText cash(Update update);

    SendMessage writeFirstName(Update update);

    SendMessage writeLastName(Update update);

    SendMessage writePhoneNumber(Update update);

    SendMessage saveAllDataUser(Update update);

    EditMessageText debt(Update update);

    SendMessage askPassword(Update update);

    SendMessage checkAdminPassword(Update update);

    SendMessage reklama(Update update);

    SendMessage saveReklamaImage(Update update);

    SendMessage reklamaLanguage(Update update);

    SendPhoto reklamaDescription(Update update);

    SendMessage deleteReklama(Update update);

    SendMessage sendReklama(Update update);
}
