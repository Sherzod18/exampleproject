package uz.pdp.mfaktor.bot2;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendInvoice;
import org.telegram.telegrambots.meta.api.methods.send.SendLocation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Contact;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.payments.LabeledPrice;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.mfaktor.bot.BotConstants;
import uz.pdp.mfaktor.entity.*;
import uz.pdp.mfaktor.entity.enums.EventStatus;
import uz.pdp.mfaktor.entity.enums.PlaceStatus;
import uz.pdp.mfaktor.entity.enums.RoleName;
import uz.pdp.mfaktor.payload.ReqUser;
import uz.pdp.mfaktor.payload.ResBalanceByPayType;
import uz.pdp.mfaktor.payload.ResEventPayment;
import uz.pdp.mfaktor.payload.ResEventPaymentAndBalance;
import uz.pdp.mfaktor.repository.*;
import uz.pdp.mfaktor.service.EventService;
import uz.pdp.mfaktor.service.SeatService;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class TelegramServiceImpl<T> implements TelegramService {

    @Autowired
    BotUserRepository botUserRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    SeatRepository seatRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RegionRepository regionRepository;

    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    SeatService seatService;

    @Autowired
    PositionRepository positionRepository;

    @Autowired
    RealMFaktorBot realMFaktorBot;

    @Autowired
    EventService eventService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ReklamaRepository reklamaRepository;
    String ru_week[] = {"воскресене", "понедельник", "вторник", "среду", "четверг", "пятницу", "субботу"};
    String kr_week[] = {"Якшанба", "Душанба", "Сешанба", "Чоршанба", "Пайшанба", "Жума", "Шанба"};

    String providerToken = "371317599:TEST:137701";

    @Override
    public SendMessage chooseLanguage(Update update) {
        SendMessage sendMessage = new SendMessage().setParseMode(ParseMode.MARKDOWN).setChatId(update.getMessage().getChatId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        button1.setText(BotConstants.LANG_KR);
        button2.setText(BotConstants.LANG_RU);
        button1.setCallbackData("Lang#KR");
        button2.setCallbackData("Lang#RU");
        row1.add(button1);
        row1.add(button2);
        rows.add(row1);
        inlineKeyboardMarkup.setKeyboard(rows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        sendMessage.setText(BotConstants.CHOOSE_LANG_TEXT);
        return sendMessage;
    }

    @Override
    public EditMessageText saveLanguage(Update update) {
        EditMessageText editMessageText = new EditMessageText().setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        String data = update.getCallbackQuery().getData();
        String lang = data.substring(data.indexOf('#') + 1);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        BotUser bUser;
        if (botUser.isPresent()) {
            bUser = botUser.get();
            bUser.setLanguage(lang);
            bUser.setTgUserId(update.getCallbackQuery().getFrom().getId());
        } else {
            bUser = new BotUser();
            bUser.setLanguage(lang);
            bUser.setTgUserId(update.getCallbackQuery().getFrom().getId());
        }
        bUser.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        bUser.setChatId(update.getCallbackQuery().getMessage().getChatId());
        botUserRepository.save(bUser);

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        List<InlineKeyboardButton> row3 = new ArrayList<>();
        List<InlineKeyboardButton> row4 = new ArrayList<>();
        List<InlineKeyboardButton> row5 = new ArrayList<>();
        List<InlineKeyboardButton> row6 = new ArrayList<>();
        List<InlineKeyboardButton> row7 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        InlineKeyboardButton button4 = new InlineKeyboardButton();
        InlineKeyboardButton button5 = new InlineKeyboardButton();
        InlineKeyboardButton button6 = new InlineKeyboardButton();
        InlineKeyboardButton button7 = new InlineKeyboardButton();
        if (lang.equals(BotConstants.KR)) {
            editMessageText.setText("Сизнинг ID: " + update.getCallbackQuery().getFrom().getId() + "\n" +
                    "@MfaktorBot га хуш келибсиз!\n\n" +
                    "➡️@MFaktorUz каналида янгиликларимизни кузатиб боринг\n\n" +
                    "Сизнинг профил маълумотларингиз тўлиқ эмас. Фақат тўлиқ профил маълумотлар билан сиз ботнинг барча функциялар ва хусусиятларидан фойдаланишингиз мумкин.⛔️");
            button1.setText("Менинг профилим \uD83D\uDC64");
            button2.setText("Тадбирлар");
            button3.setText(BotConstants.CONNECT_TO_ADMIN_KR);
            button4.setText(BotConstants.ADDRESS_US_KR);
        } else {
            button1.setText("Мой профиль \uD83D\uDC64");
            button2.setText("События");
            button3.setText(BotConstants.CONNECT_TO_ADMIN_RU);
            button4.setText(BotConstants.ADDRESS_US_RU);
            editMessageText.setText("Ваш ID: " + update.getCallbackQuery().getFrom().getId() + "\n" +
                    "Вас приветствует @MfaktorBot\n\n" +
                    "➡️Следите за нашими новостями на канале @MFaktorUz\n\n" +
                    "Ваш профиль не заполнен полностью. Только при полном заполнении профиля вы можете получить доступ ко всем функциям и возможностям. ⛔️");
        }
        button1.setCallbackData("Profile");
        button2.setCallbackData("Event");
        button3.setUrl("https://t.me/impulsealoqa");
        button4.setCallbackData("Address");
        Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
        row1.add(button1);
        row2.add(button2);
        row3.add(button3);
        row4.add(button4);
        rows.add(row1);
        rows.add(row2);
        rows.add(row3);
        rows.add(row4);
        if (user.isPresent()) {
            if ((int) user.get().getRoles().stream().filter(role -> role.getName().equals(RoleName.ROLE_DIRECTOR)).count() > 0) {
                button5.setText("Balans");
                button5.setCallbackData("Cash");
                row5.add(button5);
                rows.add(row5);

                button6.setText("Qarzlar");
                button6.setCallbackData("Debt");
                row6.add(button6);
                rows.add(row6);

                button7.setText("Reklama");
                button7.setCallbackData("Reklama");
                row7.add(button7);
                rows.add(row7);
            }
        }
        inlineKeyboardMarkup.setKeyboard(rows);
        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        return editMessageText;
    }

    @Override
    public EditMessageText backMain(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        List<InlineKeyboardButton> row3 = new ArrayList<>();
        List<InlineKeyboardButton> row4 = new ArrayList<>();
        List<InlineKeyboardButton> row5 = new ArrayList<>();
        List<InlineKeyboardButton> row6 = new ArrayList<>();
        List<InlineKeyboardButton> row7 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        InlineKeyboardButton button4 = new InlineKeyboardButton();
        InlineKeyboardButton button5 = new InlineKeyboardButton();
        InlineKeyboardButton button6 = new InlineKeyboardButton();
        InlineKeyboardButton button7 = new InlineKeyboardButton();
        EditMessageText editMessageText = new EditMessageText().setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            editMessageText.setText("Сизнинг ID: " + update.getCallbackQuery().getFrom().getId() + "\n" +
                    "@MfaktorBot га хуш келибсиз!\n\n" +
                    "➡️@MFaktorUz каналида янгиликларимизни кузатиб боринг\n\n" +
                    "Сизнинг профил маълумотларингиз тўлиқ эмас. Фақат тўлиқ профил маълумотлар билан сиз ботнинг барча функциялар ва хусусиятларидан фойдаланишингиз мумкин.⛔️");
            button1.setText("Менинг профилим \uD83D\uDC64");
            button2.setText("Тадбирлар");
            button3.setText(BotConstants.CONNECT_TO_ADMIN_KR);
            button4.setText(BotConstants.ADDRESS_US_KR);
        } else {
            button1.setText("Мой профиль \uD83D\uDC64");
            button2.setText("События");
            button3.setText(BotConstants.CONNECT_TO_ADMIN_RU);
            button4.setText(BotConstants.ADDRESS_US_RU);
            editMessageText.setText("Ваш ID: " + update.getCallbackQuery().getFrom().getId() + "\n" +
                    "Вас приветствует @MfaktorBot\n\n" +
                    "➡️Следите за нашими новостями на канале @MFaktorUz\n\n" +
                    "Ваш профиль не заполнен полностью. Только при полном заполнении профиля вы можете получить доступ ко всем функциям и возможностям. ⛔️");
        }
        button1.setCallbackData("Profile");
        button2.setCallbackData("Event");
        button3.setUrl("https://t.me/impulsealoqa");
        button4.setCallbackData("Address");
        row1.add(button1);
        row2.add(button2);
        row3.add(button3);
        row4.add(button4);
        rows.add(row1);
        rows.add(row2);
        rows.add(row3);
        rows.add(row4);
        Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
        if (user.isPresent()) {
            if ((int) user.get().getRoles().stream().filter(role -> role.getName().equals(RoleName.ROLE_DIRECTOR)).count() > 0) {
                button5.setText("Balans");
                button5.setCallbackData("Cash");
                row5.add(button5);
                rows.add(row5);


                button6.setText("Qarzlar");
                button6.setCallbackData("Debt");
                row6.add(button6);
                rows.add(row6);

                button7.setText("Reklama");
                button7.setCallbackData("Reklama");
                row7.add(button7);
                rows.add(row7);
            }
        }
        inlineKeyboardMarkup.setKeyboard(rows);
        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        return editMessageText;
    }

    @Override
    public SendMessage backMain2(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        List<InlineKeyboardButton> row3 = new ArrayList<>();
        List<InlineKeyboardButton> row4 = new ArrayList<>();
        List<InlineKeyboardButton> row5 = new ArrayList<>();
        List<InlineKeyboardButton> row6 = new ArrayList<>();
        List<InlineKeyboardButton> row7 = new ArrayList<>();

        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        InlineKeyboardButton button4 = new InlineKeyboardButton();
        InlineKeyboardButton button5 = new InlineKeyboardButton();
        InlineKeyboardButton button6 = new InlineKeyboardButton();
        InlineKeyboardButton button7 = new InlineKeyboardButton();
        SendMessage sendMessage = new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            sendMessage.setText("Сизнинг ID: " + update.getCallbackQuery().getFrom().getId() + "\n" +
                    "@MfaktorBot га хуш келибсиз!\n\n" +
                    "➡️@MFaktorUz каналида янгиликларимизни кузатиб боринг\n\n" +
                    "Сизнинг профил маълумотларингиз тўлиқ эмас. Фақат тўлиқ профил маълумотлар билан сиз ботнинг барча функциялар ва хусусиятларидан фойдаланишингиз мумкин.⛔️");
            button1.setText("Менинг профилим \uD83D\uDC64");
            button2.setText("Тадбирлар");
            button3.setText(BotConstants.CONNECT_TO_ADMIN_KR);
            button4.setText(BotConstants.ADDRESS_US_KR);
        } else {
            button1.setText("Мой профиль \uD83D\uDC64");
            button2.setText("События");
            button3.setText(BotConstants.CONNECT_TO_ADMIN_RU);
            button4.setText(BotConstants.ADDRESS_US_RU);
            sendMessage.setText("Ваш ID: " + update.getCallbackQuery().getFrom().getId() + "\n" +
                    "Вас приветствует @MfaktorBot\n\n" +
                    "➡️Следите за нашими новостями на канале @MFaktorUz\n\n" +
                    "Ваш профиль не заполнен полностью. Только при полном заполнении профиля вы можете получить доступ ко всем функциям и возможностям. ⛔️");
        }
        button1.setCallbackData("Profile");
        button2.setCallbackData("Event");
        button3.setUrl("https://t.me/impulsealoqa");
        button4.setCallbackData("Address");
        row1.add(button1);
        row2.add(button2);
        row3.add(button3);
        row4.add(button4);
        rows.add(row1);
        rows.add(row2);
        rows.add(row3);
        rows.add(row4);
        Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
        if (user.isPresent()) {
            if ((int) user.get().getRoles().stream().filter(role -> role.getName().equals(RoleName.ROLE_DIRECTOR)).count() > 0) {
                button5.setText("Balans");
                button5.setCallbackData("Cash");
                row5.add(button5);
                rows.add(row5);


                button6.setText("Qarzlar");
                button6.setCallbackData("Debt");
                row6.add(button6);
                rows.add(row6);

                button7.setText("Reklama");
                button7.setCallbackData("Reklama");
                row7.add(button7);
                rows.add(row7);
            }
        }
        inlineKeyboardMarkup.setKeyboard(rows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    @Override
    public EditMessageText debt(Update update) {
        EditMessageText editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
        botUser.ifPresent(bUser -> {
            double v = eventService.debtToUs();
            double v1 = eventService.weDebt();
            editMessageText.setText("Bizning qarzlar: " + decimalFormat.format(v1) + "\n" +
                    "Bizga qarzlar: " + decimalFormat.format(v));

            if (bUser.getLanguage().equals(BotConstants.KR)) {
                button1.setText("⬅️Орқага");
            } else {
                button1.setText("⬅️Назад");
            }
            button1.setCallbackData("BackMain2");
            row1.add(button1);
            rows.add(row1);
            inlineKeyboardMarkup.setKeyboard(rows);
            editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        });
        return editMessageText;
    }

    @Override
    public SendMessage reklama(Update update) {
        SendMessage sendMessage = new SendMessage().setParseMode(ParseMode.MARKDOWN).setChatId(update.getCallbackQuery().getMessage().getChatId());
        sendMessage.setText("Reklama rasmini yuboring.");
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        BotUser bUser = botUser.get();
        bUser.setState("ReklamaImage");
        botUserRepository.save(bUser);
        return sendMessage;
    }

    @Override
    public SendMessage saveReklamaImage(Update update) {
        SendMessage sendMessage = new SendMessage().setParseMode(ParseMode.MARKDOWN).setChatId(update.getMessage().getChatId());
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true).setSelective(true);
        List<KeyboardRow> rows = new ArrayList<>();
        KeyboardRow keyboardRow1 = new KeyboardRow();
        KeyboardRow keyboardRow2 = new KeyboardRow();
        keyboardRow1.add(BotConstants.KR);
        keyboardRow2.add(BotConstants.RU);
        rows.add(keyboardRow1);
        rows.add(keyboardRow2);
        replyKeyboardMarkup.setKeyboard(rows);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        BotUser botUser1 = botUser.get();
        try {
            URL url = new URL("https://api.telegram.org/bot" + realMFaktorBot.getBotToken() + "/getFile?file_id=" + update.getMessage().getPhoto().get(0).getFileId());
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String res = in.readLine();
            JSONObject jresult = new JSONObject(res);
            JSONObject path = jresult.getJSONObject("result");
            String file_path = path.getString("file_path");
            URL downoload = new URL("https://api.telegram.org/file/bot" + realMFaktorBot.getBotToken() + "/" + file_path);
            System.out.println("Start upload");
            ReadableByteChannel rbc = Channels.newChannel(downoload.openStream());
            rbc.close();
            System.out.println("Uploaded!");
            Reklama reklama = new Reklama();
            reklama.setImgUrl(downoload.toString());
            Reklama saveReklama = reklamaRepository.save(reklama);
            botUser1.setRekId(saveReklama.getId().toString());
            botUser1.setState("ReklamaLanguage");
            botUserRepository.save(botUser1);

            sendMessage.setText("Tilni tanlang:");
            sendMessage.setReplyMarkup(replyKeyboardMarkup);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendMessage;
    }

    @Override
    public SendMessage reklamaLanguage(Update update) {
        ReplyKeyboardRemove replyKeyboardRemove = new ReplyKeyboardRemove();
        SendMessage sendMessage = new SendMessage().setChatId(update.getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        String text = update.getMessage().getText();
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        BotUser botUser1 = botUser.get();
        Optional<Reklama> reklama = reklamaRepository.findById(UUID.fromString(botUser1.getRekId()));
        Reklama reklama1 = reklama.get();
        reklama1.setReklamaLanguage(text);
        reklamaRepository.save(reklama1);
        botUser1.setState("ReklamaDescription");
        botUserRepository.save(botUser1);
        sendMessage.setText("Text yuboring");
        sendMessage.setReplyMarkup(replyKeyboardRemove);
        return sendMessage;
    }

    @Override
    public SendPhoto reklamaDescription(Update update) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        String text = update.getMessage().getText();
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        BotUser botUser1 = botUser.get();
        Optional<Reklama> reklama = reklamaRepository.findById(UUID.fromString(botUser1.getRekId()));
        Reklama reklama1 = reklama.get();
        reklama1.setDescription(text);

        Reklama saveReklama = reklamaRepository.save(reklama1);
        SendPhoto sendPhoto = new SendPhoto().setParseMode(ParseMode.MARKDOWN).setChatId(update.getMessage().getChatId());
        try {
            URL url = null;
            url = new URL(saveReklama.getImgUrl());
            sendPhoto.setPhoto("sanjar", new ByteArrayInputStream(readAllBytes(url.openStream())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendPhoto.setCaption(text);
        button1.setText("Yuborish");
        button2.setText("O'chirish");
        button3.setText("Orqaga");
        button1.setCallbackData("SendReklamaId#" + reklama1.getId());
        button2.setCallbackData("DeleteReklamaId#" + reklama1.getId());
        button3.setCallbackData("BackMain2");
        row.add(button1);
        row.add(button2);
        row1.add(button3);
        rows.add(row);
        rows.add(row1);
        inlineKeyboardMarkup.setKeyboard(rows);
        sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
        return sendPhoto;
    }

    private static byte[] readAllBytes(InputStream input) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int numRead;
        byte[] buffer = new byte[16384];
        while ((numRead = input.read(buffer, 0, buffer.length)) != -1) {
            out.write(buffer, 0, numRead);
        }
        return out.toByteArray();
    }

    @Override
    public SendMessage deleteReklama(Update update) {
        SendMessage sendMessage = new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        String data = update.getCallbackQuery().getData();
        String rekId = data.substring(data.indexOf('#') + 1);
        Optional<Reklama> reklama = reklamaRepository.findById(UUID.fromString(rekId));
        if (reklama.isPresent()) {
            reklamaRepository.delete(reklama.get());
            sendMessage.setText("Reklama o'chirildi.");
        } else {
            sendMessage.setText("Reklama oldin o'chirilgan.");
        }
        return sendMessage;
    }

    @Override
    public SendMessage sendReklama(Update update) {
        SendMessage sendMessage = new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        String data = update.getCallbackQuery().getData();
        String rekId = data.substring(data.indexOf('#') + 1);
        Optional<Reklama> reklama = reklamaRepository.findById(UUID.fromString(rekId));
        if (reklama.isPresent()) {
            List<User> users = userRepository.findAll();
            for (int i = 0; i < users.size(); i++) {
                if (users.get(i).getChatId() != null) {
                    try {
                        realMFaktorBot.execute(sendReklamaAllUsers(users.get(i), reklama.get()));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return sendMessage;
    }

    public SendPhoto sendReklamaAllUsers(User user, Reklama reklama) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button = new InlineKeyboardButton();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        SendPhoto sendPhoto = new SendPhoto().setParseMode(ParseMode.MARKDOWN).setChatId(user.getChatId());
        try {
            URL url;
            url = new URL(reklama.getImgUrl());
            sendPhoto.setPhoto("sanjar", new ByteArrayInputStream(readAllBytes(url.openStream())));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (reklama.getReklamaLanguage().equals(BotConstants.KR)) {
            button.setText("Батафсил");
            button1.setText(BotConstants.BACK_KR);
            sendPhoto.setCaption("Ҳурматли " + user.getFirstName() + " " + user.getLastName() + "\n\n" + reklama.getDescription());
        } else {
            button.setText("Узнать больше");
            button1.setText(BotConstants.BACK_RU);
            sendPhoto.setCaption("Уважаемый(ая) " + user.getFirstName() + " " + user.getLastName() + "\n\n" + reklama.getDescription());
        }
        button.setUrl("https://t.me/impulsealoqa");
        button1.setCallbackData("BackMain2");
        row.add(button);
        row1.add(button1);
        rows.add(row);
        rows.add(row1);
        inlineKeyboardMarkup.setKeyboard(rows);
        sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
        return sendPhoto;
    }

    @Override
    public EditMessageText myProfile(Update update) {
        EditMessageText editMessageText = new EditMessageText();
        SendMessage sendMessage = new SendMessage();
        Optional<BotUser> botUser;
        Optional<User> user;

        if (update.hasCallbackQuery()) {
            botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
            user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
        } else {
            botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
            user = userRepository.findFirstByTelegramId(update.getMessage().getFrom().getId());
        }
        botUser.ifPresent(bUser -> {
            if (update.hasCallbackQuery()) {
                editMessageText.setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
            } else {
                sendMessage.setChatId(update.getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
            }
            if (user.isPresent()) {
                bUser.setFirstName(user.get().getFirstName());
                bUser.setLastName(user.get().getLastName());
                bUser.setPhoneNumber(user.get().getPhoneNumber());
                bUser.setTgUserId(user.get().getTelegramId());
                bUser.setChatId(user.get().getChatId());
                bUser.setBirthDate(user.get().getBirthDate());
                bUser.setAware(user.get().getAware());
                bUser.setActivity(user.get().getActivity());
                bUser.setPosition(user.get().getPosition());
                bUser.setCompany(user.get().getCompany());
                bUser.setRegion(user.get().getRegion());
                botUserRepository.save(bUser);
            }
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rows = new ArrayList<>();
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            List<InlineKeyboardButton> row2 = new ArrayList<>();
            List<InlineKeyboardButton> row3 = new ArrayList<>();
            List<InlineKeyboardButton> row4 = new ArrayList<>();
            List<InlineKeyboardButton> row5 = new ArrayList<>();
            InlineKeyboardButton button1 = new InlineKeyboardButton();
            InlineKeyboardButton button2 = new InlineKeyboardButton();
            InlineKeyboardButton button3 = new InlineKeyboardButton();
            InlineKeyboardButton button4 = new InlineKeyboardButton();
            InlineKeyboardButton button5 = new InlineKeyboardButton();
            InlineKeyboardButton button6 = new InlineKeyboardButton();
            InlineKeyboardButton button7 = new InlineKeyboardButton();
            InlineKeyboardButton button8 = new InlineKeyboardButton();
            InlineKeyboardButton button9 = new InlineKeyboardButton();
            if (bUser.getLanguage().equals(BotConstants.KR)) {
                editMessageText.setText(
                        ("Менинг маълумотларим\n\n") +
                                (((bUser.getPhoneNumber() == null || bUser.getPhoneNumber().equals("")) ? "❗" : "✅") + "️Телефон: " + ((bUser.getPhoneNumber() != null) ? bUser.getPhoneNumber() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getFirstName() == null || bUser.getFirstName().equals("")) ? "❗" : "✅") + "️Исм: " + ((bUser.getFirstName() != null) ? bUser.getFirstName() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getLastName() == null || bUser.getLastName().equals("")) ? "❗" : "✅") + "️Фамилия: " + ((bUser.getLastName() != null) ? bUser.getLastName() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getRegion() == null) ? "❗" : "✅") + "️Вилояти: " + ((bUser.getRegion() != null) ? bUser.getRegion().getNameUz() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getActivity() == null) ? "❗" : "✅") + "️Фаолият тури: " + ((bUser.getActivity() != null) ? bUser.getActivity().getNameUz() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getCompany() == null) ? "❗" : "✅") + "️Компания номи: " + ((bUser.getCompany() != null) ? bUser.getCompany() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getPosition() == null) ? "❗" : "✅") + "️Лавозими: " + ((bUser.getPosition() != null) ? bUser.getPosition().getNameUz() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getBirthDate() == null) ? "❗" : "✅") + "️Туғилган вақти: " + ((bUser.getBirthDate() != null) ? bUser.getBirthDate().toString().substring(0, 10) : "Кўрсатилмаган"))
                );
                button1.setText("Телефон \uD83D\uDCDD");
                button2.setText("Исм \uD83D\uDCDD");
                button3.setText("Фамилия \uD83D\uDCDD");
                button4.setText("Вилояти \uD83D\uDCDD");
                button5.setText("Фаолият тури \uD83D\uDCDD");
                button6.setText("Компания номи \uD83D\uDCDD");
                button7.setText("Лавозими \uD83D\uDCDD");
                button8.setText("Туғилган вақти \uD83D\uDCDD");
                button9.setText("⬅️Орқага");
            } else {
                editMessageText.setText(
                        ("Мой профиль\n\n") +
                                (((bUser.getPhoneNumber() == null || bUser.getPhoneNumber().equals("")) ? "❗" : "✅") + "️Телефон: " + ((bUser.getPhoneNumber() != null) ? bUser.getPhoneNumber() : "не указан") + "\n") +
                                (((bUser.getFirstName() == null || bUser.getFirstName().equals("")) ? "❗" : "✅") + "️Имя: " + ((bUser.getFirstName() != null) ? bUser.getFirstName() : "не указан") + "\n") +
                                (((bUser.getLastName() == null || bUser.getLastName().equals("")) ? "❗" : "✅") + "️Фамилия: " + ((bUser.getLastName() != null) ? bUser.getLastName() : "не указан") + "\n") +
                                (((bUser.getRegion() == null) ? "❗" : "✅") + "️Область: " + ((bUser.getRegion() != null) ? bUser.getRegion().getNameRu() : "не указан") + "\n") +
                                (((bUser.getActivity() == null) ? "❗" : "✅") + "️Тип деятельности: " + ((bUser.getActivity() != null) ? bUser.getActivity().getNameRu() : "не указан") + "\n") +
                                (((bUser.getCompany() == null) ? "❗" : "✅") + "️Название компании: " + ((bUser.getCompany() != null) ? bUser.getCompany() : "не указан") + "\n") +
                                (((bUser.getPosition() == null) ? "❗" : "✅") + "️Должность: " + ((bUser.getPosition() != null) ? bUser.getPosition().getNameRu() : "не указан") + "\n") +
                                (((bUser.getBirthDate() == null) ? "❗" : "✅") + "️Дата рождения: " + ((bUser.getBirthDate() != null) ? bUser.getBirthDate().toString().substring(0, 10) : "не указан"))
                );
                button1.setText("Телефон \uD83D\uDCDD");
                button2.setText("Имя \uD83D\uDCDD");
                button3.setText("Фамилия \uD83D\uDCDD");
                button4.setText("Область \uD83D\uDCDD");
                button5.setText("Тип деятельности \uD83D\uDCDD");
                button6.setText("Название компании \uD83D\uDCDD");
                button7.setText("Должность \uD83D\uDCDD");
                button8.setText("Дата рождения \uD83D\uDCDD");
                button9.setText("⬅️Назад");
            }
            button1.setCallbackData("Phone_number");
            button2.setCallbackData("First_name");
            button3.setCallbackData("Last_name");
            button4.setCallbackData("Region");
            button5.setCallbackData("Activity");
            button6.setCallbackData("Company");
            button7.setCallbackData("Position");
            button8.setCallbackData("Birth_date");
            button9.setCallbackData("BackMain");

            row1.add(button1);
            row1.add(button2);
            row2.add(button3);
            row2.add(button4);
            row3.add(button5);
            row3.add(button6);
            row4.add(button7);
            row4.add(button8);
            row5.add(button9);

            rows.add(row1);
            rows.add(row2);
            rows.add(row3);
            rows.add(row4);
            rows.add(row5);
            inlineKeyboardMarkup.setKeyboard(rows);
            editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        });

        return editMessageText;
    }

    @Override
    public SendMessage showMyProfile(Update update) {
        SendMessage sendMessage = new SendMessage();
        Optional<BotUser> botUser;
        Optional<User> user;

        if (update.hasCallbackQuery()) {
            sendMessage.setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
            botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
            user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
        } else {
            sendMessage.setChatId(update.getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
            botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
            user = userRepository.findFirstByTelegramId(update.getMessage().getFrom().getId());
        }
        botUser.ifPresent(bUser -> {
            if (user.isPresent()) {
                bUser.setFirstName(user.get().getFirstName());
                bUser.setLastName(user.get().getLastName());
                bUser.setPhoneNumber(user.get().getPhoneNumber());
                bUser.setTgUserId(user.get().getTelegramId());
                bUser.setChatId(user.get().getChatId());
                bUser.setBirthDate(user.get().getBirthDate());
                bUser.setAware(user.get().getAware());
                bUser.setActivity(user.get().getActivity());
                bUser.setPosition(user.get().getPosition());
                bUser.setCompany(user.get().getCompany());
                bUser.setRegion(user.get().getRegion());
                botUserRepository.save(bUser);
            }
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rows = new ArrayList<>();
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            List<InlineKeyboardButton> row2 = new ArrayList<>();
            List<InlineKeyboardButton> row3 = new ArrayList<>();
            List<InlineKeyboardButton> row4 = new ArrayList<>();
            List<InlineKeyboardButton> row5 = new ArrayList<>();
            InlineKeyboardButton button1 = new InlineKeyboardButton();
            InlineKeyboardButton button2 = new InlineKeyboardButton();
            InlineKeyboardButton button3 = new InlineKeyboardButton();
            InlineKeyboardButton button4 = new InlineKeyboardButton();
            InlineKeyboardButton button5 = new InlineKeyboardButton();
            InlineKeyboardButton button6 = new InlineKeyboardButton();
            InlineKeyboardButton button7 = new InlineKeyboardButton();
            InlineKeyboardButton button8 = new InlineKeyboardButton();
            InlineKeyboardButton button9 = new InlineKeyboardButton();
            if (bUser.getLanguage().equals(BotConstants.KR)) {
                sendMessage.setText(
                        ("Менинг маълумотларим\n\n") +
                                (((bUser.getPhoneNumber() == null || bUser.getPhoneNumber().equals("")) ? "❗" : "✅") + "️Телефон: " + ((bUser.getPhoneNumber() != null) ? bUser.getPhoneNumber() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getFirstName() == null || bUser.getFirstName().equals("")) ? "❗" : "✅") + "️Исм: " + ((bUser.getFirstName() != null) ? bUser.getFirstName() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getLastName() == null || bUser.getLastName().equals("")) ? "❗" : "✅") + "️Фамилия: " + ((bUser.getLastName() != null) ? bUser.getLastName() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getRegion() == null) ? "❗" : "✅") + "️Вилояти: " + ((bUser.getRegion() != null) ? bUser.getRegion().getNameUz() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getActivity() == null) ? "❗" : "✅") + "️Фаолият тури: " + ((bUser.getActivity() != null) ? bUser.getActivity().getNameUz() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getCompany() == null) ? "❗" : "✅") + "️Компания номи: " + ((bUser.getCompany() != null) ? bUser.getCompany() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getPosition() == null) ? "❗" : "✅") + "️Лавозими: " + ((bUser.getPosition() != null) ? bUser.getPosition().getNameUz() : "Кўрсатилмаган") + "\n") +
                                (((bUser.getBirthDate() == null) ? "❗" : "✅") + "️Туғилган вақти: " + ((bUser.getBirthDate() != null) ? bUser.getBirthDate().toString().substring(0, 10) : "Кўрсатилмаган"))
                );
                button1.setText("Телефон \uD83D\uDCDD");
                button2.setText("Исм \uD83D\uDCDD");
                button3.setText("Фамилия \uD83D\uDCDD");
                button4.setText("Вилояти \uD83D\uDCDD");
                button5.setText("Фаолият тури \uD83D\uDCDD");
                button6.setText("Компания номи \uD83D\uDCDD");
                button7.setText("Лавозими \uD83D\uDCDD");
                button8.setText("Туғилган вақти \uD83D\uDCDD");
                button9.setText("⬅️Орқага");
            } else {
                sendMessage.setText(
                        ("Мой профиль\n\n") +
                                (((bUser.getPhoneNumber() == null || bUser.getPhoneNumber().equals("")) ? "❗" : "✅") + "️Телефон: " + ((bUser.getPhoneNumber() != null) ? bUser.getPhoneNumber() : "не указан") + "\n") +
                                (((bUser.getFirstName() == null || bUser.getFirstName().equals("")) ? "❗" : "✅") + "️Имя: " + ((bUser.getFirstName() != null) ? bUser.getFirstName() : "не указан") + "\n") +
                                (((bUser.getLastName() == null || bUser.getLastName().equals("")) ? "❗" : "✅") + "️Фамилия: " + ((bUser.getLastName() != null) ? bUser.getLastName() : "не указан") + "\n") +
                                (((bUser.getRegion() == null) ? "❗" : "✅") + "️Область: " + ((bUser.getRegion() != null) ? bUser.getRegion().getNameRu() : "не указан") + "\n") +
                                (((bUser.getActivity() == null) ? "❗" : "✅") + "️Тип деятельности: " + ((bUser.getActivity() != null) ? bUser.getActivity().getNameRu() : "не указан") + "\n") +
                                (((bUser.getCompany() == null) ? "❗" : "✅") + "️Название компании: " + ((bUser.getCompany() != null) ? bUser.getCompany() : "не указан") + "\n") +
                                (((bUser.getPosition() == null) ? "❗" : "✅") + "️Должность: " + ((bUser.getPosition() != null) ? bUser.getPosition().getNameRu() : "не указан") + "\n") +
                                (((bUser.getBirthDate() == null) ? "❗" : "✅") + "️Дата рождения: " + ((bUser.getBirthDate() != null) ? bUser.getBirthDate().toString().substring(0, 10) : "не указан"))
                );
                button1.setText("Телефон \uD83D\uDCDD");
                button2.setText("Имя \uD83D\uDCDD");
                button3.setText("Фамилия \uD83D\uDCDD");
                button4.setText("Область \uD83D\uDCDD");
                button5.setText("Тип деятельности \uD83D\uDCDD");
                button6.setText("Название компании \uD83D\uDCDD");
                button7.setText("Должность \uD83D\uDCDD");
                button8.setText("Дата рождения \uD83D\uDCDD");
                button9.setText("⬅️Назад");
            }
            button1.setCallbackData("Phone_number");
            button2.setCallbackData("First_name");
            button3.setCallbackData("Last_name");
            button4.setCallbackData("Region");
            button5.setCallbackData("Activity");
            button6.setCallbackData("Company");
            button7.setCallbackData("Position");
            button8.setCallbackData("Birth_date");
            button9.setCallbackData("BackMain");

            row1.add(button1);
            row1.add(button2);
            row2.add(button3);
            row2.add(button4);
            row3.add(button5);
            row3.add(button6);
            row4.add(button7);
            row4.add(button8);
            row5.add(button9);

            rows.add(row1);
            rows.add(row2);
            rows.add(row3);
            rows.add(row4);
            rows.add(row5);
            inlineKeyboardMarkup.setKeyboard(rows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        });

        return sendMessage;
    }

    @Override
    public EditMessageText setFirstName(Update update) {
        EditMessageText editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        botUser.ifPresent(bUser -> {
            if (bUser.getFirstName() == null) {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Исмингизни киритинг");
                } else {
                    editMessageText.setText("Введите ваше имя");
                }
            } else {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Сизнинг исмингиз: " + bUser.getFirstName() + "\nМаълумотни янгилаш учун янги ном киритинг.");
                } else {
                    editMessageText.setText("Ваше имя: " + bUser.getFirstName() + "\nДля обновления информации введите новое имя.");
                }
            }
            if (bUser.getLanguage().equals(BotConstants.KR)) {
                button1.setText("⬅️Орқага");
            } else {
                button1.setText("⬅️Назад");
            }
            button1.setCallbackData("BackProfile");
            row1.add(button1);
            rows.add(row1);
            inlineKeyboardMarkup.setKeyboard(rows);
            editMessageText.setReplyMarkup(inlineKeyboardMarkup);
            bUser.setState("SaveFirstName");
            bUser.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            botUserRepository.save(bUser);
        });
        return editMessageText;
    }

    @Override
    public EditMessageText setLastName(Update update) {
        EditMessageText editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        botUser.ifPresent(bUser -> {
            if (bUser.getLastName() == null) {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Фамилиянгизни киритинг");
                } else {
                    editMessageText.setText("Введите ваше фамилия");
                }
            } else {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Сизнинг фамилиянгиз: " + bUser.getLastName() + "\nМаълумотни янгилаш учун янги ном киритинг.");
                } else {
                    editMessageText.setText("Ваше фамилия: " + bUser.getLastName() + "\nДля обновления информации введите новое фамилия.");
                }
            }
            if (bUser.getLanguage().equals(BotConstants.KR)) {
                button1.setText("⬅️Орқага");
            } else {
                button1.setText("⬅️Назад");
            }
            button1.setCallbackData("BackProfile");
            row1.add(button1);
            rows.add(row1);
            inlineKeyboardMarkup.setKeyboard(rows);
            editMessageText.setReplyMarkup(inlineKeyboardMarkup);
            bUser.setState("SaveLastName");
            bUser.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            botUserRepository.save(bUser);
        });
        return editMessageText;
    }

    @Override
    public EditMessageText setPhoneNumber(Update update) {
        EditMessageText editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        botUser.ifPresent(bUser -> {
            if (bUser.getPhoneNumber() == null) {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Тел номерингизни киритинг.\nМасалан: +998971234567");
                } else {
                    editMessageText.setText("Введите номер телефона.\nНапример: +998971234567");
                }
            } else {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Сизнинг телефон рақамингиз: " + bUser.getPhoneNumber() + "\nМаълумотни янгилаш учун янги телефон рақам киритинг.");
                } else {
                    editMessageText.setText("Ваш телефон номер: " + bUser.getPhoneNumber() + "\nДля обновления информации введите новый номер.");
                }
            }
            if (bUser.getLanguage().equals(BotConstants.KR)) {
                button1.setText("⬅️Орқага");
            } else {
                button1.setText("⬅️Назад");
            }
            button1.setCallbackData("BackProfile");
            row1.add(button1);
            rows.add(row1);
            inlineKeyboardMarkup.setKeyboard(rows);
            editMessageText.setReplyMarkup(inlineKeyboardMarkup);
            bUser.setState("SavePhoneNumber");
            bUser.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            botUserRepository.save(bUser);
        });
        return editMessageText;
    }

    @Override
    public SendMessage askPassword(Update update) {
        SendMessage sendMessage = new SendMessage().setParseMode(ParseMode.MARKDOWN)
                .setChatId(update.getMessage().getChatId());
        String text = update.getMessage().getText();
        if (isCheckPhoneNumber(update)) {
            Optional<User> user = userRepository.findByPhoneNumber(text);
            Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
            BotUser botUser1 = botUser.get();
            User user1 = user.get();
            user1.setTelegramId(update.getMessage().getFrom().getId());
            user1.setTelegramUsername(update.getMessage().getFrom().getUserName());
            userRepository.save(user1);
            botUser1.setFirstName(user1.getFirstName());
            botUser1.setLastName(user1.getLastName());
            botUser1.setPhoneNumber(user1.getPhoneNumber());
            botUser1.setTgUserId(update.getMessage().getFrom().getId());
            botUser1.setChatId(update.getMessage().getChatId());
            botUser1.setState("AskPassword");
            botUserRepository.save(botUser1);
            if (botUser1.getLanguage().equals(BotConstants.KR)) {
                sendMessage.setText(BotConstants.REG_WRITE_PASSWORD_KR);
            } else {
                sendMessage.setText(BotConstants.REG_WRITE_PASSWORD_RU);
            }
        }
        return sendMessage;
    }

    @Override
    public SendMessage checkAdminPassword(Update update) {
        SendMessage sendMessage = new SendMessage().setParseMode(ParseMode.MARKDOWN)
                .setChatId(update.getMessage().getChatId());
        Optional<User> user = userRepository.findFirstByTelegramId(update.getMessage().getFrom().getId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        BotUser bUser = botUser.get();
        User user1 = user.get();
        String password = update.getMessage().getText();
        if (passwordEncoder.matches(password, user1.getPassword())) {
            bUser.setState("SuccessAdminPassword");
            if (bUser.getLanguage().equals(BotConstants.KR)) {
                sendMessage.setText("Тизимга муваффаққиятли кирдингиз!");
                try {
                    realMFaktorBot.execute(showMyProfile(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else {
                sendMessage.setText("Успешно вошел в систему.");
            }
        } else {
            sendMessage = errorPassword(update);
        }

        return sendMessage;
    }

    private SendMessage errorPassword(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            sendMessage.setText(BotConstants.ERROR_PASSWORD_KR);
        } else {
            sendMessage.setText(BotConstants.ERROR_PASSWORD_RU);
        }
        return sendMessage;
    }

    @Override
    public EditMessageText setRegion(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        EditMessageText sendMessage = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setChatId(update.getCallbackQuery().getMessage().getChatId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<Region> allRegions = regionRepository.findAll();
        for (int i = 0; i < allRegions.size(); i++) {
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            InlineKeyboardButton button1 = new InlineKeyboardButton();
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                button1.setText(allRegions.get(i).getNameUz());
            } else {
                button1.setText(allRegions.get(i).getNameRu());
            }
            button1.setCallbackData("RegionId#" + allRegions.get(i).getId());
            row1.add(button1);
            rows.add(row1);
        }

        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            sendMessage.setText(BotConstants.CHOOSE_REGION_KR);
        } else {
            sendMessage.setText(BotConstants.CHOOSE_REGION_RU);
        }
        inlineKeyboardMarkup.setKeyboard(rows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    @Override
    public EditMessageText setActivity(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        EditMessageText sendMessage = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId())
                .setChatId(update.getCallbackQuery().getMessage().getChatId());
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            sendMessage.setText(BotConstants.CHOOSE_ACTIVITY_KR);
        } else {
            sendMessage.setText(BotConstants.CHOOSE_ACTIVITY_RU);
        }
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<Activity> allActivities = activityRepository.findAll();
        for (int i = 0; i < allActivities.size(); i++) {
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            InlineKeyboardButton button1 = new InlineKeyboardButton();
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                button1.setText(allActivities.get(i).getNameUz());
            } else {
                button1.setText(allActivities.get(i).getNameRu());
            }
            row1.add(button1);
            rows.add(row1);
            button1.setCallbackData("ActivityId#" + allActivities.get(i).getId());
        }
        inlineKeyboardMarkup.setKeyboard(rows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    @Override
    public DeleteMessage deleteFirstNameMessage(Update update) {
        String text = update.getMessage().getText();
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        DeleteMessage deleteMessage = new DeleteMessage();
        botUser.ifPresent(bUser -> {
            deleteMessage.setChatId(update.getMessage().getChatId()).setMessageId(bUser.getMessageId());
            bUser.setFirstName(text);
            botUserRepository.save(bUser);

            Optional<User> user = userRepository.findFirstByTelegramId(update.getMessage().getFrom().getId());
            if (user.isPresent()) {
                User user1 = user.get();
                user1.setFirstName(text);
                userRepository.save(user1);
            }
        });
        return deleteMessage;
    }

    @Override
    public DeleteMessage deleteLastNameMessage(Update update) {
        DeleteMessage deleteMessage = new DeleteMessage().setChatId(update.getMessage().getChatId()).setMessageId(update.getMessage().getMessageId());
        String text = update.getMessage().getText();
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        botUser.ifPresent(bUser -> {
            deleteMessage.setChatId(update.getMessage().getChatId()).setMessageId(bUser.getMessageId());
            bUser.setLastName(text);
            botUserRepository.save(bUser);
            Optional<User> user = userRepository.findFirstByTelegramId(update.getMessage().getFrom().getId());
            if (user.isPresent()) {
                User user1 = user.get();
                user1.setFirstName(bUser.getFirstName());
                user1.setLastName(text);
                userRepository.save(user1);
            }
        });
        return deleteMessage;
    }

    @Override
    public DeleteMessage deletePhoneNumber(Update update) {
        DeleteMessage deleteMessage = new DeleteMessage().setChatId(update.getMessage().getChatId()).setMessageId(update.getMessage().getMessageId());
        String text = update.getMessage().getText();
        if (isCheckPhoneNumber(update)) {
            Optional<User> user = userRepository.findByPhoneNumber(text);
            Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
            BotUser botUser1 = botUser.get();
            User user1;
            if (user.isPresent()) {
                user1 = user.get();
                user1.setTelegramId(update.getMessage().getFrom().getId());
                user1.setTelegramUsername(update.getMessage().getFrom().getUserName());
                user1.setChatId(update.getMessage().getChatId());
            } else {
                user1 = new User();
                user1.setTelegramId(update.getMessage().getFrom().getId());
                user1.setTelegramUsername(update.getMessage().getFrom().getUserName());
                user1.setChatId(update.getMessage().getChatId());
                user1.setPhoneNumber(text);
                user1.setLastName("");
                user1.setFirstName("");
            }
            user1.setRoles(roleRepository.findAllByName(RoleName.ROLE_USER));
            userRepository.save(user1);
            botUser1.setFirstName(user1.getFirstName());
            botUser1.setLastName(user1.getLastName());
            botUser1.setChatId(update.getMessage().getChatId());
            botUser1.setPhoneNumber(user1.getPhoneNumber());
            botUser1.setTgUserId(update.getMessage().getFrom().getId());
            botUserRepository.save(botUser1);
        }
        return deleteMessage;
    }

    @Override
    public DeleteMessage deleteRegion(Update update) {
        DeleteMessage deleteMessage = new DeleteMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        botUser.ifPresent(bUser -> {
            String data = update.getCallbackQuery().getData();
            String regionId = data.substring(data.indexOf('#') + 1);
            Optional<Region> region = regionRepository.findById(Integer.parseInt(regionId));
            deleteMessage.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            bUser.setRegion(region.get());
            botUserRepository.save(bUser);
            Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
            user.ifPresent(user1 -> {
                user1.setRegion(region.get());
                userRepository.save(user1);
            });
        });
        return deleteMessage;
    }

    @Override
    public DeleteMessage deleteActivity(Update update) {
        DeleteMessage deleteMessage = new DeleteMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        botUser.ifPresent(bUser -> {
            String data = update.getCallbackQuery().getData();
            String activityId = data.substring(data.indexOf('#') + 1);
            Optional<Activity> activity = activityRepository.findById(Integer.parseInt(activityId));
            deleteMessage.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            bUser.setActivity(activity.get());
            botUserRepository.save(bUser);
            Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
            user.ifPresent(user1 -> {
                user1.setActivity(activity.get());
                userRepository.save(user1);
            });
        });
        return deleteMessage;
    }

    @Override
    public EditMessageText setCompany(Update update) {
        EditMessageText editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        botUser.ifPresent(bUser -> {
            if (bUser.getCompany() == null) {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Компания номини киритинг");
                } else {
                    editMessageText.setText("Пожалуйста, введите название компании");
                }
            } else {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Сизнинг компаниянгиз: " + bUser.getCompany() + "\nМаълумотни янгилаш учун янги ном киритинг.");
                } else {
                    editMessageText.setText("Ваше компания: " + bUser.getCompany() + "\nДля обновления информации введите новое компания.");
                }
            }
            if (bUser.getLanguage().equals(BotConstants.KR)) {
                button1.setText("⬅️Орқага");
            } else {
                button1.setText("⬅️Назад");
            }
            button1.setCallbackData("BackProfile");
            row1.add(button1);
            rows.add(row1);
            inlineKeyboardMarkup.setKeyboard(rows);
            editMessageText.setReplyMarkup(inlineKeyboardMarkup);
            bUser.setState("SaveCompany");
            bUser.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            botUserRepository.save(bUser);
        });
        return editMessageText;
    }

    @Override
    public DeleteMessage deleteCompany(Update update) {
        String text = update.getMessage().getText();
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        DeleteMessage deleteMessage = new DeleteMessage();
        botUser.ifPresent(bUser -> {
            deleteMessage.setChatId(update.getMessage().getChatId()).setMessageId(bUser.getMessageId());
            bUser.setCompany(text);
            botUserRepository.save(bUser);
            Optional<User> user = userRepository.findByPhoneNumber(bUser.getPhoneNumber());
            if (user.isPresent()) {
                User user1 = user.get();
                user1.setCompany(text);
                userRepository.save(user1);
            }
        });
        return deleteMessage;
    }

    @Override
    public EditMessageText setPosition(Update update) {
        EditMessageText sendMessage = new EditMessageText()
                .setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            sendMessage.setText(BotConstants.POSITION_TEXT_KR);
        } else {
            sendMessage.setText(BotConstants.POSITION_TEXT_RU);
        }
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<Position> allPositions = positionRepository.findAll();
        for (int i = 0; i < allPositions.size(); i++) {
            List<InlineKeyboardButton> row1 = new ArrayList<>();
            InlineKeyboardButton button1 = new InlineKeyboardButton();
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                button1.setText(allPositions.get(i).getNameUz());
            } else {
                button1.setText(allPositions.get(i).getNameRu());
            }
            button1.setCallbackData("PositionId#" + allPositions.get(i).getId());
            row1.add(button1);
            rows.add(row1);
        }
        inlineKeyboardMarkup.setKeyboard(rows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    @Override
    public DeleteMessage deletePosition(Update update) {
        DeleteMessage deleteMessage = new DeleteMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        botUser.ifPresent(bUser -> {
            String data = update.getCallbackQuery().getData();
            String positionId = data.substring(data.indexOf('#') + 1);
            Optional<Position> position = positionRepository.findById(Integer.parseInt(positionId));
            deleteMessage.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            bUser.setPosition(position.get());
            botUserRepository.save(bUser);
            Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
            user.ifPresent(user1 -> {
                user1.setPosition(position.get());
                userRepository.save(user1);
            });
        });
        return deleteMessage;
    }

    @Override
    public EditMessageText setBirthDate(Update update) {
        EditMessageText editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        botUser.ifPresent(bUser -> {
            if (bUser.getBirthDate() == null) {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Туғилган кунингизни киритинг: Масалан, 06.05.1996");
                } else {
                    editMessageText.setText("Пожалуйста, введите свой день рождения: Например, 06.05.1996");
                }
            } else {
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Сизнинг туғилган кунингиз: " + bUser.getBirthDate() + "\nМаълумотни янгилаш учун қайтадан киритинг. Масалан, 06.05.1996");
                } else {
                    editMessageText.setText("Ваше день рождения: " + bUser.getBirthDate() + "\nДля обновления информации введите новое день рождения. Например, 06.05.1996");
                }
            }
            if (bUser.getLanguage().equals(BotConstants.KR)) {
                button1.setText("⬅️Орқага");
            } else {
                button1.setText("⬅️Назад");
            }
            button1.setCallbackData("BackProfile");
            row1.add(button1);
            rows.add(row1);
            inlineKeyboardMarkup.setKeyboard(rows);
            editMessageText.setReplyMarkup(inlineKeyboardMarkup);
            bUser.setState("SaveBirthDate");
            bUser.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            botUserRepository.save(bUser);
        });
        return editMessageText;
    }

    @Override
    public DeleteMessage deleteBirthDate(Update update) {
        String text = update.getMessage().getText();
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        DeleteMessage deleteMessage = new DeleteMessage();
        botUser.ifPresent(bUser -> {
            deleteMessage.setChatId(update.getMessage().getChatId()).setMessageId(bUser.getMessageId());
            bUser.setBirthDate(validateDateFormat(text));
            botUserRepository.save(bUser);
            Optional<User> user = userRepository.findByPhoneNumber(bUser.getPhoneNumber());
            if (user.isPresent()) {
                User user1 = user.get();
                user1.setBirthDate(validateDateFormat(text));
                userRepository.save(user1);
            }
        });
        return deleteMessage;
    }

    public Date validateDateFormat(String dateToValdate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        formatter.setLenient(false);
        Date parsedDate = null;
        try {
            parsedDate = formatter.parse(dateToValdate);
            System.out.println("++validated DATE TIME ++" + formatter.format(parsedDate));

        } catch (ParseException e) {
        }
        return parsedDate;
    }

    public boolean isCheckPhoneNumber(Update update) {
        String regex = "[0-9*#+() -]*";
        Pattern pattern = Pattern.compile(regex);
        if (update.getMessage().hasContact()) {
            Contact contact = update.getMessage().getContact();
            Matcher matcher = pattern.matcher(contact.getPhoneNumber());
            if (matcher.matches() && contact.getPhoneNumber().length() <= 13 && contact.getPhoneNumber().length() > 11) {
                return true;
            }
        } else if (update.getMessage().hasText()) {
            Matcher matcher = pattern.matcher(update.getMessage().getText());
            if (matcher.matches() && update.getMessage().getText().length() <= 13 && update.getMessage().getText().length() > 11) {
                return true;
            }
        }
        return false;
    }

    @Override
    public EditMessageText showAllEventData(Update update) {
        EditMessageText editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        List<InlineKeyboardButton> row3 = new ArrayList<>();
        List<InlineKeyboardButton> row4 = new ArrayList<>();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        InlineKeyboardButton button4 = new InlineKeyboardButton();

        if (user.isPresent() && !user.get().getFirstName().equals("") && !user.get().getLastName().equals("") && !user.get().getPhoneNumber().equals("")) {
            if (user.get().getFirstName() != null && user.get().getLastName() != null && user.get().getPhoneNumber() != null) {//todo:tekshirish
                User user1 = user.get();
                user1.setTelegramUsername(update.getCallbackQuery().getFrom().getUserName());
                user1.setTelegramId(botUser.get().getTgUserId());
                user1.setChatId(botUser.get().getChatId());
                user1.setBirthDate(botUser.get().getBirthDate());
                user1.setAware(botUser.get().getAware());
                user1.setActivity(botUser.get().getActivity());
                user1.setPosition(botUser.get().getPosition());
                user1.setCompany(botUser.get().getCompany());
                user1.setRegion(botUser.get().getRegion());
                userRepository.save(user1);

                if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                    editMessageText.setText("Муваффакият - тажриба натижаси.\n" +
                            "Тажриба - мағлубиятлар натижаси.\n" +
                            "\n" +
                            "Хар босиб ўтилган йул, уз изини колдиради.");
                    button2.setText("Навбатдаги тадбирлар");
                    button3.setText("Рўйхатдан ўтилган тадбирлар");
                    button4.setText("⬅️Орқага");
                } else {
                    editMessageText.setText("Муваффакият - тажриба натижаси.\n" +
                            "Тажриба - мағлубиятлар натижаси.\n" +
                            "\n" +
                            "Хар босиб ўтилган йул, уз изини колдиради.");
                    button2.setText("Предстоящие события");
                    button3.setText("Зарегистрированные события");
                    button4.setText("⬅️Назад");
                }
                button2.setCallbackData("UpcomingEvents");
                button3.setCallbackData("RegisterEvents");
                button4.setCallbackData("BackMain");
                row2.add(button2);
                row3.add(button3);
                row4.add(button4);
                rows.add(row2);
                rows.add(row3);
                rows.add(row4);
                inlineKeyboardMarkup.setKeyboard(rows);
                editMessageText.setReplyMarkup(inlineKeyboardMarkup);
            } else {
                User user1 = new User();
                user1.setFirstName(botUser.get().getFirstName());
                user1.setLastName(botUser.get().getLastName());
                user1.setPhoneNumber(botUser.get().getPhoneNumber());
                user1.setTelegramUsername(update.getCallbackQuery().getFrom().getUserName());
                user1.setTelegramId(update.getCallbackQuery().getFrom().getId());
                user1.setChatId(update.getCallbackQuery().getMessage().getChatId());
                user1.setBirthDate(botUser.get().getBirthDate());
                user1.setAware(botUser.get().getAware());
                user1.setActivity(botUser.get().getActivity());
                user1.setPosition(botUser.get().getPosition());
                user1.setCompany(botUser.get().getCompany());
                user1.setRegion(botUser.get().getRegion());
                userRepository.save(user1);
            }
        } else {
            editMessageText = notFullData(update);
        }
        return editMessageText;
    }

    @Override
    public EditMessageText notFullData(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        EditMessageText editMessageText = null;
        editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            editMessageText.setText("Сизнинг профил маълумотларингиз тўлиқ эмас. Илтимос маълумотларингизни тўлиқ киритинг.");
            button1.setText("Менинг профилим \uD83D\uDC64");
            button2.setText("⬅️Орқага");
        } else {
            editMessageText.setText("Информация в вашем профиле неполная. Пожалуйста, введите ваши полные данные.");
            button1.setText("Мой профил \uD83D\uDC64");
            button2.setText("⬅️Назад");
        }
        button1.setCallbackData("Profile");
        button2.setCallbackData("BackMain");
        row1.add(button1);
        row2.add(button2);
        rows.add(row1);
        rows.add(row2);
        inlineKeyboardMarkup.setKeyboard(rows);
        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        return editMessageText;
    }

    @Override
    public SendMessage notFullData2(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        SendMessage editMessageText = null;
        if (botUser.isPresent()) {
            editMessageText = new SendMessage().setParseMode(ParseMode.MARKDOWN).setChatId(update.getCallbackQuery().getMessage().getChatId());
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                editMessageText.setText("Сизнинг профил маълумотларингиз тўлиқ эмас. Илтимос маълумотларингизни тўлиқ киритинг.");
                button1.setText("Менинг профилим \uD83D\uDC64");
                button2.setText("⬅️Орқага");
            } else {
                editMessageText.setText("Информация в вашем профиле неполная. Пожалуйста, введите ваши полные данные.");
                button1.setText("Мой профил \uD83D\uDC64");
                button2.setText("⬅️Назад");
            }
            button1.setCallbackData("Profile");
            button2.setCallbackData("BackMain");
            row1.add(button1);
            row2.add(button2);
            rows.add(row1);
            rows.add(row2);
            inlineKeyboardMarkup.setKeyboard(rows);
            editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        }
        return editMessageText;
    }

    @Override
    public SendPhoto registerForByEvent(Update update) {
        String data = update.getCallbackQuery().getData();
        String eventId = data.substring(data.indexOf('#') + 1);
        Optional<Event> event = eventRepository.findById(UUID.fromString(eventId));
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        SendPhoto sendPhoto = null;
        if (botUser.isPresent()) {
            if (event.isPresent()) {
                Attachment photo = event.get().getPhoto();
                Optional<AttachmentContent> attachmentContent = attachmentContentRepository.findByAttachment(photo);

                List<Seat> emptySeats = seatRepository.findAllByEventAndPlaceStatusOrderByCreatedAt(event.get(), PlaceStatus.EMPTY);
                int maxRow = seatRepository.maxRow(event.get().getId());
                StringBuilder emptyText = new StringBuilder();
                for (int j = 1; j <= maxRow; j++) {
                    emptyText.append("\n");
                    List<InlineKeyboardButton> row1 = new ArrayList<>();
                    String price = "";
                    for (int i = 0; i < emptySeats.size(); i++) {
                        InlineKeyboardButton button1 = new InlineKeyboardButton();
                        if (emptySeats.get(i).getRow() == j) {
                            price = String.valueOf(emptySeats.get(i).getPrice());
                            button1.setText(emptySeats.get(i).getName());
                            button1.setCallbackData("SeatId#" + emptySeats.get(i).getId());
                            row1.add(button1);
                        }
                    }

                    String sum = String.format("%s", price);
                    if ((event.get().getEventType().getId() == 1 || event.get().getEventType().getId() == 3) && !price.equals("")) {
                        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                            emptyText.append(j).append("-қатор: *").append(myPrice(sum.substring(0, sum.length() - 2))).append("* cўм");
                        } else {
                            emptyText.append(j).append("-ряд: *").append(myPrice(sum.substring(0, sum.length() - 2))).append("* cўм");
                        }
                    } else if (!price.equals("")) {
                        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                            emptyText.append(j).append("-стол: *").append(myPrice(sum.substring(0, sum.length() - 2))).append("* cўм");
                        } else {
                            emptyText.append(j).append("-стол: *").append(myPrice(sum.substring(0, sum.length() - 2))).append("* cўм");
                        }
                    }
                    rows.add(row1);
                }
                if (attachmentContent.isPresent()) {
                    sendPhoto = new SendPhoto().setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
                    sendPhoto.setPhoto(photo.getName(), new ByteArrayInputStream(attachmentContent.get().getContent()));
                    if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                        if (emptySeats.size() > 0) {
                            sendPhoto.setCaption(event.get().getTitle() + " " + BotConstants.FREE_PLACE_KR + emptyText);
                        } else {
                            sendPhoto.setCaption("Ҳурматли *" + botUser.get().getFirstName() + " " + botUser.get().getLastName() + "*,\n" +
                                    "\n" +
                                    "Тадбиримизга қизиқиш билдирганингиз учун раҳмат, лекин жойлар қолмади, кейинги тадбирларимизда кўришгунча.\n");
                        }
                    } else {
                        if (emptySeats.size() > 0) {
                            sendPhoto.setCaption(event.get().getTitle() + " " + BotConstants.FREE_PLACE_RU + emptyText);
                        } else {
                            sendPhoto.setCaption("Уважаемый *" + botUser.get().getFirstName() + " " + botUser.get().getLastName() + "*, \n\nСпасибо за проявленный интерес к нашим мероприятим, но к сожалению нет свободных мест! До встречи на наших новых мероприятиях!");
                        }
                    }
                    inlineKeyboardMarkup.setKeyboard(rows);
                    sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
                }
            }

            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                button2.setText("⬅️Орқага");
            } else {
                button2.setText("⬅️Назад");
            }
        }
        BotUser user = botUser.get();
        button2.setCallbackData("MyProfile");
        user.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        botUserRepository.save(user);
        row2.add(button2);
        rows.add(row2);
        inlineKeyboardMarkup.setKeyboard(rows);
        return sendPhoto;
    }

    @Override
    public SendMessage showTypeForRegisterSeat(Update update) {
        Optional<BotUser> botUser;
        SendMessage editMessageText;
        String data = null;
        if (update.hasCallbackQuery()) {
            data = update.getCallbackQuery().getData();
            editMessageText = new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
            botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        } else {
            editMessageText = new SendMessage().setChatId(update.getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
            botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        }
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        String seatId;
        BotUser user = botUser.get();
        if (data != null) {
            seatId = data.substring(data.indexOf('#') + 1);
        } else {
            seatId = user.getSeatId();
        }
        user.setSeatId(seatId);
        botUserRepository.save(user);
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        if (botUser.isPresent()) {
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                editMessageText.setText("Тўлов турини танланг");
                button1.setText("Нақд пул");
                button2.setText("Тўлов картаси");
                button3.setText("⬅️Орқага");
            } else {
                editMessageText.setText("Пожалуйста, выберите тип оплаты");
                button1.setText("Наличные деньги");
                button2.setText("Платежная карта");
                button3.setText("⬅️Назад");
            }
            button1.setCallbackData("Money");
            button2.setCallbackData("Karta");
            button3.setCallbackData("Event");
            row1.add(button1);
            row1.add(button2);
            row2.add(button3);
            rows.add(row1);
            rows.add(row2);
            inlineKeyboardMarkup.setKeyboard(rows);
            editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        }
        return editMessageText;
    }

    @Override
    public EditMessageText payByMoneyForSeat(Update update) {
        EditMessageText editMessageText = new EditMessageText().setMessageId(update.getCallbackQuery().getMessage().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());

        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        List<InlineKeyboardButton> row3 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();
        InlineKeyboardButton button4 = new InlineKeyboardButton();

        botUser.ifPresent(mainBotUser -> {
            Optional<Seat> seat = seatRepository.findById(UUID.fromString(mainBotUser.getSeatId()));
            seat.ifPresent(mainSeat -> {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                Date date1 = new Date();
                Date date2 = new Date();
                date2.setTime(mainSeat.getEvent().getRegisterTime().getTime());
                String currentBronDate = simpleDateFormat.format(date1);
                String eventBeginDate = simpleDateFormat.format(date2);
                try {
                    DateTime dateTime1 = new DateTime(date1);
                    DateTime dateTime2 = new DateTime(date2);
                    String tomorrowDate = simpleDateFormat.format(new Date().getTime() + (24 * 60 * 60 * 1000));
                    int space = currentBronDate.indexOf(' ');
                    int t_space = tomorrowDate.indexOf(' ');
                    String time = eventBeginDate.substring(11);
                    if (mainBotUser.getLanguage().equals(BotConstants.KR)) {
                        if (Days.daysBetween(dateTime1, dateTime2).getDays() > 1) {
                            button1.setText(BotConstants.TODAY_KR);
                            button2.setText(BotConstants.TOMORROW_KR);
                            button3.setText(BotConstants.GO_TO_PAY_KR);
                            button1.setCallbackData("PayMoney#" + currentBronDate.substring(0, space) + " " + time);
                            button2.setCallbackData("PayMoney#" + tomorrowDate.substring(0, t_space) + " " + time);
                            button3.setCallbackData("PayMoney#" + eventBeginDate.substring(0, t_space) + " " + time);
                        } else if (Days.daysBetween(dateTime1, dateTime2).getDays() < 1 && Hours.hoursBetween(dateTime1, dateTime2).getHours() % 24 > 0) {
                            button1.setText(BotConstants.TODAY_KR);
                            button3.setText(BotConstants.GO_TO_PAY_KR);
                            button1.setCallbackData("PayMoney#" + currentBronDate.substring(0, space) + " " + time);
                            button3.setCallbackData("PayMoney#" + eventBeginDate.substring(0, t_space) + " " + time);
                        }
                    } else if (mainBotUser.getLanguage().equals(BotConstants.RU)) {
                        if (Days.daysBetween(dateTime1, dateTime2).getDays() > 1) {
                            button1.setText(BotConstants.TODAY_RU);
                            button2.setText(BotConstants.TOMORROW_RU);
                            button3.setText(BotConstants.GO_TO_PAY_RU);
                            button1.setCallbackData("PayMoney#" + currentBronDate.substring(0, space) + " " + time);
                            button2.setCallbackData("PayMoney#" + tomorrowDate.substring(0, t_space) + " " + time);
                            button3.setCallbackData("PayMoney#" + eventBeginDate.substring(0, t_space) + " " + time);
                        } else if (Days.daysBetween(dateTime1, dateTime2).getDays() < 1 && Hours.hoursBetween(dateTime1, dateTime2).getHours() % 24 > 0) {
                            button1.setText(BotConstants.TODAY_RU);
                            button3.setText(BotConstants.GO_TO_PAY_RU);
                            button1.setCallbackData("PayMoney#" + currentBronDate.substring(0, space) + " " + time);
                            button3.setCallbackData("PayMoney#" + eventBeginDate.substring(0, t_space) + " " + time);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            if (mainBotUser.getLanguage().equals(BotConstants.KR)) {
                button4.setText("⬅️Орқага");
                editMessageText.setText("Қачон тўлов қиласиз?");
            } else {
                button4.setText("⬅️Назад");
                editMessageText.setText("Когда вы платите?");
            }
            button4.setCallbackData("Event");
        });

        row1.add(button1);
        row1.add(button2);
        row2.add(button3);
        row3.add(button4);
        rows.add(row1);
        rows.add(row2);
        rows.add(row3);
        inlineKeyboardMarkup.setKeyboard(rows);
        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        return editMessageText;
    }

    @Override
    public SendPhoto showAcceptedForSeat(Update update) {
        SendPhoto sendPhoto = new SendPhoto().setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        InlineKeyboardButton button3 = new InlineKeyboardButton();

        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        String data = update.getCallbackQuery().getData();
        String bronDate = data.substring(data.indexOf('#') + 1);

        botUser.ifPresent(bUser -> {
            bUser.setExpireDate(bronDate);
            botUserRepository.save(bUser);
            Optional<Seat> seat = seatRepository.findById(UUID.fromString(bUser.getSeatId()));
            seat.ifPresent(mainSeat -> {
                String sum = String.format("%s", mainSeat.getPrice());
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < mainSeat.getEvent().getSpeakers().size(); i++) {
                    stringBuilder.append(mainSeat.getEvent().getSpeakers().get(i).getFirstName()).append(" ").append(mainSeat.getEvent().getSpeakers().get(i).getLastName()).append("\n");
                }
                Optional<Event> byId = eventRepository.findById(mainSeat.getEvent().getId());

                String startTimeWithSecond = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(byId.get().getStartTime());
                String openTimeWithSecond = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(byId.get().getRegisterTime());
                Attachment photo = mainSeat.getEvent().getPhoto();
                Optional<AttachmentContent> attachmentContent = attachmentContentRepository.findByAttachment(photo);
                sendPhoto.setPhoto(mainSeat.getEvent().getPhoto().getName(), new ByteArrayInputStream(attachmentContent.get().getContent()));
                int mainDay = 0;
                try {
                    mainDay = new SimpleDateFormat("dd.MM.yyyy").parse(startTimeWithSecond.substring(0, startTimeWithSecond.length() - 5)).getDay();
                    System.out.println("MainDay" + mainDay + " \n" + kr_week[mainDay]);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (bUser.getLanguage().equals(BotConstants.KR)) {
                    sendPhoto.setCaption("Ҳурматли *" + bUser.getFirstName() + " " + bUser.getLastName() + "*, \n" +
                            "\n" +
                            "Табриклаймиз, сиз *" + startTimeWithSecond.substring(startTimeWithSecond.length() - 5) + "*, *" + kr_week[mainDay] + "* куни бўлиб ўтадиган " + byId.get().getEventType().getNameUz() + " тадбирига тадбирига жой *ҳарид қилмоқдасиз. Тасдиқлаш тугмасини босинг*. \n" +
                            "\n" +
                            "\uD83D\uDC64Спикеримиз: *" + stringBuilder.toString() + "* \n" +
                            "\uD83D\uDCE2Мавзу: \"*" + byId.get().getTitle() + "*\"\n" +
                            "Тадбир тили: *" + byId.get().getLanguage().getNameUz() + "*\n" +
                            "\n" +
                            "\uD83D\uDD54Рўйҳатга олиш вақти: *" + openTimeWithSecond.substring(startTimeWithSecond.length() - 5) + "* дан\n" +
                            "\uD83D\uDD67Бошланиш вақти: *" + startTimeWithSecond.substring(openTimeWithSecond.length() - 5) + "* дан\n" +
                            "\n" +
                            "Танлаган жойингиз: *" + mainSeat.getName() + "*\n" +
                            "Жойингиз баҳоси: *" + myPrice(sum.substring(0, sum.length() - 2)) + "* сўм. \n" +
                            "Тўлов усули: Нақд пул" +
                            "\n\n" +
                            "Тадбирда кутиб қоламиз, кўришгунча!\n");
                } else {
                    sendPhoto.setCaption("Уважаемый *" + bUser.getFirstName() + " " + bUser.getLastName() + "*,\n" +
                            "\n" +
                            "Поздравляем, вы выкупили билет на мероприятие " + byId.get().getEventType().getNameRu() + ", которое будет проходить *" + startTimeWithSecond.substring(startTimeWithSecond.length() - 5) + "* " + (mainDay == 2 ? "во" : "в") + " *" + ru_week[mainDay] + "*. \n" +
                            "\n" +
                            "Спикер мероприятия: *" + stringBuilder.toString() + "*\n" +
                            "Тема: \"*" + byId.get().getTitle() + "*\"\n" +
                            "Язык: *" + byId.get().getLanguage().getNameRu() + "*\n" +
                            "\n" +
                            "Начало регистрации: с *" + openTimeWithSecond.substring(startTimeWithSecond.length() - 5) + "*\n" +
                            "Начало тренинга: с *" + startTimeWithSecond.substring(openTimeWithSecond.length() - 5) + "*\n" +
                            "\n" +
                            "Выбранное место: *" + mainSeat.getName() + "*\n" +
                            "Цена выбранного места: *" + myPrice(sum.substring(0, sum.length() - 2)) + "* сум. \n" +
                            "Форма оплаты: Наличные деньги" +
                            "\n\n" +
                            "До встречи на мероприятии!");
                }
            });

            if (bUser.getLanguage().equals(BotConstants.KR)) {
                button1.setText("✅" + BotConstants.ACCEPTED_KR);
                button2.setText("❌" + BotConstants.CLOSE_BRON_KR);
                button3.setText("⬅️Орқага");
            } else {
                button1.setText("✅" + BotConstants.ACCEPTED_RU);
                button2.setText("❌" + BotConstants.CLOSE_BRON_RU);
                button3.setText("⬅️Назад");
            }
            button1.setCallbackData("AcceptBron");
            button2.setCallbackData("NoAcceptBron");
            button3.setCallbackData("Event");

            row1.add(button1);
            row1.add(button2);
            row2.add(button3);
            rows.add(row1);
            rows.add(row2);
            inlineKeyboardMarkup.setKeyboard(rows);
            sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
        });
        return sendPhoto;

    }

    @Override
    public SendMessage acceptBron(Update update) {
        SendMessage sendMessage = new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        botUser.ifPresent(bUser -> {
            User user = userRepository.findFirstByTelegramId(bUser.getTgUserId()).get();
            Optional<Seat> seat = seatRepository.findById(UUID.fromString(bUser.getSeatId()));
            seat.ifPresent(mainSeat -> {
                if (mainSeat.getPlaceStatus().equals(PlaceStatus.EMPTY)) {
                    ReqUser reqUser = new ReqUser();
                    reqUser.setUserId(user.getId());
                    reqUser.setSeatId(mainSeat.getId());
                    reqUser.setPlaceStatus(PlaceStatus.BOOKED);
                    reqUser.setSale(user.getSale());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                    Timestamp timestamp = null;
                    try {
                        timestamp = new Timestamp(simpleDateFormat.parse(bUser.getExpireDate()).getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    reqUser.setBookedExpireDate(timestamp);
                    seatService.seatOrder(reqUser, mainSeat.getId(), user);
                    if (bUser.getLanguage().equals(BotConstants.KR)) {
                        button1.setText("⬅️Орқага");
                        sendMessage.setText("Ҳурматли "+user.getFirstName()+" "+user.getLastName()+".\n" +
                                "Сиз "+mainSeat.getEvent().getStartTime()+" санасида "+mainSeat.getEvent().getTitle()+" тадбирига "+mainSeat.getName()+" жойни банд қилдингиз.\n" +
                                "Эътиборингиз учун ташаккур.\n");
                    } else {
                        button1.setText("⬅️Назад");
                        sendMessage.setText("Уважаемый "+user.getFirstName()+" "+user.getLastName()+".\n" +
                                "Вы забронировали "+ mainSeat.getName()+" "+mainSeat.getEvent().getStartTime()+"на "+mainSeat.getEvent().getTitle()+" событие.\n" +
                                "Спасибо за ваше внимание.");
                    }
                    button1.setCallbackData("BackMain2");
                    row1.add(button1);
                    rows.add(row1);
                    inlineKeyboardMarkup.setKeyboard(rows);
                    sendMessage.setReplyMarkup(inlineKeyboardMarkup);

                    Role allByName = roleRepository.findByName(RoleName.ROLE_DIRECTOR).get();
                    List<uz.pdp.mfaktor.entity.User> allAdmins = userRepository.findAllByRolesIn(allByName);
                    for (int i = 0; i < allAdmins.size(); i++) {
                        if (allAdmins.get(i).getChatId() != null) {
                            try {
                                realMFaktorBot.execute(sendInformationToAdmin(update, allAdmins.get(i).getChatId(), 1));
                            } catch (TelegramApiException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    if (bUser.getLanguage().equals(BotConstants.KR)) {
                        sendMessage.setText(BotConstants.NOT_FOUND_SEAT_KR);
                        button1.setText("⬅️Орқага");
                    } else {
                        button1.setText("⬅️Назад");
                        sendMessage.setText(BotConstants.NOT_FOUND_SEAT_RU);
                    }
                    button1.setCallbackData("Event");
                    row1.add(button1);
                    rows.add(row1);
                    inlineKeyboardMarkup.setKeyboard(rows);
                    sendMessage.setReplyMarkup(inlineKeyboardMarkup);
                }

            });
        });
        return sendMessage;
    }

    @Override
    public SendMessage noAcceptBron(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        botUser.ifPresent(bUser -> {
            if (bUser.getLanguage().equals(BotConstants.KR)) {
                button1.setText("⬅️Орқага");
                sendMessage.setText("Тадбирга жой брон қилиш жараёни бекор қилинди.");
            } else {
                button1.setText("⬅️Назад");
                sendMessage.setText("Бронирование мероприятия отменено.");
            }
            button1.setCallbackData("Event");
            row1.add(button1);
            rows.add(row1);
            inlineKeyboardMarkup.setKeyboard(rows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        });
        return sendMessage;
    }

    @Override
    public SendMessage sendInformationToAdmin(Update update, Long chatId, int type) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId)
                .setParseMode(ParseMode.MARKDOWN);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();

        botUser.ifPresent(bUser -> {
            Optional<Seat> seat1 = seatRepository.findById(UUID.fromString(botUser.get().getSeatId()));
            seat1.ifPresent(mainSeat -> {
                StringBuilder stringBuilder = new StringBuilder();
                String sum = String.format("%s", mainSeat.getPrice());
                String startTimeWithSecond = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(mainSeat.getEvent().getStartTime());
                String openTimeWithSecond = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(mainSeat.getEvent().getRegisterTime());
                List<Seat> seats = seatRepository.findAllByEvent(mainSeat.getEvent());
                StringBuilder strBldr = new StringBuilder();
                mainSeat.getEvent().getSpeakers().forEach(speaker -> {
                    strBldr.append(speaker.getFirstName()).append(" ").append(speaker.getLastName()).append("\n");
                });

                DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                ResEventPaymentAndBalance balanceByEvent = eventService.getBalanceByEvent(mainSeat.getEvent().getId());

                double naqd = 0;
                double plastik = 0;
                double payme = 0;
                double boshqalar = 0;
                double all = 0;
                for (ResEventPayment resEventPayment : balanceByEvent.getResEventPayments()) {
                    if (resEventPayment.getPayTypeUz().equals("Naqd")) {
                        naqd = resEventPayment.getSum();
                    } else if (resEventPayment.getPayTypeUz().equals("Plastik")) {
                        plastik = resEventPayment.getSum();
                    } else if (resEventPayment.getPayTypeUz().equals("Payme")) {
                        payme = resEventPayment.getSum();
                    } else {
                        boshqalar = resEventPayment.getSum();
                    }
                    all += resEventPayment.getSum();
                }
                Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
                User user1 = user.get();
                stringBuilder.append("Брон санаси/вақти: *" + simpleDateFormat.format(new Date()) + "*\n" +
                        "\n" +
                        "\uD83D\uDC64 Мижоз: *" + user1.getLastName() + " " + user1.getFirstName() + "*\n" +
                        "\uD83D\uDD12 Банд қилинган жой: *" + mainSeat.getName() + "*\n" +
                        "\uD83D\uDCB4 Жой нарҳи: *" + myPrice(sum.substring(0, sum.length() - 2)) + "* сўм\n" +
                        "\n" +
                        "⏳ Тўлов муддати: *" + bUser.getExpireDate() + "*\n" +
                        "❗️ Статус: " + (type == 1 ? "*Брон*" : "*Харид*") + "\n" +
                        "\uD83D\uDCC4 Тўлов тури: " + (type == 1 ? "*Нақд*" : "*Плвстик*") + "\n" +
                        "\n" +
                        "===========================\n" +
                        "\n" +
                        "\uD83D\uDD22 Тадбир серияси: *" + mainSeat.getEvent().getSerialNumber() + "*\n" +
                        "▫ Тадбир тури: *" + mainSeat.getEvent().getEventType().getNameUz() + "*\n" +
                        "\uD83D\uDCC6 Тадбир санаси: *" + startTimeWithSecond.substring(0, startTimeWithSecond.length() - 5) + "* \n" +
                        "\uD83D\uDD52 Тадбир вақти: *" + startTimeWithSecond.substring(openTimeWithSecond.length() - 5) + "*\n" +
                        "\uD83D\uDC64 Спикер: *" + strBldr.toString() + "*\n" +
                        "\uD83D\uDCDA Мавзу: *" + mainSeat.getEvent().getTitle() + "*\n" +
                        "\n" +
                        "=============================\n" +
                        "\n" +
                        "Жами жойлар сони: *" + seats.size() + "*\n" +
                        "Брон қилинганлар сони: *" + seats.stream().filter(seat -> (seat.getPlaceStatus().equals(PlaceStatus.BOOKED) || seat.getPlaceStatus().equals(PlaceStatus.PARTLY_PAID))).count() + "*\n" +
                        "Сотиб олинганлар сони: *" + seats.stream().filter(seat -> seat.getPlaceStatus().equals(PlaceStatus.SOLD)).count() + "*\n" +
                        "Бўш ўринлар сони: *" + seats.stream().filter(seat -> seat.getPlaceStatus().equals(PlaceStatus.EMPTY)).count() + "*\n" +
                        "Меҳмонлар сони: *" + seats.stream().filter(seat -> seat.getPlaceStatus().equals(PlaceStatus.VIP)).count() + "*\n" +
                        "\n" +
                        "=============================\n" +
                        "\n" +
                        "\uD83D\uDCB5Balance:\n" +
                        "▪️Нақд:" + decimalFormat.format(naqd) + "\n" +
                        "▪️Пластик:" + decimalFormat.format(plastik) + "\n" +
                        "▪️Payme:" + decimalFormat.format(payme) + "\n" +
                        "▪️Пул ўтказиш:" + decimalFormat.format(boshqalar) + "\n" +
                        "\n" +
                        "\uD83D\uDCB5Хаммаси:\n" + decimalFormat.format(all)
                );

                sendMessage.setText(stringBuilder.toString());

            });

            if (bUser.getLanguage().equals(BotConstants.KR)) {
                button1.setText(BotConstants.HOME_KR);
            } else {
                button1.setText(BotConstants.HOME_RU);
            }
            button1.setCallbackData("BackMain");
            row1.add(button1);
            rows.add(row1);
            inlineKeyboardMarkup.setKeyboard(rows);
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        });
        return sendMessage;

    }

    @Override
    public <T> T payByPlasticForSeat(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        T message = null;
        SendInvoice invoice = new SendInvoice();
        SendMessage sendMessage;

        if (botUser.isPresent()) {
            BotUser bUser = botUser.get();
            Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
            Optional<Seat> seat = seatRepository.findById(UUID.fromString(bUser.getSeatId()));
            if (seat.get().getPlaceStatus().equals(PlaceStatus.EMPTY)) {
                Seat seat1 = seat.get();
                StringBuilder stringBuilder = new StringBuilder();
                seat1.getEvent().getSpeakers().forEach(spiker -> {
                    stringBuilder.append(spiker.getFirstName() + " " + spiker.getLastName()+" ");
                });
                seat1.setPlaceStatus(PlaceStatus.BOOKED);
                seat1.setUser(user.get());
                seatRepository.save(seat1);
                List<LabeledPrice> prices = new ArrayList<>();
                prices.add(new LabeledPrice(seat.get().getEvent().getTitle() + "-" + seat.get().getName(),
                        seat.get().getPrice().intValue() * 100));
                invoice.setChatId(update.getCallbackQuery().getMessage().getChat().getId().intValue())
                        .setTitle(seat.get().getEvent().getSerialNumber() + "." + stringBuilder.toString() + " \uD83D\uDD39 " + seat1.getEvent().getTitle())
                        .setDescription("Жой номи: " + seat1.getName() + "\nТўловни амалга оширинг.")
                        .setPayload(seat.get().getEvent().getId().toString())
                        .setProviderToken(providerToken)
                        .setStartParameter("busycube")
                        .setCurrency("UZS")
                        .setPrices(prices);
                message = (T) invoice;
            } else {
                sendMessage = sendErrorMessage(update);
                message = (T) sendMessage;
            }
        }
        return message;
    }

    @Override
    public SendPhoto successFullPayTypePlastic(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        Optional<User> user = userRepository.findFirstByTelegramId(update.getMessage().getFrom().getId());
        Seat seat1 = seatRepository.findById(UUID.fromString(botUser.get().getSeatId())).get();

        String sum = String.format("%s", seat1.getPrice());
        SendPhoto sendPhoto = new SendPhoto().setChatId(update.getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < seat1.getEvent().getSpeakers().size(); i++) {
            stringBuilder.append(seat1.getEvent().getSpeakers().get(i).getFirstName()).append(" ").append(seat1.getEvent().getSpeakers().get(i).getLastName()).append("\n");
        }
        Optional<Event> byId = eventRepository.findById(seat1.getEvent().getId());
        String startTime = new SimpleDateFormat("dd.MM.yyyy").format(byId.get().getStartTime());
        Calendar c = Calendar.getInstance();
        c.setTime(byId.get().getStartTime());

        String startTimeWithSecond = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(byId.get().getStartTime());
        String openTimeWithSecond = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(byId.get().getRegisterTime());
        Attachment photo = seat1.getEvent().getPhoto();
        Optional<AttachmentContent> attachmentContent = attachmentContentRepository.findByAttachment(photo);
        sendPhoto.setPhoto(seat1.getEvent().getPhoto().getName(), new ByteArrayInputStream(attachmentContent.get().getContent()));
        int mainDay = 0;
        try {
            mainDay = new SimpleDateFormat("dd.MM.yyyy").parse(startTime).getDay();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            sendPhoto.setCaption("Ҳурматли *" + user.get().getFirstName() + " " + user.get().getLastName() + "*, \n" +
                    "\n" +
                    "Табриклаймиз, сиз *" + startTime + "*, *" + kr_week[mainDay] + "* куни бўлиб ўтадиган " + byId.get().getEventType().getNameUz() + " тадбирига тадбирига жой *ҳарид қилдингиз*. \n" +
                    "\n" +
                    "\uD83D\uDC64Спикеримиз: *" + stringBuilder.toString() + "* \n" +
                    "\uD83D\uDCE2Мавзу: \"*" + byId.get().getTitle() + "*\"\n" +
                    "Тадбир тили: *" + byId.get().getLanguage().getNameUz() + "*\n" +
                    "\n" +
                    "\uD83D\uDD54Рўйҳатга олиш вақти: *" + openTimeWithSecond.substring(startTimeWithSecond.length() - 5) + "* дан\n" +
                    "\uD83D\uDD67Бошланиш вақти: *" + startTimeWithSecond.substring(openTimeWithSecond.length() - 5) + "* дан\n" +
                    "\n" +
                    "Танлаган жойингиз: *" + seat1.getName() + "*\n" +
                    "Жойингиз баҳоси: *" + myPrice(sum.substring(0, sum.length() - 2)) + "* сўм. \n" +
                    "Тўлов усули: ПайМе" +
                    "\n\n" +
                    "Тадбирда кутиб қоламиз, кўришгунча!\n");
        } else {

            sendPhoto.setCaption("Уважаемый *" + user.get().getFirstName() + " " + user.get().getLastName() + "*,\n" +
                    "\n" +
                    "Поздравляем, вы выкупили билет на мероприятие " + byId.get().getEventType().getNameRu() + ", которое будет проходить *" + startTime + "* " + (mainDay == 2 ? "во" : "в") + " *" + ru_week[mainDay] + "*. \n" +
                    "\n" +
                    "Спикер мероприятия: *" + stringBuilder.toString() + "*\n" +
                    "Тема: \"*" + byId.get().getTitle() + "*\"\n" +
                    "Язык: *" + byId.get().getLanguage().getNameRu() + "*\n" +
                    "\n" +
                    "Начало регистрации: с *" + openTimeWithSecond.substring(startTimeWithSecond.length() - 5) + "*\n" +
                    "Начало тренинга: с *" + startTimeWithSecond.substring(openTimeWithSecond.length() - 5) + "*\n" +
                    "\n" +
                    "Выбранное место: *" + seat1.getName() + "*\n" +
                    "Цена выбранного места: *" + myPrice(sum.substring(0, sum.length() - 2)) + "* сум. \n" +
                    "Форма оплаты: PayMe" +
                    "\n\n" +
                    "До встречи на мероприятии!");
        }
        return sendPhoto;
    }

    @Override
    public <T> T allEvents(Update update) {
        T message = null;
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        EditMessageText editMessageText;
        SendMessage sendMessage;
        StringBuilder stringBuilder = new StringBuilder();
        if (botUser.isPresent()) {
            List<Event> allTopEvents = eventRepository.findAllByStatusOrderByStartTime(EventStatus.OPEN);
            editMessageText = new EditMessageText().setMessageId(botUser.get().getMessageId()).setChatId(update.getCallbackQuery().getMessage().getChatId());
            sendMessage = new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                button2.setText("⬅️Орқага");
                stringBuilder.append("\uD83D\uDD25 Навбатдаги тадбирлар\n\n");
            } else {
                button2.setText("⬅️Назад");
                stringBuilder.append("\uD83D\uDD25 Предстоящие события\n\n");
            }
            allTopEvents.forEach(event -> {
                StringBuilder speakers = new StringBuilder();
                event.getSpeakers().forEach(user -> {
                    speakers.append(user.getFirstName()).append(" ").append(user.getLastName()+" ");
                });

                Timestamp startTime = event.getStartTime();
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                String time = format.format(startTime);
                stringBuilder.append("\uD83D\uDD25 ")
                        .append(event.getSerialNumber())
                        .append(".")
                        .append(speakers.toString())
                        .append(" \uD83D\uDD39 ")
                        .append(event.getTitle())
                        .append(" \uD83D\uDD54").append(time).append("\n\n");
                List<InlineKeyboardButton> row = new ArrayList<>();
                InlineKeyboardButton button = new InlineKeyboardButton();
                button.setText(event.getTitle());
                button.setCallbackData("EventId#" + event.getId());
                row.add(button);
                rows.add(row);
            });

            button2.setCallbackData("Event");
            row2.add(button2);
            rows.add(row2);
            inlineKeyboardMarkup.setKeyboard(rows);
            if (botUser.get().getMessageId().equals(update.getCallbackQuery().getMessage().getMessageId())) {
                editMessageText.setText(stringBuilder.toString());
                editMessageText.setReplyMarkup(inlineKeyboardMarkup);
                message = (T) editMessageText;
            } else {
                sendMessage.setText(stringBuilder.toString());
                sendMessage.setReplyMarkup(inlineKeyboardMarkup);
                message = (T) sendMessage;
            }
        }
        return message;
    }

    @Override
    public SendMessage allRegisterEvents(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
        Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
        List<Seat> seats = seatRepository.findAllByUser(user.get());
        if (seats.size() > 0) {
            StringBuilder mainStringBuilder = new StringBuilder();
            for (int i = 0; i < seats.size(); i++) {
                List<InlineKeyboardButton> row = new ArrayList<>();
                InlineKeyboardButton button = new InlineKeyboardButton();
                StringBuilder stringBuilder1 = new StringBuilder();
                List<uz.pdp.mfaktor.entity.User> speakers = seats.get(i).getEvent().getSpeakers();
                for (int j = 0; j < speakers.size(); j++) {
                    stringBuilder1.append(speakers.get(j).getFirstName()).append(" ").append(speakers.get(j).getLastName()+" ");
                }
                button.setText(seats.get(i).getName()+". "+seats.get(i).getEvent().getSerialNumber() + ". " + stringBuilder1.toString() + BotConstants.SPLITTER + seats.get(i).getEvent().getTitle() + " " + seats.get(i).getEvent().getRegisterTime().toString().substring(0, 16) + "\n\n");
                button.setCallbackData("RegisterSeatId#" + seats.get(i).getId());
                mainStringBuilder.append(seats.get(i).getEvent().getSerialNumber()).append(". ").append(stringBuilder1.toString()).append(BotConstants.SPLITTER).append(seats.get(i).getEvent().getTitle()).append(" ").append(seats.get(i).getEvent().getRegisterTime().toString().substring(0, 16)).append(botUser.get().language.equals(BotConstants.KR)?"\nБанд қилинган жой: "+seats.get(i).getName():"\nЗанято место: "+seats.get(i).getName()).append("\n\n");
                row.add(button);
                rows.add(row);
            }
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                sendMessage.setText(BotConstants.REGISTERED_EVENT_KR + "\n\n" + mainStringBuilder.toString());
                inlineKeyboardButton.setText(BotConstants.BACK_KR);
            } else {
                sendMessage.setText(BotConstants.REGISTERED_EVENT_RU + "\n\n" + mainStringBuilder.toString());
                inlineKeyboardButton.setText(BotConstants.BACK_RU);
            }
            inlineKeyboardButton.setCallbackData("Event");
            row2.add(inlineKeyboardButton);
            rows.add(row2);
            inlineKeyboardMarkup.setKeyboard(rows);
        } else {
            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                inlineKeyboardButton.setText(BotConstants.BACK_KR);
                sendMessage.setText(BotConstants.NOT_FOUND_BY_USER_FOR_EVENT_KR);
            } else {
                inlineKeyboardButton.setText(BotConstants.BACK_RU);
                sendMessage.setText(BotConstants.NOT_FOUND_BY_USER_FOR_EVENT_RU);
            }
            inlineKeyboardButton.setCallbackData("Event");
            row2.add(inlineKeyboardButton);
            rows.add(row2);
            inlineKeyboardMarkup.setKeyboard(rows);
        }
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    @Override
    public SendPhoto showOneRegisterEvent(Update update) {
        SendPhoto sendPhoto = new SendPhoto().setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row1 = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        InlineKeyboardButton button1 = new InlineKeyboardButton();
        InlineKeyboardButton button2 = new InlineKeyboardButton();

        String data = update.getCallbackQuery().getData();
        String seatId = data.substring(data.indexOf('#') + 1);
        Seat seat = seatRepository.findById(UUID.fromString(seatId)).get();
        StringBuilder stringBuilder = new StringBuilder();
        String sum = String.format("%s", seat.getPrice());
        String startTime = new SimpleDateFormat("dd.MM.yyyy").format(seat.getEvent().getStartTime());
        String startTimeWithSecond = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(seat.getEvent().getStartTime());
        String openTimeWithSecond = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(seat.getEvent().getRegisterTime());

        String bronDate = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(seat.getBookedExpireDate());
        StringBuilder strBldr = new StringBuilder();
        seat.getEvent().getSpeakers().forEach(speaker -> {
            strBldr.append(speaker.getFirstName()).append(" ").append(speaker.getLastName()).append("\n");
        });

        Attachment photo = seat.getEvent().getPhoto();
        Optional<AttachmentContent> attachmentContent = attachmentContentRepository.findByAttachment(photo);
        sendPhoto.setPhoto(seat.getEvent().getPhoto().getName(), new ByteArrayInputStream(attachmentContent.get().getContent()));

        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            stringBuilder.append("Брон санаси/вақти: *" + bronDate + "*\n" +
                    "\n" +
                    "\uD83D\uDC64 Мижоз: *" + seat.getUser().getLastName() + " " + seat.getUser().getFirstName() + "*\n" +
                    "\uD83D\uDD12 Банд қилинган жой: *" + seat.getName() + "*\n" +
                    "\uD83D\uDCB4 Жой нарҳи: *" + myPrice(sum.substring(0, sum.length() - 2)) + "* сўм\n" +
                    "\n" +
                    "⏳ Тўлов муддати: *" + seat.getEvent().getStartTime() + "*\n" +
                    "❗️ Статус: *Брон*\n" +
                    "\uD83D\uDCC4 Тўлов тури: *Нақд*" + "\n" +
                    "\n" +
                    "===========================\n" +
                    "\n" +
                    "\uD83D\uDD22 Тадбир серияси: *" + seat.getEvent().getSerialNumber() + "*\n" +
                    "▫ Тадбир тури: *" + seat.getEvent().getEventType().getNameUz() + "*\n" +
                    "\uD83D\uDCC6 Тадбир санаси: *" + startTime + "* \n" +
                    "\uD83D\uDD52 Тадбир вақти: *" + startTimeWithSecond.substring(openTimeWithSecond.length() - 5) + "*\n" +
                    "\uD83D\uDC64 Спикер: *" + strBldr.toString() + "*\n" +
                    "\uD83D\uDCDA Мавзу: *" + seat.getEvent().getTitle() + "*");
            button1.setText(BotConstants.CLOSE_BRON_KR);
            button2.setText(BotConstants.BACK_KR);
        } else {
            stringBuilder.append("Дата/время бронирования: *" + bronDate + "*\n" +
                    "\n" +
                    "\uD83D\uDC64 Клиент: *" + seat.getUser().getLastName() + " " + seat.getUser().getFirstName() + "*\n" +
                    "\uD83D\uDD12 Занято место: *" + seat.getName() + "*\n" +
                    "\uD83D\uDCB4 Цена места: *" + myPrice(sum.substring(0, sum.length() - 2)) + "* сум\n" +
                    "\n" +
                    "⏳ Период оплаты: *" + botUser.get().getExpireDate() + "*\n" +
                    "❗️ Статус: *Брон*\n" +
                    "\uD83D\uDCC4 Тип оплаты: *Нақд*" + "\n" +
                    "\n" +
                    "===========================\n" +
                    "\n" +
                    "\uD83D\uDD22 Серия событий: *" + seat.getEvent().getSerialNumber() + "*\n" +
                    "▫ Тип события: *" + seat.getEvent().getEventType().getNameUz() + "*\n" +
                    "\uD83D\uDCC6 Дата события: *" + startTime + "* \n" +
                    "\uD83D\uDD52 Время события: *" + startTimeWithSecond.substring(openTimeWithSecond.length() - 5) + "*\n" +
                    "\uD83D\uDC64 Спикер: *" + strBldr.toString() + "*\n" +
                    "\uD83D\uDCDA Тема: *" + seat.getEvent().getTitle() + "*");
            button1.setText(BotConstants.CLOSE_BRON_RU);
            button2.setText(BotConstants.BACK_RU);
        }
        sendPhoto.setCaption(stringBuilder.toString());
        button1.setCallbackData("CloseBron#" + seat.getId());
        button2.setCallbackData("RegisterEvents");
        row1.add(button1);
        row2.add(button2);
        rows.add(row1);
        rows.add(row2);
        inlineKeyboardMarkup.setKeyboard(rows);
        sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
        return sendPhoto;
    }

    @Override
    public SendMessage closeBron(Update update) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();

        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        String data = update.getCallbackQuery().getData();
        String seatId = data.substring(data.indexOf('#') + 1);
        Optional<Seat> seat = seatRepository.findById(UUID.fromString(seatId));
        seat.ifPresent(seat1 -> {
            seat1.setPlaceStatus(PlaceStatus.EMPTY);
            seat1.setUser(null);
            seatRepository.save(seat1);
        });
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            sendMessage.setText("✅" + seat.get().getEvent().getTitle() + BotConstants.CLOSE_BRON_TEXT_KR);
            inlineKeyboardButton.setText(BotConstants.BACK_KR);
        } else {
            sendMessage.setText("✅" + BotConstants.CLOSE_BRON_TEXT_RU + seat.get().getEvent().getTitle() + ".");
            inlineKeyboardButton.setText(BotConstants.BACK_RU);
        }
        inlineKeyboardButton.setCallbackData("RegisterEvents");
        row.add(inlineKeyboardButton);
        rows.add(row);
        inlineKeyboardMarkup.setKeyboard(rows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);
        return sendMessage;
    }

    @Override
    public SendPhoto referalLink(Update update) {
        String eventName = update.getMessage().getText().substring(7);
        Optional<Event> event = eventRepository.findBySerialNumber(eventName);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row2 = new ArrayList<>();
        InlineKeyboardButton button2 = new InlineKeyboardButton();
        SendPhoto sendPhoto = null;
        BotUser botUser1;
        if (!botUser.isPresent()) {
            BotUser bUser = new BotUser();
            bUser.setTgUserId(update.getMessage().getFrom().getId());
            bUser.setLanguage("KR");
            bUser.setMessageId(update.getMessage().getMessageId());
            botUser1 = botUserRepository.save(bUser);
        } else {
            botUser1 = botUser.get();
            botUser1.setMessageId(update.getMessage().getMessageId());
            botUserRepository.save(botUser1);
        }
        if (event.isPresent()) {
            Attachment photo = event.get().getPhoto();
            Optional<AttachmentContent> attachmentContent = attachmentContentRepository.findByAttachment(photo);

            List<Seat> emptySeats = seatRepository.findAllByEventAndPlaceStatusOrderByCreatedAt(event.get(), PlaceStatus.EMPTY);
            int maxRow = seatRepository.maxRow(event.get().getId());
            StringBuilder emptyText = new StringBuilder();
            for (int j = 1; j <= maxRow; j++) {
                emptyText.append("\n");
                List<InlineKeyboardButton> row1 = new ArrayList<>();
                String price = "";
                for (int i = 0; i < emptySeats.size(); i++) {
                    InlineKeyboardButton button1 = new InlineKeyboardButton();
                    if (emptySeats.get(i).getRow() == j) {
                        price = String.valueOf(emptySeats.get(i).getPrice());
                        button1.setText(emptySeats.get(i).getName());
                        button1.setCallbackData("SeatId#" + emptySeats.get(i).getId());
                        row1.add(button1);
                    }
                }

                String sum = String.format("%s", price);
                if ((event.get().getEventType().getId() == 1 || event.get().getEventType().getId() == 3) && !price.equals("")) {
                    if (botUser1.getLanguage().equals(BotConstants.KR)) {
                        emptyText.append(j).append("-қатор: *").append(myPrice(sum.substring(0, sum.length() - 2))).append("* cўм");
                    } else {
                        emptyText.append(j).append("-ряд: *").append(myPrice(sum.substring(0, sum.length() - 2))).append("* cўм");
                    }
                } else if (!price.equals("")) {
                    if (botUser1.getLanguage().equals(BotConstants.KR)) {
                        emptyText.append(j).append("-стол: *").append(myPrice(sum.substring(0, sum.length() - 2))).append("* cўм");
                    } else {
                        emptyText.append(j).append("-стол: *").append(myPrice(sum.substring(0, sum.length() - 2))).append("* cўм");
                    }
                }
                rows.add(row1);
            }
            if (attachmentContent.isPresent()) {
                sendPhoto = new SendPhoto().setChatId(update.getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
                sendPhoto.setPhoto(photo.getName(), new ByteArrayInputStream(attachmentContent.get().getContent()));
                if (botUser1.getLanguage().equals(BotConstants.KR)) {
                    if (emptySeats.size() > 0) {
                        sendPhoto.setCaption(event.get().getTitle() + " " + BotConstants.FREE_PLACE_KR + emptyText);
                    } else {
                        sendPhoto.setCaption("Ҳурматли *" + botUser1.getFirstName() + " " + botUser1.getLastName() + "*,\n" +
                                "\n" +
                                "Тадбиримизга қизиқиш билдирганингиз учун раҳмат, лекин жойлар қолмади, кейинги тадбирларимизда кўришгунча.\n");
                    }
                } else {
                    if (emptySeats.size() > 0) {
                        sendPhoto.setCaption(event.get().getTitle() + " " + BotConstants.FREE_PLACE_RU + emptyText);
                    } else {
                        sendPhoto.setCaption("Уважаемый *" + botUser1.getFirstName() + " " + botUser1.getLastName() + "*, \n\nСпасибо за проявленный интерес к нашим мероприятим, но к сожалению нет свободных мест! До встречи на наших новых мероприятиях!");
                    }
                }
                inlineKeyboardMarkup.setKeyboard(rows);
                sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
            }
        }

        if (botUser1.getLanguage().equals(BotConstants.KR)) {
            button2.setText("⬅️Орқага");
        } else {
            button2.setText("⬅️Назад");
        }
        button2.setCallbackData("MyProfile");
        row2.add(button2);
        rows.add(row2);
        inlineKeyboardMarkup.setKeyboard(rows);
        return sendPhoto;
    }

    @Override
    public SendMessage writeFirstName(Update update) {
        String data = update.getCallbackQuery().getData();
        String seatId;
        seatId = data.substring(data.indexOf('#') + 1);

        SendMessage editMessageText = new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        editMessageText.setText(BotConstants.REG_WRITE_FIRST_KR);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        BotUser bUser = botUser.get();
        bUser.setState("FirstName");
        bUser.setSeatId(seatId);
        botUserRepository.save(bUser);
        return editMessageText;
    }

    @Override
    public SendMessage writeLastName(Update update) {
        SendMessage editMessageText = new SendMessage().setChatId(update.getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        editMessageText.setText(BotConstants.REG_WRITE_LAST_KR);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        BotUser bUser = botUser.get();
        bUser.setFirstName(update.getMessage().getText());
        bUser.setState("LastName");
        botUserRepository.save(bUser);
        return editMessageText;
    }

    @Override
    public SendMessage writePhoneNumber(Update update) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true).setResizeKeyboard(true);
        List<KeyboardRow> rows = new ArrayList<>();
        KeyboardRow keyboardRow = new KeyboardRow();
        KeyboardButton button = new KeyboardButton().setRequestContact(true).setText(BotConstants.SHARE_CONTACT_KR);
        keyboardRow.add(button);
        rows.add(keyboardRow);
        replyKeyboardMarkup.setKeyboard(rows);
        SendMessage editMessageText = new SendMessage().setChatId(update.getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        editMessageText.setText(BotConstants.SHARE_CONTACT_DESCRIPTION_KR);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        BotUser bUser = botUser.get();
        bUser.setLastName(update.getMessage().getText());
        bUser.setState("PhoneNumber");
        botUserRepository.save(bUser);
        editMessageText.setReplyMarkup(replyKeyboardMarkup);
        return editMessageText;
    }

    @Override
    public SendMessage saveAllDataUser(Update update) {
        ReplyKeyboardRemove replyKeyboardRemove = new ReplyKeyboardRemove();
        SendMessage editMessageText = new SendMessage().setChatId(update.getMessage().getChatId()).setParseMode(ParseMode.MARKDOWN);
        editMessageText.setText(BotConstants.SUCCESSFULLY_REGISTERED_KR);
        editMessageText.setReplyMarkup(replyKeyboardRemove);
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        BotUser bUser = botUser.get();
        String phone_number;
        if (update.getMessage().hasContact()) {
            phone_number = update.getMessage().getContact().getPhoneNumber();
        } else {
            phone_number = update.getMessage().getText();
        }
        bUser.setPhoneNumber(phone_number);
        bUser.setTgUserId(update.getMessage().getFrom().getId());
        bUser.setState("SaveAllData");
        botUserRepository.save(bUser);
        User user = new User();
        user.setRoles(roleRepository.findAllByName(RoleName.ROLE_USER));
        user.setFirstName(bUser.getFirstName());
        user.setLastName(bUser.getLastName());
        user.setPhoneNumber(bUser.getPhoneNumber());
        user.setTelegramId(bUser.getTgUserId());
        userRepository.save(user);
        return editMessageText;
    }


    @Override
    public SendLocation showAddress(Update update) {
        SendLocation sendLocation = new SendLocation();
        sendLocation.setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setLongitude(69.274492f).setLatitude(41.29227f);
        return sendLocation;
    }

    @Override
    public SendPhoto showImage(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        InlineKeyboardButton button = new InlineKeyboardButton();
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            button.setText("⬅️Орқага");
        } else {
            button.setText("⬅️Назад");
        }
        button.setCallbackData("BackMain2");

        SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        try {
            sendPhoto.setPhoto("impulse.jpg", new ClassPathResource("/impulse.jpg").getInputStream());
            sendPhoto.setCaption("\uD83C\uDFE2Манзил: *Тошкент шаҳар, Миробод тумани, Авлиё Ота кўчаси-4, Техномарт биноси пастки қаватида*.\n\n\uD83D\uDCCDМўлжал: *Миробод бозори*.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        row.add(button);
        rows.add(row);
        inlineKeyboardMarkup.setKeyboard(rows);
        sendPhoto.setReplyMarkup(inlineKeyboardMarkup);
        return sendPhoto;
    }

    @Override
    public EditMessageText cash(Update update) {
        EditMessageText editMessageText = new EditMessageText().setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setMessageId(update.getCallbackQuery().getMessage().getMessageId());
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();
        InlineKeyboardButton button = new InlineKeyboardButton();

        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            button.setText(BotConstants.BACK_KR);
        } else {
            button.setText(BotConstants.BACK_RU);
        }
        button.setCallbackData("BackMain");
        row.add(button);
        rows.add(row);
        inlineKeyboardMarkup.setKeyboard(rows);

        List<ResBalanceByPayType> resBalanceByPayTypes = eventService.resBalanceByPayTypes();

        DecimalFormat decimalFormat = new DecimalFormat("###,###,###");

        double naqd = 0;
        double plastik = 0;
        double payme = 0;
        double boshqalar = 0;
        double all = 0;
        double dailyIncomeNaqd = 0;
        double dailyIncomePlastik = 0;
        double dailyIncomePayme = 0;
        double dailyIncomeBoshqalar = 0;

        double dailyOutcomeNaqd = 0;
        double dailyOutcomePlastik = 0;
        double dailyOutcomePayme = 0;
        double dailyOutcomeBoshqalar = 0;


        for (ResBalanceByPayType resBalanceByPayType : resBalanceByPayTypes) {
            if (resBalanceByPayType.getPayTypeUz().equals("Naqd")) {
                naqd = resBalanceByPayType.getBalance();
                dailyIncomeNaqd = resBalanceByPayType.getDailyIncome();
                dailyOutcomeNaqd = resBalanceByPayType.getDailyOutcome();
            } else if (resBalanceByPayType.getPayTypeUz().equals("Plastik")) {
                plastik = resBalanceByPayType.getBalance();
                dailyIncomePlastik = resBalanceByPayType.getDailyIncome();
                dailyOutcomePlastik = resBalanceByPayType.getDailyOutcome();
            } else if (resBalanceByPayType.getPayTypeUz().equals("Payme")) {
                payme = resBalanceByPayType.getBalance();
                dailyIncomePayme = resBalanceByPayType.getDailyIncome();
                dailyOutcomePayme = resBalanceByPayType.getDailyOutcome();
            } else {
                boshqalar = resBalanceByPayType.getBalance();
                dailyIncomeBoshqalar = resBalanceByPayType.getDailyIncome();
                dailyOutcomeBoshqalar = resBalanceByPayType.getDailyOutcome();
            }
            all += resBalanceByPayType.getBalance();
        }

        SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String format = sd.format(new Date());

        editMessageText.setText(
                "\uD83D\uDCB5Balance:\n" +
                        "▪️Нақд:" + decimalFormat.format(naqd) + "\n" +
                        "▪️Пластик:" + decimalFormat.format(plastik) + "\n" +
                        "▪️Payme:" + decimalFormat.format(payme) + "\n" +
                        "▪️Пул ўтказиш:" + decimalFormat.format(boshqalar) + "\n" +
                        "\n" +
                        "\uD83D\uDCB5ALL:\n" + decimalFormat.format(all) +
                        "\n" +
                        "==================================\n" +
                        "\n" +
                        "\uD83D\uDCB5Бугунги тушумлар:\n" +
                        "▪️Нақд:" + decimalFormat.format(dailyIncomeNaqd) + "\n" +
                        "▪️Пластик:" + decimalFormat.format(dailyIncomePlastik) + "\n" +
                        "▪️Payme:" + decimalFormat.format(dailyIncomePayme) + "\n" +
                        "▪️Пул ўтказиш:" + decimalFormat.format(dailyIncomeBoshqalar) + "\n" +
                        "\n" +
                        "\uD83D\uDCB5Бугунги харажатлар:\n" +
                        "▪️Нақд:" + decimalFormat.format(dailyOutcomeNaqd) + "\n" +
                        "▪️Пластик:" + decimalFormat.format(dailyOutcomePlastik) + "\n" +
                        "▪️Payme:" + decimalFormat.format(dailyOutcomePayme) + "\n" +
                        "▪️Пул ўтказиш:" + decimalFormat.format(dailyOutcomeBoshqalar) + "\n" +
                        "==================================\n" +
                        "\uD83D\uDCC5Вақт: " + format + "");

        editMessageText.setReplyMarkup(inlineKeyboardMarkup);
        return editMessageText;
    }

    @Override
    public SendMessage sendErrorMessage(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(update.getCallbackQuery().getMessage().getChatId())
                .setParseMode(ParseMode.MARKDOWN);
        if (botUser.get().getLanguage().equals(BotConstants.KR)) {
            sendMessage.setText(BotConstants.NOT_FOUND_SEAT_KR);
        } else {
            sendMessage.setText(BotConstants.NOT_FOUND_SEAT_RU);
        }
        return sendMessage;
    }

    @Override
    public DeleteMessage deleteTopMessage(Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getCallbackQuery().getFrom().getId());
        DeleteMessage deleteMessage = new DeleteMessage();
        botUser.ifPresent(bUser -> {
            deleteMessage.setChatId(update.getCallbackQuery().getMessage().getChatId()).setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            bUser.setMessageId(update.getCallbackQuery().getMessage().getMessageId());
            botUserRepository.save(bUser);
        });
        return deleteMessage;
    }

    public String myPrice(String price) {
        StringBuilder myPrice = new StringBuilder();
        if (price.length() <= 6) {
            myPrice.append(price, 0, price.length() / 2).append(" ").append(price.substring(price.length() / 2));
        } else if (price.length() > 6) {
            myPrice.append(price, 0, 3).append(price, 3, 6).substring(6);
        }
        return myPrice.toString();
    }
}

