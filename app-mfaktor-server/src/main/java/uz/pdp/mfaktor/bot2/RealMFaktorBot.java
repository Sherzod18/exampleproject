package uz.pdp.mfaktor.bot2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerPreCheckoutQuery;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendInvoice;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.payments.PreCheckoutQuery;
import org.telegram.telegrambots.meta.api.objects.payments.SuccessfulPayment;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.pdp.mfaktor.bot.BotConstants;
import uz.pdp.mfaktor.entity.Payment;
import uz.pdp.mfaktor.entity.Role;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.entity.enums.PlaceStatus;
import uz.pdp.mfaktor.entity.enums.RoleName;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ReqPayment;
import uz.pdp.mfaktor.repository.EventRepository;
import uz.pdp.mfaktor.repository.RoleRepository;
import uz.pdp.mfaktor.repository.UserRepository;
import uz.pdp.mfaktor.service.PaymentService;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class RealMFaktorBot<T> extends TelegramLongPollingBot {
    @Value("${bot.token}")
    private String botToken;
    @Value("${bot.username}")
    private String botUsername;
    private T t;

    @Autowired
    TelegramService telegramService;

    @Autowired
    BotUserRepository botUserRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PaymentService paymentService;

    @Autowired
    EventRepository eventRepository;

    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasMessage()) {

            if (update.getMessage().hasPhoto()) {
                Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
                if (botUser.get().getState().equals("ReklamaImage")) {
                    try {
                        execute(telegramService.saveReklamaImage(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            } else if (update.getMessage().hasContact()) {
                Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
                if (botUser.get().getState().equals("PhoneNumber")) {
                    try {
                        execute(telegramService.saveAllDataUser(update));
                        execute(telegramService.showTypeForRegisterSeat(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            } else if (update.getMessage().hasText()) {
                Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
                String text = update.getMessage().getText();

                if (text.equals("/start")) {
                    try {
                        execute(telegramService.chooseLanguage(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (text.startsWith("/start ")) {
                    try {

                        execute(telegramService.referalLink(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("SaveFirstName")) {
                    try {
                        execute(telegramService.deleteFirstNameMessage(update));
                        execute(telegramService.showMyProfile(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("SaveLastName")) {
                    try {
                        execute(telegramService.deleteLastNameMessage(update));
                        execute(telegramService.showMyProfile(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("SavePhoneNumber")) {
                    Role role = roleRepository.findByName(RoleName.ROLE_DIRECTOR).orElseThrow(() -> new ResourceNotFoundException("Role", "name", RoleName.ROLE_DIRECTOR));
                    Optional<User> user = userRepository.findByPhoneNumber(text);
                    if (user.isPresent() && user.get().getRoles().contains(role)) {
                        try {
                            execute(telegramService.askPassword(update));
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            execute(telegramService.deletePhoneNumber(update));
                            execute(telegramService.showMyProfile(update));
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (botUser.get().getState().equals("SaveCompany")) {
                    try {
                        execute(telegramService.deleteCompany(update));
                        execute(telegramService.showMyProfile(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("SaveBirthDate")) {
                    try {
                        execute(telegramService.deleteBirthDate(update));
                        execute(telegramService.showMyProfile(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("FirstName")) {
                    try {
                        execute(telegramService.writeLastName(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("LastName")) {
                    try {
                        execute(telegramService.writePhoneNumber(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("PhoneNumber")) {
                    try {
                        execute(telegramService.saveAllDataUser(update));
                        execute(telegramService.showTypeForRegisterSeat(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("SaveAllData")) {
                    try {
                        execute(telegramService.showTypeForRegisterSeat(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("AskPassword")) {
                    try {
                        execute(telegramService.checkAdminPassword(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("SuccessAdminPassword")) {
                    try {
                        execute(telegramService.showMyProfile(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("ReklamaLanguage")) {
                    try {
                        execute(telegramService.reklamaLanguage(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else if (botUser.get().getState().equals("ReklamaDescription")) {
                    try {
                        execute(telegramService.reklamaDescription(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            } else if (update.getMessage().hasSuccessfulPayment()) {
                SuccessfulPayment payment = update.getMessage().getSuccessfulPayment();
                successfulPayment(payment, update);
            }
        } else if (update.hasCallbackQuery()) {
            String data = update.getCallbackQuery().getData();
            if (data.equals("Lang#KR")) {
                try {
                    execute(telegramService.saveLanguage(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Lang#RU")) {
                try {
                    execute(telegramService.saveLanguage(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Profile")) {
                try {
                    execute(telegramService.myProfile(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("First_name")) {
                try {
                    execute(telegramService.setFirstName(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Last_name")) {
                try {
                    execute(telegramService.setLastName(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Phone_number")) {
                try {
                    execute(telegramService.setPhoneNumber(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Region")) {
                try {
                    execute(telegramService.setRegion(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.startsWith("RegionId#")) {
                try {
                    execute(telegramService.deleteRegion(update));
                    execute(telegramService.showMyProfile(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Activity")) {
                try {
                    execute(telegramService.setActivity(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.startsWith("ActivityId#")) {
                try {
                    execute(telegramService.deleteActivity(update));
                    execute(telegramService.showMyProfile(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Company")) {
                try {
                    execute(telegramService.setCompany(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Position")) {
                try {
                    execute(telegramService.setPosition(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.startsWith("PositionId#")) {
                try {
                    execute(telegramService.deletePosition(update));
                    execute(telegramService.showMyProfile(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Birth_date")) {
                try {
                    execute(telegramService.setBirthDate(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("BackMain")) {
                try {
                    execute(telegramService.backMain(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("BackMain2")) {
                try {
                    execute(telegramService.deleteTopMessage(update));
                    execute(telegramService.backMain2(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("BackProfile")) {
                try {
                    execute(telegramService.myProfile(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Event")) {
                try {
                    execute(telegramService.showAllEventData(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("MyProfile")) {
                try {
                    t = telegramService.allEvents(update);
                    if (t instanceof SendMessage) {
                        execute((SendMessage) t);
                    } else {
                        execute((EditMessageText) t);
                    }
                    execute(telegramService.deleteTopMessage(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("UpcomingEvents")) {
                try {
                    t = telegramService.allEvents(update);
                    if (t instanceof SendMessage) {
                        execute((SendMessage) t);
                        execute(telegramService.deleteTopMessage(update));
                    } else {
                        execute((EditMessageText) t);
                    }
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("RegisterEvents")) {
                try {
                    execute(telegramService.allRegisterEvents(update));
                    execute(telegramService.deleteTopMessage(update));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (data.startsWith("RegisterSeatId#")) {
                try {
                    execute(telegramService.showOneRegisterEvent(update));
                    execute(telegramService.deleteTopMessage(update));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (data.startsWith("EventId#")) {
                try {
                    execute(telegramService.deleteTopMessage(update));
                    execute(telegramService.registerForByEvent(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.startsWith("DeleteReklamaId#")) {
                try {
                    execute(telegramService.deleteReklama(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.startsWith("SendReklamaId#")) {
                try {
                    execute(telegramService.sendReklama(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.startsWith("SeatId#")) {
                Optional<User> user = userRepository.findFirstByTelegramId(update.getCallbackQuery().getFrom().getId());
                if (user.isPresent()) {
                    try {
                        execute(telegramService.showTypeForRegisterSeat(update));
                        execute(telegramService.deleteTopMessage(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        execute(telegramService.deleteTopMessage(update));
                        execute(telegramService.writeFirstName(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

            } else if (data.startsWith("CloseBron#")) {
                try {
                    execute(telegramService.closeBron(update));
                    execute(telegramService.deleteTopMessage(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Money")) {
                try {
                    execute(telegramService.payByMoneyForSeat(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Karta")) {

                try {
                    t = telegramService.payByPlasticForSeat(update);
                    if (t instanceof SendInvoice) {
                        execute((SendInvoice) t);
                    } else {
                        execute((SendMessage) t);
                    }
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.startsWith("PayMoney#")) {
                try {
                    execute(telegramService.showAcceptedForSeat(update));
                    execute(telegramService.deleteTopMessage(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("AcceptBron")) {
                try {
                    execute(telegramService.acceptBron(update));
                    execute(telegramService.deleteTopMessage(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("NoAcceptBron")) {
                try {
                    execute(telegramService.noAcceptBron(update));
                    execute(telegramService.deleteTopMessage(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Address")) {
                try {
                    execute(telegramService.deleteTopMessage(update));
                    execute(telegramService.showAddress(update));
                    execute(telegramService.showImage(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Cash")) {
                try {
                    execute(telegramService.cash(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Debt")) {
                try {
                    execute(telegramService.debt(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (data.equals("Reklama")) {
                try {
                    execute(telegramService.reklama(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            } else if (update.hasPreCheckoutQuery()) {
                PreCheckoutQuery preCheckoutQuery = update.getPreCheckoutQuery();
                AnswerPreCheckoutQuery answerPreCheckoutQuery = new AnswerPreCheckoutQuery(preCheckoutQuery.getId(), true);
                try {
                    execute(answerPreCheckoutQuery);
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void successfulPayment(SuccessfulPayment payment, Update update) {
        Optional<BotUser> botUser = botUserRepository.findByTgUserId(update.getMessage().getFrom().getId());
        Optional<User> user = userRepository.findFirstByTelegramId(update.getMessage().getFrom().getId());
        try {
            ReqPayment reqPayment = new ReqPayment();
            reqPayment.setDetails("Payme orqali tushdi");
            reqPayment.setPayDate(new Timestamp(System.currentTimeMillis()));
            reqPayment.setPaySum(((double) payment.getTotalAmount() / 100));
            reqPayment.setId(userRepository.findByPhoneNumber(user.get().getPhoneNumber()).get().getId());
            reqPayment.setUserId(userRepository.findByPhoneNumber(user.get().getPhoneNumber()).get().getId());
            reqPayment.setPayTypeId(4);
            reqPayment.setSeatId(UUID.fromString(botUser.get().getSeatId()));
            reqPayment.setPlaceStatus(PlaceStatus.SOLD);
            reqPayment.setFromTelegram(true);
            ApiResponse apiResponse = paymentService.addPayment(reqPayment, user.get());

            if (apiResponse.isSuccess()) {

                execute(telegramService.showMyProfile(update));
                execute(telegramService.successFullPayTypePlastic(update));
                Role roleDirector = roleRepository.findByName(RoleName.ROLE_DIRECTOR).get();
                List<User> allAdmins = userRepository.findAllByRolesIn(roleDirector);
                for (int i = 0; i < allAdmins.size(); i++) {
                    execute(telegramService.sendInformationToAdmin(update, allAdmins.get(i).getChatId(), 2));
                }
            } else {
                execute(telegramService.sendErrorMessage(update));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendNotification(Payment payment) {
        Long chatId1 = payment.getUser().getChatId();
        if (chatId1 != null) {
            InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> rows = new ArrayList<>();
            List<InlineKeyboardButton> row = new ArrayList<>();
            InlineKeyboardButton button = new InlineKeyboardButton();
            Optional<User> user = userRepository.findByChatId(chatId1);
            Optional<BotUser> botUser = botUserRepository.findByTgUserId(user.get().getTelegramId());

            if (botUser.get().getLanguage().equals(BotConstants.KR)) {
                button.setText(BotConstants.BACK_KR);
            } else {
                button.setText(BotConstants.BACK_RU);
            }
            button.setCallbackData("AgainTopEvents");
            row.add(button);
            rows.add(row);
            inlineKeyboardMarkup.setKeyboard(rows);
            SendMessage sendMessage = new SendMessage();
            sendMessage.setChatId(chatId1)
                    .setParseMode(ParseMode.MARKDOWN);
            sendMessage.setText("✅*Тўловни тасдиқловчи хабар!*\n\n" +
                    "Тўлов миқдори: *" + payment.getPaySum() + "* сўм\n" +
                    "Тўлов тури: *" + payment.getPayType().getNameUz() + "*");
            sendMessage.setReplyMarkup(inlineKeyboardMarkup);
            try {
                execute(sendMessage);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
