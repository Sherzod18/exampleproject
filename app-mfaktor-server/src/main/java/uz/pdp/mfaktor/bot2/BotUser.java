package uz.pdp.mfaktor.bot2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.mfaktor.entity.*;
import uz.pdp.mfaktor.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BotUser extends AbsEntity {

    @Column(unique = true)
    private String phoneNumber;

    private String password;

    private String firstName;

    private String lastName;

    private String state;

    private Integer messageId;

    private String rekId;

    private Long chatId;

    private String seatId;

    private String expireDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;

    @ManyToOne(fetch = FetchType.LAZY)
    private Activity activity;

    private String company;

    @ManyToOne(fetch = FetchType.LAZY)
    private Position position;

    private Date birthDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Aware aware;

    public Integer tgUserId;

    public String language;
}
