package uz.pdp.mfaktor.bot2;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface BotUserRepository extends JpaRepository<BotUser, UUID> {

    Optional<BotUser> findByTgUserId(Integer tgUserId);
}
