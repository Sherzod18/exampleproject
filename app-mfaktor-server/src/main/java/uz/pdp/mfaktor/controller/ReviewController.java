package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.Event;
import uz.pdp.mfaktor.entity.Review;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqReview;
import uz.pdp.mfaktor.payload.ResReviewList;
import uz.pdp.mfaktor.repository.ReviewRepository;
import uz.pdp.mfaktor.repository.UserRepository;
import uz.pdp.mfaktor.security.CurrentUser;
import uz.pdp.mfaktor.service.ReviewService;
import uz.pdp.mfaktor.utils.AppConstants;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/review")
public class ReviewController {

    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ReviewService reviewService;

    @GetMapping
    public HttpEntity<?> getPublishedReviews() {
        return ResponseEntity.ok(new ApiResponseModel(true, "Reviews", reviewService.getReviews()));
    }

    @GetMapping("list")
    public HttpEntity<?> getAllReviews(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size
    ) {
        Page<Review> reviewPage = reviewRepository.findAll(PageRequest.of(page, size, Sort.Direction.DESC, "createdAt"));
        List<Review> reviews = reviewPage.getContent();
        List<ResReviewList> revList=new ArrayList<>();
        for (Review review : reviews) {
            revList.add(reviewService.getReviewList(review));
        }
        return ResponseEntity.ok(new ApiResponseModel(true, "Reviews", revList));
    }

    @GetMapping("totalPages")
    public Integer totalPages(@RequestParam(value = "size", defaultValue = "10") Integer size,
                              @RequestParam(value = "page", defaultValue = "0") Integer page) {
        return reviewRepository.findAll(PageRequest.of(page, size)).getTotalPages();
    }

    @PutMapping("published/{id}")
    public HttpEntity<?> changePublished(@PathVariable UUID id) {
        try {
            Review review = reviewRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("review", "id", id));
            review.setPublished(!review.isPublished());
            reviewRepository.save(review);
            return ResponseEntity.ok(new ApiResponseModel(true, "success", review));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.ok(new ApiResponse("error", false));
        }
    }

    @PostMapping("/save")
    public HttpEntity<?> addReview(@Valid @RequestBody ReqReview reqReview) {
        User user = userRepository.findById(reqReview.getEventOrSpeakerId()).orElseThrow(() -> new ResourceNotFoundException("addReview", "id", reqReview.getEventOrSpeakerId()));
        Review review = new Review(
                reqReview.getText(),
                reqReview.getStarCount(),
                true,
                new Event(), user);
        reviewRepository.save(review);
        return ResponseEntity.status(HttpStatus.CREATED).body("Review saved!");
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getOwnReview(@CurrentUser User user, @PathVariable(value = "id") UUID eventOrSpeakerId) {
        return ResponseEntity.ok().body(reviewService.getOwnReview(user.getId(), eventOrSpeakerId));
    }

    @GetMapping("/all/{eventId}")
    public HttpEntity<?> getReviews(@PathVariable UUID eventId,
                                    @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                    @RequestParam(value = "size", defaultValue = "5") int size
    ) {
        return ResponseEntity.ok().body(new ApiResponseModel(true, "Reviews of event", reviewService.getEventReviews(eventId, page, size)));
    }

    @PostMapping
    public HttpEntity<?> postReview(@RequestBody ReqReview reqReview) {
        return ResponseEntity.status(201).body(reviewService.addReview(reqReview));
    }
}
