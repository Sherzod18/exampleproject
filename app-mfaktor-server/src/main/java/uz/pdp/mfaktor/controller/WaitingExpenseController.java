package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.Usd;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqWaitingExpense;
import uz.pdp.mfaktor.payload.ResWaitingExpense;
import uz.pdp.mfaktor.repository.UsdRepository;
import uz.pdp.mfaktor.repository.WaitingExpenseRepository;
import uz.pdp.mfaktor.service.EventService;
import uz.pdp.mfaktor.service.WaitingExpenseService;

import java.util.UUID;

@RestController
@RequestMapping("/api/waitingExpense")
public class WaitingExpenseController {

    @Autowired
    WaitingExpenseRepository waitingExpenseRepository;
    @Autowired
    WaitingExpenseService waitingExpenseService;
    @Autowired
    UsdRepository usdRepository;
    @Autowired
    EventService eventService;

    @PostMapping
    public HttpEntity<?> addWaitingExpense(@RequestBody ReqWaitingExpense request) {
        ApiResponse response = waitingExpenseService.addWaitingExpense(request);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(new ApiResponse(response.getMessage(), response.isSuccess()));
    }

    @PutMapping
    public HttpEntity<?> editWaitingExpense(@RequestBody ReqWaitingExpense request) {
        ApiResponse response = waitingExpenseService.editWaitingExpense(request);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(new ApiResponse(response.getMessage(), response.isSuccess()));
    }

    @GetMapping
    public HttpEntity<?> getWaitingExpense() {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana Waiting expenses", waitingExpenseService.getWaitingExpenses()));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteWaitingExpense(@PathVariable UUID id) {
        ApiResponse response = waitingExpenseService.deleteWaitingExpense(id);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(new ApiResponse(response.getMessage(), response.isSuccess()));
    }

    @PostMapping("/usd")
    public HttpEntity<?> addUsd(@RequestBody Usd usd) {
        usdRepository.save(usd);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse("USD qo'shildi", true));
    }

    @PutMapping("/usd")
    public HttpEntity<?> editUsd(@RequestBody Usd usd) {
        usdRepository.save(usd);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse("USD tahrirlandi", true));
    }

    @GetMapping("/usd")
    public HttpEntity<?> getUsd() {
        return ResponseEntity.ok(new ApiResponseModel(true, "Dollar kursi", usdRepository.maxUsd()));
    }

    @GetMapping("/debtAndDebt")
    public HttpEntity<?> getDebt() {
        return ResponseEntity.ok(new ApiResponseModel(true, "Qarzlar", eventService.weDebt()));
    }


}
