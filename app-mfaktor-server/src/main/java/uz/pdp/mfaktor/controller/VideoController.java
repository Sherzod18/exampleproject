package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqVideo;
import uz.pdp.mfaktor.repository.VideoTypeRepository;
import uz.pdp.mfaktor.repository.VideoUrlRepository;
import uz.pdp.mfaktor.service.VideoService;

import java.util.UUID;

@RestController
@RequestMapping("/api/video")
public class VideoController {
    @Autowired
    VideoService videoService;
    @Autowired
    VideoUrlRepository videoUrlRepository;
    @Autowired
    VideoTypeRepository videoTypeRepository;

    @GetMapping
    public ApiResponseModel getAllVideos(@RequestParam(value = "size", defaultValue = "12") Integer size,
                                         @RequestParam(value = "page", defaultValue = "0") Integer page) {
        return new ApiResponseModel(true, "getAllVideos", videoService.getAll(page, size));


    }
    @GetMapping("/getPraktikums")
    public ApiResponseModel getVideoByType(@RequestParam(value = "size", defaultValue = "12") Integer size,
                                           @RequestParam(value = "page", defaultValue = "0") Integer page,
    @RequestParam String vidTypeNam){
        return new ApiResponseModel(true,"getAllByType",videoService.getPraktikums(vidTypeNam,page,size));
    }

    @PostMapping
    public HttpEntity<?> saveVideo(@RequestBody ReqVideo reqVideo) {
        return ResponseEntity.status(HttpStatus.CREATED).body(videoService.saveVideo(reqVideo));
    }

    @DeleteMapping("/{videoId}")
    public HttpEntity<?> deleteVideo(@PathVariable UUID videoId) {
        return ResponseEntity.ok(videoService.deleteVideo(videoId));
    }

    @GetMapping("/lasVideos")
    public ApiResponseModel getLast5Videos() {
        return new ApiResponseModel(true, "Success", videoService.getLast5Videos());
    }

    @GetMapping("/getVideoTypes")
    public ApiResponseModel getVideoTypes() {
        return new ApiResponseModel(true, "videoTypes", videoTypeRepository.findAll());
    }

    @GetMapping("/searchByTitle/{title}")
    public ApiResponseModel searchByTitle(
            @PathVariable String title
    ){
        return new ApiResponseModel(true,"searchByTitle",videoService.searchByTitle(title));
    }

    @GetMapping("/anonsType")
    public ApiResponseModel getAnonsType(){
        return new ApiResponseModel(true,"Anons Type",videoService.getAnonsType());
    }

}
