package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqPayment;
import uz.pdp.mfaktor.security.CurrentUser;
import uz.pdp.mfaktor.service.PaymentService;
import uz.pdp.mfaktor.utils.AppConstants;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {
    @Autowired
    PaymentService paymentService;

    @PostMapping
    public HttpEntity<?> addPayment(@RequestBody ReqPayment reqPayment, @CurrentUser User user) {
        ApiResponse apiResponse = paymentService.addPayment(reqPayment, user);
        if (apiResponse.isSuccess()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse(apiResponse.getMessage(), true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(apiResponse.getMessage(), false));
    }

    @GetMapping
    public HttpEntity<?> getAll(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponseModel(true,"Mana paymentlar",paymentService.getPayments(page, size)));
    }


}
