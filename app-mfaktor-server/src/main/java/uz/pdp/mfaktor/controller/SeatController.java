package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.component.SeatListener;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.payload.*;
import uz.pdp.mfaktor.repository.SeatRepository;
import uz.pdp.mfaktor.security.CurrentUser;
import uz.pdp.mfaktor.service.SeatService;

import javax.persistence.EntityListeners;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/seat")
@EntityListeners(SeatListener.class)
public class SeatController {
    @Autowired
    SeatRepository seatRepository;
    @Autowired
    SeatService seatService;

    @GetMapping("/{eventId}")
    public HttpEntity<?> getSeatsByEvent(@PathVariable UUID eventId) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana seats", seatService.eventSeats(eventId)));
    }

    @PostMapping("/book")
    public HttpEntity<?> bookSeat(@RequestBody ReqSeat request) {
        return ResponseEntity.ok(seatService.bookSeats(request));
    }

    @PostMapping
    public HttpEntity<?> seatOrder(@RequestBody ReqUser reqUser, @CurrentUser User user) {
        ApiResponse apiResponse = seatService.seatOrder(reqUser, reqUser.getSeatId(), user);
        if (apiResponse.isSuccess()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse(apiResponse.getMessage(), true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(apiResponse.getMessage(), false));
    }

    @PutMapping("/cancelBooked/{seatId}")
    public HttpEntity<?> cancelBooked(@PathVariable UUID seatId) {
        ApiResponse response = seatService.cancelBooked(seatId);
        if (response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse(response.getMessage(), true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(response.getMessage(), false));
    }

    @PutMapping("/participate")
    public HttpEntity<?> setParticipated(@RequestBody ReqSeatParticipate request) {
        ApiResponse response = seatService.setParticipated(request);
        if (response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse(response.getMessage(), response.isSuccess()));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(response.getMessage(), response.isSuccess()));
    }

    @PutMapping("/cancelSold/{seatId}")
    public HttpEntity<?> cancelSold(@PathVariable UUID seatId) {
        ApiResponse response = seatService.cancelSold(seatId);
        if (response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse(response.getMessage(), response.isSuccess()));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(response.getMessage(), response.isSuccess()));
    }

    @PostMapping("/addChair")
    public ApiResponse addChair(@RequestBody ReqAddChair reqAddChair) {
        return seatService.addSeat(reqAddChair);
    }

    @PostMapping("/addRow")
    public ApiResponse addRow(@RequestBody List<ReqAddChair> reqAddChairs) {
        return seatService.addRow(reqAddChairs);
    }
}
