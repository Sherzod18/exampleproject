package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.Event;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqEvent;
import uz.pdp.mfaktor.payload.ReqEventStatus;
import uz.pdp.mfaktor.repository.EventRepository;
import uz.pdp.mfaktor.service.EventService;
import uz.pdp.mfaktor.service.ExportExcelService;
import uz.pdp.mfaktor.utils.AppConstants;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/event")
public class EventController {
    @Autowired
    EventService eventService;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    ExportExcelService exportExcelService;

    @GetMapping
    public HttpEntity<?> getEvents(@RequestParam(value = "status", defaultValue = "all") String status,
                                   @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                   @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana eventlar", eventService.getEvents(status, page, size)));
    }

    @GetMapping("/forUser")
    public HttpEntity<?> getEventsForUser(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                          @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana eventlar user uchun", eventService.getEventsForUser(page, size)));
    }

    @GetMapping("/archiveAndCancel")
    public HttpEntity<?> getArchiveAndCancelEvents(@RequestParam(value = "status", defaultValue = "all") String status,
                                                   @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                   @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana arxiv va cancel evenetlar", eventService.getArchiveAndCancelEvents(status, page, size)));
    }

    @GetMapping("/type/{id}")
    public HttpEntity<?> getEventsByType(@PathVariable Integer id) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Events By Type", eventService.getEventsByType(id)));
    }

    @GetMapping("search")
    public HttpEntity<?> searchEvent(@RequestParam String word) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Searching events", eventService.searchEvent(word)));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getEvent(@PathVariable UUID id) {
        Event event = eventRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("event", "id", id));
        if (event.getOpenTime() != null) {
            return ResponseEntity.ok(new ApiResponseModel(true, "Mana event", eventService.getEvent(event)));
        }
        return ResponseEntity.ok(new ApiResponse("Event DRAFT holatida", false));
    }

    @GetMapping("/user-event/{userId}")
    public HttpEntity<?> getEventsByUser(@PathVariable(value = "userId") UUID id) {
        return ResponseEntity.ok().body(new ApiResponseModel(true, "Animal's upcoming events", eventService.getEventsByUser(id)));
    }

    @GetMapping("/user-archive-event/{userId}")
    public HttpEntity<?> getArchiveEventsByUser(@PathVariable(value = "userId") UUID id) {
        return ResponseEntity.ok().body(new ApiResponseModel(true, "Animal's participated events", eventService.getParticipatedEvent(id)));
    }

    @GetMapping("/upcoming")
    public HttpEntity<?> getUpcomingEvents(@RequestParam(name = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                           @RequestParam(name = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok().body(new ApiResponseModel(true, "Upcoming events", eventService.getUpcomingEvents(page, size)));
    }


    @GetMapping("/get/{id}")
    public HttpEntity<?> getEventForEdit(@PathVariable UUID id) {
        Event event = eventRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getEvent", "eventId", id));
        return ResponseEntity.ok(event);
    }

    @PostMapping
    public HttpEntity<?> addEvent(@Valid @RequestBody ReqEvent reqEvent) {
        ApiResponse apiResponse = eventService.addEvent(reqEvent);
        if (apiResponse.isSuccess()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse(apiResponse.getMessage(), true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(apiResponse.getMessage(), false));
    }

    @PutMapping("/{id}")
    public HttpEntity<?> editEvent(@Valid @RequestBody ReqEvent reqEvent, @PathVariable UUID id) {
        ApiResponse apiResponse = eventService.editEvent(reqEvent, id);
        if (apiResponse.isSuccess()) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse(apiResponse.getMessage(), true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(apiResponse.getMessage(), false));
    }

    @GetMapping("/income/{eventId}")
    public HttpEntity<?> getEventBalance(@PathVariable UUID eventId) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Eventga oid hisob-kitoblar", eventService.getBalanceByEvent(eventId)));
    }

    @GetMapping("/users/{eventId}")
    public HttpEntity<?> getEventUsers(@PathVariable UUID eventId) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana eventni userlari", eventService.getEventUsers(eventId)));
    }

    @GetMapping("/participate/{eventId}")
    public HttpEntity<?> getParticipatedAndGuest(@PathVariable UUID eventId) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana nehmonlar va kelganlar soni", eventService.getParticipatedAndGuest(eventId)));
    }

    @PutMapping("/open")
    public HttpEntity<?> changeStatusEventOpen(@RequestBody ReqEventStatus reqEventStatus) {
        ApiResponse response = eventService.changeStatusEventOpen(reqEventStatus);
        if (response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponse(response.getMessage(), true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(response.getMessage(), false));
    }

    @GetMapping("/seatPayment/{id}")
    public HttpEntity<?> getPaymentBySeat(@PathVariable UUID id) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana seat bo'yicha to'lovlar", eventService.getSeatPaymentsByEvent(id)));
    }

    @GetMapping("/status")
    public HttpEntity<?> getEventStatuses() {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana event statuslari", eventService.getEventStatuses()));
    }


    @GetMapping("/dashboard")
    public HttpEntity<?> getDashboardInfo() {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana dashboard uchun", eventService.getInfo()));
    }


    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteEvent(@PathVariable UUID id) {
        eventRepository.deleteById(id);
        return ResponseEntity.ok("O'chirildi");
    }

    @GetMapping("/exportExcel/{id}")
    public HttpEntity<?> exportEventUsers(@PathVariable UUID id) {
        ApiResponseModel apiResponseModel = exportExcelService.exportDataToExcel(id);
        if (apiResponseModel.isSuccess()) {
            return ResponseEntity.ok(apiResponseModel);
        }
        return ResponseEntity.ok(apiResponseModel);
    }

    @GetMapping("/open")
    public HttpEntity<?> getOpenEvents() {
        return ResponseEntity.ok(new ApiResponseModel(true, "OpenEvnets", eventService.getOpenEvents()));
    }


}
