package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.mfaktor.payload.ReqTicket;
import uz.pdp.mfaktor.repository.TicketRepository;
import uz.pdp.mfaktor.service.TicketService;

@RestController
@RequestMapping("/api/ticket")
public class TicketController {
    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    TicketService ticketService;

    @PostMapping
    public HttpEntity<?> addTicket(@RequestBody ReqTicket reqTicket){
        return ResponseEntity.status(HttpStatus.CREATED).body(ticketService.addTicket(reqTicket));
    }
}
