package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqVoucher;
import uz.pdp.mfaktor.service.VoucherService;

import java.util.UUID;

@RestController
@RequestMapping("/api/voucher")
public class VoucherController {

    @Autowired
    VoucherService voucherService;

    @GetMapping("/{id}")
    public HttpEntity<?> getVoucher(@PathVariable UUID id) {
        return ResponseEntity.ok().body(new ApiResponseModel(true, "Voucher", voucherService.getVoucher(id)));
    }

    @GetMapping
    public HttpEntity<?> getAllVouchers() {
        return ResponseEntity.ok().body(new ApiResponseModel(true, "All vouchers", voucherService.getAllVouchers()));
    }

    @PostMapping
    public HttpEntity<?> addVoucher(@RequestBody ReqVoucher reqVoucher) {
        return ResponseEntity.status(201).body(voucherService.addVoucher(reqVoucher));
    }

    @PutMapping
    public HttpEntity<?> editVoucher(@RequestBody ReqVoucher reqVoucher) {
        return ResponseEntity.status(201).body(voucherService.editVoucher(reqVoucher));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteVoucher(@PathVariable UUID id) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(voucherService.deleteVoucher(id));
    }
}
