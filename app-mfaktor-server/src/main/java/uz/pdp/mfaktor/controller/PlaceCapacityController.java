package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.Chair;
import uz.pdp.mfaktor.entity.PlaceCapacity;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqTemplatePlace;
import uz.pdp.mfaktor.repository.ChairRepository;
import uz.pdp.mfaktor.repository.PlaceCapacityRepository;
import uz.pdp.mfaktor.service.PlaceCapacityService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/place")
public class PlaceCapacityController {
    @Autowired
    PlaceCapacityService placeCapacityService;
    @Autowired
    PlaceCapacityRepository placeCapacityRepository;
    @Autowired
    ChairRepository chairRepository;
    @GetMapping
    public ApiResponseModel getTemplatePlaces() {
        return new ApiResponseModel(true, "", placeCapacityRepository.findAll());
    }

    @GetMapping("/{id}")
    public ApiResponseModel getTemplatePlaces(@PathVariable UUID id) throws Exception {
        return new ApiResponseModel(true, "", placeCapacityRepository.findById(id).orElseThrow(() -> new Exception("")));
    }

    @PostMapping
    public HttpEntity<?> saveTemplatePlace(@Valid @RequestBody ReqTemplatePlace reqTemplatePlace) {
        return ResponseEntity.status(201).body(placeCapacityService.savePlace(reqTemplatePlace));
    }

    @DeleteMapping("{id}")
    public ApiResponse deleteTemplatePlace(@PathVariable UUID id) {
        return placeCapacityService.deletePlace(id);
    }
}
