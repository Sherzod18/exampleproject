package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.Comment;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqComment;
import uz.pdp.mfaktor.repository.CommentRepository;
import uz.pdp.mfaktor.service.CommentService;

import java.util.UUID;

@RestController
@RequestMapping("/api/comment")
public class CommentController {
    @Autowired
    CommentService commentService;
    @Autowired
    CommentRepository commentRepository;

    @GetMapping("/{articleId}")
    public HttpEntity<?> getCommentByArticle(@PathVariable UUID articleId){
        return ResponseEntity.ok(new ApiResponseModel(true,"Mana kommentlar ol",commentService.getCommentsByArticle(articleId)));
    }

    @PostMapping
    public HttpEntity<?> addComment(@RequestBody ReqComment reqComment){
        ApiResponse apiResponse = commentService.addComment(reqComment);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse(apiResponse.getMessage(),true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(apiResponse.getMessage(),false));
    }

    @DeleteMapping("/{commentId}")
    public HttpEntity<?> deleteComment(@PathVariable UUID commentId){
        ApiResponse apiResponse = commentService.deleteComment(commentId);
        if (apiResponse.isSuccess()){
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new ApiResponse(apiResponse.getMessage(),true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(apiResponse.getMessage(),false));
    }




}
