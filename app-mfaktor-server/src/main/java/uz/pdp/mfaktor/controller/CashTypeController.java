package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import uz.pdp.mfaktor.entity.CashType;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqCashType;
import uz.pdp.mfaktor.payload.ReqTransfer;
import uz.pdp.mfaktor.repository.CashTypeRepository;
import uz.pdp.mfaktor.service.CashTypeService;

@RestController
@RequestMapping("/api/cashType")
public class CashTypeController {
    @Autowired
    CashTypeRepository cashTypeRepository;
    @Autowired
    CashTypeService cashTypeService;

    @PostMapping
    public HttpEntity<?> addCashType(@RequestBody ReqCashType reqCashType) {
        ApiResponse response = cashTypeService.addCashType(reqCashType);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping
    public HttpEntity<?> getCashTypes() {
        return ResponseEntity.ok(new ApiResponseModel(true, "CashTypes", cashTypeService.getCashTypes()));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getCashType(@PathVariable Integer id) {
        CashType cashType = cashTypeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getcashType", "cashType", id));
        return ResponseEntity.ok(new ApiResponseModel(true, "CashTypes", cashTypeService.getCashType(cashType)));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteCashType(@PathVariable Integer id) {
        cashTypeRepository.deleteById(id);
        return ResponseEntity.ok(true);
    }

    @PostMapping("/transfer")
    public HttpEntity<?> transfer(@RequestBody ReqTransfer reqTransfer) {
        ApiResponse response = cashTypeService.transfer(reqTransfer);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }
}
