package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.Expense;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqExpense;
import uz.pdp.mfaktor.repository.ExpenseRepository;
import uz.pdp.mfaktor.service.ExpenseService;
import uz.pdp.mfaktor.utils.AppConstants;

import java.util.UUID;

@RestController
@RequestMapping("/api/expense")
public class ExpenseController {
    @Autowired
    ExpenseService expenseService;
    @Autowired
    ExpenseRepository expenseRepository;

    @PostMapping
    public HttpEntity<?> addExpense(@RequestBody ReqExpense reqExpense) {
        ApiResponse response = expenseService.addExpense(reqExpense);
        if (response.isSuccess()) {
            return ResponseEntity.status(reqExpense.getId() == null ? HttpStatus.CREATED : HttpStatus.ACCEPTED).body(new ApiResponse(response.getMessage(),true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(response.getMessage(),false));
    }

    @GetMapping
    public HttpEntity<?> getExpenses(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                     @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponseModel(true,"Mana harajatlar",expenseService.getExpenses(page,size)));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getExpense(@PathVariable UUID id) {
        Expense expense = expenseRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getExpense", "expenseId", id));
        return ResponseEntity.ok(expenseService.getExpense(expense));
    }


}
