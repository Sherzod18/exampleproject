package uz.pdp.mfaktor.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.*;
import uz.pdp.mfaktor.repository.UserRepository;
import uz.pdp.mfaktor.security.AuthService;
import uz.pdp.mfaktor.security.CurrentUser;
import uz.pdp.mfaktor.service.UserService;
import uz.pdp.mfaktor.utils.AppConstants;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AuthService authService;

    @PostMapping("/checkPhone")
    public ResponseEntity<?> checkPhone(@Valid @RequestBody ReqCheckPhone checkPhone) {
        Optional<User> user = userRepository.findByPhoneNumber(checkPhone.getPhoneNumber());
        Boolean hasPassword = false;
        if (user.isPresent()) {
            hasPassword = user.get().getPassword() != null;
        }
        return ResponseEntity.ok(new ResCheckPhone(userRepository.existsByPhoneNumber(checkPhone.getPhoneNumber()), hasPassword));
    }

    @PostMapping("/checkUser")
    public ResponseEntity<?> checkUser(@Valid @RequestBody ReqSignIn reqSignIn) {
        Optional<User> user = userRepository.findByPhoneNumber(reqSignIn.getPhoneNumber());
        if (user.isPresent()) {
            if (passwordEncoder.matches(reqSignIn.getPassword(), user.get().getPassword())) {
                return ResponseEntity.ok(new ApiResponse("Animal topildi", true));
            } else {
                return ResponseEntity.ok(new ApiResponse("Parol xato", false));
            }
        } else {
            return ResponseEntity.ok(new ApiResponse("Animal topilmadi", false));
        }
    }

    @GetMapping("/me")
    public HttpEntity<?> getUser(@CurrentUser User user) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana user", user));
    }

    @PreAuthorize("hasAnyRole('ROLE_MANAGER','ROLE_ADMIN','ROLE_CASHIER','ROLE_DIRECTOR')")
    @GetMapping
    public HttpEntity<?> getUsers(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                  @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size,
                                  @RequestParam(value = "search", defaultValue = "") String search) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana userlar", userService.getUsers(page, size, search)));
    }

    @PreAuthorize("hasAnyRole('ROLE_MANAGER','ROLE_ADMIN','ROLE_CASHIER','ROLE_DIRECTOR')")
    @GetMapping("/byCountVisit")
    public HttpEntity<?> getUsersByCountVisit(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                  @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana userlar", userService.getUsersByCountVisit(page, size)));
    }

    @GetMapping("/{userId}")
    public HttpEntity<?> getUser(@PathVariable UUID userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("getUser", "userId", userId));
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana user", userService.getUser(user)));
    }

    @PutMapping
    public HttpEntity<?> editUser(@Valid @RequestBody ReqUser reqUser) {
        ApiResponse response = userService.editUser(reqUser, reqUser.getId());
        if (response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(response.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(response.getMessage());
    }

    @PutMapping("/changePassword")
    public HttpEntity<?> editPassword(@RequestBody ReqPassword reqPassword, @CurrentUser User user) {
        return userService.editPassword(reqPassword, user);
    }

    @PutMapping("/setPassword")
    public ResponseEntity<?> changePassword(@RequestBody ReqChangeUserPassword reqUser) {
        try {
            UserRecord userRecord = FirebaseAuth.getInstance().getUserByPhoneNumber(reqUser.getPhoneNumber());
            if (userRecord == null) {
                throw new UsernameNotFoundException("Bunday foydalanuvchi tizimda mavjud emas!");
            } else {
                User user = userRepository.findByPhoneNumber(userRecord.getPhoneNumber())
                        .orElseThrow(() -> new ResourceNotFoundException("Animal", "PhoneNumber", userRecord.getPhoneNumber()));
                if (userRepository.existsByPhoneNumber(userRecord.getPhoneNumber())) {
//                    FirebaseAuth.getInstance().deleteUser(userRecord.getUid());
                    if (reqUser.getPassword().equals(reqUser.getPrePassword())) {
                        user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
                        userRepository.save(user);
                        return ResponseEntity.status(HttpStatus.OK).body(new ApiResponse("Parol o'zgartirildi", true));
                    } else {
                        return ResponseEntity.status(HttpStatus.OK).body(new ApiResponse("Parol xato", false));
                    }

                } else {
                    return ResponseEntity.status(HttpStatus.OK).body(new ApiResponse("Bunday foydalanuvchi tizimda mavjud emas!", false));
                }
            }
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
            throw new UsernameNotFoundException("Tekshirishda xatolik!");
        }
    }

    @PreAuthorize("hasAnyRole('ROLE_MANAGER','ROLE_ADMIN','ROLE_DIRECTOR')")
    @PostMapping
    public HttpEntity<?> createUser(@Valid @RequestBody ReqUser reqUser) {
        ApiResponse response = userService.addUser(reqUser);
        if (response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(response.getMessage());
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(response.getMessage());
    }

    @PostMapping("/search")
    public HttpEntity<?> getSearchingUser(@RequestBody ReqSearch word) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana", userService.search(word)));
    }


    @GetMapping("/incomeAndOutcome/{userId}")
    public HttpEntity<?> getUserIncomes(@PathVariable UUID userId) {
        return ResponseEntity.ok(new ApiResponseModel(true, "Mana foydalanuvchinig to'lovlari", userService.getUserIncomesAndOutCome(userId)));
    }

}
