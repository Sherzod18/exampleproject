package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.Article;
import uz.pdp.mfaktor.entity.Category;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqArticle;
import uz.pdp.mfaktor.payload.ResArticle;
import uz.pdp.mfaktor.repository.ArticleRepository;
import uz.pdp.mfaktor.repository.CategoryRepository;
import uz.pdp.mfaktor.service.ArticleService;
import uz.pdp.mfaktor.utils.AppConstants;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/article")
public class ArticleController {
    @Autowired
    ArticleService articleService;
    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping
    public ApiResponseModel getArticles(@RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size,
                                        @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page) {
        return new ApiResponseModel(true, "", articleService.getArticle(size, page));
    }

    @GetMapping("totalPages")
    public Integer totalPages(@RequestParam(value = "size", defaultValue = "10") Integer size,
                              @RequestParam(value = "page", defaultValue = "0") Integer page) {
        return articleRepository.findAll(PageRequest.of(page, size)).getTotalPages();
    }

    @GetMapping("/{id}")
    public Article getArticle(@PathVariable UUID id) throws Exception {
        return articleRepository.findById(id).orElseThrow(() -> new Exception("Topilmadi"));
    }

    @DeleteMapping("{id}")
    public ApiResponse deleteArticle(@PathVariable UUID id) throws Exception {
        Article article = articleRepository.findById(id).orElseThrow(() -> new Exception("Topilmadi"));
        article.setDeleted(true);
        articleRepository.save(article);
        return new ApiResponse("", true);
    }

    @PostMapping
    public HttpEntity<?> createArticle(@Valid @RequestBody ReqArticle reqArticle) {
        return ResponseEntity.status(HttpStatus.CREATED).body(articleService.createArticle(reqArticle));
    }

    @PutMapping
    public HttpEntity<?> editArticle(@Valid @RequestBody ReqArticle reqArticle) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(articleService.createArticle(reqArticle));
    }

    @GetMapping("active")
    public ApiResponseModel getArticlesByActive(@RequestParam(value = "size", defaultValue = "6") Integer size,
                                                @RequestParam(value = "page", defaultValue = "0") Integer page) {
        Page<Article> articles = articleRepository.findAllByActive(true, PageRequest.of(page, size));
        return new ApiResponseModel(true, "", new ResArticle(articles.getContent(), articles.getTotalPages()));
    }

    @GetMapping("byCategory")
    public ApiResponseModel getArticlesByCategory(@RequestParam(value = "size", defaultValue = "6") Integer size,
                                                  @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                  @RequestParam(value = "categoryId") Integer categoryId) throws Exception {
        Category category = categoryRepository.findById(categoryId).orElseThrow(() -> new Exception(""));
        Page<Article> articles = articleRepository.findAllByActiveAndCategory(true, category, PageRequest.of(page, size));
        return new ApiResponseModel(true, "", new ResArticle(articles.getContent(), articles.getTotalPages()));
    }

    @GetMapping("byUrl")
    public Article getByUrl(@RequestParam("url") String url) {
        return articleRepository.getByUrlAndActive(url, true);
    }

    @GetMapping("latest")
    public ApiResponseModel getLatest() {
        return new ApiResponseModel(true, "", articleRepository.findAllByActive(true, PageRequest.of(0, 10)).getContent());
    }

    @GetMapping("latest3")
    public ApiResponseModel getLatest3() {
        return new ApiResponseModel(true, "", articleRepository.findAllByActive(true, PageRequest.of(0, 3)).getContent());
    }

    @GetMapping("search")
    public ApiResponseModel searchByName(@RequestParam String word){
        return new ApiResponseModel(true, "", articleRepository.findAllByTitleContainingIgnoreCase(word));
    }
}
