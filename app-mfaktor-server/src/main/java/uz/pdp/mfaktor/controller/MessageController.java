package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqMessage;
import uz.pdp.mfaktor.service.MessageService;

@RestController
@RequestMapping("api/message")
public class MessageController {
    @Autowired
    MessageService messageService;

    @PostMapping
    public HttpEntity<?> addMessage(@RequestBody ReqMessage reqMessage) {
        ApiResponse response = messageService.addMessage(reqMessage);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping
    public HttpEntity<?> getMessages() {
        return ResponseEntity.ok(new ApiResponseModel(true, "MAna sms lar", messageService.getMessages()));
    }

}
