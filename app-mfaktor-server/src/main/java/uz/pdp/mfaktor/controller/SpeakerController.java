package uz.pdp.mfaktor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mfaktor.entity.User;
import uz.pdp.mfaktor.exception.ResourceNotFoundException;
import uz.pdp.mfaktor.payload.ApiResponse;
import uz.pdp.mfaktor.payload.ApiResponseModel;
import uz.pdp.mfaktor.payload.ReqSpeaker;
import uz.pdp.mfaktor.payload.ResSpeaker;
import uz.pdp.mfaktor.repository.UserRepository;
import uz.pdp.mfaktor.service.SpeakerService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/speaker")
public class SpeakerController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    SpeakerService speakerService;

    @PostMapping
    public HttpEntity<?> addSpeaker(@Valid @RequestBody ReqSpeaker reqSpeaker) {
        ApiResponse response = speakerService.addSpeaker(reqSpeaker);
        if (response.isSuccess()) {
            return ResponseEntity.status(HttpStatus.CREATED).body(new ApiResponse(response.getMessage(),true));
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(response.getMessage(),false));
    }

    @PutMapping
    public HttpEntity<?> editSpeaker(@Valid @RequestBody ReqSpeaker reqSpeaker) {
        ApiResponse response = speakerService.editSpeaker(reqSpeaker);
        return response.isSuccess() ?
                ResponseEntity.status(HttpStatus.ACCEPTED).body(response.getMessage()) :
                ResponseEntity.status(HttpStatus.CONFLICT).body(response.getMessage());
    }

    @GetMapping
    public ApiResponseModel getSpeakers() {
        return new ApiResponseModel(true, "", speakerService.getSpeakers());
    }

    @GetMapping("/pagination")
    public ApiResponseModel getSpeakersPagination(@RequestParam Integer page,@RequestParam Integer size) {
        return new ApiResponseModel(true, "", speakerService.getSpeakersPagination(page,size));
    }


    @GetMapping("/{speakerId}")
    public HttpEntity<?> getSpeaker(@PathVariable UUID speakerId) {
        User user = userRepository.findById(speakerId).orElseThrow(() -> new ResourceNotFoundException("getSpeaker", "speakerId", speakerId));
        return ResponseEntity.ok(speakerService.getSpeaker(user));
    }

    @DeleteMapping("/{speakerId}")
    public HttpEntity<?> deleteSpeaker(@PathVariable UUID speakerId) {
        User user = userRepository.findById(speakerId).orElseThrow(() -> new ResourceNotFoundException("getSpeaker", "speakerId", speakerId));
        user.setEnabled(false);
        userRepository.save(user);
        return ResponseEntity.ok(new ApiResponse("", true));
    }


}
