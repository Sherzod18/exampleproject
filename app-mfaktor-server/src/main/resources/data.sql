-- Rollarni kiritish
INSERT INTO role(name) values('ROLE_USER'),('ROLE_DIRECTOR'),('ROLE_ADMIN'), ('ROLE_MANAGER'), ('ROLE_CASHIER'), ('ROLE_SPEAKER');
-- Kim orqali reklamani ko'rganligi'
INSERT INTO aware(name_uz, name_ru) VALUES ('Telegram', 'Телеграм'),('Facebook', 'Фейсбук'),('Instagram', 'Инстаграм'),('Mover','Мовер'),('Do''stlar orqali', 'Через друзей');
-- Viloyatlar
insert into region(name_uz, name_ru) values ('ТОШКЕНТ ШАҲАР', 'ТОШКЕНТ ШАҲАР');
insert into region(name_uz, name_ru) values ('ТОШКЕНТ ВИЛОЯТИ', 'ТОШКЕНТ ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('АНДИЖОН ВИЛОЯТИ', 'АНДИЖОН ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('БУХОРО ВИЛОЯТИ', 'БУХОРО ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('ЖИЗЗАХ ВИЛОЯТИ', 'ЖИЗЗАХ ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('ҚАШҚАДАРЁ ВИЛОЯТИ', 'ҚАШҚАДАРЁ ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('НАВОИЙ ВИЛОЯТИ', 'НАВОИЙ ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('НАМАНГАН ВИЛОЯТИ', 'НАМАНГАН ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('САМАРҚАНД ВИЛОЯТИ', 'САМАРҚАНД ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('СУРХОНДАРЁ ВИЛОЯТИ', 'СУРХОНДАРЁ ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('СИРДАРЁ ВИЛОЯТИ', 'СИРДАРЁ ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('ФАРҒОНА ВИЛОЯТИ', 'ФАРҒОНА ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('ХОРАЗМ ВИЛОЯТИ', 'ХОРАЗМ ВИЛОЯТИ');
insert into region(name_uz, name_ru) values ('ҚОРАҚАЛПОҒИСТОН РЕСП', 'ҚОРАҚАЛПОҒИСТОН РЕСП');
-- Faoliyat sohasi
insert into activity(name_uz, name_ru) values ('Ишлаб чиқариш','Производство');
insert into activity(name_uz, name_ru) values ('Хизмат кўрсатиш','Обслуживание');
insert into activity(name_uz, name_ru) values ('Савдо-сотиқ','Торговли');
insert into activity(name_uz, name_ru) values ('Таълим','Образование');
insert into activity(name_uz, name_ru) values ('Бошқалар','Другие');
--To'lov turlarini qo'shish
insert into pay_type(name_uz,name_ru)values ('Naqd','Наличие'),('Pul o''tkazish','Перечесление'),('Plastik','Пластик'),('Payme','Payme');
insert into position(name_uz,name_ru)values ('Бизнес эгаси(таъсисчи)','Владелец бизнеса(основатель)'),('Компания рахбари','Руководитель компании'), ('Иш бошқарувчи','Бизнес менеджер'), ('Маркетинг/Савдо бўлими рахбари','Маркетинг/Менеджер по продажам'),('Маркетинг/Савдо бўлими ходими','Маркетинг/Специалист по продажам'),('Стажер(талаба)','Стажер(студент)');

insert into event_type(name_uz,name_ru)values ('#MFaktorPraktikum','#MFaktorPraktikum'),('#ImpulseBusinessClub','#ImpulseBusinessClub');
insert into category(name_uz,name_ru)values ('Biznes haqida','О бизнеса'),('Shaxsiy rivojlanish','Индивидулная развития'),('Yetakchilik','Лидерства');
insert into language(name_uz,name_ru)values ('O''zbek tili','Узбекский'),('Rus tili','Русский'),('Ingliz tili','Английский');
-- insert into expence_type(name_uz,name_ru)values ('Elektr eneregiya','Свет'),('Bino ijarasi','Аренда здание'),('Oziq-ovqat','Питание');


