/**
 * Created by Pinup on 26.07.2019.
 */
import React, {Component} from 'react';
import NavigationAdmin from "components/Layout/NavigationAdmin"
import FooterAdmin from "components/Layout/FooterAdmin";
import Navigation from "components/Layout/Navigation"
import Footer from "components/Layout/Footer";
import {connect} from "dva";
import {config} from 'utils'
import UserCabinetNavigation from "../components/UserCabinetNavigation";

const {openPages, userPages} = config;

@connect(({app}) => ({app}))
class App extends Component {

  componentDidMount() {
    document.getElementById('mfaktor-loader').style.display = "none"
  }

  render() {
    const {app, dispatch} = this.props;
    const {currentUser, pathname} = app;
    const layoutUser = (<div style={{
      background: '#EEF2F5', minHeight: "100vh",
      position: "relative",
      paddingBottom: "200px"
    }}>
      <Navigation user={currentUser} path={pathname}/>
      {this.props.children}
      <Footer/>
    </div>);
    const modalShow = (e, modalName) => {
      e.preventDefault();
      dispatch({
        type: modalName + '/updateState',
        payload: {
          modalType: 'new',
          photoId: '',
          isModalShow: true,
          selectedUser: {}
        }
      })
    };
    if (pathname === '/login') {
      return this.props.children
    }
    if (currentUser.roles &&
      (openPages.includes(pathname) || pathname.includes('/videos/') || pathname.includes('/article/'))) {
      return layoutUser
    }
    return (
      currentUser.roles ?
        (currentUser.roles.filter(i => i.name === 'ROLE_ADMIN' || i.name === 'ROLE_DIRECTOR' || i.name === 'ROLE_CASHIER' || i.name === 'ROLE_MANAGER').length > 0 ?
          <div style={{background: '#E5E5E5', position: "relative", paddingBottom: "100px", minHeight: "100vh"}}>
            <NavigationAdmin user={currentUser} modalShow={modalShow} path={pathname}/>
            {this.props.children}
            <FooterAdmin/>
          </div>
          :
          currentUser.roles.length === 1 && currentUser.roles.filter(i => i.name === "ROLE_USER").length > 0 ?
            <div style={{background: '#E5E5E5', position: "relative", paddingBottom: "100px", minHeight: "100vh"}}>
              <UserCabinetNavigation/>
              {userPages.includes(pathname) || pathname.includes('/user/') ? this.props.children : ''}
              <div>
                <FooterAdmin/>
              </div>
            </div> : layoutUser)
        : layoutUser
    )
  }
}

export default App;
