import {Fragment} from "react";
import {ToastContainer} from 'react-toastify';
import App from './app';

function BasicLayout(props) {
  return (
    <Fragment>
      <App>
        <ToastContainer/>
        {props.children}
      </App>
    </Fragment>
  );
}

export default BasicLayout;
