module.exports={
  CORS:[],
  apiPrefix: '/api',
  openPages:['/login','/register','/','/events','/speakers','/article','/videos', '/events/registration', '/events/one'],
  userPages:['/user/edit','/user/editPassword', '/faq'],
  cashierPages:['/catalog/payType','/catalog/position',
    '/catalog/tariff','/catalog/voucher','/client','/expense','/payment','/voucher']
};
