import firebase from 'firebase/app';
import 'firebase/auth';

const prodConfig = {
  apiKey: "AIzaSyCoueoim7SwiFwzf6M8zlKrSYKjcug8cOg",
  authDomain: "app-mfaktor.firebaseapp.com",
  databaseURL: "https://app-mfaktor.firebaseio.com",
  projectId: "app-mfaktor",
  storageBucket: "",
  messagingSenderId: "969733401604",
  appId: "1:969733401604:web:41156b03ca1cda37"
};

const devConfig = {
  apiKey: "AIzaSyCoueoim7SwiFwzf6M8zlKrSYKjcug8cOg",
  authDomain: "app-mfaktor.firebaseapp.com",
  databaseURL: "https://app-mfaktor.firebaseio.com",
  projectId: "app-mfaktor",
  storageBucket: "",
  messagingSenderId: "969733401604",
  appId: "1:969733401604:web:41156b03ca1cda37"
};

const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const firebaseAuth = firebase.auth();

export {
  firebaseAuth, firebase
};
