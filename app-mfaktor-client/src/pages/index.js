import Header from "../components/Header";
import {Link} from "umi";
import OwlCarousel from 'react-owl-carousel2';
import {Button, Card, CardBody, CardFooter, CardImg, CardImgOverlay, Col, Container, Row} from 'reactstrap';
import "jquery/dist/jquery.min"
import "bootstrap/dist/js/bootstrap"
import React, {Component} from "react";
import ModalVideo from 'react-modal-video';
import {connect} from 'dva';
import {toast} from "react-toastify";

@connect(({landingPage}) => ({landingPage}))
class Index extends Component {

  componentDidMount() {
    this.openNotification();

    const {dispatch} = this.props;
    if (localStorage.getItem('token')) {
      dispatch({
        type: 'app/userMe'
      })

    }
    dispatch({
      type: 'landingPage/get3Article'
    });
    dispatch({
      type: 'landingPage/getLastVides'
    });
    dispatch({
      type: 'landingPage/getUpcomingEvents',
      payload: {
        size: 2
      }
    });
    dispatch({
      type: 'landingPage/getPublishedReviews',
    });
  }

  constructor() {
    super();
    this.state = {
      isOpen: false,
      random: [],
      speakers: [
        {
          img: "/assets/images/random/husanaka.jpg",
          name: "Husan Mamasaidov"
        },
        {
          img: "/assets/images/random/2.png",
          name: "Umid Ishmuhamedov"
        },
        {
          img: "/assets/images/random/3.png",
          name: "Laziz Adhamov"
        }, {
          img: "/assets/images/random/1.png",
          name: "Nodir Zokirov"
        }, {
          img: "/assets/images/random/alisher.jpg",
          name: "Alisher Isaev"
        }, {
          img: "/assets/images/random/6.png",
          name: "Zafar Hoshimov"
        }, {
          img: "/assets/images/random/8.png",
          name: "Murod Nazarov"
        }, {
          img: "/assets/images/random/hasanaka.jpg",
          name: "Hasan Mamasaidov"
        }, {
          img: "/assets/images/random/9.png",
          name: "Hikmat Abdurahmonov"
        }, {
          img: "/assets/images/random/12.png",
          name: "Akmal Paiziyev"
        }, {
          img: "/assets/images/random/bobir.jpg",
          name: "Bobir Akilkhanov"
        },
      ]
    };
    this.openModal = this.openModal.bind(this)
  }

  componentWillMount() {

    const random = this.state.random;
    const speakers = this.state.speakers;

    for (let i = 0; i < speakers.length; i++) {
      let x;
      let y;
      let zIndex;
      x = parseFloat(Math.floor(Math.random() * (1200 - 97 + 1))); //(max - min + 1)) + min;
      y = parseFloat(Math.floor(Math.random() * (550 - 97 + 1)));
      zIndex = Math.floor(Math.random() * (speakers.length - 1 + 1)) + 1;

      if (random.length !== 0) {
        for (let j = 0; j < random.length; j++) {
          let randomX = random[j].x;
          let randomY = random[j].y;
          if (randomX - 100 > x && randomX + 100 < x && randomY - 100 > y && randomY + 100 < y) {
            random.push({
              x: x,
              y: y,
              z: zIndex
            })
            break;
          } else {
            random.push({
              x: x + 100,
              y: y + 100,
              z: zIndex
            })
            break;
          }
        }
      } else {
        random.push({
          x: x,
          y: y,
          z: zIndex
        })
      }
    }
    this.setState({
      random: random
    })

  }

  openModal() {
    this.setState({isOpen: true})
  }

  openNotification = () => {
    toast.info('Platforma test rejimida ishlamoqda.');
  };

  render() {
    const {dispatch, landingPage} = this.props;
    const {

      latest3, fiveVideo, fourVideo, thirVideo, secVideo,
      firVideo, ytubeId, isModalShoww, reviewList, firVideoTit,
      secVideoTit, thirVideoTit, fourVideoTit, fiveVideoTit,
      upcomingEvents, lasVid
    } = landingPage;

    const modalShow2 = (youtubeId) => {
      dispatch({
        type: 'landingPage/getVideoIfameIds',
        payload: {
          frameUrl: youtubeId
        }
      });
    };
    const random = this.state.random;
    const speakers = this.state.speakers;
    const options = {
      items: 1,
      nav: false,
      loop: true,
      rewind: true,
      autoplay: true
    };

    return (
      <div className="mf-home">
        <Header/>
        <div className="container">
          <Row className="mt-5 mb-4">
            <Col md={6}>
              <h4 className="font-weight-bold">Navbatdagi tadbirlar</h4>
            </Col>
            <Col md={6} className="text-right">
              <Link to="/events" className="nav-link active pr-0">
                <Button color="outline-dark" className="btn-sm px-4">
                  Barchasi
                </Button>
              </Link>
            </Col>
          </Row>
          {upcomingEvents ?
            <div>
              {upcomingEvents.length > 0 ? upcomingEvents.map(i =>
                <Row className="py-3" key={i.id}>
                  <Col md={12}>
                    <Card className="card-list">
                      <div className="row mx-0 ">

                        <div className="col-md-4  p-0">
                          <CardImg src={i.photoUrl} className="rounded-0"/>
                        </div>

                        <div className="col-md-4 py-4 red-border">
                          <h3 className="font-weight-light text-ellipses px-md-5 px-0"
                              style={{boxOrient: "vertical"}}>“{i.title.length > 30 ?
                            <span>{i.title.substring(0, 45)}...</span> : i.title}”</h3>


                          <Row className=" ml-3  media-speker">
                            <Col md="6" className="col-6">
                              <p className="m-0 text-muted small">Spiker</p>
                              <p className="m-0 font-weight-bold text-body">{i.speakers}</p>
                            </Col>

                            <Col md="6" className="col-6">
                              <p className="m-0 small text-muted">Vaqti</p>
                              <h6
                                className="m-0 ">{i.startTime ? i.startTime.substring(0, 10) + ' ' + new Date(i.startTime).toTimeString().substring(0, 5) : ''}</h6>
                            </Col>
                          </Row>
                        </div>
                        <div className="col-md-4 px-0 red-border right-border ">
                          <CardFooter className="bg-transparent position-relative border-0">
                            {i.minPrice === i.maxPrice ? <div className="card-price">
                                <h1 className="text-center font-weight-bold m-0 pt-4">
                                  {i.maxPrice} <span className="font-weight-light text-upparcase small">UZS</span>
                                </h1>
                                <p className="m-0 text-muted text-center">
                                  Tadbir narxi
                                </p>
                              </div> :
                              <div className="row py-2">
                                <div className="col-md-6 text-center pt-2 border-right">
                                  <div className="font-weight">
                                    <span className="bold bold-span">{i.minPrice} </span>
                                    <span className="light">UZS</span>
                                  </div>
                                </div>
                                <div className="col-md-6 text-center pt-2">
                                  <div className="font-weight">
                                    <span className="bold bold-span">{i.maxPrice} </span>
                                    <span className="light">UZS</span>
                                  </div>
                                </div>
                                <div className="col-md-12 text-center">
                                  <p className="mb-0">Tadbir narxi shu oraliqda</p>
                                </div>
                              </div>}

                            <CardImgOverlay className="card-hover-block d-flex align-items-center py-3 rounded-top">
                              <p className="w-50 m-0 text-white font-weight-light">Tadbir uchun joy band qilish</p>
                              <div className="w-50 text-right">
                                <span className="icon icon-arrow-right bg-white"/>
                              </div>
                              <a href={"/events/" + i.id}/>
                            </CardImgOverlay>

                          </CardFooter>
                        </div>
                      </div>
                    </Card>
                  </Col>
                </Row>
              ) : <Row>
                <Col md={{size: 6, offset: 3}}>
                  <h4 className="text-center">Hozirda kutilayotgan tadbirlar mavjud emas</h4>
                </Col>
              </Row>}
            </div>
            : ''}


          <Row className="mt-5 mb-4 section-padding">

            <Col md={6}>
              <h4 className="font-weight-bold">Tadbirlar videolari</h4>
            </Col>
            <Col md={6} className="text-right">
              <Link to="/videos" className="nav-link active pr-0">
                <Button color="outline-dark" className="btn-sm px-4">Barchasi</Button>
              </Link>
            </Col>
          </Row>
          {lasVid ? <Row className="mb-3">
            <Col md={6}>
              {lasVid ? lasVid.length > 0 ?
                <Card className="card-video-block">
                  <div className='blog-theme blog-theme-lg' dangerouslySetInnerHTML={{
                    __html: lasVid[0].videoUrl ? lasVid[0].videoUrl : ''
                  }}>
                  </div>
                  <CardBody className="px-4 py-3">
                    <a onClick={() => modalShow2(lasVid[0].youtubeId)}>
                      <h3
                        className="m-0">{lasVid[0].title.length > 60 ? lasVid[0].title.substring(0, 60) + "..." : lasVid[0].title}</h3>
                    </a>
                  </CardBody>
                </Card>
                : "" : ''
              }
            </Col>

            <Col md={6} className="video-card ">
              <Row>
                {lasVid ? lasVid.map((item, i) =>
                  i !== 0 ?
                    <Col md={6}>
                      <Card style={{height: '100%'}} className="card-video-block">
                        <div className='blog-theme' dangerouslySetInnerHTML={{
                          __html: item.videoUrl ? item.videoUrl : ''
                        }}>
                        </div>
                        <CardBody className="px-2 py-3">
                          <a onClick={() => modalShow2(item.youtubeId)}>
                            <h4 className="m-0 pb-3 overflow-hidden">{item.title}</h4>
                          </a>
                        </CardBody>
                      </Card>
                    </Col> : ""
                ) : ""}
              </Row>
            </Col>
          </Row> : <Row>
            <Col md={{size: 6, offset: 3}}>
              <h4 className="text-center">Hozirda videolar mavjud emas</h4>
            </Col>
          </Row>}

          <Row className="mt-5 mb-4 section-padding">
            <Col md={6}>
              <h4 className="font-weight-bold">MFaktor spikerlari</h4>
              <p className="font-weight-normal font-md">
                Tadbirlarni olib boruvchi mutaxasislar O’zbekistonning
                muvaffaqiyatli tadbirkorlari, xalqaro darajadagi huquqshunoslar
                va o’z kasbining ustalari olib borishadi.
              </p>
            </Col>
            <Col md={6} className="text-right">
              <Link to="/speakers" className="nav-link active pr-0">
                <Button color="outline-dark" className="btn-sm px-4">Barchasi</Button>
              </Link>
            </Col>
          </Row>
          <Row className="my-5 py-5">
            <Col md={12}>
              <div className="mf-speakers">
                <div className="mf-brand mx-auto">
                  <img src="/assets/images/logoFooter.png" className="img-fluid" alt="a"/>
                </div>

                {
                  speakers.length !== 0 ?
                    speakers.map((item, i) => {
                      return (
                        <div className="mf-speaker-users" key={i}>
                          <img src={item.img} className="mf-speaker-avatar" alt="a"/>
                          <div className="mf-speaker-user-info font-weight-bold">
                            {item.name}
                          </div>
                        </div>
                      )
                    }) : ""
                }
              </div>
            </Col>
          </Row>

          <Row className="mt-5 mb-4 section-padding">
            <Col md={6}>
              <h4 className="font-weight-bold">Maqolalar</h4>
            </Col>
            <Col md={6} className="text-right">
              <Link to="/article" className="link">
                <Button color="outline-dark" className="btn-sm px-4">Barchasi</Button>
              </Link>
            </Col>
          </Row>

          {latest3 ? <div>
            {latest3.length > 0 ? <div>

              <Row className="mt-5 mb-4">

                {latest3.map((post, i) => <Col md={4} key={i}>
                  <Card className="card-article article-card mb-3">
                    <CardImg className="articles-cards" style={{"height": "250px"}} src={'/api/file/' + post.photo.id}/>
                    <CardBody>
                      <p className="mb-3 text-muted border-left border-danger p-0 pl-2">{post.category.nameUz}</p>
                      <Link to={"/article/" + post.url}><h4 dangerouslySetInnerHTML={{
                        __html: post.title
                      }}></h4></Link>
                    </CardBody>
                    <CardFooter className="article-footer">
                      <div className="d-flex m-0 text-muted text-center">
                        <div className="w-40">{post.createdAt.substring(0, 10)}</div>
                        <div className="w-25"><span className="icon icon-chat text-muted small"/> {post.viewsCount}
                        </div>
                      </div>
                    </CardFooter>
                  </Card>
                </Col>)}
              </Row>
            </div> : <Row>
              <Col md={{size: 6, offset: 3}}>
                <h4 className="text-center">Hozirda maqolalar mavjud emas</h4>
              </Col>
            </Row>}
          </div> : <Row>
            <Col md={{size: 6, offset: 3}}>
              <h4 className="text-center">Hozirda maqolalar mavjud emas</h4>
            </Col>
          </Row>}

          <Row className="mt-5 mb-4 section-padding">
            <Col md={3}>
              <h4 className="font-weight-bold">MFaktor haqida boshqalar fikri</h4>
            </Col>
          </Row>

          {reviewList ? <div>
            {reviewList.length > 0 ?

              <div>
                <Row className="mb-5">
                  <Col md={{offset: 1, size: 10}} className="px-4">
                    <OwlCarousel ref="car" options={options}>

                      {reviewList.map((review, i) => {
                        return (
                          <Card key={i} className="card-feedback bg-transparent py-5">
                            <CardImg src="assets/images/img/quad.png" className="img-quete"/>
                            <CardBody>
                              <p className="font-weight-normal p-5 m-0">
                                {review.text}
                              </p>
                            </CardBody>
                            <div className="d-flex card-feedback-user align-items-center">
                              <div className="w-25">
                                <CardImg src={review.photoUrl ? review.photoUrl : '/assets/images/icon/user.svg'}/>
                              </div>
                              <div className="w-70 px-4">
                                <h4 className="font-weight-bold mb-0">{review.firstName} {review.lastName}</h4>
                                <p className="text-danger m-0">{review.company}</p>
                              </div>
                            </div>
                          </Card>
                        )
                      })}
                    </OwlCarousel>
                  </Col>
                </Row>
              </div> : <Row>
                <Col md={{size: 6, offset: 3}}>
                  <h4 className="text-center">Hozirda sharhlar mavjud emas</h4>
                </Col>
              </Row>}
          </div> : <Row>
            <Col md={{size: 6, offset: 3}}>
              <h4 className="text-center">Hozirda sharhlar mavjud emas</h4>
            </Col>
          </Row>}


        </div>
        <ModalVideo channel='youtube' isOpen={isModalShoww} videoId={ytubeId}
                    onClose={() => modalShow2("")}/>


      </div>
    )
  }
}

export default Index;
