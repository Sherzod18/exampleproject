import React, {Component} from 'react';
import {connect} from "react-redux";
import {Card, CardFooter, CardImg, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import {Link} from "react-router-dom";
import SpeakerModal from "components/SpeakerModal";
import PlaceTemplateModal from "components/PlaceTemplateModal";
import DatePicker from "react-datepicker/es";
import Loader from "react-loader-spinner";
import {toast} from "react-toastify";
import Button from 'react-bootstrap-button-loader';

@connect(({eventModel, app}) => ({eventModel, app}))
class Event extends Component {
  componentDidMount() {
    const {dispatch, eventModel, payType} = this.props;
    dispatch({
      type: 'eventModel/getEvents'
    });
    dispatch({
      type: 'app/getPosition'
    });
    dispatch({
      type: 'app/getLanguages'
    });
    dispatch({
      type: 'app/getSpeakers'
    });
    dispatch({
      type: 'eventModel/getTemplatePlaces'
    });
    dispatch({
      type: 'eventModel/getEventTypes'
    });

  }

  scrollChairsBlock = () => {
    setTimeout(function () {
      const elements = document.getElementsByClassName("set-list");
      for (const e of elements) {
        e.scrollTo(e.clientWidth, e.clientHeight);
      }
    }, 200)
  }


  render() {
    const {dispatch, eventModel, app} = this.props;
    const {photoId, speakers, isLoading, languages, eventBannerId} = app;
    const {
      events, isModalShow, templatePlaces, fixed, eventTypes, dateTime, speakersId,
      reqChairs, letters, place, places, isLoadingEvent, event, eventStatuses, isModalShowStatus,
      registerTime, eventStatus, loading, page, size, status, isFilter
    } = eventModel;

    const closeModal = () => {
      dispatch({
        type: 'app/updateState',
        payload: {
          isModalPlaceShow: false,
          isLoading: false
        }
      })
    };
    const getEvents = (e, v) => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          status: v,
          isFilter: true
        }
      });
      dispatch({
        type: 'eventModel/getEvents',
      });

    };
    const moreEvent = () => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          page: page,
          size: size + 10
        }
      });
      dispatch({
        type: 'eventModel/getEvents',
      })
    };
    const openModalSpeaker = () => {
      dispatch({
        type: "app/updateState",
        payload: {
          isModalShow: !app.isModalShow,
          currentItem: '',
          photoId: '',
          logosId: []
        }
      });
      dispatch({
        type: "app/updateState",
        payload: {
          photoId: '',
        }
      });
    };
    const saveSpeaker = (e, v) => {
      if (photoId !== '') {
        dispatch({
          type: 'app/registerSpeaker',
          payload: {
            ...v,
          }
        })
      } else {
        toast.error("Rasm yuklash majburiy")
      }
    };
    const logosSpeaker = (e) => {
      dispatch({
        type: 'app/uploadLogo',
        payload: {
          file: e.target.files,
          fileUpload: true,
          type: 'COMPANY_LOGO'
        }
      })
    };
    const uploadAvatarSpeaker = (e) => {
      dispatch({
        type: 'app/uploadAvatar',
        payload: {
          file: e.target.files,
          fileUpload: true,
          type: 'SPEAKER_AVATAR'
        }
      })
    };
    const addRowChairs = () => {
      let line = document.getElementById("place_line_count").value;
      let price = document.getElementById("place_price").value;
      if (Number(line) && Number(price)) {
        dispatch({
          type: 'eventModel/addRow',
          payload: {
            line,
            price,
          },
        });
        document.getElementById("place_line_count").value = "";
        document.getElementById("place_price").value = "";
        this.scrollChairsBlock();
      }
    };
    const deleteChair = (index) => {
      let chairs = reqChairs[index].slice();
      chairs.splice(chairs.length - 1, 1);
      if (chairs.length === 0) {
        reqChairs.splice(index, 1);
        reqChairs.filter(function (item) {
          return item !== undefined;
        });
        dispatch({
          type: 'eventModel/updateState',
          payload: {
            reqChairs: reqChairs,
          }
        })
      } else {
        reqChairs[index] = chairs.slice();
        dispatch({
          type: 'eventModel/updateState',
          payload: {
            reqChairs: reqChairs,
          }
        })
      }

    };
    const addChair = (index) => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          reqChairs: reqChairs.map((item, i) => {
            if (index === i) {
              item.push({
                id: item.length,
                row: index + 1,
                name: (index + 1) + letters[item.length],
                price: item[0].price,
              })
            }
            return item;
          })
        }
      });
      this.scrollChairsBlock();
    };
    const getNamePlace = (e) => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    };
    const saveChairs = () => {
      dispatch({
        type: 'eventModel/saveChairs',
        payload: {
          name: document.getElementById("place_name").value,
          id: places ? places.selectedTemplateId : ''
        }
      })
    };
    const openModalPlace = () => {
      dispatch({
        type: "app/updateState",
        payload: {
          isModalPlaceShow: !app.isModalPlaceShow,
        }
      });
      dispatch({
        type: "eventModel/updateState",
        payload: {
          reqChairs: [],
        }
      });

    };
    const changeDateTime = (date) => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          dateTime: date
        }
      })
    };
    const changeRegisterTime = (date) => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          registerTime: date
        }
      })
    };
    const editEvent = (item) => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          event: item,
          isModalShow: true,
          dateTime: new Date(item.startTime),
          registerTime: new Date(item.registerTime)
        }
      });
      dispatch({
        type: 'app/updateState',
        payload: {
          eventBannerId: item.photoUrl.substring(item.photoUrl.lastIndexOf("/") + 1)
        }
      })
    };
    const changeStatus = (event) => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          isModalShowStatus: !isModalShowStatus,
          eventId: event.id,
          eventStatus: event.status
        }
      });
      dispatch({
        type: 'eventModel/getEventStatuses',
      });
    };
    const saveChangeStatus = (e, v) => {
      dispatch({
        type: 'eventModel/changeEventStatus',
        payload: {
          ...v
        }
      })
    };
    const save = (e, v) => {
      dispatch({
        type: 'eventModel/addEvent',
        payload: {
          ...v,
          path: event ? event.id : '',
          eventBannerId: event ? event.eventBannerId : eventBannerId,
          startTime: dateTime,
          eventTypeId: event.eventTypeId ? event.eventTypeId : v.eventTypeId,
          registerTime: registerTime
        }
      })
    };
    const uploadEventBanner = (e) => {
      dispatch({
        type: 'app/uploadEventBanner',
        payload: {
          file: e.target.files,
          fileUpload: true,
          type: "EVENT_BANNER"
        }
      })
    };
    const fixedPrice = () => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          fixed: true
        }
      })
    };
    const noFixedPrice = () => {
      dispatch({
        type: 'eventModel/updateState',
        payload: {
          fixed: false
        }
      })
    };
    const openModal = () => {
      dispatch({
        type: "eventModel/updateState",
        payload: {
          isModalShow: !isModalShow,
          dateTime: new Date(),
          event: {},
          registerTime: new Date(),
          fixed: false

        }
      });
      dispatch({
        type: "app/updateState",
        payload: {
          photoId: '',
          eventBannerId: '',
        }
      });
    };

    return (
      <div className="mb-5">
        <Button className="btn-plus-32" onClick={(e) => this.props.modalShow(e, "eventModel")}>ADD
        </Button>

        {loading ? <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px', marginBottom: '150px'}}>
          <Loader
            type="Triangle"
            color="#ED2939"
            height="100"
            width="100"
          />
        </div> : <div>
          {
            events.length !== 0
              ?
              <div className="container event-page">
                <Row className="pt-2 pt-md-5">
                  <Col md={2}>
                    <h3 className="header-title">Tadbirlar</h3>
                  </Col>
                  <Col md={3} className="offset-5">
                    <AvForm>
                      <AvInput type="select" value="0" name="status" onChange={getEvents}>
                        <option value="0" disabled>Tadbirlarni saralash</option>
                        <option value="all">Hammasi</option>
                        <option value="draft">Qoralama</option>
                        <option value="open">Ochiq</option>
                        <option value="registered">Ro'yxatdan o'tkazish yakunlangan</option>
                      </AvInput>
                    </AvForm>
                  </Col>

                  <Col md={2}>
                    <button className="btn btn-success px-4 py-2 float-right" onClick={openModal}>Qo'shish</button>
                  </Col>
                </Row>
                <section>

                  <Row className="icon-row">
                    {events.map((item, i) =>
                      <Col md={12} className="mb-4" key={item.id}>
                        <Card className="card-list">
                          <div className="d-flex">
                            <div className="w-30">
                              <CardImg src={item.photoUrl} className="rounded-0"/>
                            </div>
                            <div className="w-40 py-4">
                              <h3 className="font-weight-light px-5">{item.isOpen ?
                                <Link to={"/event/clients/" + item.id}><h3
                                  className="event_name">{item.title}</h3></Link> : <h3
                                  className="event_name">{item.title}</h3>}</h3>
                              <div className="d-flex px-5 pt-4">
                                <div className="w-50">
                                  <p className="m-0 text-muted small">Spiker</p>
                                  <p className="m-0 font-weight-bold text-body">{item.speakers}</p>
                                </div>
                                <div className="w-50">
                                  <p className="m-0 small text-muted">Vaqti</p>
                                  <p className="m-0 font-weight-bold text-body">
                                    {item.startTime ? item.startTime.substring(0, 10) + ' ' + new Date(item.startTime).toTimeString().substring(0, 5) : ''}</p>
                                </div>
                              </div>
                            </div>
                            <div className="w-30 pt-4">
                              <CardFooter className="bg-transparent position-relative border-0">
                                <div className="card-price">
                                  <div className="d-flex">

                                    <div className="w-33 text-center ">
                                      <h1 className="m-0 font-weight-bold">
                                        {item.allSeat}
                                      </h1>
                                      <p className="small font-weight-light text-muted">
                                        Joylar
                                      </p>
                                    </div>
                                    <div className="w-33 text-center ">
                                      <h1 className="m-0 font-weight-bold text-muted">
                                        {(item.bookedAndPartly)}
                                      </h1>
                                      <p className="small font-weight-light text-muted">
                                        Band
                                      </p>
                                    </div>
                                    <div className="w-33 text-center ">
                                      <h1 className="m-0 font-weight-bold text-success">
                                        {(item.soldSeat)}
                                      </h1>
                                      <p className="small font-weight-light text-muted">
                                        Sotilgan
                                      </p>
                                    </div>
                                    <div className="w-33 text-center ">
                                      <h1 className="m-0 font-weight-bold text-danger">
                                        {item.emptySeat}
                                      </h1>
                                      <p className="small font-weight-light text-muted">
                                        Sotilmagan
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div className="card-price">
                                  <div className="d-flex">
                                    <div className="text-center d-flex pl-4">
                                      <div className="rate pt-4 pl-2">
                                        <p className="price-sum font-weight-bold">
                                      <span className="pr-3 "><img className="eyes-img" src="/assets/images/eye.png"
                                                                   alt=""/></span>
                                          {(new Intl.NumberFormat('fr-FR').format(item.sum))} <span>UZS</span></p>
                                        <p className="all-sum">Umumiy yig’ilgan pul miqdori</p>
                                      </div>
                                    </div>

                                  </div>
                                </div>
                              </CardFooter>
                            </div>
                          </div>
                          <div><Button className="btn-plus-46" onClick={() => editEvent(item)}>
                            <span className="icon icon-edit bg-white"/>
                          </Button>
                            <Button className="btn-event-status-edit" onClick={() => changeStatus(item)}>
                              <span className="icon icon-check bg-white"/>
                            </Button></div>
                        </Card>
                      </Col>)}
                  </Row>
                  <Row className="mt-4 mt-md-5">
                    <Col md={{size: 2, offset: '5'}} className="col-6 mb-5">
                      <Button onClick={moreEvent} className="btn-block btn-danger">
                        Yana yuklash
                      </Button>
                    </Col>
                  </Row>
                </section>
              </div>
              :
              <div>
                {isFilter && events.length === 0 ?
                  <div className="container event-page">
                    <Row className="pt-2 pt-md-5">
                      <div className="col-md-3 offset-7">
                        <AvForm>
                          <AvInput type="select" value="0" name="status" onChange={getEvents}>
                            <option value="0" disabled>Tadbirlarni saralash</option>
                            <option value="all">Hammasi</option>
                            <option value="draft">Qoralama</option>
                            <option value="open">Ochiq</option>
                            <option value="registered">Ro'yxatdan o'tkazish yakunlangan</option>
                          </AvInput>
                        </AvForm>
                      </div>
                      <Col md={2}>
                        <button className="btn btn-success px-4 py-2 float-right" onClick={openModal}>Qo'shish</button>
                      </Col>
                    </Row>
                  </div>
                  : ''}
                <div className="empty-event">
                  <div className="container">
                    <p className="now-event">Hozirda {isFilter ? 'siz saralagan turdagi ' : ''}tadbirlar mavjud emas</p>
                    {isFilter ? '' : <div className="row">
                      <div className="col-md-12 mb-5" onClick={openModal}>
                        + <span className="pl-2">Yangi tadbir yaratish</span>
                      </div>
                    </div>}
                  </div>
                </div>
              </div>
          }
        </div>
        }


        <Modal isOpen={isModalShow} toggle={openModal} className="custom-modal">
          <ModalHeader toggle={openModal} className="border-0"/>
          <ModalBody className="">
            <AvForm>
              <main className="add-events">
                <section className="speaker_form">
                  <div className="container">
                    <h2 className="title pl-3">
                      Tadbir {event && event.id ? 'tahrirlash' : 'yaratish'}</h2>
                    <div className=" border-0">
                      <div className="card-body bg-white pt-0">
                        <div className="col-md-12 mb-4">
                          <AvForm onValidSubmit={save}>
                            <div className="row mt-4">
                              <div className="col-md-6">
                                <label htmlFor="eventImg" className="label-for-img">
                                  <div className="row image-upload">
                                    <img style={{width: '100%', borderRadius: '5px'}}
                                         src={eventBannerId ? '/api/file/' + eventBannerId : ''}
                                         width="100"/>
                                    <div
                                      style={eventBannerId ? {display: 'none'} : {display: 'block'}}
                                      className="col-md-6 offset-md-3 upload-box text-center">
                                      <div className="border upload-button">
                                        <span className="icon icon-upload"/> Tadbir uchun rasm
                                        <input className="d-none" name="attachment" id="eventImg"
                                               onChange={uploadEventBanner}
                                               type="file" required/>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-12 mt-3 text-center">
                                    <h3 className="recommend">Tavsiya qilingan o'lcham: 1280x858</h3>
                                  </div>
                                </label>
                              </div>
                              <div className="col-md-6">
                                <AvGroup>
                                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                                  <AvInput name="serialNumber" type="text" value={event && event.serialNumber}
                                           required/>
                                  <label>Tadbir tartib raqami</label>
                                </AvGroup>
                                <AvGroup className="mt-4">
                                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                                  <AvInput name="title" type="text" value={event && event.title} required/>
                                  <label>Tadbir nomi</label>
                                </AvGroup>
                                <div className="row mt-4">
                                  <div className="col-md-9">
                                    <AvGroup>
                                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                                      <AvInput type="select" multiple name="speakersId"
                                               value={event && event.speakersId} required={event && !event.id}>
                                        <option>Spikerni tanlang</option>
                                        {speakers ? speakers.map((i) => <option key={i.id}
                                                                                value={i.id}>{i.fullName}</option>) : ""}
                                      </AvInput>
                                      <label>Spiker</label>
                                    </AvGroup>
                                  </div>
                                  <div className="col-md-3">
                                    <button type="button" className="btn btn-block add_button h-100"
                                            onClick={openModalSpeaker}>+
                                    </button>
                                  </div>
                                </div>
                                <AvGroup className="mt-4">
                                  <AvInput type="select" name="eventTypeId" disabled={event.id}
                                           required={event && !event.id}>
                                    {event && event.eventType ? <option>{event.eventType}</option> : ''}
                                    {eventTypes.map((i) => <option key={i.id} value={i.id}>{i.nameUz}</option>)}
                                  </AvInput>
                                  <label>Tadbir turini</label>
                                </AvGroup>
                              </div>
                              <div className="col-md-6 pt-4 pl-0 pr-0">
                                <div className="input_wrap border pl-3">
                                  <label htmlFor="more" className="mb-0 mt-1 ml-2">Batafsil</label>
                                  <AvInput type="textarea" name="more" id="more" cols={30} rows={2} placeholder="..."
                                           value={event.id ? event.aboutEvent : ''}
                                           className="form-control border-0 p-0 pl-2 "/>
                                </div>
                              </div>
                              <div className="col-md-6 pl-0 pr-0 mt-4">
                                <div className="col-md-12">
                                  <div className="row">
                                    <div className="col-md-9">
                                      <AvGroup>
                                        <AvInput type="select" name="placeCapacityId" disabled={event.id}
                                                 required={event && !event.id}>
                                          <option disabled={true}>Stollar joylashuvini tanlang</option>
                                          {templatePlaces.map((i) => <option key={i.id} value={i.id}>{i.name}</option>)}
                                        </AvInput>
                                        <label>Stollar joylashuvi</label>
                                      </AvGroup>
                                    </div>
                                    <div className="col-md-3 pr-0">
                                      <button type="button" className="h-100 btn add_button" disabled={event.id}
                                              onClick={openModalPlace}>+
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-6 pr-0 pl-0">
                                <div className="row mt-3">
                                  <div className="col-md-9">
                                    <AvGroup>
                                      <DatePicker
                                        name="birthDate"
                                        selected={registerTime}
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        className="form-control"
                                        onChange={changeRegisterTime}
                                        timeIntervals={30}
                                        dateFormat="dd.MM.yyyy  hh:mm aa"
                                        timeCaption="time"
                                      />
                                      <label className="label-date">Ro'yxatga olish vaqti</label>
                                    </AvGroup>
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-3 mb-3">
                                <div className="col-md-12">
                                  <label className="mb-0" htmlFor="price">Narx ko'rinishi</label>
                                </div>
                                <div className="col-md-12"><input type="radio" name="fixed" id="price"
                                                                  onChange={fixedPrice}
                                                                  className=" border-0 p-0 mb-2"/> O'zgarmas
                                  {fixed ?
                                    <AvGroup className="mt-4">
                                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                                      <AvInput name="fixedPrice" type="text" required/>
                                      <label>Narxni kiriting</label>
                                    </AvGroup> : ''}
                                </div>
                                <div className="col-md-12"><input type="radio" name="fixed" checked={!fixed}
                                                                  onChange={noFixedPrice}
                                                                  className=" border-0 p-0 mb-2"/> Joy bo'yicha
                                </div>
                              </div>
                              <div className="col-md-3 mt-2">
                                <AvField type="checkbox" value={event.isTop} label="Tadbirning botda yuqori chiqishi"
                                         name="isTop"/>
                              </div>
                              <div className="col-md-6 pr-0 pl-0">
                                <div className="row mt-3">
                                  <div className="col-md-9">
                                    <AvGroup>
                                      <DatePicker
                                        name="birthDate"
                                        selected={dateTime}
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        className="form-control"
                                        onChange={changeDateTime}
                                        timeIntervals={30}
                                        dateFormat="dd.MM.yyyy  hh:mm aa"
                                        timeCaption="time"
                                      />
                                      <label className="label-date">Boshlanish vaqti</label>
                                    </AvGroup>
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-6 pr-0 pl-0">
                                <div className="row">
                                  <div className="col-md-9">
                                    <AvGroup className="mt-4">
                                      <AvInput type="select" name="languageId" disabled={event.id}
                                               required={event && !event.id}>
                                        {event && event.languageId ?
                                          <option>{event.language ? event.language : ""}</option> : ''}
                                        {languages.map((i) => <option key={i.id} value={i.id}>{i.nameUz}</option>)}
                                      </AvInput>
                                      <label>Tadbir tilini tanlang</label>
                                    </AvGroup>
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-3 offset-md-9 mt-5">
                                <Button type="submit" bsStyle="success"
                                        className="d-block btn-block ml-auto btn ml-4 py-2 px-5"
                                        loading={isLoadingEvent}>Saqlash</Button>
                              </div>
                            </div>
                          </AvForm>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </main>
            </AvForm>
          </ModalBody>
        </Modal>

        <SpeakerModal openModal={openModalSpeaker} save={saveSpeaker}
                      currentItem={""}
                      isLoading={isLoading}
                      photoId={photoId}
                      logosId={app.logosId}
                      logosSpeaker={logosSpeaker}
                      uploadAvatarSpeaker={uploadAvatarSpeaker}
                      positions={app.positions}
                      isModalShow={app.isModalShow}
        />

        <PlaceTemplateModal openModal={openModalPlace}
                            isModalShow={app.isModalPlaceShow}
                            addChair={addChair}
                            isLoading={isLoadingEvent}
                            deleteChair={deleteChair}
                            addRowChairs={addRowChairs}
                            saveChairs={saveChairs}
                            reqChairs={reqChairs}
                            place={place}
                            getName={getNamePlace}
                            closeModal={closeModal}
        />

        <Modal isOpen={isModalShowStatus} toggle={changeStatus}>
          <ModalHeader toggle={changeStatus} className="border-0">
            <h3>Tadbir holatini tahrirlash</h3>
          </ModalHeader>
          <ModalBody className="pl-5 pr-5">
            <AvForm onValidSubmit={saveChangeStatus}>
              <div className="col-md-12 mb-5">
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput type="select" name="eventStatus" value={eventStatus} required>
                    {eventStatuses.map((i) => <option value={i.name}>{i.value}</option>)}
                  </AvInput>
                  <label>Tadbir statusi</label>
                </AvGroup>
              </div>
              <div className="row mt-3" style={{justifyContent: "flex-end"}}>
                <Button onClick={changeStatus}
                        className="mr-4" outline>Bekor qilish</Button>
                <Button type="submit" bsStyle="success"
                        className="d-block ml-auto btn ml-4 py-2 px-5"
                        loading={isLoadingEvent}>Saqlash</Button>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>

      </div>
    );
  }
}

Event.propTypes = {};

export default Event;
