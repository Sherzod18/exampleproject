import api from 'api'
// import {routerRedux} from 'dva/router'
import {parsePhoneNumber} from 'libphonenumber-js'
import {STORAGE_NAME} from 'utils/constant';
const {getTemplatePlaces, deleteTemplate} = api;

export default ({
  namespace: 'places',
  state: {
    placesList: [],
    selectedTemplateId: '',
    delModalShow: false,
    deleteId: 0,
  },
  subscriptions: {},
  effects: {
    * getPlaces({payload}, {call, put, select}){
      const res = yield call(getTemplatePlaces);
      yield put({
        type: "updateState",
        payload: {
          placesList: res.object
        }
      })
    },
    * deleteTemplate({payload}, {call, put, select}){
      const res = yield call(deleteTemplate, payload)
      if (res.success) {
        yield put({
          type: 'getPlaces'
        })
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})

