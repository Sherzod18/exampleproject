import React, {Component} from 'react';
import {connect} from "react-redux";
import {
  Row,
  Col,
  Button,
  Card,
  CardImg,
  CardFooter,
  Modal,
  ModalHeader,
  ModalBody,
  Container,
  UncontrolledPopover, PopoverHeader, PopoverBody, ModalFooter
} from "reactstrap";
import {Table} from 'reactstrap';
import {router, Link} from 'umi';
import FooterAdmin from "../../../components/Layout/FooterAdmin";
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import DatePicker from "react-datepicker/es";
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({places}) => ({places}))
class Places extends Component {
  componentDidMount() {
    const {dispatch, places} = this.props;
    dispatch({
      type: 'places/updateState',
      payload: {
        selectedTemplateId: ''
      }
    });
    dispatch({
      type: 'places/getPlaces'
    })
  }

  render() {
    const {dispatch, places} = this.props;
    const {placesList, delModalShow, deleteId} = places;
    const deleteTemplate = () => {
      dispatch({
        type: 'places/deleteTemplate',
        payload: {
          id: deleteId
        }
      });
      deleteModalShow(-1);
    }
    const editTemplate = (id) => {
      dispatch({
        type: 'places/updateState',
        payload: {
          selectedTemplateId: id
        }
      });
      router.push('/event/addSeats');
    };
    const openAddSeatPage = () => {
      dispatch({
        type: 'places/updateState',
        payload: {
          selectedTemplateId: ''
        }
      });
      router.push('/event/addSeats');
    }
    const deleteModalShow = (id) => {
      if (id !== -1) {
        dispatch({
          type: 'places/updateState',
          payload: {
            deleteId: id
          }
        });
      }
      dispatch({
        type: 'places/updateState',
        payload: {
          delModalShow: !delModalShow
        }
      });
    }
    return (
      <div className="container">

        <div className="places-page">
          <Row className="pt-2 pt-md-5 pl-5">
            <Col md={2}>
              <h3 className="header-title pl-5">Joylashuv</h3>
            </Col>
            <Col md={2} className="offset-8">
              <button className="btn btn-success px-4 py-2" onClick={openAddSeatPage}>Qo'shish</button>
            </Col>
          </Row>
          <Row className="mt-4">
            <Col md="3" className="pl-4 text-center">
              <h2 className="font-weight-bolder">Nomi</h2>
            </Col>
            <Col md="3" className="text-center">
              <h2 className="font-weight-bolder">Stullar</h2>
            </Col>
            <Col md="3" className="text-center">
              <h2 className="font-weight-bolder">Arzon-Qimmat</h2>
            </Col>
            <Col md="3" className="text-center">
              <h2 className="font-weight-bolder">Amallar</h2>
            </Col>
          </Row>
          {placesList.map((item, a) =>
            <div key={a} className="mt-3">
              <div className="client-list-item pt-2">
                <Row>
                  <Col md="3" className='pl-4 text-center'>
                    <p>{item.name}</p>
                  </Col>
                  <Col md="3" className="text-center">
                    <p>{item.chairs.length}</p>
                  </Col>
                  <Col md="3" className="text-center">
                    <p>{Math.min(...item.chairs.map(i => i.price)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' - '
                    + Math.max(...item.chairs.map(i => i.price)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</p>
                  </Col>
                  <Col md="3" className="text-center">
                    <button className="btn btn-edit-28 p-0"><span
                      className="icon icon-edit" onClick={() => editTemplate(item.id)}/></button>
                    <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(item.id)}><span
                      className="icon icon-delete"/></button>
                  </Col>
                </Row>
                <div className="line"/>
              </div>
            </div>
          )}

          <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
            <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
            <ModalFooter>

              <Button className="btn-sm" color="primary" onClick={deleteTemplate}>Ha</Button>
              <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
            </ModalFooter>
          </Modal>
        </div>

      </div>

    );
  }
}

export default Places;
