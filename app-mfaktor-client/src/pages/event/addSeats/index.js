import React, {Component} from 'react';
import {connect} from "react-redux";
import {Row, Col, Badge, Button, Modal, ModalHeader, ModalBody} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import FooterAdmin from "../../../components/Layout/FooterAdmin";

@connect(({addSeat, places}) => ({addSeat, places}))
class AddSeats extends Component {
  componentDidMount() {
    const {dispatch, places} = this.props;
    dispatch({
      type: 'addSeat/updateState',
      payload: {
        reqChairs: [],
        tempIndex: 1,
        tempId: 1,
        name: '',
        place: {},
        row: 1,
      }
    });
    if (places) {
      if (places.selectedTemplateId) {
        dispatch({
          type: 'addSeat/getPlaceById',
          payload: {
            path: places.selectedTemplateId
          }
        })
      }
    }
    this.scrollChairsBlock();
  }

  scrollChairsBlock = () => {
    setTimeout(function () {
      const elements = document.getElementsByClassName("set-list");
      for (const e of elements) {
        e.scrollTo(e.clientWidth, e.clientHeight);
      }
    }, 200)
  };

  render() {
    const {dispatch, addSeat, places} = this.props;
    const {reqChairs, letters, place, closeModal, isLoading} = addSeat;
    const deleteChair = (index) => {
      let chairs = reqChairs[index].slice();
      chairs.splice(chairs.length - 1, 1)
      if (chairs.length === 0) {
        reqChairs.splice(index, 1)
        reqChairs.filter(function (item) {
          return item !== undefined;
        });
        dispatch({
          type: 'addSeat/updateState',
          payload: {
            reqChairs: reqChairs,
          }
        })
      } else {
        reqChairs[index] = chairs.slice();
        dispatch({
          type: 'addSeat/updateState',
          payload: {
            reqChairs: reqChairs,
          }
        })
      }

    }
    const addRowChairs = (e) => {
      let line = document.getElementById("place_line_count").value;
      let price = document.getElementById("place_price").value;
      if (Number(line) && Number(price)) {
        dispatch({
          type: 'addSeat/addRow',
          payload: {
            line,
            price,
          },
        });
        document.getElementById("place_line_count").value = "";
        document.getElementById("place_price").value = "";
        this.scrollChairsBlock();
      }
    };
    const addChair = (index) => {
      dispatch({
        type: 'addSeat/updateState',
        payload: {
          reqChairs: reqChairs.map((item, i) => {
            if (index === i) {
              item.push({
                id: item.length,
                row: index + 1,
                name: (index + 1) + letters[item.length],
                price: item[0].price,
              })
            }
            return item;
          })
        }
      })
      this.scrollChairsBlock();
    }
    const getName = (e) => {
      dispatch({
        type: 'addSeat/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    };
    const saveChairs = () => {
      dispatch({
        type: 'addSeat/saveChairs',
        payload: {
          name: document.getElementById("place_name").value,
          id: places ? places.selectedTemplateId : ''
        }
      })
    };
    return (
      <div>
        <main>
          <section className="place_page">

            <div className="container">
              <h2 className="title pt-5">Stollar joylashuvi qo'shish</h2>
              <div className="row m-0">
                <div className="col-md-12 set-card bg-white">
                  <AvForm>

                    <div className="row mb-2 pt-3">
                      <div className="col-md-12">
                        <div className="row">
                          <div className="col-md-4 wrap_input px-3">
                            <label htmlFor="place_name">Joylashuv nomi</label>
                            <AvField type="text" onChange={getName} id="place_name" name="name"
                                     value={place.name || ''}
                                     className="form-control border-0"/>
                          </div>
                          <div className="col-md-2 wrap_input ml-md-4 ml-0 mt-md-0 mt-3 px-3">
                            <label htmlFor="place_count">O'rinlar soni</label>
                            <h1><Badge
                              color="danger">{reqChairs.reduce((res, item) => res + item.length, 0)}</Badge></h1>
                          </div>

                        </div>
                      </div>
                    </div>

                    {reqChairs ? <div>{reqChairs.length > 0 ? <div className="row">
                        <div className="col-md-4 wrap_input px-3">
                          <label htmlFor="place_name">Qator narxi</label>
                        </div>
                      </div>
                      : ''}</div> : ''}


                    {reqChairs.map((row, index) => <div className="row mt-2 mb-2 pt-3 mr-3 ml-3 set-list">
                      <Col md={2} className="pl-0">
                        <div>
                          <h3><Badge
                            color="danger" style={{lineHeight: '45px'}}>
                            {row.length !== 0 ? row[0].price : ''}
                          </Badge></h3>
                        </div>
                      </Col>
                      <Col md={10}>
                        <Row className="set-list">

                          {row.map(i => <AvField className="border-0 set-name"
                                                 value={i.name} disabled
                                                 name="setName" type="text"/>)}
                          {reqChairs[index].length !== 0 ? <div>
                              <div className="form-group-btn">
                                <Button className="add-button border-0 mr-2" onClick={() => addChair(index)}>
                                  +
                                </Button>
                              </div>
                            </div>
                            : ''}
                          {reqChairs[index].length !== 0 ? <div>
                              <div className="form-group-btn">
                                <Button className="add-button btn-danger border-0" onClick={() => deleteChair(index)}>
                                  -
                                </Button>
                              </div>
                            </div>
                            : ''}
                        </Row>
                      </Col>
                    </div>)}
                    <div className="row">
                      <div className="col-md-12 pl-0">
                        <div className="float-right d-md-flex d-block">
                          <div className="wrap_input mx-md-2 mx-0 mt-md-0 mt-3">
                            <label htmlFor="place_price">Qator narxi</label>
                            <AvField type="text" onChange={getName} id="place_price" name="name"
                                     value={place.name || ''}
                                     className="form-control border-0"/>
                          </div>
                          <div className="wrap_input mx-md-2 mx-0 mt-md-0 mt-3">
                            <label htmlFor="place_line_count">Qatordagi o'rinlar soni</label>

                            <AvField type="number" onChange={getName} id="place_line_count" name="name"
                                     value={place.name || ''}
                                     className="form-control border-0"/>
                          </div>
                          <div className="wrap_place mb_btn mx-md-2 mx-0" style={{marginTop: '38px'}}>
                            <button className="btn btn-success mt-0" type="button" onClick={addRowChairs}>+</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row mt-4 pb-4">
                      <div className="col-md-12">
                        <div className="wrap_place  float-right ">
                          <button className="btn btn-secondary px-4 mr-2" onClick={closeModal} type="button">Bekor
                            qilish
                          </button>


                          <Button className="btn btn-success btn-save px-4 " bsStyle="success" loading={isLoading}
                                  onClick={saveChairs}
                                  type="submit">Saqlash
                          </Button>
                        </div>
                      </div>
                    </div>
                  </AvForm>
                </div>
              </div>
            </div>

          </section>
        </main>
      </div>
    );
  }
}

export default AddSeats;
