import api from 'api'
// import {routerRedux} from 'dva/router'
import {parsePhoneNumber} from 'libphonenumber-js'
import {STORAGE_NAME} from 'utils/constant';
import {toast} from 'react-toastify';
import {router} from 'umi';

const {saveTemplatePlace, getTemplatePlaces} = api;

export default ({
  namespace: 'addSeat',
  state: {
    reqChairs: [],
    letters: [
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
      'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ',
      'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'
    ],
    tempIndex: 1,
    tempId: 1,
    name: '',
    place: {},
    row: 1,
    isLoading: false,
  },
  subscriptions: {},
  effects: {
    * addRow({payload}, {put, call, select}) {
      const {line, price} = payload;
      let {reqChairs, letters, tempIndex, tempId, row} = yield select(_ => _.addSeat);
      let chairs = [];
      for (let i = 0; i < line; i++) {
        chairs.push({
          name: (reqChairs.length === 0 ? 1 : reqChairs.length + 1) + letters[i],
          row: reqChairs.length === 0 ? 1 : reqChairs.length + 1,
          price,
        })
      }
      reqChairs.push(chairs);
      yield put({
        type: 'updateState',
        payload: {
          tempId: parseInt(line) + tempId,
          tempIndex: tempIndex + 1,
          row: reqChairs.length === 0 ? 1 : reqChairs.length + 1,
          reqChairs,
        },
      });
    },
    * saveChairs({payload}, {put, call, select}) {
      const {reqChairs, name} = yield select(_ => _.addSeat);
      let array = [];
      reqChairs.forEach(row => {
        row.forEach(item => {
          array.push(item);
        })
      });
      if (array.length > 0) {
        const res = yield call(saveTemplatePlace, {id: payload.id, name: payload.name, reqChairs: array});
        toast.success(res.message);
        router.push('/event/places');
      }
    },
    * getPlaceById({payload}, {call, put, select}) {
      const res = yield call(getTemplatePlaces, payload);
      if (res.success) {
        let reqChairs = [];
        res.object.chairs.forEach(i => {
          const arrs = reqChairs.filter(row => row[0].row === i.row)
          if (arrs.length === 0) {
            reqChairs.push(res.object.chairs.filter(j => j.row === i.row));
          }
        });
        yield put({
          type: 'updateState',
          payload: {
            place: res.object,
            reqChairs
          }
        })
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
