import api from 'api'
import {toast} from "react-toastify";

const {
  getEvents, getEvent, addEvent, editEvent, eventIncome, getUpcomingEvents,
  getEventUsers, getEventTypes, getEventForEdit, saveTemplatePlace,
  getTemplatePlaces, getParticipatedEvents, changeStatusEventOpen, getEventStatuses
} = api;
export default ({
  namespace: 'eventModel',
  state: {
    oldEventUsers: [],
    event: {},
    editEvent: {},
    events: [],
    templatePlaces: [],
    fixed: false,
    eventTypes: [],
    dateTime: new Date(),
    upcomingEvents: [],
    eventUsers: [],
    speakersId: [],
    isModalShow: false,
    reqChairs: [],
    loading: true,
    isLoadingEvent: false,
    letters: [
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
      'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ',
      'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'
    ],
    tempIndex: 1,
    tempId: 1,
    name: '',
    place: {},
    row: 1,
    participatedEvents: [],
    eventStatuses: [],
    isModalShowStatus: false,
    registerTime: new Date(),
    eventStatus: '',
    isArchive: false,
    totalPages: 0,
    page: 0,
    size: 10,
    status: 'all',
    isFilter: false

  },
  subscriptions: {},
  effects: {
    * addEvent({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEvent: true
        }
      });
      if (payload.path) {
        let {eventBannerId} = yield select(_ => _.app);
        const res1 = yield call(editEvent, {...payload, eventBannerId});
        if (res1.success) {
          toast.success(res1.message);
          yield put({
            type: 'updateState',
            payload: {
              isModalShow: false
            }
          });
          yield put({
            type: 'getEvents'
          });
        } else {
          toast.error("Ma'lumotlar to'liq kiritilmagan yoki saqlashda xatolik")
        }
      } else {
        let {eventBannerId} = yield select(_ => _.app);
        const res = yield call(addEvent, {...payload, eventBannerId});
        if (res.success) {
          toast.success(res.message);
          yield put({
            type: 'updateState',
            payload: {
              isModalShow: false,
            }
          });
          yield put({
            type: 'getEvents'
          });
        } else {
          toast.error("Ma'lumotlar to'liq kiritilmagan yoki saqlashda xatolik")
        }
      }
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEvent: false
        }
      });
    },
    * getEvent({payload}, {call, put, select}) {
      const res = yield call(getEvent, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            event: res.object,
            loading: false,
            isArchive: res.object.status === "ARCHIVE"
          }
        })
      } else {
        toast.error(res.message)
      }

    },
    * getEventForEdit({payload}, {call, put, select}) {
      const res = yield call(getEventForEdit, payload);
      yield put({
        type: 'updateState',
        payload: {
          editEvent: res
        }
      })
    },
    * getEvents({payload}, {call, put, select}) {
      let {page, size, status} = yield select(_ => _.eventModel);
      payload = {page, size, status};
      const res = yield call(getEvents, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            events: res.object.resEvents,
            loading: false,
          }
        })
      }
    },
    * getEventUsers({payload}, {call, put, select}) {
      const res = yield call(getEventUsers, payload);
      yield put({
        type: 'updateState',
        payload: {
          eventUsers: res.object
        }
      })
    },
    * getTemplatePlaces({payload}, {call, put, select}) {
      const res = yield call(getTemplatePlaces);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            templatePlaces: res.object
          }
        })
      }
    },
    * getEventTypes({payload}, {call, put, select}) {
      const res = yield call(getEventTypes);
      yield put({
        type: 'updateState',
        payload: {
          eventTypes: res._embedded.eventTypes
        }
      })
    },
    * getEventStatuses({payload}, {call, put, select}) {
      const res = yield call(getEventStatuses);
      yield put({
        type: 'updateState',
        payload: {
          eventStatuses: res.object
        }
      })
    },
    * getUpcomingEvents({}, {call, put, select}) {
      const res = yield call(getUpcomingEvents);
      yield put({
        type: 'updateState',
        payload: {
          upcomingEvents: res.object
        }
      })
    },
    * addRow({payload}, {put, call, select}) {
      const {line, price} = payload;
      let {reqChairs, letters, tempIndex, tempId, row} = yield select(_ => _.eventModel);
      let chairs = [];
      for (let i = 0; i < line; i++) {
        chairs.push({
          name: (reqChairs.length === 0 ? 1 : reqChairs.length + 1) + letters[i],
          row: reqChairs.length === 0 ? 1 : reqChairs.length + 1,
          price,
        })
      }
      reqChairs.push(chairs);
      yield put({
        type: 'updateState',
        payload: {
          tempId: parseInt(line) + tempId,
          tempIndex: tempIndex + 1,
          row: reqChairs.length === 0 ? 1 : reqChairs.length + 1,
          reqChairs,
        },
      });
    },
    * saveChairs({payload}, {put, call, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEvent: true,
        }
      });
      const {reqChairs, name} = yield select(_ => _.eventModel);
      let array = [];
      reqChairs.forEach(row => {
        row.forEach(item => {
          array.push(item);
        })
      });
      if (array.length > 0) {
        const res = yield call(saveTemplatePlace, {id: payload.id, name: payload.name, reqChairs: array});
        if (res.success) {
          toast.success(res.message);
          yield put({
            type: 'updateState',
            payload: {
              isLoadingEvent: false,
            }
          });
          yield put({
            type: 'eventModel/getTemplatePlaces'
          });
          yield put({
            type: 'app/updateState',
            payload: {
              isModalPlaceShow: false,
            }
          });
        } else {
          toast.error("Salqashda xatolik");
          yield put({
            type: 'updateState',
            payload: {
              isLoadingEvent: false,
            }
          });
        }
      }
    },
    * getPlaceById({payload}, {call, put, select}) {
      const res = yield call(getTemplatePlaces, payload);
      if (res.success) {
        let reqChairs = [];
        res.object.chairs.forEach(i => {
          const arrs = reqChairs.filter(row => row[0].row === i.row);
          if (arrs.length === 0) {
            reqChairs.push(res.object.chairs.filter(j => j.row === i.row));
          }
        });
        yield put({
          type: 'updateState',
          payload: {
            place: res.object,
            reqChairs
          }
        })
      }
    },
    * changeEventStatus({payload}, {call, put, select}) {
      let {isLoadingEvent} = yield select(_ => _.eventModel);
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEvent: true
        }
      });
      let {eventId} = yield select(_ => _.eventModel);
      const res = yield call(changeStatusEventOpen, {...payload, eventId});
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            isModalShowStatus: false
          }
        });
        yield put({
          type: 'getEvents'
        });
      } else {
        toast.error("Xatolik")
      }
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEvent: false
        }
      });
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
