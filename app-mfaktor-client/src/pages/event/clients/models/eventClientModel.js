import api from 'api'
import {toast} from "react-toastify";

const {
  searchUser, getSeats, addPayment,
  seatOrder, cancelBooked, eventIncome,
  getParticipatedAndGuest, addVip, getSeatPaymentsByEvent, participate, cancelSold,
  addChairr, addRow, addExpense, exportExcel
} = api;
export default ({
  namespace: "eventClientModel",
  state: {
    isAdmin: false,
    excelFile: {},
    activeTab: '1',
    modalOpen: false,
    modalOpenPay: false,
    modalType: 'addClients',
    searchUsers: [],
    seats: [],
    userId: '',
    inputValue: '',
    seatId: '',
    dateTime: new Date(),
    isSold: false,
    isBooked: false,
    isReserve: false,
    isBalance: false,
    cancelModalOpen: false,
    participateModalOpen: false,
    seatName: '',
    path: '',
    expenseModal: false,
    phoneNumber: '',
    eventIncome: {},
    eventPayments: [],
    participatedAndGuest: {},
    user: {},
    birthDate: new Date(),
    isVip: false,
    oldSeatPaymentsByEvent: [],
    seatPaymentsByEvent: [],
    allPaymentValue: '',
    activeBtn: 'week',
    searchEventUsers: [],
    isLoadingEventClient: false,
    participated: '',
    cancelSoldOpen: false,
    isUsd: false,
    letters: [
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
      'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ',
      'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'
    ],
    reqChairs: [],
    addingChairName: '',
    addingChairPrice: 0,
    addingChairRow: 0,
    isAddChairMadal: false,
    isAddRowMadal: false,
    lastRowIndex: 0,
    amountOfNewRowSeats: 0,
    nameUser: '',
    outcome: 0,
    income: 0,
  },

  subscriptions: {},
  effects: {
    * addExpense({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEventClient: true
        }
      });
      const res = yield call(addExpense, payload);
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            expenseModal: false
          }
        });
        yield put({
          type: 'refreshState'
        });
      } else {
        toast.error(res.message);
      }
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEventClient: false
        }
      });
    },
    * searchUser({payload}, {call, put, select}) {
      const res = yield call(searchUser, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            searchUsers: res.object
          }
        })
      }
    },
    * exportExcel({payload}, {call, put, select}) {
      const res = yield call(exportExcel, payload);
      let excelFile = {success: res.success, obj: res.object};
      if (res.success) {
        let excelFile = {success: res.success, obj: res.object};
        yield put({
          type: 'updateState',
          payload: {
            excelFile
          }
        })
      }
      return excelFile;
    },
    * getSeats({payload}, {call, put, select}) {
      const res = yield call(getSeats, payload);
      if (res.success) {
        let seats = [];
        res.object.forEach(i => {
          const arrs = seats.filter(row => row[0].row === i.row);
          if (arrs.length === 0) {
            seats.push(res.object.filter(j => j.row === i.row));
          }
        });
        yield put({
          type: 'updateState',
          payload: {
            seats
          }
        })
      }
    },
    * addPayment({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEventClient: true
        }
      });
      const res = yield call(addPayment, payload);
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            modalOpen: false,
            modalOpenPay: false,
            isLoadingEventClient: false,
            isVip: false,
            isBooked: false,
            isSold: false,
            searchUsers: [],
            inputValue: ''
          }
        });
        let {path} = yield select(_ => _.eventClientModel);
        yield put({
          type: 'refreshState',
        });
      } else {
        yield put({
          type: 'updateState',
          payload: {
            isLoadingEventClient: false
          }
        });
        toast.error(res.message);
      }
    },
    * seatOrder({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEventClient: true
        }
      });
      const res = yield call(seatOrder, payload);
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            modalOpen: false,
            modalType: 'addClients',
            isLoadingEventClient: false,
            isVip: false,
            isBooked: false,
            isSold: false,
            searchUsers: [],
            inputValue: ''
          }
        });
        yield put({
          type: 'refreshState',
        });
      } else {
        yield put({
          type: 'updateState',
          payload: {
            isLoadingEventClient: false
          }
        });
        toast.error(res.message);
      }
    },
    * cancelBooked({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEventClient: true
        }
      });
      try {
        const res = yield call(cancelBooked, payload);
        if (res.success) {
          toast.success(res.message);
          yield put({
            type: 'refreshState',
          });
          yield put({
            type: 'updateState',
            payload: {
              cancelModalOpen: false,
              isLoadingEventClient: false,
              isVip: false,
              isBooked: false,
              isSold: false,
              searchUsers: [],
              inputValue: ''
            }
          });
        } else {
          yield put({
            type: 'updateState',
            payload: {
              isLoadingEventClient: false
            }
          });
          toast.error(res.message);
        }
      } catch (e) {
        yield put({
          type: 'updateState',
          payload: {
            isLoadingEventClient: false
          }
        });
        toast.error("Xatolik");
      }
    },
    * cancelSold({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEventClient: true
        }
      });
      try {
        const res = yield call(cancelSold, payload);
        if (res.success) {
          toast.success(res.message);
          yield put({
            type: 'refreshState',
          });
          yield put({
            type: 'updateState',
            payload: {
              cancelModalOpen: false,
              isLoadingEventClient: false,
              isVip: false,
              isBooked: false,
              isSold: false,
              searchUsers: [],
              seatId: '',
              cancelSoldOpen: false,
              inputValue: ''
            }
          });
        } else {
          yield put({
            type: 'updateState',
            payload: {
              isLoadingEventClient: false
            }
          });
          toast.error(res.message);
        }
      } catch (e) {
        yield put({
          type: 'updateState',
          payload: {
            isLoadingEventClient: false
          }
        });
        toast.error("Xatolik");
      }
    },
    * reserveSeat({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEventClient: true
        }
      });
      try {
        const res = yield call(cancelBooked, payload);
        if (res.success) {
          toast.success(res.message);
          let {path} = yield select(_ => _.eventClientModel);
          yield put({
            type: 'getSeats',
            payload: {
              path
            }
          });
          yield put({
            type: 'eventIncome',
            payload: {
              path
            }
          });
          yield put({
            type: 'updateState',
            payload: {
              cancelModalOpen: false,
              isLoadingEventClient: false,
              isVip: false,
              isBooked: false,
              isSold: false,
              searchUsers: []
            }
          });

        } else {
          yield put({
            type: 'updateState',
            payload: {
              isLoadingEventClient: false
            }
          });
          toast.error(res.message);
        }
      } catch (e) {
        yield put({
          type: 'updateState',
          payload: {
            isLoadingEventClient: false
          }
        });
        toast.error("Xatolik");
      }
    },
    * eventIncome({payload}, {call, put, select}) {
      const res = yield call(eventIncome, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            eventIncome: res.object,
            eventPayments: res.object.resEventPayments,
            allPaymentValue: res.object.weekSum,
            activeBtn: 'week',
            outcome: res.object.outcome,
            income: res.object.income,
          }
        })
      }
    },
    * getParticipatedAndGuest({payload}, {call, put, select}) {
      const res = yield call(getParticipatedAndGuest, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            participatedAndGuest: res.object,
          }
        })
      }
    },
    * addVip({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoadingEventClient: true
        }
      });
      const res = yield call(addVip, payload);
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            modalOpen: false,
            isLoadingEventClient: false,
          }
        });
        yield put({
          type: 'refreshState',
        });
      } else {
        yield put({
          type: 'updateState',
          payload: {
            isLoadingEventClient: false
          }
        });
        toast.error(res.message);
      }
    },
    * getSeatPaymentsByEvent({payload}, {call, put, select}) {
      const res = yield call(getSeatPaymentsByEvent, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            seatPaymentsByEvent: res.object
          }
        })
      }
    },
    * refreshState({payload}, {call, put, select}) {
      let {path} = yield select(_ => _.eventClientModel);
      yield put({
        type: 'getSeats',
        payload: {
          path
        }
      });
      yield put({
        type: 'eventModel/getEvent',
        payload: {
          path
        }
      });
      yield put({
        type: 'eventIncome',
        payload: {
          path
        }
      });
      yield put({
        type: 'getSeatPaymentsByEvent',
        payload: {
          path: path
        }
      });
      yield put({
        type: 'getParticipatedAndGuest',
        payload: {
          path: path
        }
      });
      yield put({
        type: 'eventModel/getEventUsers',
        payload: {
          path
        }
      });
      yield put({
        type: 'eventModel/getEvent',
        payload: {
          path
        }
      })
    },
    * saveRow({payload}, {put, call, select}) {
      let {seats, letters, reqChairs} = yield select(_ => _.eventClientModel);
      let chairs = [];
      for (let i = 0; i < payload.count; i++) {
        chairs.push({
          name: (reqChairs.length === 0 ? 1 : reqChairs.length + 1) + letters[i],
          row: seats.length === 0 ? 1 : seats.length + 1,
        })
      }
      reqChairs.push(chairs);
    },
    * addChairToRow({payload}, {call, put, select}) {
      const res = yield call(addChairr, payload);
      const {isAddChairMadal} = yield select(_ => _.eventClientModel);
      yield put({
        type: 'getSeats',
        payload: {
          path: payload.eventId
        }
      });
      yield put({
        type: 'updateState',
        payload: {
          isAddChairMadal: !isAddChairMadal
        }
      });
    },
    * addNewRow({payload}, {call, put, select}) {
      const res = yield call(addRow, payload.newRow);
      const {path} = yield select(_ => _.eventClientModel);
      yield put({
        type: 'refreshState',
      });
      yield put({
        type: 'updateState',
        payload: {
          isAddRowMadal: false
        }
      });
    },
    * participate({payload}, {call, put, select}) {
      const res = yield call(participate, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            participateModalOpen: false
          }
        })
        toast.success(res.message);
        yield put({
          type: 'refreshState',
        });
      } else {
        toast.error(res.message)
      }
    }

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
