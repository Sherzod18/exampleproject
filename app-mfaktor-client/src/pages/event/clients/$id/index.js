import React, {Component} from 'react';
import {
  Button,
  ButtonGroup,
  Card,
  CardFooter,
  CardImg,
  Col,
  CustomInput,
  Input,
  InputGroup,
  InputGroupAddon,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  TabPane
} from 'reactstrap'
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';
import {connect} from "react-redux";
import DatePicker from "react-datepicker/es";
import "react-datepicker/dist/react-datepicker.css";
import {Link} from "react-router-dom";
import {toast} from "react-toastify";
import CurrencyInput from 'react-currency-input'
import Loader from "react-loader-spinner";


@connect(({eventClientModel, eventModel, payType, app,}) => ({eventClientModel, eventModel, payType, app}))
class EventClients extends Component {
  componentDidMount() {
    const {dispatch, eventModel, eventClientModel, app} = this.props;
    dispatch({
      type: 'eventModel/getEvent',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'eventClientModel/updateState',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'eventClientModel/getSeatPaymentsByEvent',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'eventClientModel/getSeats',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'eventClientModel/eventIncome',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'eventModel/getEventUsers',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'eventClientModel/getParticipatedAndGuest',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'app/getRegions',
    });
    dispatch({
      type: 'app/getAwares',
    });
    dispatch({
      type: 'app/getPosition',
    });
    dispatch({
      type: 'app/getPayTypes',
    });
    dispatch({
      type: 'app/getExpenseTypes'
    });


    dispatch({
      type: 'eventClientModel/updateState',
      payload: {
        isAdmin: app.currentUser.roles.filter(i =>
          i.name === 'ROLE_ADMIN'
        ).length > 0
      }
    })


  }

  render() {
    const {dispatch, eventClientModel, eventModel, app} = this.props;
    const {
      activeTab, modalOpen, modalType, searchUsers, seats, userId, seatId,
      inputValue, dateTime, isSold, isBooked, seatName, cancelModalOpen, cancelSoldOpen,
      eventIncome, eventPayments, participatedAndGuest, user, modalOpenPay,
      isBalance, birthDate, isVip, seatPaymentsByEvent, oldSeatPaymentsByEvent, allPaymentValue, activeBtn,
      searchEventUsers, isLoadingEventClient, isAdmin, isReserve, letters,
      addingChairRow, addingChairPrice, phoneNumber, addingChairName, path, excelFile,
      isAddChairMadal, amountOfNewRowSeats, lastRowIndex, isAddRowMadal,
      participateModalOpen, participated, nameUser, expenseModal, income, outcome, isUsd,
    } = eventClientModel;

    const {event, eventUsers, loading, isArchive, oldEventUsers} = eventModel;
    const {regions, awares, positions, users, payTypes, currentUser, expenseTypes} = app;
    const modalShowAddChair = (rowIndex, rowLeng, chairPrice) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          isAddChairMadal: !isAddChairMadal,
          addingChairRow: rowIndex + 1,
          addingChairPrice: chairPrice,
          addingChairName: (rowIndex + 1) + letters[rowLeng]
        }
      });
    };
    const modalShowAddRow = (lastRowIndex) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          isAddRowMadal: !isAddRowMadal,
          lastRowIndex: lastRowIndex
        }
      });
    };
    const addChair = () => {
      dispatch({
        type: 'eventClientModel/addChairToRow',
        payload: {
          eventId: path,
          name: addingChairName,
          price: addingChairPrice,
          row: addingChairRow
        }
      })
    };
    const saveNewRow = (e, val) => {
      let newRow = [];
      for (let i = 0; i < val.rowSeatsSize; i++) {
        let newSeat = {
          eventId: path,
          name: (lastRowIndex + 1) + letters[i],
          price: val.rowPrice,
          row: lastRowIndex + 1
        };
        newRow.push(newSeat);
      }
      dispatch({
        type: 'eventClientModel/addNewRow',
        payload: {
          newRow
        }
      })
    };
    const saveExpense = (e, v) => {
      v.sum = v.sum.replace(/ /g, '');
      if (!isLoadingEventClient) {
        dispatch({
          type: 'eventClientModel/addExpense',
          payload: {
            ...v,
            eventId: this.props.match.params.id,
            date: dateTime
          }
        })
      }
    };
    const openExpenseModal = () => {
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          expenseModal: !expenseModal
        }
      });
    };
    const cancelModal = (seatId) => {
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          cancelModalOpen: !cancelModalOpen
        }
      });
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          seatId
        }
      });
    };
    const participateModal = (item) => {
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          participateModalOpen: !participateModalOpen
        }
      });
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          seatId: item.id
        }
      });
    };
    const cancelBooked = () => {
      dispatch({
        type: "eventClientModel/cancelBooked",
        payload: {
          path: seatId
        }
      });
    };
    const come = () => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          participated: true,
        }
      })
    };
    const notCome = () => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          participated: false,
        }
      })
    };
    const participate = () => {
      if (participated === '') {
        toast.error("Belgilash majburiy")
      } else
        dispatch({
          type: "eventClientModel/participate",
          payload: {
            seatId: seatId,
            participated
          }
        });
    };
    const cancelSold = () => {
      dispatch({
        type: 'eventClientModel/cancelSold',
        payload: {
          path: seatId
        }
      })
    };
    const cancelSoldModal = (seatId) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          cancelSoldOpen: !cancelSoldOpen,
          seatId
        }
      })
    };
    const reserve = () => {
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          isReserve: true,
          isBooked: false,
          isSold: false,
          isBalance: false,
          isVip: false,
        }
      });
    };
    const toggle = (tab) => {
      if (activeTab !== tab) {
        dispatch({
          type: 'eventClientModel/updateState',
          payload: {
            activeTab: tab
          }
        });
      }
    };
    const openModal = (i) => {
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          seatId: i.seatId,
          seatName: i.name,
          dateTime: new Date(),
          birthDate: new Date(),
          inputValue: '',
          modalOpen: !modalOpen,
          isBooked: false,
          isSold: false,
          isBalance: false,
          isVip: false,
          isReserve: false,
          modalType: 'addClients',
          searchUsers: [],
          userId: ''
        }
      });
      dispatch({
        type: 'app/getUsers',
      });
    };
    const refreshState = () => {
      dispatch({
        type: 'eventClientModel/refreshState'
      })
    };
    const openModalPay = (i) => {
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          user: i,
          userId: i.userId,
          modalOpenPay: !modalOpenPay,
          seatId: '',
          isBooked: false,
          isSold: true,
          isBalance: false,
          isVip: false,
          isReserve: false
        }
      });
    };
    const closePayModal = () => {
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          modalOpenPay: !modalOpenPay
        }
      });
    };
    const changeModalType = (type) => {
      dispatch({
        type: "eventClientModel/updateState",
        payload: {
          modalType: type,
          userId: '',
          isSold: false,
          isBalance: false,
          isBooked: false,
          isArchive: false
        }
      });
    };
    const changeDateTime = (date) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          dateTime: date,
        }
      })
    };
    const changeBirthDate = (date) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          birthDate: date,
        }
      })
    };
    const sold = () => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          isSold: true,
          isBalance: false,
          isBooked: false,
          isArchive: false
        }
      });
    };
    const booked = () => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          isBooked: true,
          isBalance: false,
          isSold: false,
          isArchive: false
        }
      });
    };
    const soldWithBalance = () => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          isBalance: true,
          isSold: false,
          isBooked: false
        }
      });
    };
    const vip = () => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          isVip: true,
          isBalance: false,
          isSold: false,
          isBooked: false,
          isArchive: false
        }
      });
    };
    const usd = () => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          isUsd: !isUsd,
        }
      });
    };
    const searchUser = (e) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          inputValue: e.target.value
        }
      });
      if (e.target.value.length === 0) {
        dispatch({
          type: 'eventClientModel/updateState',
          payload: {
            searchUsers: []
          }
        });
      } else {
        dispatch({
          type: 'eventClientModel/searchUser',
          payload: {
            word: inputValue
          }
        });
      }
    };
    const clearInput = () => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          inputValue: '',
          searchUsers: [],
          userId: ''
        }
      });
    };
    const getUserId = (userId) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          userId
        }
      })
    };
    const saveClient = (e, v) => {
      if (v.paySum) {
        v.paySum = v.paySum.replace(/ /g, '');
      }
      if (!isLoadingEventClient) {
        if (isBooked || isBalance || isReserve || isVip) {
          if (userId || isReserve || v.phoneNumber) {
            dispatch({
              type: 'eventClientModel/seatOrder',
              payload: {
                ...v,
                userId: userId,
                seatId,
                bookedExpireDate: dateTime,
                placeStatus: isBooked ? 'BOOKED' : isReserve ? 'RESERVED' : isVip ? 'VIP' : 'SOLD',
                birthDate: birthDate
              }
            });
          } else {
            toast.error("Foydalanuvchini tanlang yoki qidiring")
          }
        } else if (isSold) {
          if (userId || v.phoneNumber) {
            if (v.paySum > 0) {
              dispatch({
                type: 'eventClientModel/addPayment',
                payload: {
                  ...v,
                  userId: userId,
                  seatId,
                  payDate: dateTime,
                  placeStatus: 'SOLD',
                  birthDate: birthDate,
                  usd: v.isUsd === 'true'
                }
              });
            } else {
              toast.error("To'lov summasi 0 dan katta bo'lishi shart")
            }
          } else {
            toast.error("Foydalanuvchini tanlang yoki qidiring")
          }
        } else {
          toast.error("Biror bir turni tanlang")
        }
      }
    };
    const changeSum = (type) => {
      switch (type) {
        case 'week':
          dispatch({
            type: 'eventClientModel/updateState',
            payload: {
              allPaymentValue: eventIncome.weekSum,
              activeBtn: type
            }
          });
          break;
        case 'old':
          dispatch({
            type: 'eventClientModel/updateState',
            payload: {
              allPaymentValue: (eventIncome.allSum - eventIncome.weekSum),
              activeBtn: type
            }
          });
          break;
        case 'all':
          dispatch({
            type: 'eventClientModel/updateState',
            payload: {
              allPaymentValue: eventIncome.allSum,
              activeBtn: type
            }
          });
          break;
      }
    };
    const print = () => {
      let w = window.open();
      w.document.write(document.getElementById('eventUsers').innerHTML);
      w.print();
      w.close();
    };
    const getPhoneNumber = (e) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    };
    const exportExcel = () => {
      dispatch({
        type: 'eventClientModel/exportExcel',
        payload: {
          path: this.props.match.params.id
        }
      }).then(excelFile => {
        if (excelFile.success) {
          var a = document.createElement("a");
          a.href = excelFile.obj;
          document.body.appendChild(a);
          a.target = "_blank";
          a.click();
        }
      });

    };
    const searching = (e) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          inputValue: e.target.value
        }
      })
    };
    const searchSeatPaymentsUser = (e) => {
      dispatch({
        type: 'eventClientModel/updateState',
        payload: {
          inputValue: e.target.value
        }
      })
    };
    return (
      <div>

        {loading ? <div className="container">
          <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px'}}><Loader
            type="Triangle"
            color="#ED2939"
            height="100"
            width="100"
          /></div>
        </div> : <main>
          {event ? <section>
            <div className="container">
              <Row className="mt-4 mb-4">
                <Col md={12}>
                  <h4 className="font-weight-bold">{event.serialNumber}</h4>
                </Col>
              </Row>
              <Row className="icon-row">
                <Col md={12}>
                  <Card className="card-list event-card">
                    <div className="d-flex">
                      <div className="w-30">
                        <CardImg src={event.photoUrl} className="rounded-0"/>
                      </div>
                      <div className="w-40 py-4">
                        <h3 className="font-weight-light px-5"><Link to={"/event/clients/" + event.id}><h5
                          className="event_name">{event.title}</h5></Link> <br/></h3>
                        <div className="d-flex px-5 pt-4">
                          <div className="w-50">
                            <p className="m-0 text-muted small">Spiker</p>
                            <p className="m-0 font-weight-bold text-body">{event.speakers}</p>
                          </div>
                          <div className="w-50">
                            <p className="m-0 small text-muted">Vaqti</p>
                            <p
                              className="m-0 font-weight-bold text-body">{event.startTime ? event.startTime.substring(0, 10) + ' ' + new Date(event.startTime).toTimeString().substring(0, 5) : ''}</p>
                          </div>
                        </div>
                      </div>
                      <div className="w-30 pt-4">
                        <CardFooter className="bg-transparent position-relative border-0">
                          <div className="card-price ">
                            <div className="d-flex">
                              <div className="w-33 text-center ">
                                <h1 className="m-0 font-weight-bold">
                                  {event.allSeat}
                                </h1>
                                <p className="small font-weight-light text-muted">
                                  Joylar
                                </p>
                              </div>
                              <div className="w-33 text-center ">
                                <h1 className="m-0 font-weight-bold text-success">
                                  {(event.soldSeat)}
                                </h1>
                                <p className="small font-weight-light text-muted">
                                  Sotilgan
                                </p>
                              </div>
                              <div className="w-33 text-center ">
                                <h1 className="m-0 font-weight-bold text-danger">
                                  {event.emptySeat}
                                </h1>
                                <p className="small font-weight-light text-muted">
                                  Sotilmagan
                                </p>
                              </div>
                            </div>
                          </div>
                          <div className="card-price ">
                            <div className="d-flex">
                              <div className="w-33 text-center ">
                                <h1 className="m-0 font-weight-bold">
                                  {event.bookedAndPartly}
                                </h1>
                                <p className="small font-weight-light text-muted">
                                  Band
                                </p>
                              </div>
                              <div className="w-33 text-center ">
                                <h1 className="m-0 font-weight-bold text-success">
                                  {(event.vipSeat)}
                                </h1>
                                <p className="small font-weight-light text-muted">
                                  Mehmon
                                </p>
                              </div>
                              <div className="w-33 text-center ">
                                <h1 className="m-0 font-weight-bold text-danger">
                                  {event.reservedSeat}
                                </h1>
                                <p className="small font-weight-light text-muted">
                                  Zahira
                                </p>
                              </div>
                            </div>
                          </div>
                        </CardFooter>
                      </div>
                    </div>
                  </Card>
                </Col>

              </Row>


            </div>
          </section> : ''}
          {participatedAndGuest ? <section className="event_client_page1">
            <div className="container">
              <div className="row">
                <div className="col-md-4">
                  <div className="card border-0">
                    <div className="card-body">

                      <div className="row">
                        <div className="col-md-4">
                          <p>Mijozlar <br/> aktivligi</p>
                        </div>

                        <div className="col-md-8 mt-3">
                          <div className="row">
                            <div className="col-md-6 pr-0">
                              <h3 className="mb-0">{participatedAndGuest.registeredCount}
                                <span>Ro'yxatdan o'tganlar</span>
                              </h3>
                            </div>

                            <div className="col-md-6 pr-0">
                              <h3 className="mb-0">{participatedAndGuest.notParticipatedCount}
                                <span>Kelmagan mijozlar soni</span></h3>
                            </div>
                            <div className="col-md-6 pr-0">
                              <h3 className="mb-0">{participatedAndGuest.participatedCount} <span
                                className="guest">Kelgan mijojlar soni</span></h3>
                            </div>
                            <div className="col-md-6 pr-0">
                              <h3 className="mb-0">{participatedAndGuest.guestCount} <span
                                className="guest">Mehmonlar</span></h3>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {isAdmin ?
                  <div className="col-md-4 pl-0">
                    <div className="card border-0">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-4">
                            <p>Avval yig'ilgan pul miqdori</p>
                          </div>
                          <div className="col-md-8">
                            <ButtonGroup>
                              <Button onClick={() => changeSum('week')}
                                      className={activeBtn === 'week' ? "active-btn" : ''}>Shu hafta</Button>
                              <Button onClick={() => changeSum('old')}
                                      className={activeBtn === 'old' ? "active-btn" : ''}>Avvalgi</Button>
                              <Button onClick={() => changeSum('all')}
                                      className={activeBtn === 'all' ? "active-btn" : ''}>Barchasi</Button>
                            </ButtonGroup>
                          </div>
                        </div>
                        <h2>
                          {(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(allPaymentValue))}
                          <span> UZS</span>
                        </h2>
                      </div>
                    </div>
                  </div>
                  : ''}
                {isAdmin ? <div className="col-md-4">
                  <div className="row">
                    {eventPayments.map((item, i) => {
                        return item.payTypeUz === 'Naqd' || item.payTypeUz === 'Plastik' ?
                          <div className="col-md-6 pl-0">
                            <div className="card border-0 h-100">
                              <div className="card-body">
                                <p>{item.payTypeUz}
                                  <div
                                    className={item.payTypeUz === 'Naqd' ? "payment payment-cash" : "payment payment-card"}/>
                                </p>
                                <h4
                                  className="text-center mt-4">{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(item.sum))}
                                  <span> UZS</span></h4>
                                <h5 className="text-center">Bugun
                                  : <span>+{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(item.paymentToday))} UZS</span>
                                </h5>
                              </div>
                            </div>
                          </div>
                          : ''
                      }
                    )}
                  </div>
                </div> : ''}
              </div>
              {isAdmin ?
                <div className="row mt-3">
                  <div className="col-md-4">
                    <div className="card border-0">
                      <div className="card-body">
                        <p>Kunlik <br/> umumiy tushum</p>
                        <h2
                          className="text-right green-number">{(new Intl.NumberFormat('fr-FR').format(eventIncome.todaySum))}
                          <span> UZS</span></h2>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 pl-0">
                    <div className="card border-0">
                      <div className="card-body">
                        <h1
                          className="text-center mb-0 mt-2">{(new Intl.NumberFormat('fr-FR').format(eventIncome.debtSum))}
                          <span> UZS</span></h1>
                        <p className="text-center mb-0">To'lanmagan pul miqdori</p>
                      </div>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="row">
                      {eventPayments.map((item, i) => {
                          return item.payTypeUz === 'Pul o\'tkazish' || item.payTypeUz === 'Payme' ?
                            <div className="col-md-6 pl-0">
                              <div className="card border-0 h-100">
                                <div className="card-body">
                                  <p>{item.payTypeUz}
                                    <div
                                      className={item.payTypeUz === 'Pul o\'tkazish' ? "payment payment-transfer" : "payment payment-payme"}/>
                                  </p>
                                  <h4 className="text-center mt-4">{(new Intl.NumberFormat('fr-FR').format(item.sum))}
                                    <span> UZS</span></h4>
                                  <h5 className="text-center">Bugun
                                    : <span>+{(new Intl.NumberFormat('fr-FR').format(item.paymentToday))} UZS</span>
                                  </h5>
                                </div>
                              </div>
                            </div>
                            : ''
                        }
                      )}
                    </div>
                  </div>
                </div> : ''}

              {isAdmin ? <div className="row mt-3">
                <div className="col-md-4">
                  <div className="card border-0">
                    <div className="card-body">
                      <p>Tadbir bo'yicha umumiy foyda</p>
                      <h2
                        className="text-left green-number">{(new Intl.NumberFormat('fr-FR').format(eventIncome.income))}
                        <span> UZS</span></h2>
                    </div>
                  </div>
                </div>
                <div className="col-md-4 pl-0">
                  <div className="card border-0">
                    <div className="card-body">
                      <p>Tadbir bo'yicha umumiy xarajat</p>
                      <h2
                        className="text-left red-number">{(new Intl.NumberFormat('fr-FR').format(eventIncome.outcome))}
                        <span> UZS</span></h2>
                    </div>
                    <div className="add-expense" onClick={openExpenseModal}/>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="row">
                    {eventPayments.map((item, i) => {
                        return item.payTypeUz === 'Husan Payme' ?
                          <div className="col-md-6 pl-0">
                            <div className="card border-0 h-100">
                              <div className="card-body">
                                <p>{item.payTypeUz}
                                  <div className="payment payment-payme-husan"/>
                                </p>
                                <h4 className="text-center mt-4">{(new Intl.NumberFormat('fr-FR').format(item.sum))}
                                  <span> UZS</span></h4>
                                <h5 className="text-center">Bugun
                                  : <span>+{(new Intl.NumberFormat('fr-FR').format(item.paymentToday))} UZS</span>
                                </h5>
                              </div>
                            </div>
                          </div>
                          :
                          ''
                      }
                    )}
                  </div>
                </div>
              </div> : ''}

              {/*<div className="row mt-3">*/}
              {/*<div className="col-md-4">*/}
              {/*<div className="card border-0">*/}
              {/*<div className="card-body">*/}
              {/*<p className="text-center">Tadbirga band qilinganlar statistikasi</p>*/}
              {/*<h2*/}
              {/*className="text-left green-number">{(new Intl.NumberFormat('fr-FR').format(eventIncome.income))}*/}
              {/*<span> UZS</span></h2>*/}
              {/*</div>*/}
              {/*</div>*/}
              {/*</div>*/}
              {/*</div>*/}
            </div>
          </section> : ''}
          {eventUsers ? <section className="event_client_page2">
            <div className="container">
              <Nav tabs className="border-0">
                <NavItem>
                  <NavLink
                    className={activeTab === '1' ? "active-tab" : ''}
                    onClick={() => toggle('1')}
                  >
                    Qatnashuvchilar
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={activeTab === '2' ? "active-tab" : ''}
                    onClick={() => toggle('2')}
                  >
                    Joylar bandligi
                  </NavLink>
                </NavItem>

                <NavItem>
                  <NavLink
                    className={activeTab === '3' ? "active-tab" : ''}
                    onClick={() => toggle('3')}
                  >
                    Tadbir tushumlari
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={activeTab}>
                <TabPane tabId="1" className="px-4 py-5">
                  <Row>
                    <Col md="4">
                      <InputGroup className={eventUsers.length === 0 ? "d-none" : ''}>
                        <InputGroupAddon addonType="prepend">
                          <span className="icon icon-search"/>
                        </InputGroupAddon>
                        <input onChange={searching} id="ss" name="search" className="hr-search-input client-search"
                               placeholder="So'zni kiriting"/>
                      </InputGroup>
                    </Col>
                    <Col className="col-md-1 offset-7">
                      <button onClick={refreshState} className="border-0 icon icon-refresher"/>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="12">
                      {eventUsers.length !== 0 ?
                        <div>
                          <Row className="mt-5">
                            <Col md="3">
                              <h3 className="text-center">Ma'lumot</h3>
                            </Col>
                            <Col md="2">
                              <h3 className="text-center">Balans</h3>
                            </Col>
                            <Col md="1">
                              <h3 className="text-center">Joy</h3>
                            </Col>
                            <Col md="2">
                              <h3 className="text-center">Vaqti</h3>
                            </Col>
                            <Col md="1">
                              <h3 className="text-center">To'lov vaqti</h3>
                            </Col>
                            <Col md="1">
                              <h3 className="text-center">Keldi</h3>
                            </Col>
                            <Col md="2">
                              <h3 className="text-center">Izoh</h3>
                            </Col>
                          </Row>
                          {eventUsers.filter(it =>
                            it.phoneNumber.toLowerCase().includes(inputValue.toLowerCase()) || it.fullName.toLowerCase().includes(inputValue.toLowerCase())).map((item, i) =>
                            <div>
                              <Row className="user-info mx-0">
                                <Col md="3" className="pt-3">
                                  <Row>
                                    <Col md="3" className="pr-0">
                                      <div className="user-avatar ml-auto"
                                           style={{backgroundImage: item.photoUrl.length ? 'url("/api/file' + item.photoUrl + '")' : 'url("/assets/images/user.png")'}}/>
                                    </Col>
                                    <Col md="9">
                                      {item.telegramUsername ? <div>
                                          <a target="_blank" href={"http://t.me/" + item.telegramUsername}><h3
                                            className="mb-0">{item.fullName}</h3>
                                            <p>{item.phoneNumber}</p></a></div> :
                                        <div><h3 className="mb-0">{item.fullName}</h3>
                                          <p>{item.phoneNumber}</p></div>}
                                    </Col>
                                  </Row>
                                </Col>
                                <Col md="2" className="pt-3 text-center">
                                  <h3>{(new Intl.NumberFormat('fr-FR').format(item.balance))}
                                  </h3>
                                </Col>
                                <Col md="1" className="pt-2">
                                  <p className="mt-1">{item.seatName}</p>
                                </Col>
                                <Col md="2" className="pt-2 px-4">
                                  <p
                                    className="mt-1 text-center">{item.targetTime ? item.targetTime.substring(8, 10) + '.' + item.targetTime.substring(5, 7) + '.' + item.targetTime.substring(0, 4) + ' ' + item.targetTime.substring(11, 16) : ''}</p>
                                </Col>
                                <Col md="1" className="px-0 pt-2 text-center">
                                  <p
                                    className="mt-1 text-center">{item.payType ? item.payTime.substring(8, 10) + '.' + item.payTime.substring(5, 7) + '.' + item.payTime.substring(0, 4) + ' ' + item.payTime.substring(11, 16) : 'To\'lov mavjud emas'}</p>
                                </Col>
                                <Col md="1" className="text-center pt-2">
                                  <button className="btn"
                                          onClick={() => participateModal(item)}>{item.participated === null ?
                                    <p className="mt-1 text-center">???</p> : item.participated ?
                                      <div className="text-success">Keldi</div> :
                                      <div className="text-danger">Kelmadi</div>}</button>

                                </Col>
                                <Col md="1" className="text-center pt-2">
                                  <p className="mt-1">{item.comment}</p>
                                </Col>
                                <div className="edit-info" onClick={() => openModalPay(item)}/>
                                <div className="line"/>
                              </Row>
                            </div>
                          )}
                          <div id="eventUsers" style={{display: 'none'}}>
                            <table border="1" style={{borderCollapse: 'collapse', width: '100%'}}>
                              <thead>
                              <tr>
                                <th>Ma'lumot</th>
                                <th>Balans</th>
                                <th>Joy</th>
                                <th>Vaqti</th>
                                <th>To'lov vaqti</th>
                                <th>Keldi</th>
                                <th>Izoh</th>
                              </tr>
                              </thead>
                              <tbody>
                              {eventUsers.map((item, i) => <tr>
                                <td>{item.fullName}<br/> {item.phoneNumber}</td>
                                <td>{(new Intl.NumberFormat('fr-FR').format(item.balance))}</td>
                                <td>{item.seatName}</td>
                                <td>{item.targetTime ? item.targetTime.substring(8, 10) + '.' + item.targetTime.substring(5, 7) + '.' + item.targetTime.substring(0, 4) + ' ' + item.targetTime.substring(11, 16) : ''}</td>
                                <td>{item.payType ? item.payTime.substring(8, 10) + '.' + item.payTime.substring(5, 7) + '.' + item.payTime.substring(0, 4) + ' ' + item.payTime.substring(11, 16) : 'To\'lov mavjud emas'}</td>
                                <td>{item.participated ? 'Keldi' : 'Kelmadi'}</td>
                                <td>{item.comment}</td>
                              </tr>)}
                              </tbody>
                            </table>
                          </div>
                          <Button className="float-right mt-2" size="sm" onClick={print}>Print</Button>
                          <Button className="btn-success float-right mt-2 mr-2" size="sm" onClick={exportExcel}>Export
                            Excel</Button>
                        </div>
                        :
                        <Row>
                          <Col className="mt-3"><h3 className="text-center">Tadbirga qatnashuvchilar mavjud emas</h3>
                          </Col>
                        </Row>
                      }
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="2" className="px-5 pt-5">
                  <div className="row">
                    <div className="col-md-10 offset-1">
                      <span><span className="circle circle-booked"/> Band qilingan</span>
                      <span><span className="circle circle-partly_paid"/> Qisman to'langan</span>
                      <span><span className="circle circle-sold"/> Sotilgan</span>
                      <span><span className="circle circle-empty"/> Sotilmagan</span>
                      <span><span className="circle circle-VIP"/> Mehmon</span>
                      <span><span className="circle circle-reserved"/>Zahiraga olingan</span>
                    </div>
                    <Col className="col-md-1">
                      <button onClick={refreshState} className="border-0 icon icon-refresher"/>
                    </Col>
                  </div>
                  {seats.map((row, index) =>
                    <div className="row mt-4 py-3 set-list">
                      {row.map(i => <div className="register-empty">
                        <div className="position-relative">
                          <input className="border-0 set-name"
                                 style={{
                                   backgroundColor: i.placeStatus === 'BOOKED' ? '#C4C4C4' :
                                     i.placeStatus === 'SOLD' ? '#D0021B' : i.placeStatus === 'RESERVED' ?
                                       '#000' : i.placeStatus === 'PARTLY_PAID' ? '#5f5789' :
                                         i.placeStatus === 'VIP' ? '#E9E011' : '#1DD1A1'
                                 }}
                                 value={i.name} readOnly={true}
                                 name="name" type="text"/>
                          {i.user || i.phoneNumber ?
                            <div className="register-user small">
                              {i.user ? i.user : i.phoneNumber}
                            </div> : ""}
                        </div>
                        {i.placeStatus === 'EMPTY' ?
                          <div className="reg-empty-btn" onClick={() => openModal(i)}/> :
                          i.placeStatus === 'BOOKED' || i.placeStatus === 'RESERVED' || i.placeStatus === 'VIP' ?
                            <div>
                              <div className="reg-empty-btn" onClick={() => openModal(i)}/>
                              <div className="reg-booked-btn" onClick={() => cancelModal(i.seatId)}/>
                            </div> :
                            i.placeStatus === 'SOLD' ?
                              <div className="reg-delete-btn" onClick={() => cancelSoldModal(i.seatId)}/> :
                              i.placeStatus === 'PARTLY_PAID' ?
                                <div className="reg-delete-btn" onClick={() => cancelSoldModal(i.seatId)}/> :
                                ""}
                      </div>)}
                      <Button type="button" className="add-buttonn btn-primary border-0 mt-2"
                              onClick={() => modalShowAddChair(index, row.length, row[0].price)}>+</Button>
                    </div>)}

                  {seats ?
                    <div className="row">
                      <div className="col-md-2 offset-5">
                        <button type="button" className="btn border-0  qator-qoshish mt-3"
                                onClick={() => modalShowAddRow(seats[seats.length - 1][0].row)}>Qator qo'shish
                        </button>
                      </div>
                    </div> : ""
                  }

                </TabPane>
                <TabPane tabId="3" className="px-4 py-5">

                  <Row>
                    <Col md="4">
                      <InputGroup className={seatPaymentsByEvent.length === 0 ? "d-none" : ''}>
                        <InputGroupAddon addonType="prepend">
                          <span className="icon icon-search"/>
                        </InputGroupAddon>
                        <input id="dd" onChange={searchSeatPaymentsUser} name="search"
                               className="hr-search-input client-search"
                               placeholder="So'zni kiriting"/>
                      </InputGroup>
                    </Col>
                    <Col className="col-md-1 offset-7">
                      <button onClick={refreshState} className="border-0 icon icon-refresher"/>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="12">
                      {seatPaymentsByEvent.length !== 0 ?
                        <div>
                          <Row className="mt-5">
                            <Col md="1">
                              <h3 className="text-center">T/r</h3>
                            </Col>
                            <Col md="2">
                              <h3 className="text-center">FISH</h3>
                            </Col>
                            <Col md="2">
                              <h3 className="text-center">Telefon raqami</h3>
                            </Col>
                            <Col md="1">
                              <h3 className="text-center">Joy</h3>
                            </Col>
                            <Col md="2">
                              <h3 className="text-center">To'lov vaqti</h3>
                            </Col>
                            <Col md="2">
                              <h3 className="text-center">To'lov summasi</h3>
                            </Col>
                            <Col md="2">
                              <h3 className="text-center">To'lov turi</h3>
                            </Col>
                          </Row>
                          {seatPaymentsByEvent.filter(it => it.phoneNumber.includes(inputValue) || it.fullName.toLowerCase().includes(inputValue.toLowerCase())).map((item, i) =>
                            <div>
                              <Row className="user-info mx-0">
                                <Col md="1" className="pt-3">
                                  <h5 className="mb-0">{i + 1}</h5>
                                </Col>
                                <Col md="2" className="pt-3">
                                  <h5 className="mb-0">{item.fullName}</h5>
                                </Col>
                                <Col md="2" className="pt-3 text-center my-1">
                                  <h5>{item.phoneNumber}
                                  </h5>
                                </Col>
                                <Col md="1" className="pt-3 text-center">
                                  <p className="mt-1">{item.seatName}</p>
                                </Col>
                                <Col md="2" className="px-0 pt-3 text-center">
                                  <p
                                    className="mt-1">{item.payDate.substring(8, 10) + '.' + item.payDate.substring(5, 7) + '.' + item.payDate.substring(0, 4) + ' ' + item.payDate.substring(11, 16)}</p>
                                </Col>
                                <Col md="2" className="text-center pt-3">
                                  <p
                                    className="mt-1">{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(item.sum)) + " "} UZS</p>
                                </Col>
                                <Col md="2" className="text-center pt-3">
                                  <p className="mt-1">{item.payType}</p>
                                </Col>
                                <div className="line"/>
                              </Row>
                            </div>)}
                        </div>

                        :
                        <Row>
                          <Col className="mt-3"><h3 className="text-center">Tadbirga uchun to'lovlar mavjud emas</h3>
                          </Col>
                        </Row>
                      }

                    </Col>
                  </Row>

                </TabPane>
              </TabContent>
            </div>

          </section> : ''}
        </main>}
        <Modal isOpen={modalOpenPay} toggle={closePayModal} className="event-client-modal">
          <ModalHeader toggle={closePayModal} className="border-0"/>
          <ModalBody className="pl-5 pr-5">
            <AvForm onValidSubmit={saveClient}>
              <h3>To'lov qilish ({user.fullName})</h3>
              <div>
                <div className="col-md-12"><input type="radio" name="usd" onChange={usd}
                                                  className=" border-0 p-0 mb-2"/>
                  To'lov dollarda
                </div>
                <div className="col-md-12"><input type="radio" name="usd" checked={!isUsd} onChange={usd}
                                                  className=" border-0 p-0 mb-2"/> Dollarda emas
                </div>
                <div className="row mt-3">
                  <div className="col-md-4 offset-1">
                    <AvGroup>
                      <AvInput name="paySum" tag={CurrencyInput} precision="0" thousandSeparator=" "/>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      <label>{isUsd ? "So'mdagi miqdori" : "To'langan pul miqdori"}</label>
                    </AvGroup>
                  </div>
                  <div className="col-md-4 offset-2">
                    {!isUsd ? <AvGroup>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      <AvInput type="select" name="payTypeId" required>
                        <option>To'lov turi</option>
                        {payTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                      </AvInput>
                      <label>To'lov turi</label>
                    </AvGroup> : <AvGroup>
                      <DatePicker
                        selected={dateTime}
                        showTimeSelect
                        timeFormat="HH:mm"
                        className="form-control"
                        onChange={changeDateTime}
                        timeIntervals={30}
                        dateFormat="dd.MM.yyyy  hh:mm"
                        timeCaption="time"
                      />
                      <label className="label-date">To'lov vaqti</label>
                    </AvGroup>}
                  </div>
                  <div className="col-md-4 offset-1">
                    {isUsd ? <AvGroup>
                      <AvInput name="sumUsd" tag={CurrencyInput} precision="0" thousandSeparator=" "/>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      <label>Dollardagi pul miqdori</label>
                    </AvGroup> : <AvGroup>
                      <AvInput name="details"/>
                      <label>Qo'shimcha ma'lumot</label>
                    </AvGroup>}
                  </div>
                  <div className="col-md-4 offset-2">
                    {!isUsd ? <AvGroup>
                      <DatePicker
                        selected={dateTime}
                        showTimeSelect
                        timeFormat="HH:mm"
                        className="form-control"
                        onChange={changeDateTime}
                        timeIntervals={30}
                        dateFormat="dd.MM.yyyy  hh:mm"
                        timeCaption="time"
                      />
                      <label className="label-date">To'lov vaqti</label>
                    </AvGroup> : <AvGroup>
                      <AvInput name="details"/>
                      <label>Qo'shimcha ma'lumot</label>
                    </AvGroup>}
                  </div>
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-md-2 offset-7">
                </div>
                <div className="col-md-2">
                  <Button type="submit" block color="success">Saqlash</Button>
                </div>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>
        <Modal isOpen={modalOpen} toggle={openModal} className="event-client-modal">
          <ModalHeader toggle={openModal} className="border-0"/>
          <ModalBody className="pl-5 pr-5">
            <AvForm onValidSubmit={saveClient}>
              <h3>Mijoz qo'shish ({seatName})</h3>
              {modalType === 'addClients' ?
                <div>
                  <div className="row mt-5">
                    <div className="col-md-4">
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <span className="icon icon-search"/>
                        </InputGroupAddon>
                        <button type="button" onClick={clearInput}>x</button>
                        <Input onChange={searchUser} value={inputValue} placeholder="Qidirish"/>
                      </InputGroup>
                    </div>
                  </div>
                  <div className="row mt-5">
                    {searchUsers.map((item, i) => <div className="col-md-12">
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-1 pt-2">
                              <input type="radio" name="userId" onChange={() => getUserId(item.id)}
                                     className="form-control"/>
                            </div>
                            <div className="col-md-3 pt-2 pl-0">
                              {item.fullName}
                            </div>
                            <div className="col-md-2 pt-2 text-center">
                              {item.phoneNumber}
                            </div>
                            <div className="col-md-2 pt-2 text-center">
                              {item.company} {item.positionUz ? ',' + item.positionUz : ''}
                            </div>
                            <div className="col-md-2 pt-2 text-center">
                              {item.awareUz}
                            </div>
                            <div className="col-md-2 pt-2 text-center">
                              {item.countVisit} marta
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>)}
                    {searchUsers.length === 0 ?
                      <p onClick={() => changeModalType('addNewClient')} className="mx-auto"><img
                        src="/assets/images/icon/plus-green.png" alt=""/>Yangi mijoz qo'shish
                      </p> : ''
                    }
                  </div>
                </div>
                :
                <div>
                  <div className="row mt-5">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput name="firstName" required/>
                        <label>Ismi</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-4 offset-2">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput name="company" required/>
                        <label>Tashkilot nomi</label>
                      </AvGroup>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput name="lastName" required/>
                        <label>Familiyasi</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-4 offset-2">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput type="select" name="positionId" required>
                          <option>Lavozimni tanlang</option>
                          {positions.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                        </AvInput>
                        <label>Lavozimi</label>
                      </AvGroup>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput name="phoneNumber" type="text" required/>
                        <label>Telefon raqami</label>
                      </AvGroup>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-4 offset-1">
                      <div className="form-group">
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <DatePicker
                            name="birthDate"
                            selected={birthDate}
                            className="form-control"
                            onChange={changeBirthDate}
                            timeIntervals={30}
                            dateFormat="dd.MM.yyyy"
                            timeCaption="time"
                          />
                          <label className="label-date">Tug'ilgan sanasi</label>
                        </AvGroup>
                      </div>
                    </div>
                    <div className="col-md-4 offset-2">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput type="select" name="regionId" required>
                          <option>Viloyatni tanlang</option>
                          {regions.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                        </AvInput>
                        <label>Viloyati</label>
                      </AvGroup>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvInput type="select" name="awareId" required>
                          <option>Reklamani ko'rgan joyini tanlang</option>
                          {awares.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                        </AvInput>
                        <label>Reklama ko'rgan joyi</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-4 offset-2">
                      <AvGroup>
                        <AvInput name="sale"/>
                        <label>Chegirma miqdori</label>
                      </AvGroup>
                    </div>
                  </div>
                </div>
              }

              <div className="col-md-12"><input type="radio" name="price" onChange={booked}
                                                className=" border-0 p-0 mb-2"/> Band qilish
              </div>
              <div className="col-md-12"><input type="radio" name="price" onChange={sold}
                                                className=" border-0 p-0 mb-2"/> To'lov amalga oshirish
              </div>
              {modalType !== 'addNewClient' ?
                <div className="col-md-12"><input type="radio" name="price" onChange={soldWithBalance}
                                                  className=" border-0 p-0 mb-2"/> Mavjud balans orqali band
                  qilish
                </div> : ''}
              <div className="col-md-12"><input type="radio" name="price" onChange={vip}
                                                className=" border-0 p-0 mb-2"/> Mehmon uchun band qilish
              </div>
              {modalType !== 'addNewClient' && userId === '' ?
                <div className="col-md-12"><input type="radio" name="price" onChange={reserve}
                                                  className=" border-0 p-0 mb-2"/> Joyni zahiraga olish
                </div> : ''}
              {isSold ?
                <div>
                  <h3 className="mt-5 ml-5 pl-5">To'lovlar</h3>
                  <div className="col-md-12"><input type="radio" name="usd" onChange={usd}
                                                    className=" border-0 p-0 mb-2"/>
                    To'lov dollarda
                  </div>
                  <div className="col-md-12"><input type="radio" name="usd" checked={!isUsd} onChange={usd}
                                                    className=" border-0 p-0 mb-2"/> Dollarda emas
                  </div>
                  <div className="row mt-3">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvInput name="paySum" tag={CurrencyInput} precision="0" thousandSeparator=" "/>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <label>{isUsd ? "So'mdagi miqdori" : "To'langan pul miqdori"}</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-4 offset-2">
                      {!isUsd ? <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput type="select" name="payTypeId" required>
                          <option>To'lov turi</option>
                          {payTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                        </AvInput>
                        <label>To'lov turi</label>
                      </AvGroup> : <AvGroup>
                        <DatePicker
                          selected={dateTime}
                          showTimeSelect
                          timeFormat="HH:mm"
                          className="form-control"
                          onChange={changeDateTime}
                          timeIntervals={30}
                          dateFormat="dd.MM.yyyy  hh:mm"
                          timeCaption="time"
                        />
                        <label className="label-date">To'lov vaqti</label>
                      </AvGroup>}
                    </div>
                    <div className="col-md-4 offset-1">
                      {isUsd ? <AvGroup>
                        <AvInput name="sumUsd" tag={CurrencyInput} precision="0" thousandSeparator=" "/>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <label>Dollardagi pul miqdori</label>
                      </AvGroup> : <AvGroup>
                        <AvInput name="details"/>
                        <label>Qo'shimcha ma'lumot</label>
                      </AvGroup>}
                    </div>
                    <div className="col-md-4 offset-2">
                      {!isUsd ? <AvGroup>
                        <DatePicker
                          selected={dateTime}
                          showTimeSelect
                          timeFormat="HH:mm"
                          className="form-control"
                          onChange={changeDateTime}
                          timeIntervals={30}
                          dateFormat="dd.MM.yyyy  hh:mm"
                          timeCaption="time"
                        />
                        <label className="label-date">To'lov vaqti</label>
                      </AvGroup> : <AvGroup>
                        <AvInput name="details"/>
                        <label>Qo'shimcha ma'lumot</label>
                      </AvGroup>}
                    </div>
                  </div>
                </div>
                : isBooked ?
                  <div>
                    <div className="row mt-2">
                      <div className="col-md-6 offset-1">
                        <AvGroup>
                          <DatePicker
                            selected={dateTime}
                            showTimeSelect
                            timeFormat="HH:mm"
                            className="form-control"
                            onChange={changeDateTime}
                            timeIntervals={30}
                            dateFormat="dd.MM.yyyy  hh:mm"
                            timeCaption="time"
                          />
                          <label className="label-date">To'lov muddati</label>
                        </AvGroup>
                      </div>
                      <div className="col-md-4">
                        <AvGroup>
                          <AvInput name="comment"/>
                          <label>Qo'shimcha ma'lumot</label>
                        </AvGroup>
                      </div>
                    </div>
                  </div> :
                  ''}
              <div className="row mt-3">
                <div className="col-md-3 offset-6">
                  {modalType !== 'addClients' ?
                    <Button type="button" onClick={() => changeModalType('addClients')} outline block
                            color="secondary">Bekor qilish</Button>
                    : ""
                  }
                </div>
                <div className="col-md-2">
                  <Button type="submit" block color="success">Saqlash</Button>
                </div>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>
        <Modal isOpen={cancelSoldOpen} toggle={cancelSoldModal} className="event-client-modal">
          <ModalHeader toggle={cancelSoldModal} className="border-0"/>
          <ModalBody className="pl-5 pr-5">
            <h3 className="text-center text-danger">Mijozni joyini bekor qilsangiz, mijoz tomonidan
              to'langan
              mablag'lar qaytariladi. <br/>Shu sababli o'ylab ko'ring. <br/> Qaroringiz qat'iymi?</h3>
            <AvForm onValidSubmit={cancelSold}>
              {/*<div className="col-md-4">*/}
              {/*<AvField name="cause" label="Siz ushbu mijozni joyini bekor qilsangiz"/>*/}
              {/*</div>*/}
              <div className="row mt-3">
                <div className="col-md-2 offset-7">
                  <Button type="button" onClick={cancelSoldModal} outline block>Yo'q</Button>
                </div>
                <div className="col-md-2">
                  <Button type="submit" block color="success">Ha</Button>
                </div>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>
        <Modal isOpen={cancelModalOpen} toggle={cancelModal} className="event-client-modal">
          <ModalHeader toggle={cancelModal} className="border-0"/>
          <ModalBody className="pl-5 pr-5">
            <AvForm onValidSubmit={cancelBooked}>
              <div className="col-md-4">
                <AvField name="cause" label="Bekor qilinish sababi"/>
              </div>
              <div className="row mt-3">
                <div className="col-md-2 offset-7">
                  <Button type="button" onClick={cancelModal} outline block>Yo'q</Button>
                </div>
                <div className="col-md-2">
                  <Button type="submit" block color="success">Ha</Button>
                </div>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>
        <Modal isOpen={isAddChairMadal} toggle={() => modalShowAddChair(0, 0, 0)}>
          <ModalHeader>Add Chair</ModalHeader>
          <ModalFooter>
            <Button type="button" onClick={addChair} color="success">AddChair</Button>
            <Button type="button" outline onClick={() => modalShowAddChair(0, 0, 0)}>Cancel</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isAddRowMadal} toggle={() => modalShowAddRow(0)}>
          <AvForm onValidSubmit={saveNewRow}>
            <ModalHeader>Add Row</ModalHeader>
            <ModalBody>
              <AvGroup>
                <AvInput type="number" name="rowPrice" required/>
                <label>Qator narxini kiriting</label>
              </AvGroup>
              <AvGroup>
                <AvInput name="rowSeatsSize" required/>
                <label>Joylar sonini kiriting</label>
              </AvGroup>
            </ModalBody>
            <ModalFooter>
              <Button type="submit" color="success">Add Row</Button>
              <Button type="button" outline onClick={() => modalShowAddRow(0)}>Cancel</Button>
            </ModalFooter>
          </AvForm>
        </Modal>
        <Modal isOpen={cancelModalOpen} toggle={cancelModal} className="event-client-modal">
          <ModalHeader toggle={cancelModal} className="border-0"/>
          <ModalBody className="pl-5 pr-5">
            <AvForm onValidSubmit={cancelBooked}>
              <div className="col-md-4">
                <AvField name="cause" label="Bekor qilinish sababi"/>
              </div>
              <div className="row mt-3">
                <div className="col-md-2 offset-7">
                  <Button type="button" onClick={cancelModal} outline block>Yo'q</Button>
                </div>
                <div className="col-md-2">
                  <Button type="submit" block color="success">Ha</Button>
                </div>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>
        <Modal isOpen={participateModalOpen} toggle={participateModal}>
          <ModalHeader toggle={participateModal} className="border-0"/>
          <ModalBody className="pl-5 pr-5">
            <h3 className="text-center text-danger">Mijozni kelmaganligini tasdiqlasangiz, mijoz tomonidan
              to'langan
              mablag'lar qaytariladi. <br/>Shu sababli o'ylab ko'ring. <br/> Qaroringiz qat'iymi?</h3>
            <AvForm onValidSubmit={participate}>
              <div className="row mb-5">
                <div className="col-md-6 mt-3 text-center">
                  <CustomInput type="radio" id="exampleCustomRadio" onChange={come} name="customRadio"
                               label="Keldi"/>
                </div>
                <div className="col-md-6  mt-3 text-center">
                  <CustomInput type="radio" id="exampleCustomRadio2" onChange={notCome} name="customRadio"
                               label="Kelmadi"/>
                </div>
              </div>
              <Button type="button" onClick={participateModal} outline
                      color="secondary">Yo'q</Button>
              <Button type="submit" className="float-right" color="success">Ha</Button>
            </AvForm>
          </ModalBody>
        </Modal>
        <Modal isOpen={expenseModal} toggle={openExpenseModal} className="event-client-modal">
          <ModalHeader toggle={openExpenseModal} className="border-0">
          </ModalHeader>
          <ModalBody className="pl-5 pr-5">
            <AvForm onValidSubmit={saveExpense}>
              <h3>Xarajat qo'shish</h3>
              <div className="row mt-5">
                <div className="col-md-4 offset-1">
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput type="select" name="expenseTypeId" required>
                      <option disabled>Xarajat turini tanlang</option>
                      {expenseTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                    </AvInput>
                    <label>Xarajat turi</label>
                  </AvGroup>
                </div>
                <div className="col-md-4 offset-2">
                  <AvGroup>
                    <AvInput name="sum" tag={CurrencyInput} precision="0" thousandSeparator=" " required/>
                    <label>To'langan pul miqdori</label>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  </AvGroup>
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-md-4 offset-1">
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput type="select" name="payTypeId" required>
                      <option>To'lov turi</option>
                      {payTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                    </AvInput>
                    <label>To'lov turi</label>
                  </AvGroup>
                </div>
                <div className="col-md-5 offset-2 pl-0">
                  <div className="form-group">
                    <AvGroup>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      <DatePicker
                        selected={dateTime}
                        showTimeSelect
                        timeFormat="HH:mm"
                        className="form-control"
                        onChange={changeDateTime}
                        timeIntervals={30}
                        dateFormat="dd.MM.yyyy  HH:mm"
                        timeCaption="time"
                      />
                      <label className="label-date">Xarajat qilingan vaqti</label>
                    </AvGroup>
                  </div>
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-md-4 offset-1 ">
                  <AvGroup>
                    <AvInput name="comment"/>
                    <label>Qo'shimcha ma'lumot</label>
                  </AvGroup>
                </div>
              </div>
              <div className="row mt-3">
                <div className="col-md-2 offset-8">
                  <button type="button" onClick={openExpenseModal} className=" btn btn-outline-secondary">Bekor
                    qilish
                  </button>
                </div>
                <div className="col-md-2">
                  <button type="submit" className="btn btn-success">Saqlash</button>
                </div>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }


}

export default EventClients;
