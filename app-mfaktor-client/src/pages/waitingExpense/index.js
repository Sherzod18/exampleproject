import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Loader from "react-loader-spinner";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker/es";
import Button from 'react-bootstrap-button-loader';
import {
  Card,
  CardBody,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row
} from "reactstrap";
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import CurrencyInput from "react-currency-input";
import {connect} from "react-redux";
import {toast} from "react-toastify";

@connect(({waitingExpense, app}) => ({waitingExpense, app}))
class WaitingExpense extends Component {
  componentWillMount() {
    const {dispatch, waitingExpense, app} = this.props;
    dispatch({
      type: 'waitingExpense/getWaitingExpenses'
    });
    dispatch({
      type: 'app/getPayTypes'
    });
    dispatch({
      type: 'app/getExpenseTypes'
    })

  }

  render() {
    const {dispatch, waitingExpense, app} = this.props;
    const {
      waitingExpenses, loading,
      modalOpen, date, isLoading, id, paginationList, currentExpense, eventId, showDeleteModal, payModal
    } = waitingExpense;
    const {payTypes, expenseTypes} = app;

    const openModal = () => {
      dispatch({
        type: 'waitingExpense/updateState',
        payload: {
          modalOpen: !modalOpen,
          id: '',
          currentExpense: {},
          deadline: new Date()
        }
      })
    };
    const changeDateTime = (date) => {
      dispatch({
        type: 'waitingExpense/updateState',
        payload: {
          date
        }
      })
    };


    const pay = (e, v) => {
      dispatch({
        type: 'waitingExpense/payToWaitingExpense',
        payload: {
          ...v,
          waitingExpenseId: id,
          date
        }
      });
    };

    const edit = (expense) => {
      dispatch({
        type: 'waitingExpense/updateState',
        payload: {
          modalOpen: !modalOpen,
          id: expense.id,
          currentExpense: expense,
          date: new Date(expense.deadline)
        }
      });
    };

    const openPayModal = (expense) => {
      dispatch({
        type: 'waitingExpense/updateState',
        payload: {
          payModal: !payModal,
          id: expense.id
        }
      });
    };

    const deleteModalOpen = (expense) => {
      dispatch({
        type: 'waitingExpense/updateState',
        payload: {
          showDeleteModal: !showDeleteModal,
          id: expense.id
        }
      });
    };

    const deleteItem = () => {
      dispatch({
        type: 'waitingExpense/deleteWaitingExpense',
        payload: {
          id
        }
      })
    };

    const save = (e, v) => {
      dispatch({
        type: 'waitingExpense/updateState',
        payload: {
          isLoading: true
        }
      });
      v.sum = v.sum.replace(/ /g, '');
      if (v.sum > 0) {
        dispatch({
          type: 'waitingExpense/addWaitingExpense',
          payload: {
            id,
            ...v,
            deadline: date
          }
        })
      } else {
        toast.error("Pul miqdorini kiritish majburiy");
        dispatch({
          type: 'waitingExpense/updateState',
          payload: {
            isLoading: false
          }
        });
      }
    };
    return (
      <div>

        <main>
          <section className="waiting_expense_page margin-70">
            <div className="container">
              {loading ?
                <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px'}}><Loader
                  type="Triangle"
                  color="#ED2939"
                  height="100"
                  width="100"
                /></div>

                : ''}

              {waitingExpenses.length !== 0 ?
                <div>
                  <Row className="pt-2 pt-md-5">
                    <Col md={2}>
                      <h3 className="header-title">Kutilayotgan xarajatlar</h3>
                    </Col>
                    <Col md={2} className="offset-8">
                      <button className="btn btn-success px-4 py-2 float-right" onClick={openModal}>Qo'shish</button>
                    </Col>
                  </Row>
                  <Row className="mt-4">
                    <Col md="1">
                      <h2 className="font-weight-bolder">T/r</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">Xarajat turi</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">Miqdori</h2>
                    </Col>
                    <Col md="1">
                      <h2 className="font-weight-bolder">Qoldiq</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">To'lov muddati</h2>
                    </Col>
                    <Col md="1">
                      <h2 className="font-weight-bolder">Izoh</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">To'lov qilish</h2>
                    </Col>
                    <Col md="1">
                      <h2 className="font-weight-bolder">Action</h2>
                    </Col>
                  </Row>
                  {waitingExpenses.map((item, i) =>
                    <div className="client-list-item pt-2 pb-2">
                      <Row key={item.id}>
                        <Col md="1" className="text-center">
                          <p>{i + 1}</p>
                        </Col>
                        <Col md="2" className="text-center">
                          <p>{item.expenseTypeNameUz}</p>
                        </Col>
                        <Col md="2" className="text-center">
                          <p>{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(item.sum))}</p>
                        </Col>
                        <Col md="1" className="text-center">
                          <p>{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(item.leftover))}</p>
                        </Col>
                        <Col md="2" className="text-center">
                          <p>{item.deadline ? item.deadline.substring(0, 10) + ' ' + new Date(item.deadline).toTimeString().substring(0, 5) : ''}</p>
                        </Col>
                        <Col md="1" className="text-center">
                          <p>{item.comment}</p>
                        </Col>
                        <Col md="2" className="text-center">
                          <button className="btn btn-outline-secondary p-1" onClick={() => openPayModal(item)}>To'lov
                            qilish
                          </button>
                        </Col>
                        <Col md="1" className="text-center">
                          <button className="btn btn-edit-28 siroj p-0" onClick={() => edit(item)}><span
                            className="icon icon-edit"/></button>
                          <button className="btn btn-delete-28 siroj p-0" onClick={() => deleteModalOpen(item)}><span
                            className="icon icon-delete"/></button>
                        </Col>
                      </Row>
                    </div>
                  )}
                </div>
                :
                (
                  <div>
                    <h3 className="font-weight-bolder text-center margin-70">Hozirda kutilayotgan xarajatlar mavjud
                      emas</h3>
                    <Card onClick={openModal} className="client-add-btn">
                      <CardBody>
                        <p className="text-center mb-0"><span className="icon icon-user-add"/> Kutilayotgan xarajat
                          qo'shish</p>
                      </CardBody>
                    </Card>
                  </div>
                )
              }
            </div>

            <Modal isOpen={modalOpen} toggle={openModal} className="event-client-modal">
              <ModalHeader toggle={openModal} className="border-0">
              </ModalHeader>
              <ModalBody className="pl-5 pr-5">
                <AvForm onValidSubmit={save}>
                  <h3>Kutilayotgan xarajat qo'shish</h3>
                  <div className="row mt-5">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput type="select" name="expenseTypeId" value={currentExpense.expenseTypeId} required>
                          <option disabled>Xarajat turini tanlang</option>
                          {expenseTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                        </AvInput>
                        <label>Xarajat turi</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-5 offset-2 pl-0">
                      <div className="form-group">
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <DatePicker
                            selected={date}
                            showTimeSelect
                            timeFormat="HH:mm"
                            className="form-control"
                            onChange={changeDateTime}
                            timeIntervals={30}
                            dateFormat="dd.MM.yyyy  HH:mm"
                            timeCaption="time"
                          />
                          <label className="label-date">To'lash vaqti</label>
                        </AvGroup>
                      </div>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvInput name="sum" tag={CurrencyInput} precision="0" thousandSeparator=" "
                                 value={('' + currentExpense.leftover + '')} required/>
                        <label>To'lanishi lozim bo'lgan pul miqdori</label>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      </AvGroup>
                    </div>
                    <div className="col-md-4 offset-2">
                      <AvGroup>
                        <AvInput name="comment" value={currentExpense.comment}/>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <label>Qo'shimcha ma'lumot</label>
                      </AvGroup>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-md-5 offset-8">
                      <button type="button" onClick={openModal} className="btn-outline-secondary btn mx-2">Bekor qilish
                      </button>
                      <Button type="submit" bsStyle="success"
                              loading={isLoading}>Saqlash </Button>
                    </div>
                  </div>
                </AvForm>
              </ModalBody>
            </Modal>

            <Modal isOpen={showDeleteModal} toggle={deleteModalOpen} className="event-client-modal">
              <ModalHeader toggle={deleteModalOpen} className="border-0">
                <div className="row mt-3 ml-3">
                  <h3 className="text-center  text-danger">Kutilayotgan to'lovni o'chirmoqchimisiz?</h3>
                </div>
              </ModalHeader>
              <ModalBody className="pl-5 pr-5">
                <div className="row mt-3">
                  <div className="col-md-5 offset-8">
                    <button type="button" onClick={deleteModalOpen} className="btn-outline-secondary btn mx-2">Bekor
                      qilish
                    </button>
                    <Button type="submit" onClick={deleteItem} bsStyle="success">Saqlash </Button>
                  </div>
                </div>
              </ModalBody>
            </Modal>

            <Modal isOpen={payModal} toggle={openPayModal} className="event-client-modal">
              <ModalHeader toggle={openPayModal} className="border-0">
              </ModalHeader>
              <ModalBody className="pl-5 pr-5">
                <AvForm onValidSubmit={pay}>
                  <h3>Kutilayotgan xarajat uchun to'lov qilish</h3>
                  <div className="row mt-5">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput type="select" name="payTypeId" required>
                          <option>To'lov turi</option>
                          {payTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                        </AvInput>
                        <label>To'lov turi</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-4 offset-2">
                      <AvGroup>
                        <AvInput name="sum" tag={CurrencyInput} precision="0" thousandSeparator=" " required/>
                        <label>To'lanmoqchi bo'lgan pul miqdori</label>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      </AvGroup>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-md-4 offset-1 ">
                      <AvGroup>
                        <AvInput name="comment"/>
                        <label>Qo'shimcha ma'lumot</label>
                      </AvGroup>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-md-2 offset-8">
                      <button type="button" onClick={openPayModal} className=" btn btn-outline-secondary">Bekor
                        qilish
                      </button>
                    </div>
                    <div className="col-md-2">
                      <button type="submit" className="btn btn-success">Saqlash</button>
                    </div>
                  </div>
                </AvForm>
              </ModalBody>
            </Modal>
          </section>
        </main>
      </div>
    );
  }
}

WaitingExpense.propTypes = {};

export default WaitingExpense;
