import api from 'api'
// import {routerRedux} from 'dva/router'
import {parsePhoneNumber} from 'libphonenumber-js'
import {STORAGE_NAME} from 'utils/constant';
import {toast} from "react-toastify";


const {addWaitingExpense, deleteWaitingExpense, editWaitingExpense, getWaitingExpenses, addExpense} = api;
export default ({
  namespace: "waitingExpense",
  state: {
    eventId: '',
    waitingExpenses: [],
    loading: true,
    modalOpen: false,
    date: new Date(),
    isLoading: false,
    expense: {},
    id: '',
    currentExpense: {},
    debt: '',
    showDeleteModal: false,
    payModal: false,
  },
  subscriptions: {},
  effects: {
    * getWaitingExpenses({payload}, {call, put, select}) {
      const res = yield call(getWaitingExpenses);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            waitingExpenses: res.object,
            loading: false
          }
        })
      }
    },
    * addWaitingExpense({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoading: true
        }
      });
      if (payload.id) {
        const res = yield call(editWaitingExpense, payload);
        if (res.success) {
          toast.success(res.message);
          yield put({
            type: 'updateState',
            payload: {
              modalOpen: false
            }
          });
          yield put({
            type: 'getWaitingExpenses'
          })
        } else {
          toast.error(res.message);
        }
        yield put({
          type: 'updateState',
          payload: {
            isLoading: false
          }
        })
      } else {
        const res = yield call(addWaitingExpense, payload);
        if (res.success) {
          toast.success(res.message);
          yield put({
            type: 'updateState',
            payload: {
              modalOpen: false
            }
          });
          yield put({
            type: 'getWaitingExpenses'
          })
        } else {
          toast.error(res.message);
        }
        yield put({
          type: 'updateState',
          payload: {
            isLoading: false
          }
        })
      }
    },
    * deleteWaitingExpense({payload}, {call, put, select}) {
      const res = yield call(deleteWaitingExpense, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            showDeleteModal: false
          }
        });
        yield put({
          type: 'getWaitingExpenses'
        });
        toast.success(res.message);
      } else {
        toast.error(res.message);
      }
    },
    * payToWaitingExpense({payload}, {call, put, select}) {
      const res = yield call(addExpense, payload);
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            payModal: false
          }
        });
        yield put({
          type: 'getWaitingExpenses'
        });
      } else {
        toast.error(res.message)
      }
    }

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
