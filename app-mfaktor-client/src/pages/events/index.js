import React, {Component} from 'react';
import "jquery/dist/jquery.min"
import "bootstrap/dist/js/bootstrap"
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {CardFooter, CardImgOverlay, Col, Container, InputGroup, InputGroupAddon, Row,Button} from "reactstrap";
import Loader from "react-loader-spinner";
import {AvForm} from "availity-reactstrap-validation";

@connect(({eventsModel}) => ({eventsModel}))
class Events extends Component {

  componentWillMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'eventsModel/getEvents',
    });
    dispatch({
      type: 'eventsModel/getEventTypes',
    })
  }

  clickHiding = () => {
    let x = document.getElementById("ss");
    if (!x.classList.contains("inputQidirish")) {
      x.classList.add("inputQidirish");
    } else {
      const {dispatch} = this.props;
      dispatch({
        type: 'addVideos/searchByTitle',
        payload: {
          path: x.value
        }
      })
    }
  };

  render() {
    const {dispatch, eventsModel} = this.props;
    const {eventTypes, loading, categoryId, events,size,page} = eventsModel;
    const getByEventType = (id) => {
      dispatch({
        type: 'eventsModel/getEventsByType',
        payload: {
          path: id
        }
      })
    };

    const moreEvent = () => {
      dispatch({
        type: 'eventsModel/updateState',
        payload: {
          page: page,
          size: size + 10
        }
      });
      dispatch({
        type: 'eventsModel/getEvents',
      })
    };


    const getEvents = () => {
      dispatch({
        type: 'eventsModel/getEvents',
      })
    };
    const searchEvent = (event) => {
      dispatch({
        type: 'eventsModel/searchEvent',
        payload: {
          word: event.target.value
        }
      })
    };
    return (
      <div>

        <header className="border-bottom">
          <Container>
            <Row className="mt-5 mt-md-5 pt-md-5 mb-0 mb-md-3">
              <Col md={2}>
                <h3 className="header-title">Tadbirlar</h3>
              </Col>
              <Col md={3} className="pl-md-0 pl-3 pt-2">
                <InputGroup>
                  <InputGroupAddon id='hr-search-icon' onClick={() => this.clickHiding()}
                                   className="hr-search-icon mr-4"
                                   addonType="prepend"><span
                    className="icon icon-search"/></InputGroupAddon>
                  <AvForm onChange={searchEvent}>
                    <input id="ss" className="hr-search-input inputKorinishi" placeholder="Tadbir nomini kiriting"/>
                  </AvForm>
                </InputGroup>
              </Col>

              <Col md={7} className="hr-category mt-md-0 mt-3 mb-3 mb-md-0">
                <ul className="list-unstyled mb-0">
                  <li className="d-inline-block">
                    <button type="button" className={"btn header-btn-light " + (!categoryId ? 'active' : '')}
                            onClick={getEvents}>Barchasi
                    </button>
                  </li>
                  {eventTypes.map(i => {
                    return <li key={i.id} className="d-inline-block ml-4 mt-2">
                      <button type="button" className={"btn header-btn-light " + (categoryId === i.id ? 'active' : '')}
                              onClick={() => getByEventType(i.id)}>{i.nameUz}</button>
                    </li>
                  })}
                </ul>
              </Col>

            </Row>
          </Container>
        </header>

        <main id="client">
          <div className="container">

            {loading ?
              <div className="col-md-4 offset-md-4 offset-0 text-center" style={{paddingTop: '150px', marginBottom: '150px'}}>
                <Loader
                  type="Triangle"
                  color="#ED2939"
                  height="100"
                  width="100"
                />
              </div> : <section className="next-event my-5">
                {events.length > 0 ?
                  <div>
                  <div className="row mb-4">
                    {events.map(i => {
                      return <div className="col-md-4 mt-5 " key={i.id}>
                        <div className="card event-card" style={{height: 100 + '%'}}>
                          <img src={i.photoUrl}
                               className="img-fluid rounded-top"/>
                          <div className="card-body px-0">
                            <div className="row">
                              <div className="col-md-12 px-5 text-center">
                                <h4 className="event-subject">{i.title}</h4>
                              </div>
                            </div>
                            <div className="row py-3">
                              <div className="col-md-6 pl-5 pl-md-5">
                                <p className="mb-0">Spiker</p>
                                <h4 className="event-info">{i.speakers}</h4>
                              </div>
                              <div className="col-md-6 pl-5 pl-md-0">
                                <p className="mb-0">Vaqti </p>
                                <h4
                                  className="event-info">
                                  {i.startTime ? i.startTime.substring(0, 10) + ' ' + new Date(i.startTime).toTimeString().substring(0, 5) : ''}
                                </h4>
                              </div>
                            </div>
                          </div>

                          <div className="card-footer px-0 pb-0 position-relative ">
                            {i.minPrice === i.maxPrice ? <div className="row py-2">
                                <div className="col-md-12 text-center one-price">
                                  <div className="font-weight">
                                    <span className="bold bold-span">{i.minPrice} </span>
                                    <span className="light">UZS</span>
                                  </div>
                                </div>
                                <div className="col-md-12 text-center">
                                  <p className="mb-0 events-price">Tadbir narxi</p>
                                </div>
                              </div> :
                              <div className="row py-2">
                                <div className="col-md-6 text-center border-right">
                                  <div className="font-weight">
                                    <span className="bold bold-span">{i.minPrice} </span>
                                    <span className="light">UZS</span>
                                  </div>
                                </div>
                                <div className="col-md-6 text-center">
                                  <div className="font-weight">
                                    <span className="bold bold-span">{i.maxPrice} </span>
                                    <span className="light">UZS</span>
                                  </div>
                                </div>
                                <div className="col-md-12 text-center">
                                  <p className="mb-0">Tadbir narxi shu oraliqda</p>
                                </div>
                              </div>
                            }

                            {i.status === "OPEN" ?
                              <Link to={"/events/" + i.id}>

                                <div className="row add-place red-row pb-0 ">
                                  <div className="col-md-12 py-3 px-5">
                                    <Row>
                                      <Col md={7}>
                                        <h4 className="buse-event">Tadbir uchun joy band qilish</h4>

                                      </Col>
                                      <Col md={5} className="">
                                        <span className="icon icon-arrow-right col-5-icon-right"/>

                                      </Col>

                                    </Row>
                                  </div>
                                </div>

                              </Link> : ""}
                          </div>
                        </div>
                      </div>
                    })}
                  </div>
                    <Row className="mt-4 mt-md-5">
                      <Col md={{size: 2, offset: '5'}} className="col-6 mb-5">
                        <Button onClick={moreEvent} className="btn-block btn-danger">
                          Yana yuklash
                        </Button>
                      </Col>
                    </Row>
                  </div> :
                  <div className="col-12 col-md-4 offset-md-4 text-center"><h3>Tadbirlar mavjud emas</h3></div>}
              </section>}
          </div>
        </main>
      </div>
    );
  }
}

export default Events;
