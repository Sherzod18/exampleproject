import api from 'api'
import {toast} from 'react-toastify';


const {
  getEvent, getEventsForUser, getReview, addReview, getSeats, bookSeat, getOwnReview,
  getEventTypes, getEventsByType, searchEvent
} = api;
export default ({
  namespace: 'eventsModel',
  state: {
    activeTab: 1,
    starCount: 1,
    event: {},
    events: [],
    countStars: {},
    reviews: [],
    ownReview: {},
    text: '',
    seats: [],
    dateTime: new Date(),
    register: false,
    chosenSeatsId: [],
    eventTypes: [],
    categoryId: '',
    loading: true,
    isLoading: false,
    page:0,
    size:10,
  },
  effects: {
    * getEvent({payload}, {call, put, select}) {
      const res = yield call(getEvent, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            event: res.object
          }
        })
      }
    },
    * getEvents({payload}, {call, put, select}) {
      let {page, size,status} = yield select(_ => _.eventsModel);
      payload = {page, size};
      const res = yield call(getEventsForUser,payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            events: res.object.resEvents,
            categoryId: '',
            isLoading: false,
            loading: false,
          }
        })
      }
    },
    * searchEvent({payload}, {call, put, select}) {
      const res = yield call(searchEvent, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            events: res.object,
            isLoading: false
          }
        })
      }
    },
    * getEventsByType({payload}, {call, put, select}) {
      const res = yield call(getEventsByType, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            events: res.object,
            isLoading: false,
            categoryId: payload.path
          }
        })
      }
    },
    * getEventTypes({}, {call, put, select}) {
      const res = yield call(getEventTypes);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            eventTypes: res._embedded.eventTypes
          }
        })
      }
    },
    * getOwnReview({payload}, {call, put, select}) {
      const res = yield call(getOwnReview, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            ownReview: res.object
          }
        })
      }
    },
    * getReviews({payload}, {call, put, select}) {
      const res = yield call(getReview, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            countStars: res.object,
            reviews: res.object.reviewList
          }
        })
      }
    },
    * addReview({payload}, {call, put, select}) {
      const {eventsModel} = yield select(_ => _);
      const {starCount, text} = eventsModel;
      const res = yield call(addReview, {eventOrSpeakerId: payload, starCount, text});
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            starCount:1,
            text:''
          }
        })
      } else {
        toast.error(res.message)
      }
      return res;
    },
    * bookSeat({payload}, {call, put, select}) {
      const res = yield call(bookSeat, payload);
      if (res.success) {
        toast.success(res.message)
      } else {
        toast.error(res.message)
      }
      return res;
    },
    * getSeats({payload}, {call, put, select}) {
      const res = yield call(getSeats, payload);
      if (res.success) {
        let seats = [];
        res.object.forEach(i => {
          const arrs = seats.filter(row => row[0].row === i.row);
          if (arrs.length === 0) {
            seats.push(res.object.filter(j => j.row === i.row));
          }
        });
        yield put({
          type: 'updateState',
          payload: {
            seats
          }
        })
      }
    }
  },
  subscriptions: {},
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
