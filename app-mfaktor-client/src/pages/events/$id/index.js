import React, {Component} from 'react';
import {toast} from 'react-toastify';

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardImg,
  CardImgOverlay,
  Col,
  Container,
  Input,
  Progress,
  Row,
  Spinner,
  TabContent,
  TabPane
} from "reactstrap";
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';
import StarRatingComponent from 'react-star-rating-component';
import {connect} from "react-redux";
import DatePicker from "react-datepicker";

@connect(({eventsModel, app}) => ({eventsModel, app}))
class Index extends Component {
  componentWillMount() {
    const {dispatch, app} = this.props;
    dispatch({
      type: 'eventsModel/getEvent',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'eventsModel/getReviews',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'eventsModel/getOwnReview',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'app/getPosition',
    });
    dispatch({
      type: 'app/getRegions',
    });
    dispatch({
      type: 'eventsModel/getSeats',
      payload: {
        path: this.props.match.params.id
      }
    });
    if (app.currentUser.birthDate) {
      dispatch({
        type: 'eventsModel/updateState',
        payload: {
          dateTime: new Date(app.currentUser.birthDate)
        }
      })
    }
  }

  render() {
    const {eventsModel, app, dispatch} = this.props;
    const {
      activeTab, event, starCount, countStars, reviews,
      chosenSeatsId, text, register, dateTime, seats,
      ownReview, isLoading
    } = eventsModel;
    const {currentUser, regions, positions} = app;
    const nextTab = () => {
      dispatch({
        type: "eventsModel/updateState",
        payload: {
          activeTab: activeTab + 1,
        }
      })
    };
    const prevTab = () => {
      dispatch({
        type: "eventsModel/updateState",
        payload: {
          activeTab: activeTab - 1,
        }
      })
    };

    const toaster = (v) => {
      toast.error("Bu joy band qilingan. Iltimos bo'sh o'rin tanlang.")
    };

    const buttonStyle = {
      background: '#E0E0E0',
      width: 200,
      padding: 16,
      textAlign: 'center',
      margin: '0 auto',
      marginTop: 32
    };
    const onStarClick = (nextValue, prevValue, name) => {
      dispatch({
        type: "eventsModel/updateState",
        payload: {
          starCount: nextValue
        }
      })
    };

    const changeDateTime = (date) => {
      dispatch({
        type: 'eventsModel/updateState',
        payload: {
          dateTime: date
        }
      })
    };

    const checkSeats = (v) => {
      let temp = chosenSeatsId;
      let available = false;
      for (let i = 0; i < temp.length; i++) {
        if (temp[i] === v) {
          temp.splice(i, 1);
          available = true
        }
      }
      if (!available)
        temp.push(v);
      dispatch({
        type: 'eventsModel/updateState',
        payload: {
          chosenSeatsId: temp,
          seats
        }
      });
    };

    const bookSeat = (e, v) => {
      if (chosenSeatsId.length === 0) {
      } else {
        dispatch({
          type: 'eventsModel/bookSeat',
          payload: {
            eventId: this.props.match.params.id,
            userId: currentUser.id,
            ...v,
            seatIds: chosenSeatsId,
            birthDate: dateTime,
            placeStatus: 'BOOKED'
          }
        }).then(res => {
          if (res.success) {
            dispatch({
              type: 'eventsModel/updateState',
              payload: {
                register: false
              }
            });
          }
        })
      }
    };

    const
      sendReview = (e) => {
        if (text) {
          dispatch({
            type: 'eventsModel/addReview',
            payload: this.props.match.params.id

          }).then(res => {
            if (res.success) {
              dispatch({
                type: 'eventsModel/getReviews',
                payload: {path: this.props.match.params.id}
              });
              dispatch({
                type: 'eventsModel/getOwnReview',
                payload: {
                  path: this.props.match.params.id
                }
              });
            }
          })
        } else {
        }
      };

    const
      addReview = (e) => {
        dispatch({
          type: 'eventsModel/updateState',
          payload: {
            [e.target.name]: e.target.value
          }
        })
      };
    const
      registerButton = (e) => {
        dispatch({
          type: 'eventsModel/updateState',
          payload: {
            register: !register
          }
        })
      };

    return (
      <div className="events">
        {event ?
          <div className="container">

            <section className="event-card-process">
              <Row>
                <Col md={12}>
                  <Card className="card-list">
                    <div className="row mx-0 ">

                      <div className="col-md-4  p-0">
                        <CardImg src={event.photoUrl ? event.photoUrl : ""} className="rounded-0"/>
                      </div>
                      <div className="col-md-4 py-4 bordr">
                        <h3 className="font-weight-light px-5" style={{boxOrient: "vertical"}}>“{event.title}”</h3>
                        <Row className=" pt-2 mt-2 ml-3 ">
                          <Col md="6" className="col-6">
                            <p className="m-0 text-muted small">Spiker</p>
                            <p className="m-0 font-weight-bold text-body">{event.speakers}</p>
                          </Col>

                          <Col md="6" className="col-6">
                            <p className="m-0 small text-muted">Vaqti</p>
                            <p
                              className="m-0 font-weight-bold text-body">{event.startTime ? event.startTime.substring(0, 10) + ' ' + new Date(event.startTime).toTimeString().substring(0, 5) : ''}</p>
                          </Col>
                        </Row>
                      </div>
                      <div className="col-md-4">
                        <CardFooter className="bg-transparent position-relative border-0">
                          <div className="card-price mt-2 mt-md-5">
                            <div className="d-flex">
                              <div className="text-center d-flex  mt-2 pl-3">
                                <h1 className="m-0 font-weight-bold">
                                  {countStars.avgStar}
                                </h1>
                                <div className="rate pt-1 star-size">
                                  <StarRatingComponent
                                    name="rate1"
                                    starCount={5}
                                    value={countStars.avgStar}
                                  />
                                  <p>Jami: {countStars.countStars} ta ovoz</p>
                                </div>
                              </div>
                              <div className="text-center no-padding pt-3 pl-3">
                                <Button color="danger" className="btn-sm" onClick={registerButton}>
                                  {register ? "Tadbir haqida" : "Ro'yxatdan o'tish"}</Button>

                              </div>
                            </div>
                          </div>
                        </CardFooter>
                      </div>
                    </div>
                  </Card>
                </Col>
              </Row>

            </section>

            {register ?
              <div>
                <main className="event-registration red-row">
                  <section className="bg-white">
                    <h4>Tadbirda ro'yxatdan o'tish</h4>

                    <Row className="mt-5">
                      <Col md={{size: 6, offset: 3}}>
                        <Row>
                          <Col md="1" className="px-0 col-2">
                            <div
                              className={activeTab >= 1 ? "step text-center step-active step-completed" : "step text-center"}>
                              {activeTab > 1 ? <div className="completed"/> : <span className="d-block mt-1">1</span>}
                            </div>
                          </Col>
                          <Col md="4" className="px-0 circles-process col-3">
                            <Progress color="success" value={activeTab >= 2 ? "100" : "0"}/>
                          </Col>
                          <Col md="1" className="px-0 col-2">
                            <div
                              className={activeTab >= 2 ? "step text-center step-active step-completed" : "step text-center"}>
                              {activeTab > 2 ? <div className="completed"/> : <span className="d-block mt-1">2</span>}
                            </div>
                          </Col>
                          <Col md="4" className="px-0 circles-process col-3">
                            <Progress color="success" value={activeTab > 2 ? "100" : "0"}/>
                          </Col>
                          <Col md="1" className="px-0 col-1">
                            <div
                              className={activeTab == 3 ? "step text-center step-active step-completed" : "step text-center"}>
                              <span className="d-block mt-1">3</span></div>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <AvForm model={currentUser} onValidSubmit={bookSeat}>
                      <TabContent activeTab={activeTab} className="mt-5">
                        <TabPane tabId={1}>
                          <p>Asosiy ma'lumotlar</p>
                          <Row>
                            <Col md={{size: 3}} className="px-0">
                              <AvGroup>
                                <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                                <AvInput name="firstName" type="text" required/>
                                <label>Ism</label>
                              </AvGroup>
                            </Col>
                            <Col md={3} className="px-0">
                              <AvGroup>
                                <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                                <AvInput name="lastName" type="text" required/>
                                <label>Familiya</label>
                              </AvGroup>
                            </Col>
                            <Col md={{size: 3}} className="px-0">
                              <AvGroup>
                                <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                                <AvInput name="phoneNumber" type="text" disabled={true}/>
                                <label></label>
                              </AvGroup>
                            </Col>
                            <Col md={3}>
                              <DatePicker
                                label="Tug'ilgan sana"
                                selected={dateTime}
                                className="form-control mt-2"
                                onChange={changeDateTime}
                                dateFormat="dd.MM.yyyy"
                              />
                            </Col>
                          </Row>
                        </TabPane>
                        <TabPane tabId={2}>
                          <p>Qo'shimcha ma'lumotlar</p>
                          <Row>
                            <Col md={4} className="px-0">
                              <AvGroup>
                                <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                                <AvInput name="company" type="text" required value={currentUser.company}/>
                                <label>Tashkilot nomi</label>
                              </AvGroup>
                            </Col>
                            <Col md={4} className="px-0">
                              <AvGroup>
                                <AvInput type="select" name="positionId"
                                         value={currentUser.position ? currentUser.position.id : ""}>
                                  <option>Lavozimni tanlang</option>
                                  {positions.map(i => {
                                    return <option value={i.id}>{i.nameUz}</option>
                                  })}
                                </AvInput>
                                <label>Lavozim</label>
                              </AvGroup>
                            </Col>
                            <Col md={4} className="px-0">
                              <AvGroup>
                                <AvInput type="select" name="regionId"
                                         value={currentUser.region ? currentUser.region.id : ""}>
                                  <option>Viloyatni tanlang</option>
                                  {regions.map(i => {
                                    return <option value={i.id}>{i.nameUz}</option>
                                  })}
                                </AvInput>
                                <label>Viloyat</label>
                              </AvGroup>

                            </Col>
                          </Row>
                        </TabPane>
                        <TabPane tabId={3}>
                          <div className="d-flex">
                            <p>Joylar</p>
                            <ul className="ml-auto">
                              <li className="d-inline-block mr-5">
                                <span><span className="circle event-seat-sold"/> Sotilgan joylar</span>
                              </li>
                              <li className="d-inline-block mr-5">
                                <span><span className="circle event-seat-busy"/> Band joylar</span>
                              </li>
                              <li className="d-inline-block">
                                <span><span className="circle event-seat-empty"/> Bo'sh joylar</span>
                              </li>
                            </ul>
                          </div>
                          {seats.map(row => {
                            return <Row className="event-seat-row pb-3 mb-4 mx-0">
                              {row.map(i => {
                                return chosenSeatsId.includes(i.seatId) ?
                                  <div className="position-relative" onClick={() => checkSeats(i.seatId)}>
                                    <Input type="button"
                                           disabled
                                           defaultValue={i.name}
                                           className={i.placeStatus === "EMPTY" ? "event-seat-empty event-seat  text-center" :
                                             i.placeStatus === "SOLD" ? "event-seat-sold event-seat  text-center" :
                                               "event-seat-busy event-seat  text-center"}/>
                                    <div className="seat-check position-absolute"/>
                                  </div> :
                                  <Input type="button" onClick={() => checkSeats(i.seatId)}
                                         disabled={i.placeStatus !== "EMPTY"}
                                         defaultValue={i.name}
                                         className={i.placeStatus === "EMPTY" ? "event-seat-empty event-seat  text-center" :
                                           i.placeStatus === "SOLD" ? "event-seat-sold event-seat  text-center" :
                                             "event-seat-busy event-seat  text-center"}/>
                              })}
                            </Row>
                          })}
                        </TabPane>
                        <div className="d-flex ml-auto mt-3 mt-md-0">
                          {activeTab === 3 ? <Button color="success" type="submit">Saqlash</Button> : ""}
                          {activeTab !== 3 ? <Button color="light" onClick={nextTab}>Keyingisi <img
                            src="/assets/images/icon/chevrons-right.png" alt=""/></Button> : ""}
                          {activeTab !== 1 ? <Button color="light" onClick={prevTab} className="mr-3"><img
                            src="/assets/images/icon/chevrons-left.png" alt=""/> Orqaga</Button> : ""}
                        </div>
                      </TabContent>
                    </AvForm>

                  </section>
                  {/*<section className="mt-5">
                      <h3 className="text-body">Kutilayotgan tadbirlar</h3>
                      <Row className="mt-4">
                        <Col md={4}>
                          CARD STRUCTURASI
                          <Card>
                            <CardImg src="/assets/images/img/event/photo_2019-07-13_21-52-05.png"/>
                            <CardBody>
                              <h4 className="text-center mb-4">"Biznes va hayotda shaxsiy yetakchlik"</h4>
                              <div className="d-flex">
                                <div className="w-50 pr-3">
                                  <p className="mb-0 text-muted">Spiker</p>
                                  <h4 className="no-wrap">Nodir Zakirov</h4>
                                </div>
                                <div className="w-50 pr-3">
                                  <p className="mb-0 text-muted">Vaqti</p>
                                  <h4 className="no-wrap">Se,16-iyul</h4>
                                </div>
                              </div>
                            </CardBody>
                            <CardFooter className="bg-transparent position-relative">
                              <div className="card-price">
                                <h1 className="text-center font-weight-bold m-0 pt-3">
                                  300 000 <span className="font-weight-light text-upparcase small">USD</span>
                                </h1>
                                <p className="m-0 text-muted text-center pb-3">
                                  Tadbir narxi
                                </p>
                              </div>
                              <div className="d-flex">
                                <div className="w-33 text-center ">
                                  <h1 className="m-0 font-weight-bold">
                                    80
                                  </h1>
                                  <p className="font-weight-light text-muted">
                                    Joylar
                                  </p>
                                </div>
                                <div className="w-33 text-center ">
                                  <h1 className="m-0 font-weight-bold text-muted">
                                    60
                                  </h1>
                                  <p className="font-weight-light text-muted">
                                    Sotilgan
                                  </p>
                                </div>
                                <div className="w-33 text-center ">
                                  <h1 className="m-0 font-weight-bold text-success">
                                    20
                                  </h1>
                                  <p className="font-weight-light text-muted">
                                    Sotilmagan
                                  </p>
                                </div>
                              </div>
                              <CardImgOverlay className="card-hover-block d-flex align-items-center py-4">
                                <h4 className="w-75 m-0 text-white font-weight-light">Tadbir uchun joy band qilish</h4>
                                <div className="w-25 text-right">
                                  <span className="icon icon-arrow-right bg-white"/>
                                </div>
                                <a href="#"/>
                              </CardImgOverlay>
                            </CardFooter>
                          </Card>
                          CARD STRUCTURASI
                        </Col>
                      </Row>
                    </section>*/}
                </main>
              </div>
              :
              <div>
                <section className="bg-white px-5 pb-5">
                  {event.aboutEvent ?
                    <div className="row">
                      <div className="col-md-12">
                        <p className="about-event">TADBIR HAQIDA</p>
                        <p className="text-one">{event.aboutEvent}</p>
                      </div>
                    </div> : ""}
                  {event.aboutSpeaker ?
                    <div className="row">
                      <div className="col-md-12">
                        <p className="about-spiker">SPIKER HAQIDA</p>
                        <p className="text-one">{event.aboutSpeaker}</p>
                      </div>
                    </div> : ""
                  }

                  <p className="commet">SHARHLAR</p>
                  <div className="row">

                    <div className="col-md-7">
                      <div className="row">
                        {ownReview.id ? <div className="col-md-12 mt-3" key={ownReview.id}>
                          <div className="row">
                            <div className="col-md-1">
                              <img className="girls-image"
                                   src={ownReview.photoUrl ? ownReview.photoUrl : "/assets/images/icon/user.svg"}
                                   alt={ownReview.firstName} style={{width: '35px'}}/>
                            </div>
                            <div className="col-md-11">
                              <p className="girls-name">{ownReview.firstName} {ownReview.lastName}
                                <p className="opasity-p">{ownReview.date ? ownReview.date.substring(0, 10) : ""}
                                  <StarRatingComponent
                                    starCount={5}
                                    value={ownReview.star}
                                  /></p>
                                <p>{ownReview.text}</p>
                              </p>
                            </div>
                          </div>
                        </div> : ""}

                        {reviews.map(i => {
                          return i.id !== ownReview.id ?
                            <div className="col-md-12 mt-3" key={i.id}>
                              <div className="row">
                                <div className="col-md-1">
                                  <img className="girls-image" style={{width: '40px', height: '40px'}}
                                       src={i.photoUrl ? i.photoUrl : "/assets/images/icon/user.svg"}
                                       alt={ownReview.firstName}/>
                                </div>
                                <div className="col-md-11">
                                  <p className="girls-name">{i.firstName} {i.lastName}
                                    <p className="opasity-p">{i.date ? i.date.substring(0, 10) : ""}
                                      <StarRatingComponent
                                        starCount={5}
                                        value={i.star}
                                      /></p>
                                    <p>{i.text}</p>
                                  </p>
                                </div>
                              </div>
                            </div> : ""
                        })}
                        {!ownReview.id ?
                          <div className="col-md-12">
            <textarea placeholder=" Sharhingiz" name="text" onChange={addReview}>
            </textarea>
                            <StarRatingComponent
                              name="starCount"
                              starCount={5}
                              value={starCount}
                              onStarClick={onStarClick}
                            />
                            <span>
            {starCount === 1 ? "Yomon" : starCount === 2 ? "Qoniqarsiz" :
              starCount === 3 ? "Qoniqarli" : starCount === 4 ? "Yaxshi" :
                starCount === 5 ? "A'lo" : ""}
            </span>
                            {isLoading ? <div style={{margin: "35px auto", width: "35px"}}><Spinner/></div> :
                              <Button color="danger" className="ml-auto d-block btn-sm"
                                      onClick={sendReview}>Jo'natish</Button>}
                          </div>
                          : ""}
                      </div>
                    </div>

                    <div className="col-md-5 pl-5">
                      <div className="row">
                        <div className="col-md-3">
                          <p className="four mb-0">{countStars.avgStar}</p>
                        </div>
                        <div className="col-md-9 pt-3">
                          <StarRatingComponent
                            name="rate1"
                            starCount={5}
                            value={countStars.avgStar}
                          />
                          <p className="ovoz">Jami: {countStars.countStars} ta ovoz</p>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-12">
                          <div className="d-flex align-items-center">
                            <img className="d-inline-block pr-2" src="/assets/images/star.png" alt=""/>
                            5
                            <span className="mx-3 bg-success" style={{
                              borderRadius: '5px',
                              width: countStars.five * 350 / countStars.countStars + 5 + 'px',
                              height: '5px'
                            }}/>
                            <span>{countStars.five}</span>
                          </div>
                          <div className="d-flex align-items-center">
                            <img className="d-inline-block pr-2" src="/assets/images/star.png" alt=""/>
                            4
                            <span className="mx-3 bg-primary" style={{
                              borderRadius: '5px',
                              width: countStars.four * 350 / countStars.countStars + 5 + 'px',
                              height: '5px'
                            }}/>
                            <span>{countStars.four}</span>
                          </div>
                          <div className="d-flex align-items-center">
                            <img className="d-inline-block pr-2" src="/assets/images/star.png" alt=""/>
                            3
                            <span className="mx-3 bg-info" style={{
                              borderRadius: '5px',
                              width: countStars.three * 350 / countStars.countStars + 5 + 'px',
                              height: '5px'
                            }}/>
                            <span>{countStars.three}</span>
                          </div>
                          <div className="d-flex align-items-center">
                            <img className="d-inline-block pr-2" src="/assets/images/star.png" alt=""/>
                            2
                            <span className="mx-3 bg-danger" style={{
                              borderRadius: '5px',
                              width: countStars.two * 350 / countStars.countStars + 5 + 'px',
                              height: '5px'
                            }}/>

                            <span>{countStars.two}</span>
                          </div>
                          <div className="d-flex align-items-center">
                            <img className="d-inline-block pr-2" src="/assets/images/star.png" alt=""/>
                            1
                            <span className="mx-3 bg-secondary" style={{
                              borderRadius: '5px',
                              width: countStars.one * 350 / countStars.countStars + 5 + 'px',
                              height: '5px'
                            }}/>
                            <span>{countStars.one}</span>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </section>
              </div>}

          </div> : "Sahifa topilmadi"}
      </div>
    );
  }
}

export default Index;

