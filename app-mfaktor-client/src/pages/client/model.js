import api from 'api'
import {STORAGE_NAME} from 'utils/constant';

const {getUserIncomesAndOutcomes, getUser} = api;

export default ({
  namespace: 'clientModel',
  state: {
    selectedUser: {},
    isModalShow: false,
    isModalShowInfo: false,
    modalType: "new",
    dateTime: new Date(),
    phoneNumber: '',
    activeTab: '1',
    loading: true,
    incomes: [],
    outcomes: [],
    user: {},
  },

  subscriptions: {},
  effects: {
    * getUserIncomesAndOutcomes({payload}, {call, put, select}) {
      const res = yield call(getUserIncomesAndOutcomes, payload);
      if (res.success) {
        let outcomes = [];
        res.object.outcomes.forEach(i => {
          const events = outcomes.filter(event => event[0].eventName === i.eventName);
          if (events.length === 0) {
            outcomes.push(res.object.outcomes.filter(j => j.eventName === i.eventName));
          }
        });
        yield put({
          type: 'updateState',
          payload: {
            outcomes,
            incomes: res.object.incomes
          }
        })
      }
    },
    * getUserInfo({payload}, {call, put, select}) {
      const res = yield call(getUser, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            user: res.object
          }
        })
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
