import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {
  Button,
  Card,
  Col,
  InputGroup,
  InputGroupAddon,
  Modal,
  ModalBody,
  ModalHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row
} from "reactstrap";
import CardBody from "reactstrap/es/CardBody";
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import DatePicker from "react-datepicker/es";
import Loader from "react-loader-spinner";

@connect(({clientModel, app}) => ({clientModel, app}))
class Client extends Component {
  componentDidMount() {
    const {dispatch, clientModel, app} = this.props;
    dispatch({
      type: 'app/getUsers'
    });
    dispatch({
      type: 'app/getPosition'
    });
    dispatch({
      type: 'app/getRegions'
    });
    dispatch({
      type: 'app/getAwares'
    });
  }


  clickHiding = () => {
    let x = document.getElementById("ss");
    if (!x.classList.contains("inputQidirish")) {
      x.classList.add("inputQidirish");
    } else {
      const {dispatch} = this.props;
      dispatch({
        type: 'app/searchByTitle',
        payload: {
          path: x.value
        }
      })
    }
  };


  render() {
    const {dispatch, clientModel, app} = this.props;
    const {users, totalPages, size, loading, positions, regions, awares, totalElements, isSort, photoId, paginations, page, search} = app;
    const {selectedUser, isModalShow, modalType, dateTime} = clientModel;

    const getUser = (item) => {
      dispatch({
        type: "clientModel/updateState",
        payload: {
          modalType: "edit",
          isModalShow: true,
          selectedUser: item
        }
      });
      dispatch({
        type: 'app/updateState',
        payload: {
          photoId: item.photoUrl.split("/")[item.photoUrl.split("/").length - 1]
        }
      })
    };
    const clientInfo = (user) => {
      dispatch({
        type: 'clientModel/updateState',
        payload: {
          selectedUser: user,
          modalType: "info",
          isModalShow: true
        }
      })
    };
    const openModal = () => {
      dispatch({
        type: "clientModel/updateState",
        payload: {
          isModalShow: !isModalShow,
          selectedUser: {},
          modalType: ''
        }
      });
      dispatch({
        type: 'app/updateState',
        payload: {
          photoId: ''
        }
      })
    };
    const save = (e, v) => {
      dispatch({
        type: 'app/registerClient',
        payload: {
          ...v,
          id: selectedUser.id,
          photoId: photoId,
        }
      });
      dispatch({
        type: 'app/updateState',
        payload: {
          photoId: ''
        }
      })
    };
    const getPhoneNumber = (e) => {
      dispatch({
        type: 'clientModel/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    };
    const handleFile = (e) => {
      dispatch({
        type: 'app/uploadAvatar',
        payload: {
          file: e.target.files,
          fileUpload: true
        }
      })
    };
    const changeDateTime = (date) => {
      dispatch({
        type: 'clientModel/updateState',
        payload: {
          dateTime: date
        }
      })
    };
    const getClients = (e, page) => {
      e.preventDefault();
      let payload = {page};
      if (search) payload.search = search;
      dispatch({
        type: 'app/getUsers',
        payload
      })
    };

    const searchUser = (e, error, values) => {
      let payload = {};
      let search = document.getElementById('ss').value;
      if (search) {
        payload = {search: search, page: 0}
      } else {
        payload = {page: 0}
      }
      dispatch({
        type: 'app/getUsers',
        payload
      })
    };

    const sort = () => {
      dispatch({
        type: 'app/updateState',
        payload: {
          isSort: !isSort
        }
      });
      dispatch({
        type: 'app/getUsers'
      })
    };


    return (
      <div>
        <main>
          <section className="client_page margin-70">
            <div className="container">
              {loading ?
                <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px', marginBottom: '150px'}}>
                  <Loader
                    type="Triangle"
                    color="#ED2939"
                    height="100"
                    width="100"
                  />
                </div> : <div>{users.length === 0 && !search ?
                  (<div>
                    <h3 className="font-weight-bolder text-center margin-70">Hozirda mijozlar mavjud emas</h3>
                    <Card onClick={openModal} className="client-add-btn">
                      <CardBody>
                        <p className="text-center mb-0"><span className="icon icon-user-add"/> Yangi mijoz qo'shish
                        </p>
                      </CardBody>
                    </Card></div>)
                  :
                  <div>
                    <Row className="pt-2">
                      <Col md={3}>
                        <h3 className="header-title">Mijozlar{"(" + totalElements + ")"}</h3>
                        <br/>
                        <p style={{textTransform: "uppercase", color: 'red'}}>{isSort ? "tadbir soni bo'yicha" : ""}</p>
                      </Col>
                      <Col md={6} className="pl-md-0 pl-3 pt-2">
                        <InputGroup>
                          <InputGroupAddon id='hr-search-icon' onClick={() => this.clickHiding()}
                                           className="hr-search-icon mr-4"
                                           addonType="prepend"><span
                            className="icon icon-search"/></InputGroupAddon>
                          <AvForm onSubmit={searchUser}>
                            <input id="ss" name="search" className="hr-search-input inputKorinishi client-search"
                                   placeholder="Tadbir nomini kiriting"/>
                          </AvForm>
                        </InputGroup>
                      </Col>

                      <Col md={2}>
                        <button className="btn btn-success px-4 py-2 float-right" onClick={openModal}>Qo'shish</button>
                      </Col>
                    </Row>

                    <Row className="mt-2">
                      <Col className="md-2 offset-10">
                        <button className="ml-5 btn" onClick={sort}><img src="/assets/images/icon/sort.svg"
                                                                         style={{height: "25px"}}
                                                                         alt=""/></button>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="3">
                        <h2 className="font-weight-bolder">FIO</h2>
                      </Col>
                      <Col md="2">
                        <h2 className="font-weight-bolder">Telefon raqam</h2>
                      </Col>
                      <Col md="2">
                        <h2 className="font-weight-bolder">Balans</h2>
                      </Col>
                      <Col md="2">
                        <h2 className="font-weight-bolder">Tashkilot</h2>
                      </Col>
                      <Col md="1">
                        <h2 className="font-weight-bolder">Manba</h2>
                      </Col>
                      <Col md="2">
                        <h2 className="font-weight-bolder">Tashriflar</h2>
                      </Col>
                    </Row>
                    {users.map((item, i) =>
                      (
                        <div key={"users" + i}>
                          <div className="client-list-item pt-2">
                            <Row>
                              <Col md="3" className="text-center">
                                {item.photoUrl === null || item.photoUrl === "" ?
                                  <div
                                    className="client-avatar">{item.firstName.substr(0, 1).toUpperCase() + item.lastName.substr(0, 1).toUpperCase()}</div> :
                                  <img src={item.photoUrl} alt="" className="mr-3"/>}
                                <p onClick={() => clientInfo(item)}
                                   className="user-name">{item.firstName + ' ' + item.lastName}</p>
                              </Col>
                              <Col md="2" className="text-center">
                                <p>{item.phoneNumber}</p>
                              </Col>
                              <Col md="2" className="text-center">
                                <p><Link to={"/client/" + item.id}>{item.balance}</Link></p>
                              </Col>
                              <Col md="2" className="text-center">
                                <p>{item.company}</p>
                              </Col>
                              <Col md="1" className="text-center">
                                <p>{item.awareUz}</p>
                              </Col>
                              <Col md="2" className="text-center">
                                <p>{item.countVisit} marta</p>
                              </Col>
                            </Row>
                            <div className="line"/>
                            <button className="btn btn-edit-28 p-0" onClick={() => getUser(item)}><span
                              className="icon icon-edit"/></button>
                          </div>
                        </div>
                      ))}
                    <div className="col-md-3 offset-5 text-center my-3">
                      <Pagination aria-label="Page navigation example">
                        <PaginationItem disabled={page === 0}>
                          <PaginationLink first onClick={(e) => getClients(e, 0)}/>
                        </PaginationItem>
                        <PaginationItem disabled={page === 0}>
                          <PaginationLink previous onClick={(e) => getClients(e, page !== 0 ? page - 1 : 0)}/>
                        </PaginationItem>
                        {paginations.map(i =>
                          <PaginationItem active={page === (i - 1)}
                                          disabled={page === (i - 1) || i === '...'}>
                            <PaginationLink onClick={(e) => getClients(e, i - 1)}>
                              {i}
                            </PaginationLink>
                          </PaginationItem>)}
                        <PaginationItem disabled={totalPages === page + 1}>
                          <PaginationLink next
                                          onClick={(e) => getClients(e, totalPages >= (page + 1) ? (page + 1) : page)}/>
                        </PaginationItem>
                        <PaginationItem disabled={totalPages === page + 1}>
                          <PaginationLink last
                                          onClick={(e) => getClients(e, page !== totalPages ? page + 1 : totalPages)}/>
                        </PaginationItem>
                      </Pagination>
                    </div>
                  </div>
                }</div>}

            </div>

            <Modal isOpen={isModalShow} toggle={openModal} className="custom-modal">
              <ModalHeader toggle={openModal} className="border-0"/>
              <ModalBody className="px-5 pb-5">
                {modalType === 'info' ?
                  <div className="client-info">
                    <h3>Mijoz haqida ma'lumot</h3>
                    <Row className="mt-5">
                      <Col md="2" className="text-center">
                        {selectedUser.photoUrl === null || selectedUser.photoUrl === "" ?
                          <div className="client-avatar"/> :
                          <img src={selectedUser.photoUrl} alt="" className="mr-3"/>}
                      </Col>
                      <Col md="10">
                        <h4 className="mt-3">{selectedUser.firstName + " " + selectedUser.lastName}</h4>
                        <p>Balans: <h3>{selectedUser.balance} <span>UZS</span></h3></p>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={{size: "4", offset: "2"}}>
                        <p className="mb-0 mt-4"><span className="icon icon-phone mr-3"/> Telefon raqam: </p>
                        <h4>{selectedUser.phoneNumber}</h4>

                        <p className="mb-0 mt-4"><span className="icon icon-date mr-3"/> Tug'ilgan kun: </p>
                        <h4>{selectedUser.birthDate ? selectedUser.birthDate.substring(8, 10) + '.' +
                          selectedUser.birthDate.substring(5, 7) + '.'
                          + selectedUser.birthDate.substring(0, 4) : ''}</h4>

                        <p className="mb-0 mt-4"><span className="icon icon-dashboard mr-3"/> Tashkilot: </p>
                        <h4>{selectedUser.company}</h4>

                        <p className="mb-0 mt-4"><span className="icon icon-user mr-3"/> Lavozim: </p>
                        <h4>{selectedUser.positionUz}</h4>
                      </Col>
                      <Col md={{size: "4", offset: "2"}}>
                        <p className="mb-0 mt-4"><span className="icon icon-antenna mr-3"/> Reklamani eshitgani: </p>
                        <h4>{selectedUser.awareUz}</h4>

                        <p className="mb-0 mt-4"><span className="icon icon-check mr-3"/> Tashriflar soni: </p>
                        <h4>{selectedUser.countVisit} marta</h4>
                        <p className="mb-0 mt-4"><span className="icon icon-location mr-3"/> Manzil: </p>
                        <h4>{selectedUser.addressUz}</h4>
                        <p className="mb-0 mt-4"><span className="icon icon-percent mr-3"/> Chegirma: </p>
                        <h4>{selectedUser.sale}</h4>
                      </Col>
                    </Row>
                  </div>
                  :
                  <AvForm onValidSubmit={save}>
                    <h3 className="font-weight-bolder">Mijoz qo'shish</h3>
                    <div className="client-photo-upload"
                         style={{backgroundImage: photoId ? 'url("/api/file/' + photoId + '")' : 'url("/assets/images/user.png")'}}>
                      <label htmlFor="user-avatar"/>
                      <input type="file" onChange={handleFile} id="user-avatar" className="d-none"/>
                    </div>
                    <Row className="mt-0">
                      <Col md={{size: 4, offset: 1}}>
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput name="firstName" value={selectedUser.firstName} type="text" required/>
                          <label>Ismi</label>
                        </AvGroup>
                      </Col>
                      <Col md={{size: 4, offset: 2}}>
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput value={selectedUser.company} name="company" type="text" required/>
                          <label>Tashkilot nomi </label>
                        </AvGroup>
                      </Col>
                    </Row>
                    <Row className="mt-0">
                      <Col md={{size: 4, offset: 1}}>
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput value={selectedUser.lastName} name="lastName" type="text" required/>
                          <label>Familiyasi</label>
                        </AvGroup>
                      </Col>
                      <Col md={{size: 4, offset: 2}}>
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput type="select" name="positionId" value={selectedUser.positionId} required>
                            <option>Lavozimni tanlang</option>
                            {positions.map((i) => <option key={"options" + i.id} value={i.id}>{i.nameUz}</option>)}
                          </AvInput>
                          <label>Lavozimi</label>
                        </AvGroup>
                      </Col>
                    </Row>
                    <Row className="mt-0">
                      <Col md={{size: 4, offset: 1}}>
                        <AvGroup>
                          <AvInput type="select" name="awareId" value={selectedUser.awareId} required>
                            <option>Reklamani ko'rgan joyini tanlang</option>
                            {awares.map((i) => <option key={"awares" + i.id} value={i.id}>{i.nameUz}</option>)}
                          </AvInput>
                          <label>Reklama ko'rgan joyi</label>
                        </AvGroup>
                      </Col>
                      <Col md={{size: 4, offset: 2}}>
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput value={selectedUser.phoneNumber} name="phoneNumber" type="text" required/>
                          <label>Telefon raqami</label>
                        </AvGroup>
                      </Col>
                    </Row>
                    <Row className="mt-0">
                      <Col md={{size: 4, offset: 1}}>
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <DatePicker
                            name="birthDate"
                            selected={dateTime}
                            className="form-control"
                            onChange={changeDateTime}
                            timeIntervals={30}
                            dateFormat="dd.MM.yyyy"
                            timeCaption="time"
                          />
                          <label className="label-date">Tug'ilgan sanasi</label>
                        </AvGroup>
                      </Col>
                      <Col md={{size: 4, offset: 2}}>
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput type="select" name="regionId" value={selectedUser.regionId} required>
                            <option>Viloyatni tanlang</option>
                            {regions.map((i) => <option key={"regions" + i.id} value={i.id}>{i.nameUz}</option>)}
                          </AvInput>
                          <label>Viloyati</label>
                        </AvGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={{size: 4, offset: 1}}>
                        <AvGroup>
                          <AvInput value={selectedUser.sale} name="sale"/>
                          <label>Chegirma miqdori</label>
                        </AvGroup>
                      </Col>
                    </Row>
                    <Row className="mb-5 mt-4">
                      <Col md={{size: 2, offset: 7}}>
                        <Button outline type="button" className="btn-block" onClick={openModal}>Bekor
                          qilish</Button>
                      </Col>
                      <Col md={{size: 2}}>
                        <Button type="submit" color="success" className=" btn-block ">Saqlash </Button>
                      </Col>
                    </Row>
                  </AvForm>
                }
              </ModalBody>
            </Modal>

          </section>
        </main>
      </div>
    );
  }
}

export default Client;
