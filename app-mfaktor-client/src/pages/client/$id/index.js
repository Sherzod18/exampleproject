import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {
  Button,
  Card,
  CardFooter,
  CardImg,
  Col,
  Modal, ModalBody,
  ModalHeader,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent, Table,
  TabPane
} from "reactstrap";
import {Link} from "react-router-dom";
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import MaskedInput from "../index";

@connect(({clientModel}) => ({clientModel}))
class Index extends Component {
  componentWillMount() {
    const {dispatch, clientModel} = this.props;
    dispatch({
      type: 'clientModel/getUserIncomesAndOutcomes',
      payload: {
        path: this.props.match.params.id
      }
    });
    dispatch({
      type: 'clientModel/getUserInfo',
      payload: {
        path: this.props.match.params.id
      }
    })
  }

  render() {
    const {dispatch, clientModel} = this.props;
    const {incomes, outcomes, activeTab, user} = clientModel;
    const toggle = (tab) => {
      dispatch({
        type: 'clientModel/updateState',
        payload: {
          activeTab: tab
        }
      })
    };
    var sumIncome = 0;
    var sumExpense = 0;
    var sumSale = 0;
    var sumSeatPrice = 0;


    return (
      <div>
        <div className="container mt-3 mb-3 payer-section">
          <Row className="icon-row">
            <Col md={12}>
              <Row className="user-info mx-0">
                <Col md="12" className="pt-3">
                  <Row>
                    <Col md="2" className="text-center border">
                      {user.photoUrl === null || user.photoUrl === "" ?
                        <div className="client-avatar"/> :
                        <img width='100%' src={user.photoUrl} alt="" className="img-payment-man"/>}
                    </Col>
                    <Col md="4" className="pl-0">
                      <h4 className="mb-0">{user.fullName}</h4>
                      <p className="mb-0 text-p">tomonidan amalga
                        oshirilgan {activeTab === '1' ? "to'lovlar " : "xarajatlar "}haqida ma'lumot
                      </p>
                      <h5><span className="icon icon-phone mr-3"/>{user.phoneNumber}</h5>
                    </Col>
                    <Col md={3} className="man-card offset-3">
                      <h4 className="mt-4 ml-2">Balans</h4>
                      <h3 className="ml-2 text-white">{" " + (new Intl.NumberFormat('fr-FR').format(user.balance))}
                        <span className="pl-1">so'm</span>
                      </h3>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>

          </Row>
        </div>

        <div className="container income-outcome tab-content-payment mt-5">
          <Nav tabs className="border-0">
            <NavItem>
              <NavLink className={activeTab === '1' ? "active-tab pt-3 border-bottom-0" : 'pt-3'}
                       onClick={() => toggle('1')}>
                <h4>To'lovlar</h4>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className={activeTab === '2' ? "active-tab pt-3 border-bottom-0" : 'pt-3'}
                       onClick={() => toggle('2')}>
                <h4>Chiqimlar</h4>
              </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={activeTab} className="bg-white">
            <TabPane tabId="1" className="px-4 py-5">

              <Row>
                <Col md="12">
                  {
                    incomes.length !== 0 ?
                      <div>
                        <Row className="mt-5">
                          <Col md="1">
                            <h4 className="text-right">T/r</h4>
                          </Col>
                          <Col md="3">
                            <h4 className="text-center">To'lov vaqti</h4>
                          </Col>
                          <Col md="3">
                            <h4 className="text-center">To'lov miqdori</h4>
                          </Col>
                          <Col md="2">
                            <h4 className="text-center">To'lov turi</h4>
                          </Col>
                          <Col md="3">
                            <h4 className="text-center">Izoh</h4>
                          </Col>
                        </Row>
                        {
                          incomes.map((item, i) =>
                            <div className="payment-info">
                              <Row className="user-info mx-0">
                                <Col md="1" className="pt-2 px-4">
                                  <p className="mt-1 text-center">{i + 1}</p>
                                </Col>
                                <Col md="3" className="pt-2 px-4">
                                  <p
                                    className="mt-1 text-center">{item.payDate ? item.payDate.substring(8, 10) + '.' + item.payDate.substring(5, 7) + '.' + item.payDate.substring(0, 4) + ' ' + item.payDate.substring(11, 16) : ''}</p>
                                </Col>
                                <Col md="3" className="pt-2 text-center">
                                  <p>{(new Intl.NumberFormat('fr-FR').format(item.sum))}
                                  </p>
                                </Col>
                                <Col md="2" className="pt-2">
                                  <p className="mt-1 text-center">{item.payTypeUz}</p>
                                </Col>
                                <Col md="3" className="pt-2">
                                  <p className="mt-1 text-center">{item.comment}</p>
                                </Col>
                              </Row>
                              <div className="d-none">
                                {sumIncome += item.sum}</div>
                            </div>
                          )
                        }
                        <Row className="user-info mx-0">
                          <Col className="col-md-4 offset-9 text-left">
                            <h3
                              className="text-danger">Jami: <span>{(new Intl.NumberFormat('fr-FR').format(sumIncome))}</span>
                            </h3>
                          </Col>
                        </Row>
                      </div> :
                      <div>
                        <Row className="mt-5">
                          <Col className="mt-3 text-center"><h3>To'lovlar mavjud emas</h3>
                          </Col>
                        </Row>
                      </div>
                  }
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId="2" className="px-4 py-5">
              <Row>
                <Col md="12">
                  {
                    outcomes.length !== 0 ?
                      outcomes.map((event, index) =>
                        <div style={{border: '1px solid #A4A39F'}} className="mb-2">
                          <Row>
                            <Col className="col-md-6 offset-3 mt-3">
                              <h3 className="text-center">{event[0].eventName}</h3>
                            </Col>
                          </Row>
                          <Row className="mt-1">
                            <Col className="col-md-6 offset-3">
                              <h4
                                className="text-center">{event[0].eventDate ? event[0].eventDate.substring(8, 10) + '.'
                                + event[0].eventDate.substring(5, 7) + '.' + event[0].eventDate.substring(0, 4) + ' ' + event[0].eventDate.substring(11, 16) : ''}</h4>
                            </Col>
                          </Row>
                          <Row className="mt-3">
                            <Col md="1">
                              <h5 className="text-center">T/r</h5>
                            </Col>
                            <Col md="3">
                              <h5 className="text-center">Joyi</h5>
                            </Col>
                            <Col md="3">
                              <h5 className="text-center">To'langan mablag'</h5>
                            </Col>
                            <Col md="2">
                              <h5 className="text-center">Tejalgan mablag'</h5>
                            </Col>
                            <Col md="3">
                              <h5 className="text-center">Joy narxi</h5>
                            </Col>
                          </Row>
                          <div className="d-none">
                            {sumExpense = 0}
                            {sumSale = 0}
                            {sumSeatPrice = 0}
                          </div>
                          {event.map((item, i) =>
                            <div className="fee-info">
                              <Row>
                                <Col md="1">
                                  <p className="text-center">{i + 1}</p>
                                </Col>
                                <Col md="3">
                                  <p className="text-center">{item.seatName}</p>
                                </Col>
                                <Col md="3">
                                  <p className="text-center">{item.expenseSum}</p>
                                </Col>
                                <Col md="2">
                                  <p className="text-center">{item.saleSum}</p>
                                </Col>
                                <Col md="3">
                                  <p className="text-center">{item.seatPrice}</p>
                                </Col>
                              </Row>
                              <div className="d-none">
                                {sumExpense += item.expenseSum}
                                {sumSale += item.saleSum}
                                {sumSeatPrice += item.seatPrice}
                              </div>
                            </div>
                          )}
                          <Row className="user-info mx-0 my-1">
                            <Col className="col-md-1 offset-3 pl-2">
                              <h4 className="text-danger">Jami:</h4>
                            </Col>
                            <Col className="col-md-3">
                              <h4
                                className="pl-5 text-danger">{(new Intl.NumberFormat('fr-FR').format(sumExpense))}</h4>
                            </Col>
                            <Col className="col-md-2">
                              <h4 className="text-danger pl-2">{(new Intl.NumberFormat('fr-FR').format(sumSale))}</h4>
                            </Col>
                            <Col className="col-md-2 pl-5">
                              <h4
                                className="text-danger text-center pl-4">{(new Intl.NumberFormat('fr-FR').format(sumSeatPrice))}</h4>
                            </Col>
                          </Row>
                        </div>)
                      :
                      <div>
                        <Row className="mt-5">
                          <Col className="mt-3"><h3 className="text-center">Xarajatlar mavjud emas</h3>
                          </Col>
                        </Row>
                      </div>
                  }
                </Col>
              </Row>
            </TabPane>
          </TabContent>
        </div>
      </div>
    );
  }
}

Index.propTypes = {};

export default Index;
