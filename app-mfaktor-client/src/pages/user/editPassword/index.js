import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Label, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import {goBack} from "umi/src/router";
import {toast} from "react-toastify";

@connect(({user, app}) => ({user, app}))
class Index extends Component {
  render() {
    const {dispatch, app, user} = this.props;
    const {currentUser} = app;
    const {oldPassword, newPassword, rePassword,eye} = user;
    const handleSubmit = (e, er, v) => {
      if (newPassword == rePassword) {
        dispatch({
          type: 'user/editPwd',
          payload: {
            ...v
          }
        });
      }else {
        toast.error("yangi parol noto'gri takrorlandi")
      }
    }
    const updateState = (e) => {
      dispatch({
        type: 'user/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    }
    const showPwd=(x)=>{
      let pwd;
      dispatch({
        type: 'user/updateState',
        payload: {
          eye: eye.map((i,index)=>index===x?!i:i)
        }
      })
      if (x == 0) {
        pwd=document.getElementById("oldPassword")
      }else if (x == 1) {
        pwd=document.getElementById("newPassword")
      }else{
        pwd=document.getElementById("rePassword")
      }
      if (pwd.type == 'text') {
        pwd.type = 'password';
      } else {
        pwd.type = 'text';
      }
    }
    return (
      <div className="container user_edit">
        <div className="row">
          <div className="col-md-8 offset-2">
            <h3 className="header-title2">Foydalanuvchi ma’lumotlari</h3>
            <div className="card">
              <div className="card-body p-sm-3 p-md-0 p-lg-5 p-0">
                <AvForm onSubmit={handleSubmit}>
                  <Row>
                    <Col md="6" className="offset-3">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput id="oldPassword" className="pwd"
                                 value={oldPassword}
                                 onChange={updateState} name="oldPassword" type="password" required/>
                        <label>Eski parolingiz</label>
                      </AvGroup>
                      <button type='button' className={(eye[0]?"icon-eye":"icon-eye-off")+" icon eye_btn"} onClick={()=>showPwd(0)}></button>
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput id="newPassword" className="pwd"
                                 value={newPassword}
                                 onChange={updateState} name="newPassword" type="password" required/>
                        <label>Yangi parol</label>
                      </AvGroup>
                      <button type='button' className={(eye[1]?"icon-eye":"icon-eye-off")+" icon eye_btn"} onClick={()=>showPwd(1)}></button>
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput id="rePassword" className="pwd"
                                 value={rePassword}
                                 onChange={updateState} name="rePassword" type="password" required/>
                        <label>Yangi parol takroran</label>
                      </AvGroup>
                      <button type='button' className={(eye[2]?"icon-eye":"icon-eye-off")+" icon eye_btn"} onClick={()=>showPwd(2)}></button>
                    </Col>
                  </Row>
                  <Row>
                    <Col md="6" className="offset-3">
                      <div className="d-flex justify-content-end">
                        <Button type="button" outline className="mr-4" onClick={goBack}>Bekor qilish</Button>
                        <Button type="submit" color="success">Saqlash</Button>
                      </div>
                    </Col>
                  </Row>
                </AvForm>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Index.propTypes = {};

export default Index;
