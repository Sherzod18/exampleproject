import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Label, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import {goBack} from "umi/src/router";

@connect(({user, app}) => ({user, app}))
class Index extends Component {
  componentWillMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getRegions',
    });
    dispatch({
      type: 'user/getPositions'
    });
  }
  render() {
    const {dispatch, user, app} = this.props;
    const {currentUser,regions} = app;
    const {isModalShow, photoId,positions} = user;
    const handleSubmit = (e, er, v) => {
      dispatch({
        type: 'user/save',
        payload: {
          id: currentUser.id,
          ...v
        }
      })
    }
    const uploadAvatar = (e) => {
      dispatch({
        type: 'user/uploadAvatar',
        payload: {
          file: e.target.files,
          fileUpload: true,
        }
      })
    }
    return (
      <div className="container user_edit">
        <h3 className="header-title2">Foydalanuvchi ma’lumotlari</h3>
          <div className="row">
            <div className="col-md-12">
              <div className="card">
                <div className="card-body p-sm-3 p-md-0 p-lg-5 p-0">
                  <AvForm onSubmit={handleSubmit}>
                    <Row>
                      <Col md="12" className="pb-4">
                        <img src="" alt=""/>
                        <div className="client-photo-upload" style={{backgroundImage: photoId ? 'url("/api/file/'+photoId+'")': 'url("/assets/images/user.png")',backgroundSize:'100%'}}>
                          <label htmlFor="user-avatar" />
                          <input type="file" onChange={(e) => uploadAvatar(e)} id="user-avatar" className="d-none"/>
                        </div>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="8" className="offset-2">
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput name="company" type="text" required
                                   value={currentUser.company ? currentUser.company : ''}
                          />
                          <label>Tashkilot nomi</label>
                        </AvGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="4" className="offset-2">
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput name="firstName" type="text" required
                                   value={currentUser.firstName ? currentUser.firstName : ''}
                          />
                          <label>Ismi</label>
                        </AvGroup>
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput name="lastName" type="text" required
                                   value={currentUser.lastName ? currentUser.lastName : ''}
                          />
                          <label>Familiyasi</label>
                        </AvGroup>
                        <AvGroup>
                          <AvInput type="select" name="regionId"
                                   value={currentUser.region ? currentUser.region.id : ''}
                          >
                            <option></option>
                            {regions.map((c,a) => <option key={a} value={c.id}>{c.nameUz}</option>)}
                          </AvInput>
                          <label>Viloyati</label>
                        </AvGroup>
                      </Col>
                      <Col md="4">
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput name="birthDate" type="date" required
                                   value={currentUser.birthDate ? currentUser.birthDate.substring(0,10) : ''}
                          />
                          <label>Tug’ilgan sanasi</label>
                        </AvGroup>
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput name="phoneNumber" type="text" required
                                   value={currentUser.phoneNumber ? currentUser.phoneNumber : ''}
                          />
                          <label>Telefon raqami</label>
                        </AvGroup>
                        <AvGroup>
                          <AvInput type="select" name="positionId"
                                   value={currentUser.position ? currentUser.position.id : ''}>
                            <option></option>
                            {positions.map((c,b) => <option key={b} value={c.id}>{c.nameUz}</option>)}
                          </AvInput>
                          <label>Lavozimi</label>
                        </AvGroup>
                        <div className="d-flex justify-content-end mt-4">
                            <Button type="button" outline className="mr-4"  onClick={goBack}>Bekor qilish</Button>
                            <Button type="submit" color="success">Saqlash</Button>
                        </div>
                      </Col>
                    </Row>
                  </AvForm>
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}

Index.propTypes = {};

export default Index;
