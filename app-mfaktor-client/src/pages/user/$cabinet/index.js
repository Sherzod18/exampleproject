import React, {Component} from 'react';
import {Col, Container, Row} from "reactstrap";
import {connect} from "react-redux";
import h3 from "eslint-plugin-jsx-a11y/src/util/implicitRoles/h3";
import {Link} from "react-router-dom";

@connect(({user, app}) => ({user, app}))
class UserCabinet extends Component {
  componentWillMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getRegions',
    });
    dispatch({
      type: 'app/userMe'
    });
    dispatch({
      type: 'user/getPositions'
    });
    dispatch({
      type: 'user/getUpcomingEventsOfUser',
      payload: {
        path: this.props.match.params.cabinet
      }
    });
    dispatch({
      type: 'user/getUpcomingEvents',
      payload: {
        path: this.props.match.params.cabinet
      }
    })
  }

  render() {
    const {dispatch, user, app} = this.props;
    const {currentUser, regions} = app;
    const {futureEventsOfUser, upcomingEvents, isModalShow, photoId, positions} = user;
    return (
      <div>
        <Container>
          <h3 className="text-left mt-5 pt-sm-0">Mening tadbirlarim</h3>
          <Row>
            {futureEventsOfUser.length > 0 ? futureEventsOfUser.map(i => {
                return <Col md={4} className="mt-3">
                  <div className="card" style={{height:100+'%'}} key={i.id}>
                    <img src={i.photoUrl} className="img-fluid rounded-top"/>
                    <div className="card-body px-0">
                      <div className="row">
                        <div className="col-md-12 px-5 text-center">
                          <h4 className="event-subject">{i.title}</h4>
                        </div>
                      </div>
                      <div className="row py-3">
                        <div className="col-md-6 pl-5">
                          <p className="mb-0">Spiker</p>
                          <h4 className="event-info">{i.speakers}</h4>
                        </div>
                        <div className="col-md-6 pr5">
                          <p className="mb-0">Vaqti </p>
                          <h4 className="event-info">{i.startTime ? i.startTime.substring(0, 10) + ' ' + new Date(i.startTime).toTimeString().substring(0, 5) : ''}</h4>
                        </div>
                      </div>
                      <div className="row py-3">
                        <div className="col-md-6 pl-5">
                          <p className="mb-0">To'langan pul miqdori</p>
                          <h4 className="event-info">{i.paidSum} UZS</h4>
                        </div>
                        <div className="col-md-6 pr-5">
                          <p className="mb-0">Joy</p>
                          <h4 className="event-info">{i.place}</h4>
                          <span>{i.placeStatus === "BOOKED" ? "Band qilingan" : i.placeStatus === "SOLD"
                            ? "Sotib olingan" : "Qisman to'langan"}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </Col>
              })
              : <Col>
                <div className="row not-found">
                  <div className="col-md-12 text-center"><p style={{fontSize: '24px'}}>Hozirda tadbirlar mavjud emas</p>
                  </div>
                  <div className="col-md-12 add-block">
                    <Link to={"/events"}>
                      <div className="card">
                        <div className="card-body text-center">
                          <span className="icon icon-plus mr-2"/>
                          <span style={{color: '#2F2F2F', textDecoration: 'none'}}> Tadbirga ro'yxatdan o'tish</span>
                        </div>
                      </div>
                    </Link>
                  </div>
                </div>
              </Col>
            }
          </Row>

          <h3 className="text-left mt-5 pt-sm-0">Kutilayotgan tadbirlar</h3>
          <Row>
            {upcomingEvents.length > 0 ?
              upcomingEvents.map(i => {
                  return <div className="col-md-4 mt-3" key={i.id}>
                    <div className="card event-card mb-5" style={{height:100+'%'}} >
                      <img src={i.photoUrl}
                           className="img-fluid rounded-top"/>
                      <div className="card-body px-0">
                        <div className="row">
                          <div className="col-md-12 px-5 text-center">
                            <h4 className="event-subject">{i.title}</h4>
                          </div>
                        </div>
                        <div className="row py-3">
                          <div className="col-md-6 pl-5">
                            <p className="mb-0">Spiker</p>
                            <h4 className="event-info">{i.speakers}</h4>
                          </div>
                          <div className="col-md-6 pr-5">
                            <p className="mb-0">Vaqti</p>
                            <h4
                              className="event-info">
                              {i.startTime ? i.startTime.substring(0, 10) + ' ' + new Date(i.startTime).toTimeString().substring(0, 5) : ''}
                            </h4>
                          </div>
                        </div>
                      </div>
                      <div className="card-footer px-0">
                        {i.minPrice === i.maxPrice ? <div className="row py-2 ">
                            <div className="col-md-12 text-center one-price">
                              <div className="font-weight">
                                <span className="bold bold-span">{i.minPrice} </span>
                                <span className="light">UZS</span>
                              </div>
                            </div>
                            <div className="col-md-12 text-center">
                              <p className="mb-0">Tadbir narxi</p>
                            </div>
                          </div> :
                          <div className="row py-2 ">
                            <div className="col-md-6 text-center border-right">
                              <div className="font-weight">
                                <span className="bold bold-span">{i.minPrice} </span>
                                <span className="light">UZS</span>
                              </div>
                            </div>
                            <div className="col-md-6 text-center  mb-3">
                              <div className="font-weight">
                                <span className="bold bold-span">{i.maxPrice} </span>
                                <span className="light">UZS</span>
                              </div>
                            </div>
                            <div className="col-md-12 text-center">
                              <p className="mb-0">Tadbir narxi shu oraliqda</p>
                            </div>
                          </div>
                        }
                        <Link to={"/events/" + i.id}>
                          {/*<div className="row add-place">
                            <div className="col-md-12 py-3 px-5">
                              <h4>Tadbir uchun joy band qilish</h4>
                              <span className="icon icon-arrow-right"/>
                            </div>
                          </div>*/}
                          <div className="row add-place red-row pb-0 ">
                            <div className="col-md-12 py-3 px-5">
                              <Row>
                                <Col md={7}>
                                  <h4 className="buse-event">Tadbir uchun joy band qilish</h4>

                                </Col>
                                <Col md={5} className="">
                                  <span className="icon icon-arrow-right col-5-icon-right"/>

                                </Col>

                              </Row>
                            </div>
                          </div>
                        </Link>
                      </div>
                    </div>
                  </div>
                }
              )
              : <Col>
                <div className="row not-found">
                  <div className="col-md-12 text-center"><p style={{fontSize: '24px'}}>Tadbir mavjud emas</p>
                  </div>
                </div>
              </Col>
            }
          </Row>
        </Container>
      </div>
    );
  }
}

UserCabinet.propTypes = {};

export default UserCabinet;
