import api from 'api';
import {toast} from "react-toastify";
import {routerRedux} from "dva";
import {router} from "umi";

const {getUpcomingEventsOfUser,editPassword, getUpcomingEvents, addReview,editUser,uploadFile,getPositions} = api;
export default ({
  namespace: 'user',
  state: {
    futureEventsOfUser: [],
    upcomingEvents: [],
    isModalShow: false,
    photoId:'',
    oldPassword:'',
    newPassword:'',
    rePassword:'',
    positions:[],
    eye:[false,false,false],
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        if(location.pathname==="/user/edit")
        {
          dispatch({
            type:'queryCurrentUser'
          })
        }
      })
    }
  },
  effects: {

    *queryCurrentUser({payload},{call,put,select})
    {
      const {app} = yield select(_=>_);
      const {currentUser} = app;
      yield put({
        type:'updateState',
        payload:{
          photoId:currentUser.photo && currentUser.photo!==""?currentUser.photo.id:''
        }
      })
    },

    * editPwd({payload}, {put, call, select}) {
      const res = yield call(editPassword,payload);
      if (res.success) {
        toast.success(res.message);
        router.push('/cabinet');
      } else {
        toast.error("eski parol noto'g'ri kiritilgan")
      }
    },
    * getPositions({payload}, {put, call, select}) {
      const res = yield call(getPositions);
      yield put({
        type: 'updateState',
        payload: {
          positions: res._embedded.list
        },
      });
    },
    * uploadAvatar({payload}, {call, put, select}) {
      const res = yield call(uploadFile, {payload});
      yield put({
        type: 'updateState',
        payload: {
          photoId: res.object[0].fileId,
        }
      })
    },
    * save({payload}, {call, put, select}){
      const {photoId} = yield select(_ => _.user);
      const res = yield call(editUser, {...payload, photoId});
      if (res.success) {
        toast.success("O'zgartirildi");
        yield put(routerRedux.push('/user/cabinet'));
      }else {
        toast.error(res.message);
      }
    },
    * getUpcomingEvents({}, {call, put, select}) {
      const res = yield call(getUpcomingEvents);
      yield put({
        type: 'updateState',
        payload: {
          upcomingEvents: res.object
        }
      })
    },
    * getUpcomingEventsOfUser({payload}, {call, put, select}) {
      const res = yield call(getUpcomingEventsOfUser, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            futureEventsOfUser: res.object
          }
        })
      }
    },

    * addReview({payload}, {call, put, select}) {
      const res = yield call(addReview, payload);
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }

})
