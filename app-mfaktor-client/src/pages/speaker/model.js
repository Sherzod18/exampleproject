import api from 'api'
// import {routerRedux} from 'dva/router'
import {parsePhoneNumber} from 'libphonenumber-js'
import {STORAGE_NAME} from 'utils/constant';
import {toast} from 'react-toastify';

const {deleteSpeaker} = api;

export default ({
  namespace: 'speakerModel',
  state: {
    positionId: 1,
    previewVisible: false,
    previewImage: '',
    currentItem: '',
    delModalShow: false,
    fileList: {},
    speakers: [],
    selectedSpeakerId: '',
    photoId: '',
    editedSpeaker: {},
    isModalShow: false,
    isLoading: true,
    deletingItem: '',
    page:0,
    size:6
  },

  subscriptions: {},
  effects: {
    * deleteSpeaker({payload}, {call, put, select}) {
      const res = yield call(deleteSpeaker, payload);
      if (res.success) {
        yield put({
          type: 'app/getSpeakers'
        })
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
