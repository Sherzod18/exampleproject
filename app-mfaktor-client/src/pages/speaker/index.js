import React, {Component} from 'react';
import {connect} from "react-redux";
import SpeakerModal from "components/SpeakerModal";

import {Button, Card, Col, Container, Modal, ModalFooter, ModalHeader, Row} from "reactstrap";
import CardImg from "reactstrap/es/CardImg";
import CardBody from "reactstrap/es/CardBody";
import Loader from "react-loader-spinner";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

@connect(({speakerModel, app}) => ({speakerModel, app}))
class Speaker extends Component {
  componentDidMount() {
    const {dispatch, speakerModel} = this.props;
    dispatch({
      type: 'app/getSpeakersPagination',
      payload:{
        page:0,
        size:6
      },
      loading: false
    });
    dispatch({
      type: 'speakerModel/updateState',
      payload: {
        editedSpeaker: {},
        logosId: []
      }
    });
    dispatch({
      type: 'app/updateState',
      payload: {
        photoId: ''
      }
    });
    if (speakerModel.selectedSpeakerId) {
      dispatch({
        type: 'speakerModel/getSpeaker',
        payload: {
          path: speakerModel.selectedSpeakerId
        }
      })
    }
    dispatch({
      type: 'app/getPosition'
    })
  }

  render() {
    const {dispatch, speakerModel, app} = this.props;
    const {positions, loading, photoId, speakers, isModalShow, logosId, isLoading, currentItem} = app;
    const {delModalShow,size,page} = speakerModel;
    const modelName = 'speakerModel';
    const openModal = () => {
      dispatch({
        type: "app/updateState",
        payload: {
          currentItem: '',
          isModalShow: !isModalShow,
          photoId: '',
          logosId: []
        }
      });
    };
    const save = (e, v) => {
      dispatch({
        type: 'app/registerSpeaker',
        payload: {
          ...v,
          id: currentItem.id,
          isLoading: true
        }
      })
    };
    const logosSpeaker = (e) => {
      dispatch({
        type: 'app/uploadLogo',
        payload: {
          file: e.target.files,
          fileUpload: true,
          type: 'COMPANY_LOGO'
        }
      })
    };
    const uploadAvatarSpeaker = (e) => {
      dispatch({
        type: 'app/uploadAvatar',
        payload: {
          file: e.target.files,
          fileUpload: true,
          type: 'SPEAKER_AVATAR'
        }
      })
    };
    const handleCancel = () => {
      dispatch({
        type: modelName + '/updateState',
        payload: {
          previewVisible: false
        }
      })
    };
    const handlePreview = async file => {
      if (!file.url && !file.preview) {
        file.preview = await getBase64(file.originFileObj);
      }
      dispatch({
        type: modelName + '/updateState',
        payload: {
          previewImage: file.url || file.preview,
          previewVisible: true,
        }
      })
    };
    const setSelectedSpeakerId = (item) => {
      dispatch({
        type: 'app/updateState',
        payload: {
          currentItem: item,
          isModalShow: true,
          logosId: item.logosUrl.length > 0 ? item.logosUrl.map(i => i.substring(i.lastIndexOf("/") + 1)) : [],
          photoId: item.photoUrl.substring(item.photoUrl.lastIndexOf("/") + 1),

        }
      });
      dispatch({
        type: 'app/updateState',
        payload: {
          photoId: item.photoUrl.substring(item.photoUrl.lastIndexOf("/") + 1)
        }
      });
    };
    const deleteSpeaker = (id) => {
      dispatch({
        type: 'speakerModel/updateState',
        payload: {
          delModalShow: true,
          deletingItem: id
        }
      })

    };
    const clearSelectedId = () => {
      dispatch({
        type: 'speakerModel/updateState',
        payload: {
          selectedSpeakerId: ''
        }
      });
    };

    const deleteModalShow = (id) => {
      if (id !== -1 && id !== false) {
        dispatch({
          type: 'speakerModel/deleteSpeaker',
          payload: {id}
        })
      }
      dispatch({
        type: 'speakerModel/updateState',
        payload: {
          delModalShow: !delModalShow
        }
      });
    };

    const yanaVideo = () => {

            dispatch({
              type: 'app/getSpeakers',
              payload: {
                page: page + 1,
                size: size
              }

          })
      };


    return (
      <div className="speaker-section">
        <header className="border-bottom">
          <Container>
            <Row className="pt-2 pt-md-5">
              <Col md={2}>
                <h3 className="header-title">Spikerlar</h3>
              </Col>
              <Col md={2} className="offset-8">
                <button className="btn btn-success px-4 py-2 mt-5 float-right" onClick={openModal}>Qo'shish</button>
              </Col>
            </Row>
          </Container>
        </header>
        <Container className="mt-3">

          {loading ?
            <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px', marginBottom: '150px'}}>
              <Loader
                type="Triangle"
                color="#ED2939"
                height="100"
                width="100"
              />
            </div> : ''}

          {speakers !== undefined ? speakers.length !== 0 ?
            <div>
              <Row className="mb-5">
                {speakers.map((item, i) =>
                  <Col key={i} md="4" className="mb-4">
                    <Card className="card-article speaker">
                      <CardImg src={item.photoUrl}/>
                      <CardBody>
                        <a href="#" className="mt-3">
                          <h4 className="font-weight-bold">{item.fullName}</h4>
                        </a>
                        <div className="d-flex mt-4">
                          <div className="w-75">
                            <h5 className="m-0">{item.company}</h5>
                            <h6 className="m-0 muted">{item.positionUz}</h6>
                          </div>
                        </div>
                        <div className="d-flex mt-4">
                          <div className="w-75">
                            <p className="m-0 small text-muted">Telefon raqam</p>
                            <h6 className="m-0">{item.phoneNumber}</h6>
                          </div>
                          <div className="w-25">
                            <p className="m-0 small text-muted">Tadbirlar soni</p><h6 className="m-0 ">{item.countEvent}</h6>
                          </div>
                        </div>
                        <div className="speaker-btn">
                          <button className="btn-edit-46"><span className="icon icon-edit"
                                                                onClick={() => setSelectedSpeakerId(item)}/></button>
                          <button className="mt-3 d-block btn-delete-46"><span className="icon icon-delete"
                                                                               onClick={() => deleteSpeaker(item.id)}/>
                          </button>
                        </div>
                      </CardBody>
                    </Card>
                  </Col>)}
              </Row>
              <Row className="mt-4 mt-md-5">
                 <Col md={{size: 2, offset: '5'}} className="col-6 mb-5">
                  <Button onClick={yanaVideo} color="danger" className="btn-block">
                    Yana yuklash
                  </Button>
                </Col>
              </Row>
            </div>
            : (
              <div>
                <h3 className="font-weight-bolder text-center margin-70">Hozirda spikerlar mavjud emas</h3>
                <Card onClick={openModal} className="client-add-btn">
                  <CardBody>
                    <p className="text-center mb-0"><span className="icon icon-user-add " onClick={save}/> Yangi spiker
                      qo'shish</p>
                  </CardBody>
                </Card>
              </div>
            ) : ''
          }
        </Container>

        <SpeakerModal openModal={openModal} save={save}
                      currentItem={currentItem} photoId={photoId}
                      logosId={logosId} logosSpeaker={logosSpeaker}
                      uploadAvatarSpeaker={uploadAvatarSpeaker} positions={positions}
                      isModalShow={isModalShow} isLoading={isLoading}/>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
          <ModalFooter>
            <Button className="btn-sm" color="primary"
                    onClick={() => deleteModalShow(speakerModel.deletingItem)}>Ha</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
          </ModalFooter>
        </Modal>

      </div>
    );
  }
}

Speaker.propTypes = {};

export default Speaker;
