import React, {Component} from 'react';
import {Spinner} from 'reactstrap';
import {connect} from "dva";
import {Link} from "react-router-dom";
import {firebaseAuth} from 'utils/firebase';
import firebase from 'firebase/app';
import MaskedInput from 'react-text-mask'
import router from "umi/router";
import {toast} from 'react-toastify';

@connect(({auth}) => ({auth}))
class Login extends Component {
  recaptchaVerifier = null;

  componentDidMount() {
    const {dispatch, auth} = this.props;
    dispatch({
      type: 'auth/updateState',
      payload: {
        phoneNumber: '',
        isVerifyProcess: false,
        hasRegistered: false,
        checkPhoneProcess: true,
      }
    });

    let cont = document.getElementById('reCaptcha');

    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(cont, {
      'size': 'invisible',
      'callback': (res) => {
      },
      'expired-callback': () => {
      },
      'error-callback': () => {
        dispatch(
          {
            type: 'app/updateState',
            payload: {
              isNotify: true,
              isLoading: false,
              toasterProps: {
                message: 'Xatolik yuz berdi qaytadan urunib ko\'ring'
              }
            },
          }
        )
      }
    });

    this.recaptchaVerifier.render();

    dispatch({
      type: 'auth/updateState',
      payload: {
        reCaptcha: this.recaptchaVerifier
      }
    });

  }

  componentWillMount() {
    const {dispatch, auth} = this.props;
    const {currentUser} = auth;
    if (localStorage.getItem('token')) {
      dispatch({
        type: 'auth/userMe'
      }).then(res => {
        if (res.success) {
          if (res.object.roles.filter(i => i.name === 'ROLE_ADMIN').length > 0) {
            router.push('/cabinet');
          }

          if (res.object.roles.length === 1 && res.object.roles.filter(i => i.name === "ROLE_USER")) {
            router.push('/user/' + res.object.id)
          }
        } else {
          localStorage.removeItem('token')
        }
      });
    }
  }

  render() {
    const {auth, dispatch} = this.props;
    const {
      checkPhoneProcess, hasRegistered, phoneNumber, password, isLoading,
      reCaptcha, intervalId, isVerifyProcess, confirmationResult,
      code, firstName, lastName, confirmPassword, codeTime, hasPassword, prePassword, buttonVisible
    } = auth;
    const login = (e) => {
      e.preventDefault();
      if ((!hasRegistered && confirmPassword === password)) {
        sendCode();
      } else if (hasRegistered && hasPassword) {
        dispatch({
          type: 'auth/checkUser',
        }).then(res => {
          if (res.success) {
            sendCode();
          }
        })
      } else if (hasRegistered && !hasPassword && prePassword === password) {
        sendCode();
      } else {
        toast.error("Parollar bir xil emas!");
      }
    };
    const updatePhone = (e) => {
      dispatch({
        type: 'auth/updateState',
        payload: {
          [e.target.name]: e.target.value,
          isLoading: false
        }
      })
      dispatch({
        type: 'auth/checkState'
      })
    }
    const updateState = (e) => {
      dispatch({
        type: 'auth/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    }
    const sendCode = () => {
      dispatch({
        type: 'auth/updateState',
        payload: {
          codeTime: 60,
          isLoading: true
        }
      });
      if (intervalId !== '')
        clearInterval(intervalId);
      firebaseAuth.signInWithPhoneNumber(phoneNumber, reCaptcha)
        .then((result) => {
          let timeCount = 60;
          dispatch({
            type: 'auth/updateState',
            payload: {
              phoneNumber,
              codeTime: timeCount,
              isVerifyProcess: true,
              confirmationResult: result,
              isLoading: false
            }
          });


          var intervalId = setInterval(function () {
            timeCount--;
            dispatch({
              type: 'auth/updateState',
              payload: {
                codeTime: timeCount >= 0 ? timeCount : 0
              }
            })
          }, 1000);
          dispatch({
            type: 'auth/updateState',
            payload: {
              intervalId
            }
          });

        })
        .catch((error) => {
          dispatch({
              type: 'auth/updateState',
              payload: {
                isLoading: false,
              },
            }
          )
        })
        .finally(() => {
          dispatch({
            type: 'auth/updateState',
            payload: {
              isLoading: false
            },
          });
        });
    }
    const checkVerificationCode = (e) => {
      e.preventDefault();
      confirmationResult.confirm(code)
        .then((res) => {
          dispatch({
            type: 'auth/sign',
            payload: {
              password,
              firstName,
              lastName,
              hasRegistered,
              phoneNumber: phoneNumber,
              hasPassword,
              prePassword
            }
          })
        }).catch((error) => {
        dispatch({
            type: 'auth/updateState',
            payload: {
              isNotify: true,
              isLoading: false,
              toasterProps: {
                message: "Tasdiqlash kodi noto'g'ri",
              }
            },
          }
        )
      });
    };
    return (
      <div id="login_img">
        <main>
          <section id="login_page" className="login_page">
            <div className="container">
              <div className="row">
                <div className="col-md-5 offset-md-7 offset-0">
                  <div className="login_block">
                    <div className="img_block text-center pb-5">
                      <Link to="/">
                        <img src="/assets/images/loginLogo.png"/>
                      </Link>
                    </div>
                    {!isVerifyProcess ?
                      <form onSubmit={login}>
                        <div className="wrap">
                          <label htmlFor="login">Telefon raqam</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="icon icon-user"/>
                            </div>
                            {/*<AvField name="phoneNumber"/>*/}
                            <MaskedInput
                              placeholder="+998"
                              className="form-control"
                              id="login"
                              name="phoneNumber"
                              onChange={updatePhone}
                              mask={["+", "9", "9", "8", /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                            />
                          </div>
                        </div>
                        {!checkPhoneProcess && hasRegistered && hasPassword ?
                          <div className="wrap mt-3 animated-label">
                            <div className="input-group">
                              <div className="input-group-prepend">
                                <span className="icon icon-key"/>
                              </div>
                              {/*<AvField name="password" type="password"/>*/}
                              <input type="password"
                                     id="password"
                                     name="password"
                                     className="form-control"
                                     onChange={updateState} required/>
                              <label htmlFor="password">Parol</label>
                            </div>
                          </div> : ''}
                        {!checkPhoneProcess && hasRegistered && !hasPassword ?
                          <div>
                            <div className="wrap mt-3 animated-label">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span className="icon icon-key"/>
                                </div>
                                {/*<AvField name="password" type="password"/>*/}
                                <input type="password"
                                       id="password"
                                       name="password"
                                       className="form-control"
                                       onChange={updateState} required/>
                                <label htmlFor="password">Parol</label>
                              </div>
                            </div>
                            <div className="wrap mt-3 animated-label">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span className="icon icon-key"/>
                                </div>
                                {/*<AvField name="password" type="password"/>*/}
                                <input type="password"
                                       id="pre-password"
                                       name="prePassword"
                                       className="form-control"
                                       onChange={updateState} required/>
                                <label htmlFor="password">Parolni takrorlang</label>
                              </div>
                            </div>
                          </div> : ''
                        }
                        {!checkPhoneProcess && !hasRegistered ?
                          <div>
                            <div className="wrap mt-3 animated-label">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span className="icon icon-user"/>
                                </div>
                                <input type="text"
                                       onChange={updateState}
                                       id="name" name="firstName"
                                       className="form-control" required/>
                                <label htmlFor="name">Ism</label>
                              </div>
                            </div>
                            <div className="wrap mt-3 animated-label">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span className="icon icon-user"/>
                                </div>
                                <input type="text"
                                       onChange={updateState}
                                       name="lastName" id="company"
                                       className="form-control" required/>
                                <label htmlFor="company">Familiya</label>
                              </div>
                            </div>
                            <div className="wrap mt-3 animated-label">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span className="icon icon-key"/>
                                </div>
                                <input type="password" onChange={updateState}
                                       name="password" id="password"
                                       className="form-control" required/>
                                <label htmlFor="password">Parol</label>
                              </div>
                            </div>
                            <div className="wrap mt-3 animated-label">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span className="icon icon-key"/>
                                </div>
                                <input type="password"
                                       onChange={updateState}
                                       name="confirmPassword" id="confirmPassword"
                                       className="form-control" required/>
                                <label htmlFor="confirmPassword">Takroriy parol</label>
                              </div>
                            </div>
                          </div>

                          : ''}
                        {isLoading ?
                          <div style={{margin: "35px auto", width: "35px"}}><Spinner/></div> :
                          !checkPhoneProcess && buttonVisible ? <div className="col-md-12 entrance-login">
                            <button type="submit"
                                    className="btn btn-danger font-weight-bold mt-5 py-2 px-5">Kirish
                            </button>
                          </div> : ""
                        }
                      </form> :
                      <form onSubmit={checkVerificationCode}>
                        <div className="wrap mt-3 animated-label">
                          <div className="input-group">
                            <div className="input-group-prepend"
                                 style={{fontSize: "25px", marginTop: "4px", width: "33px"}}>
                              {codeTime}
                            </div>
                            <input type="text" id="code"
                                   name="code"
                                   onChange={updateState}
                                   className="form-control" value={code} required/>
                            <label htmlFor="code">Tasdiqlash kodi</label>
                          </div>
                        </div>
                        {codeTime === 0 ?
                          <button type="button" onClick={sendCode}
                                  className="btn btn-danger font-weight-bold px-5 py-2 mt-5">Qayta
                            yuborish</button> : ''}
                        {isLoading ?
                          <div style={{margin: "35px auto", width: "35px"}}><Spinner/></div> :
                          codeTime !== 0 ?
                            <button type="submit" className="btn btn-danger px-5 py-2 mt-5">Login</button> : ""}

                      </form>}
                    {/*</AvForm>*/}
                    <Link to="https://pdp.uz/home" className="text-center login-footer">© 2019 | “Personal Development
                      Process”
                      MCHJ</Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div id="reCaptcha" render="explicit" style={{display: 'none'}}/>
        </main>
      </div>
    );
  }
}

Login.propTypes = {};

export default Login;

