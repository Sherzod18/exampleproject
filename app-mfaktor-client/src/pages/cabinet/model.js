import api from 'api';
import {toast} from "react-toastify";

const {getDashboard, addTransfer} = api;
export default ({
  namespace: 'dashboardModel',
  state: {
    dashboard: {},
    activeBtnProfit: 'week',
    profitValue: '',
    activeBtnIncome: 'week',
    incomeValue: '',
    incomeCount: '',
    activeBtnExpense: 'week',
    expenseValue: '',
    activeBtnWaiting: 'week',
    waitingValue: '',
    currentAllBalance: '',
    balanceByPayTypes: [],
    balanceByCashTypes: [],
    cashBox: 0,
    allOutcome: 0,
    allIncome: 0,
    modalOpen: false,
    date: new Date(),
    fromUsd: false,
    toUsd: false,
    notUsd:true
  },
  subscriptions: {},
  effects: {
    * getDashboard({payload}, {call, put, select}) {
      const res = yield call(getDashboard);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            dashboard: res.object,
            profitValue: res.object.weeklyProfit,
            waitingValue: res.object.weeklyWaitingPayment,
            incomeValue: res.object.weeklyIncome,
            incomeCount: res.object.weeklyIncomeCount,
            expenseValue: res.object.weeklyExpense,
            currentAllBalance: res.object.currentAllBalance,
            balanceByPayTypes: res.object.balanceByPayTypes,
            balanceByCashTypes: res.object.balanceByCashTypes
          }
        })
      }
    },
    * addTransfer({payload}, {call, put, select}) {
      const res = yield call(addTransfer, payload);
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'getDashboard',
        });
        yield put({
          type: 'updateState',
          payload: {
            isLoading: false,
            modalOpen: false
          }
        })
      } else {
        toast.error(res.message);
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }

})
