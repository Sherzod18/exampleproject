import React, {Component} from 'react';
import {Button, ButtonGroup, Card, CardBody, Col, Container, Modal, ModalBody, ModalHeader, Row} from 'reactstrap'
import {connect} from "react-redux";
import router from "umi/router";
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import DatePicker from "react-datepicker/es";
import CurrencyInput from "react-currency-input";
import {toast} from "react-toastify";

@connect(({dashboardModel, app}) => ({dashboardModel, app}))
class Cabinet extends Component {
  componentDidMount() {
    const {dispatch, dashboardModel, app} = this.props;
    dispatch({
      type: 'dashboardModel/getDashboard'
    });
    dispatch({
      type: 'app/getCashTypes'
    });

    if (app.currentUser.roles.filter(i =>
      i.name === 'ROLE_ADMIN'
    ).length === 0) {
      router.push("/event")
    }
  }

  render() {
    const {dispatch, dashboardModel, app} = this.props;
    const {
      dashboard, activeBtnProfit, profitValue, activeBtnIncome, incomeValue, activeBtnExpense,
      expenseValue, toUsd, fromUsd, notUsd, activeBtnWaiting, modalOpen, date, isLoading, balanceByCashTypes, waitingValue, incomeCount, cashBox, allOutcome, allIncome, balanceByPayTypes, currentAllBalance
    } = dashboardModel;
    const {cashTypes} = app;

    let allShot = 0;
    let income = 0;
    let outcome = 0;

    const toUsdTo = () => {
      dispatch({
        type: 'dashboardModel/updateState',
        payload: {
          toUsd: true,
          fromUsd: false,
          notUsd: false
        }
      });
    };
    const fromUsdFrom = () => {
      dispatch({
        type: 'dashboardModel/updateState',
        payload: {
          toUsd: false,
          fromUsd: true,
          notUsd: false
        }
      });
    };
    const notUsdNot = () => {
      dispatch({
        type: 'dashboardModel/updateState',
        payload: {
          toUsd: false,
          fromUsd: false,
          notUsd: true
        }
      });
    };

    const changeProfit = (type) => {
      switch (type) {
        case "day":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              profitValue: dashboard.dailyProfit,
              activeBtnProfit: type
            }
          });
          break;
        case "week":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              profitValue: dashboard.weeklyProfit,
              activeBtnProfit: type
            }
          });
          break;
        case "month":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              profitValue: dashboard.monthlyProfit,
              activeBtnProfit: type
            }
          })
      }
    };

    const changeIncome = (type) => {
      switch (type) {
        case "day":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              incomeValue: dashboard.dailyIncome,
              incomeCount: dashboard.dailyIncomeCount,
              activeBtnIncome: type
            }
          });
          break;
        case "week":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              incomeValue: dashboard.weeklyIncome,
              incomeCount: dashboard.weeklyIncomeCount,
              activeBtnIncome: type
            }
          });
          break;
        case "month":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              incomeValue: dashboard.monthlyIncome,
              incomeCount: dashboard.monthlyIncomeCount,
              activeBtnIncome: type
            }
          })
      }
    };

    const getBalanceCashType = (type) => {
      let checked = false;
      if (type === 'all') {
        allShot = dashboard.balance;
        income = dashboard.allIncome;
        outcome = dashboard.allOutcome;
        // allShot = balanceByCashTypes.reduce((prev, cur) => prev + cur.balance, 0);
        // income = balanceByCashTypes.reduce((prev, cur) => prev + cur.allIncome, 0);
        // outcome = balanceByCashTypes.reduce((prev, cur) => prev + cur.allOutcome, 0);
        checked = true;
      } else {
        balanceByCashTypes.forEach(i => {
            if (i.name === type) {
              allShot = i.balance;
              income = i.allIncome;
              outcome = i.allOutcome;
              checked = true;
            }
          }
        )
      }
      if (checked) {
        dispatch({
          type: 'dashboardModel/updateState',
          payload: {
            cashBox: allShot,
            allOutcome: outcome,
            allIncome: income
          }
        })
      } else {
        dispatch({
          type: 'dashboardModel/updateState',
          payload: {
            cashBox: 0,
            allOutcome: 0,
            allIncome: 0
          }
        })
      }
    };

    const changeCash = (type) => {
      getBalanceCashType(type);
      switch (type) {
        case "all":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              cashBox: allShot,
              activeBtnIncome: type
            }
          });
          break;
        case "shot":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              cashBox: allShot,
              activeBtnIncome: type
            }
          });
          break;
        case "cash":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              cashBox: allShot,
              activeBtnIncome: type
            }
          });
          break;
        case "payme":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              cashBox: allShot,
              activeBtnIncome: type
            }
          });
          break;
        case "card":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              cashBox: allShot,
              activeBtnIncome: type
            }
          });
          break;
        case "usd":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              cashBox: allShot,
              activeBtnIncome: type
            }
          });
          break;
      }
    };

    const changeExpense = (type) => {
      switch (type) {
        case "day":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              expenseValue: dashboard.dailyExpense,
              activeBtnExpense: type
            }
          });
          break;
        case "week":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              expenseValue: dashboard.weeklyExpense,
              activeBtnExpense: type
            }
          });
          break;
        case "month":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              expenseValue: dashboard.monthlyExpense,
              activeBtnExpense: type
            }
          })
      }
    };

    const changeWaiting = (type) => {
      switch (type) {
        case "day":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              waitingValue: dashboard.dailyWaitingPayment,
              activeBtnWaiting: type
            }
          });
          break;
        case "week":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              waitingValue: dashboard.weeklyWaitingPayment,
              activeBtnWaiting: type
            }
          });
          break;
        case "month":
          dispatch({
            type: 'dashboardModel/updateState',
            payload: {
              waitingValue: dashboard.monthlyWaitingPayment,
              activeBtnWaiting: type
            }
          })
      }
    };

    const openModal = () => {
      dispatch({
        type: 'dashboardModel/updateState',
        payload: {
          modalOpen: !modalOpen,
          date: new Date()
        }
      })
    };

    const changeDateTime = (date) => {
      dispatch({
        type: 'dashboardModel/updateState',
        payload: {
          date
        }
      })
    };

    const save = (e, v) => {
      let isChecked = false;
      dispatch({
        type: 'dashboardModel/updateState',
        payload: {
          isLoading: true
        }
      });
      v.sum = v.sum.replace(/ /g, '');
      if (v.usdSum) {
        v.usdSum = v.usdSum.replace(/ /g, '');
      }
      if (v.sum > 0) {
        if (fromUsd || toUsd) {
          if (v.usdSum > 0) {
            isChecked = true;
          }
        } else {
          isChecked = true;
        }
        if (isChecked) {
          dispatch({
            type: 'dashboardModel/addTransfer',
            payload: {
              ...v,
              time: date,
              toUsd,
              fromUsd
            }
          })
        } else {
          toast.error("Pul miqdorini kiritish majburiy");
        }
      } else {
        toast.error("Pul miqdorini kiritish majburiy");
        dispatch({
          type: 'dashboardModel/updateState',
          payload: {
            isLoading: false
          }
        });
      }
    };


    return (
      <div className="mb-3">
        <main>
          <div className="cabinet_page">
            <Container>
              <p className="mt-1">Dashboard</p>
              <Row>
                <Col md="4">
                  <Card className="border-0 primary-bg">
                    <CardBody className="px-4 pt-4 pb-1">
                      <Row>
                        <Col md="6" className="pt-1">
                          <p>Umumiy Foyda</p></Col>
                        <Col md="6">
                          <ButtonGroup className="sort" size="sm">
                            <Button onClick={() => changeProfit("day")}
                                    className={activeBtnProfit === 'day' ? 'active-btn' : ''}>Kun</Button>
                            <Button onClick={() => changeProfit("week")}
                                    className={activeBtnProfit === 'week' ? 'active-btn' : ''}>Hafta</Button>
                            <Button onClick={() => changeProfit("month")}
                                    className={activeBtnProfit === 'month' ? 'active-btn' : ''}>Oy</Button>
                          </ButtonGroup>
                        </Col>
                      </Row>
                      <Row className="mt-4">
                        <Col md="8" className="pr-0">
                          <h4 className={profitValue > 0 ? "text-success" : "text-danger"}>
                            <span>UZS </span>{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(profitValue))}
                          </h4>
                        </Col>
                        <Col md="4" className="pl-2">
                          <p className="little">Umumiy foyda miqdori</p>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>

                <Col md="4" className="pl-0">
                  <Card className="border-0 danger-bg">
                    <CardBody className="border-0 primary-bg pb-1 px-4 pt-4 ">
                      <Row>
                        <Col md="6" className="pt-1">
                          <p>Kutilayotgan to'lovlar</p></Col>
                        <Col md="6">
                          <ButtonGroup className="sort" size="sm">
                            <Button onClick={() => changeWaiting("day")}
                                    className={activeBtnWaiting === 'day' ? 'active-btn' : ''}>Kun</Button>
                            <Button onClick={() => changeWaiting("week")}
                                    className={activeBtnWaiting === 'week' ? 'active-btn' : ''}>Hafta</Button>
                            <Button onClick={() => changeWaiting("month")}
                                    className={activeBtnWaiting === 'month' ? 'active-btn' : ''}>Oy</Button>
                          </ButtonGroup>
                        </Col>
                      </Row>
                      <Row className="mt-4">
                        <Col md="8" className="pr-0">
                          <h4 className="warning"><span>UZS </span>
                            {(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(waitingValue))}
                          </h4>
                        </Col>
                        <Col md="4" className="pl-2 ">
                          <p className="little">Umumiy to'lovlar miqdori</p>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="4">
                  <Row>
                    <Col md="6" className="pl-0">
                      <Card className="border-0 primary-bg-small">
                        <CardBody className="pb-1 px-4 pt-4 ">
                          <p className="prim-info">Mijozlar <br/>soni</p>
                          <Row className="mt-3">
                            <Col md="4" className="px-0">
                              <span className="positive pl-2">{dashboard.countClients}</span>
                            </Col>
                            <Col md="8" className="pr-0">
                              <span
                                className="positive-small pl-2">{dashboard.countNewUserWeekly > 0 ? "+" + dashboard.countNewUserWeekly : dashboard.countNewUserWeekly}</span>
                              <p className="little">shu haftada</p>
                            </Col>
                          </Row>
                        </CardBody>
                      </Card>
                    </Col>
                    <Col md="6" className="pl-0">
                      <Card className="border-0 primary-bg-small">
                        <CardBody className="pb-1 px-4 pt-4 ">
                          <p className="prim-info">Tadbirlar <br/>soni</p>
                          <Row className="mt-3">
                            <Col md="4" className="px-0">
                              <span className="positive pl-2">{dashboard.countEvents}</span>
                            </Col>
                            <Col md="8" className="pr-0">
                              <span
                                className="positive-small pl-2">{dashboard.countEventMonthly > 0 ? "+" + dashboard.countEventMonthly : dashboard.countEventMonthly}</span>
                              <p className="little">shu oyda</p>
                            </Col>
                          </Row>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row className="mt-3">
                <Col md="4">
                  <Card className="border-0 info-bg">
                    <CardBody className="px-4 pt-4 pb-1">
                      <Row>
                        <Col md="6" className="pt-1">
                          <p>Umumiy tushum </p></Col>
                        <Col md="6">
                          <ButtonGroup className="sort" size="sm">
                            <Button onClick={() => changeIncome("day")}
                                    className={activeBtnIncome === 'day' ? 'active-btn' : ''}>Kun</Button>
                            <Button onClick={() => changeIncome("week")}
                                    className={activeBtnIncome === 'week' ? 'active-btn' : ''}>Hafta</Button>
                            <Button onClick={() => changeIncome("month")}
                                    className={activeBtnIncome === 'month' ? 'active-btn' : ''}>Oy</Button>
                          </ButtonGroup>
                        </Col>
                      </Row>
                      <h3 className="info">{incomeCount} <span className="pl-2 info-count">Tushumlar soni</span></h3>
                      <Row className="mt-4">
                        <Col md="8" className="pr-0">
                          <h4 className="info"><span>UZS </span>
                            {(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(incomeValue))}
                          </h4>
                        </Col>
                        <Col md="4" className="pl-2">
                          <p className="little">Umumiy pul <br/>miqdori</p>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
                <Col md="4" className="pl-0">
                  <Card className="border-0 info-bg">
                    <CardBody className="pl-4 pt-4 pb-1">
                      <Row>
                        <Col md="10" className="mb-2 offset-1">
                          <ButtonGroup className="sort" size="md">
                            <Button onClick={() => changeCash("all")}
                                    className={activeBtnIncome === 'all' ? 'active-btn' : ''}>Jami</Button>
                            <Button onClick={() => changeCash("shot")}
                                    className={activeBtnIncome === 'shot' ? 'active-btn' : ''}>Shot</Button>
                            <Button onClick={() => changeCash("cash")}
                                    className={activeBtnIncome === 'cash' ? 'active-btn' : ''}>Naqd</Button>
                            <Button onClick={() => changeCash("payme")}
                                    className={activeBtnIncome === 'payme' ? 'active-btn' : ''}>H.Payme</Button>
                            <Button onClick={() => changeCash("card")}
                                    className={activeBtnIncome === 'card' ? 'active-btn' : ''}>C.Card</Button>
                            <Button onClick={() => changeCash("usd")}
                                    className={activeBtnIncome === 'usd' ? 'active-btn' : ''}>Dollar</Button>
                          </ButtonGroup>
                        </Col>
                      </Row>
                      <h5 className="info"><span
                        className="pl-2 info-count">Jami tushum:</span>{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(allIncome))}
                      </h5>
                      <h5 className="info"><span
                        className="pl-2 info-count">Jami xarajat:</span>{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(allOutcome))}
                      </h5>
                      <Row className="mt-4">
                        <Col md="7" className="pr-0">
                          <h4 className="info"><span>UZS </span>
                            {(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(cashBox))}
                          </h4>
                        </Col>
                        <Col md="3" className="pl-2">
                          <p className="little">Umumiy pul <br/>miqdori</p>
                        </Col>
                        <Col md="2">
                          <p className="transfer-money" onClick={openModal}>
                            <img src="/assets/images/icon/Money-Transfer-128.png" className="img-fluid" alt="55"/>
                          </p>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
              <Row className="mt-3">
                <Col md="4">
                  <Card className="border-0 danger-bg">
                    <CardBody className=" pb-1">
                      <Row>
                        <Col md="6" className="pt-1">
                          <p>Xarajatlar</p></Col>
                        <Col md="6">
                          <ButtonGroup className="sort" size="sm">
                            <Button onClick={() => changeExpense("day")}
                                    className={activeBtnExpense === 'day' ? 'active-btn' : ''}>Kun</Button>
                            <Button onClick={() => changeExpense("week")}
                                    className={activeBtnExpense === 'week' ? 'active-btn' : ''}>Hafta</Button>
                            <Button onClick={() => changeExpense("month")}
                                    className={activeBtnExpense === 'month' ? 'active-btn' : ''}>Oy</Button>
                          </ButtonGroup>
                        </Col>
                      </Row>
                      <Row className="mt-4">
                        <Col md="8" className="pr-0">
                          <h4 className="danger"><span>UZS </span>
                            {(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(expenseValue))}
                          </h4>
                        </Col>
                        <Col md="4" className="pl-2">
                          <p className="little">Umumiy xarajat miqdori</p>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Container>
          </div>
        </main>

        <Modal isOpen={modalOpen} toggle={openModal} className="event-client-modal">
          <ModalHeader toggle={openModal} className="border-0">
          </ModalHeader>
          <ModalBody className="pl-5 pr-5">
            <AvForm onValidSubmit={save}>
              <h3>Transfer qilish</h3>
              <div className="col-md-12"><input type="radio" name="usd" onChange={toUsdTo}
                                                className=" border-0 p-0 mb-2"/>
                Dollarga
              </div>
              <div className="col-md-12"><input type="radio" name="usd" onChange={fromUsdFrom}
                                                className=" border-0 p-0 mb-2"/> Dollardan
              </div>
              <div className="col-md-12"><input type="radio" name="usd" checked={notUsd} onChange={notUsdNot}
                                                className=" border-0 p-0 mb-2"/> Boshqa
              </div>
              {notUsd ? <div className="row mt-3">
                <div className="col-md-4 offset-1">
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput type="select" name="fromCashId" required>
                      <option>Qaysi kassadan?</option>
                      {cashTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                    </AvInput>
                    <label>Qaysi kassadan?</label>
                  </AvGroup>
                </div>
                <div className="col-md-4 offset-2">
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput type="select" name="toCashId" required>
                      <option>Qaysi kassaga?</option>
                      {cashTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                    </AvInput>
                    <label>Qaysi kassaga?</label>
                  </AvGroup>
                </div>
              </div> : ''}
              <div className="row mt-3">
                <div className="col-md-4 offset-1">
                  <AvGroup>
                    <AvInput name="sum" tag={CurrencyInput} precision="0" thousandSeparator=" "
                             required/>
                    <label>Ko'chiriladigan pul miqdori</label>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  </AvGroup>
                </div>
                <div className="col-md-5 offset-2 pl-0">
                  <div className="form-group">
                    <AvGroup>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      <DatePicker
                        selected={date}
                        showTimeSelect
                        timeFormat="HH:mm"
                        className="form-control"
                        onChange={changeDateTime}
                        timeIntervals={30}
                        dateFormat="dd.MM.yyyy  HH:mm"
                        timeCaption="time"
                      />
                      <label className="label-date">Xarajat qilingan vaqti</label>
                    </AvGroup>
                  </div>
                </div>
              </div>
              <div className="row mt-3">
                {fromUsd || toUsd ?
                  <div className="col-md-4 offset-1">
                    <AvGroup>
                      <AvInput name="usdSum" tag={CurrencyInput} precision="0" thousandSeparator=" "
                               required/>
                      <label>Dollar kursi</label>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    </AvGroup>
                  </div>
                  :
                  <div className="col-md-4 offset-1">
                    <AvGroup>
                      <AvInput name="details"/>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      <label>Qo'shimcha ma'lumot</label>
                    </AvGroup>
                  </div>}
                {fromUsd || toUsd ? <div className="col-md-4 offset-2">
                  <AvGroup>
                    <AvInput name="details"/>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <label>Qo'shimcha ma'lumot</label>
                  </AvGroup>
                </div> : ''}
              </div>
              <div className="row mt-3">
                <div className="col-md-5 offset-8">
                  <button type="button" onClick={openModal} className="btn-outline-secondary btn mx-2">Bekor qilish
                  </button>
                  <Button type="submit" bsStyle="success"
                          loading={isLoading}>Saqlash </Button>
                </div>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>

      </div>
    );
  }
}

Cabinet.propTypes = {};

export default Cabinet;
