import React, {Component} from 'react';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table,
  Pagination, PaginationItem, PaginationLink, InputGroup, InputGroupAddon, Input, Label, FormGroup
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvInput, AvFeedback} from 'availity-reactstrap-validation';
import {connect} from 'react-redux';
import {Editor} from '@tinymce/tinymce-react';
import FooterAdmin from "../../components/Layout/FooterAdmin";

@connect(({post}) => ({post}))
class Post extends Component {
  componentDidMount() {
    const {dispatch, post} = this.props;
    dispatch({
      type: 'post/getCategories'
    });
    dispatch({
      type: 'post/getArticles'
    })
  }

  render() {
    const {dispatch, post} = this.props;
    const {isModalShow, delModalShow, list, current, modalType, photoId, categories, description, paginations, page, deleteId} = post;
    const modalShow = () => {
      dispatch({
        type: 'post/updateState',
        payload: {
          modalType: 'new',
          photoId: '',
          isModalShow: !isModalShow
        }
      })
    }
    const handleSubmit = (e, er, v) => {
      if (modalType === 'edit') {
        v = {...v, id: current.id}
      }
      dispatch({
        type: 'post/save',
        payload: {
          ...v
        }
      })
    };
    const uploadAvatar = (e) => {
      dispatch({
        type: 'post/uploadAvatar',
        payload: {
          file: e.target.files,
          fileUpload: true,
          type: 'ARTICLE_BANNER'
        }
      })
    };
    const onChangeEditor = (e) => {
      dispatch({
        type: 'post/updateState',
        payload: {
          [e.target.id]: e.target.getContent()
        }
      })
    };
    const deleteArticle = () => {
      dispatch({
        type: 'post/deleteArticle',
        payload: {
          id:deleteId
        }
      });
      deleteModalShow(-1);
    }
    const editArticle = (item) => {
      dispatch({
        type: 'post/updateState',
        payload: {
          modalType: 'edit',
          isModalShow: !isModalShow,
          current: item,
          description: item.description,
          photoId: item.photo.id
        }
      })
    }
    const getArticles = (e, page) => {
      e.preventDefault();
      dispatch({
        type: 'post/getArticles',
        payload: {
          page
        }
      })
    }
    const deleteModalShow = (id) => {
      if (id !== -1){
        dispatch({
          type:'post/updateState',
          payload:{
            deleteId: id
          }
        });
      }
      dispatch({
        type:'post/updateState',
        payload:{
          delModalShow: !delModalShow
        }
      });
    };
    return (
      <div>
        <main>
          <section className="client_page post-page">
            <div className="container">
              <h3 className="my-3">Maqolalar</h3>
              <div className="row mt-3">
                <div className="col-md-12">
                  <div className="card bg-transparent border-0">
                    <div className="card-body">
                      <div className="row">
                        <div className="col-md-1">
                        </div>
                        <div className="col-md-6">
                          <h4 className="text-center">Sarlavha</h4>
                        </div>
                        <div className="col-md-3 text-center">
                          <h4>Kategoriya</h4>
                        </div>
                        {/*<div className="col-md-3 text-center">
                          <h4>Matn</h4>
                        </div>*/}
                        <div className="col-md-2">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {list.map(item =>
                <div className="row mb-2 mt-1">
                  <div className="col-md-12">
                    <div className="client-list-item article-img pt-2 ">
                      <Row className="mt-2">
                        <Col md="1" className="text-center">
                          {item.photo === '' ?
                            <div className="client-avatar"></div> :
                            <img src={'/api/file/' + item.photo.id} alt="userlogo" className="mr-3"/>}
                        </Col>
                        <Col md="6" className="text-center mb-3">
                          <p>{item.title}</p>
                        </Col>
                        <Col md="3" className="text-center">
                          <p>{item.category.nameUz}</p>
                        </Col>
                        {/*<Col md="3" className="text-center">*/}
                        {/*  <p dangerouslySetInnerHTML={{ __html: item.description }}></p>*/}
                        {/*/!*</Col>*!/
                              //O'CHIRILMASIN*/}
                        <Col md="2 ml-auto">
                          <button className="btn btn-edit-28 p-0" onClick={() => editArticle(item)}><span
                            className="icon icon-edit"/></button>
                          <button className="btn btn-delete-28   ml-5 p-0" onClick={() => deleteModalShow(item.id)}>
                            <span className="icon icon-delete"/></button>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </div>)}
              <Pagination aria-label="Page navigation example">
                {paginations.map(i => <PaginationItem active={page === (i - 1)}
                                                      disabled={page === (i - 1) || i === '...'}>
                  <PaginationLink onClick={(e) => getArticles(e, i - 1)}>
                    {i}
                  </PaginationLink>
                </PaginationItem>)}
              </Pagination>
            </div>
          </section>
        </main>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
          <ModalFooter>

            <Button className="btn-sm" color="primary" onClick={deleteArticle}>Ha</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isModalShow} toggle={modalShow} className="custom-modal">
          <ModalHeader toggle={modalShow} className="border-0"></ModalHeader>
          <ModalBody className="px-5">
            <h3 className="font-weight-bolder">Yangi maqola qo'shish</h3>
            <AvForm onSubmit={handleSubmit}>
              <Row>
                <Col md="4">
                  <div className="speaker-image"
                       style={photoId ? {backgroundImage: 'url("/api/file/' + photoId + '")'} : {}}>
                    <label htmlFor="post-image"><img src="/assets/images/icon/upload.svg" alt=""/> Maqola rasmi</label>
                    <input type="file" id="post-image" className="d-none" onChange={(e) => uploadAvatar(e)}/>
                  </div>
                  <h6 className="pt-3">Tavsiya qilingan o'lcham: </h6>
                </Col>
                <Col md="8">
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="title" id="title" type="text" required
                             value={modalType === 'new' ? '' : current.title}/>
                    <label>Sarlavxa</label>
                  </AvGroup>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput type="select" label="" name="categoryId"
                             value={modalType === 'new' ? '' : current.category ? current.category.id : ''} required>
                      <option key="title" value=''>Kategoriyani tanlang</option>
                      {categories.map(c => <option value={c.id}>{c.nameUz}</option>)}
                    </AvInput>
                    <label>Kategoriya</label>
                  </AvGroup>
                  <AvGroup>
                    <Editor
                      id="description"
                      initialValue={modalType === "new" ? "" : description}
                      onChange={onChangeEditor}
                      apiKey="nk94j5woa7gcg2ysyz6c7kqee1ahwmv854m5k61qke02boxk"
                      init={{
                        plugins: 'link code fullscreen',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code fullscreen'
                      }}
                    />
                  </AvGroup>
                </Col>
              </Row>
              <div className="row mt-3">
                <div className="col-md-3 offset-6">
                  <Button type="button" outline block color="secondary" onClick={modalShow}>Bekor qilish</Button>
                </div>
                <div className="col-md-3">
                  <Button type="submit" block color="success">{modalType === 'new' ? 'Saqlash' : 'Tahrirlash'}</Button>
                </div>
              </div>
            </AvForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default Post;
