import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {router} from 'umi';
import {Link} from 'umi'
import FooterAdmin from "../../components/Layout/FooterAdmin";
import {Button, Card, Col, Container, Input, InputGroup, InputGroupAddon, Row} from "reactstrap";
import CardImg from "reactstrap/es/CardImg";
import CardBody from "reactstrap/es/CardBody";
import Loader from "react-loader-spinner";
import {AvForm} from "availity-reactstrap-validation";

@connect(({app}) => ({app}))
class Speaker extends Component {
  componentDidMount() {
    const {dispatch, app} = this.props;
    dispatch({
      type: 'app/getSpeakersPagination',
      payload: {
        page: 0,
        size: 6
      }
    })
  }

  clickHiding = () => {
    let x = document.getElementById("ss");
    if (!x.classList.contains("inputQidirish")) {
      x.classList.add("inputQidirish");
    } else {
      const {dispatch} = this.props;
      dispatch({
        type: 'addVideos/searchByTitle',
        payload: {
          path: x.value
        }
      })
    }

  };

  render() {
    const {dispatch, app} = this.props;
    const {speakers, speakerSize, isLoading, loading} = app;
    const getAgain = (size) => {
      dispatch({
        type: 'app/updateState',
        payload: {
          isLoading: true
        }
      });

      dispatch({
        type: 'app/getSpeakersPagination',
        payload: {
          page: 0,
          size: size + 6
        }
      })
      dispatch({
        type: 'app/updateState',
        payload: {
          speakerSize: size + 3
        }
      })
    }
    return (
      <div>
        <header className="border-bottom">
          <Container>
            <Row className="mt-5 mt-md-5 mb-3 mb-md-0 pt-md-5">
              <Col md={2}>
                <h3 className="header-title">Spikerlar</h3>
              </Col>
              <Col md={3} className="pl-md-0 pl-3 pt-2">
                <InputGroup>
                  <InputGroupAddon id='hr-search-icon' onClick={() => this.clickHiding()}
                                   className="hr-search-icon mr-4"
                                   addonType="prepend"><span
                    className="icon icon-search"/></InputGroupAddon>
                  <AvForm onSubmit={() => this.clickHiding()}>
                    <input id="ss" className="hr-search-input inputKorinishi" placeholder="Spiker ismini kiriting"/>
                  </AvForm>
                </InputGroup>
              </Col>
            </Row>
          </Container>
        </header>
        <Container className="mt-5">

          {loading ?
            <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px', marginBottom: '100px'}}><Loader
              type="Triangle"
              color="#ED2939"
              height="100"
              width="100"
            /></div> : <div>
              {speakers !== undefined ? speakers.length !== 0 ?
                <div><Row>
                  {speakers.map((item, i) => <Col md="4" className="mb-4" key={i}>
                    <Card className="card-article">
                      <CardImg src={item.photoUrl}/>
                      <CardBody>
                        <a href="#">
                          <h4 className="font-weight-bold">{item.fullName}</h4>
                        </a>
                        <div className="d-flex">
                          <div className="w-75">
                            <h6 className="m-0">{item.company}</h6>
                            <p className="m-0">{item.positionUz}</p>
                          </div>
                          <div className="w-25">
                            <p className="m-0 small text-muted">Tadbirlar soni</p>
                            <h6 className="m-0 ">{item.countEvent}</h6>
                          </div>
                        </div>
                      </CardBody>
                    </Card>
                  </Col>)}
                </Row>
                  <Row className="mt-4 mt-md-5">
                    <Col md={{size: 2, offset: '5'}} className="col-12 mb-3">
                      <Button type="button" className="btn btn-block btn-red" onClick={() => getAgain(speakerSize)}>
                        Yana yuklash
                      </Button>
                    </Col>
                    <Col md={{size: 2, offset: '5'}} className="col-12 text-center">
                      {isLoading ?
                        <Row>
                          <Col md="4" className="text-right">
                            <Loader
                              type="ThreeDots"
                              color="#ED2939"
                              // height="100"
                              width="30px"
                            />
                          </Col>
                          <Col md="8" className="text-left" style={{paddingTop: '25px'}}>
                            <span>Yuklanmoqda</span>
                          </Col>
                        </Row> : ''}

                    </Col>
                  </Row>

                </div>

                : (
                  ''
                ) : <div className='col-md-6 offset-md-3 col-12 mb-5 text-center'>
                <h3 className="margin-70">Hozirda spikerlar mavjud emas</h3>
              </div>
              }
            </div>}

        </Container>
      </div>
    );
  }
}

Speaker.propTypes = {};

export default Speaker;
