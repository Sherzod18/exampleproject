import api from 'api'
// import {routerRedux} from 'dva/router'
import {parsePhoneNumber} from 'libphonenumber-js'
import {STORAGE_NAME} from 'utils/constant';
import {toast} from "react-toastify";


const {
  getUser, getByPhone, getPayTypes, getActivities,
  getPositions, getRegions, getAwares, getPayments, addPayment, searchUser
} = api;

function pagination(page, numPages) {
  var res = [];
  var from = 1;
  var to = numPages;
  if (numPages > 10) {
    from = Math.max(page - 2, 1);
    to = Math.max(Math.min(page + 2, numPages), 5);
    if (to > 5) {
      res.push(1);
      if (from > 2) res.push(2);
      if (from > 3) {
        if (from === 4) {
          res.push(3);
        } else {
          res.push("...");
        }
      }
    }
  }
  for (var i = from; i <= to; i++) {
    res.push(i);
  }

  if (numPages > 10) {
    if (to < (numPages - 2)) {
      if (to === 8) {
        res.push(9);
      } else {
        res.push("...");
      }
    }
    if (to < numPages)
      res.push(numPages - 1);
    if (to !== numPages)
      res.push(numPages);
  }
  return res;
}

export default ({
  namespace: "paymentModel",
  state: {
    activeTab: '1',
    modalOpen: false,
    modalType: 'addClients',
    searchUsers: [],
    userId: '',
    inputValue: '',
    seatId: '',
    dateTime: new Date(),
    isSold: false,
    isBooked: false,
    isBalance: false,
    cancelModalOpen: false,
    seatName: '',
    path: '',
    eventIncome: {},
    eventPayments: [],
    participatedAndGuest: {},
    user: {},
    birthDate: new Date(),
    payments: [],
    awares: [],
    positions: [],
    regions: [],
    payTypes: [],
    totalPages: 0,
    loading: true,
    page: 0,
    size: 10,
    isLoading: false,
    phoneNumber: '',
    selectedUser: {},
    paginationList:[],
    notUsd: true,
    isUsd: false,
  },
  subscriptions: {},
  effects: {
    * searchUser({payload}, {call, put, select}) {
      const res = yield call(searchUser, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            searchUsers: res.object
          }
        })
      }
    },
    * addPayment({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoading: true
        }
      });
      const res = yield call(addPayment, payload);
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'getPayments'
        });
        yield put({
          type: 'updateState',
          payload: {
            modalOpen: false,
            modalOpenPay: false,
            inputValue: '',
            searchUser: [],
            modalType: 'addClients',
            isLoading: false
          }
        });
      } else {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            isLoading: false
          }
        });
      }
    },
    * getPayments({payload}, {call, put, select}) {
      let {page, size, totalPages} = yield select(_ => _.paymentModel);
      if (payload) {
        page = payload.page;
        payload = {size, page};
      } else {
        payload = {page, size};
      }
      const res = yield call(getPayments, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            page,
            totalPages: res.object.total,
            payments: res.object.resPayments,
            paginationList: pagination(page, res.object.total),
            loading: false,
          }
        })
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
