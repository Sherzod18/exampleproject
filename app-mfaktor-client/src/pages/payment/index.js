import React, {Component} from 'react';
import {
  Card,
  CardBody,
  Col,
  Input,
  InputGroup,
  Modal,
  ModalBody,
  ModalHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row
} from "reactstrap";
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import {connect} from "react-redux";
import DatePicker from "react-datepicker/es";
import "react-datepicker/dist/react-datepicker.css";
import {toast} from "react-toastify";
import CurrencyInput from 'react-currency-input'
import Button from 'react-bootstrap-button-loader';
import Loader from "react-loader-spinner";
import MaskedInput from "react-text-mask";

@connect(({paymentModel, app}) => ({paymentModel, app}))
class Payment extends Component {
  componentDidMount() {
    const {dispatch, paymentModel, app} = this.props;
    dispatch({
      type: 'paymentModel/getPayments',
      // payload: {
      //   page: 0,
      //   size: 10
      // }
    });
    dispatch({
      type: 'app/getRegions',
    });
    dispatch({
      type: 'app/getAwares',
    });
    dispatch({
      type: 'app/getPosition',
    });
    dispatch({
      type: 'app/getPayTypes',
    });
  }


  render() {
    const {dispatch, app, paymentModel} = this.props;
    const {photoId, speakers, regions, awares, positions, users, payTypes} = app;
    const {
      payments, selectedUser, phoneNumber, modalOpen, modalType,
      birthDate, inputValue, userId, dateTime, searchUsers, loading, page, size, totalPages,
      isLoading, paginationList, notUsd, isUsd
    } = paymentModel;

    const usd = () => {
      dispatch({
        type: 'paymentModel/updateState',
        payload: {
          isUsd: true,
          notUsd: false,
        }
      });
    };
    const isNotUsd = () => {
      dispatch({
        type: 'paymentModel/updateState',
        payload: {
          isUsd: false,
          notUsd: true,
        }
      });
    };

    const getPhoneNumber = (e) => {
      dispatch({
        type: 'paymentModel/updateState',
        payload: {
          [e.target.name]: e.target.value
        }
      })
    };
    const changeModalType = (type) => {
      dispatch({
        type: "paymentModel/updateState",
        payload: {
          modalType: type
        }
      });
      dispatch({
        type: "paymentModel/updateState",
        payload: {
          userId: ''
        }
      });
    };

    const changeDateTime = (date) => {
      dispatch({
        type: 'paymentModel/updateState',
        payload: {
          dateTime: date
        }
      })
    };
    const changeBirthDate = (date) => {
      dispatch({
        type: 'paymentModel/updateState',
        payload: {
          birthDate: date,
        }
      })
    };

    const searchUser = (e) => {
      dispatch({
        type: 'paymentModel/updateState',
        payload: {
          inputValue: e.target.value
        }
      });
      if (e.target.value.length === 0) {
        dispatch({
          type: 'paymentModel/updateState',
          payload: {
            searchUsers: []
          }
        });
      } else {
        dispatch({
          type: 'paymentModel/searchUser',
          payload: {
            word: inputValue
          }
        });
      }
    };

    const clearInput = () => {
      dispatch({
        type: 'paymentModel/updateState',
        payload: {
          inputValue: '',
          searchUsers: []
        }
      });
    };

    const getUserId = (userId) => {
      dispatch({
        type: 'paymentModel/updateState',
        payload: {
          userId
        }
      })
    };

    const save = (e, v) => {
      v.paySum = v.paySum.replace(/ /g, '');
      if (userId) {
        dispatch({
          type: 'paymentModel/addPayment',
          payload: {
            ...v,
            userId: userId,
            payDate: dateTime,
            birthDate: birthDate,
            usd: v.isUsd === 'true',
          }
        });
      } else {
        toast.error("Foydalanuvchini tanlash kerak");
      }
    };


    const openModal = (i) => {
      dispatch({
        type: "paymentModel/updateState",
        payload: {
          dateTime: new Date(),
          birthDate: new Date(),
          searchUsers: [],
          inputValue: '',
          modalType: 'addClients',
        }
      });
      dispatch({
        type: "paymentModel/updateState",
        payload: {
          modalOpen: !modalOpen
        }
      });
      dispatch({
        type: 'app/getUsers',
      });
    };

    const getPayments = (e, page) => {
      dispatch({
        type: 'paymentModel/getPayments',
        payload: {
          page
        }
      })
    };

    return (
      <div>
        <main>
          <section className="client_page margin-70">
            <div className="container">

              {loading ?

                <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px'}}><Loader
                  type="Triangle"
                  color="#ED2939"
                  height="100"
                  width="100"
                /></div>

                : ''}

              {payments.length !== 0 ?
                <div>
                  <Row className="pt-2 pt-md-5">
                    <Col md={2}>
                      <h3 className="header-title">To'lovlar</h3>
                    </Col>
                    <Col md={2} className="offset-8">
                      <button className="btn btn-success px-4 py-2 float-right" onClick={openModal}>Qo'shish</button>
                    </Col>
                  </Row>
                  <Row className="mt-4">
                    <Col md="3">
                      <h2 className="font-weight-bolder">FIO</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">Telefon raqam</h2>
                    </Col>
                    <Col md="3">
                      <h2 className="font-weight-bolder">To'lov miqdori</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">To'lov turi</h2>
                    </Col>
                    <Col>
                      <h2 className="font-weight-bolder">To'lov vaqti</h2>
                    </Col>
                  </Row>
                  {payments.map((item, i) =>
                    <div className="client-list-item pt-2">
                      <Row>
                        <Col md="3" className="text-center">
                          {item.photoUrl === '' ?
                            <div
                              className="client-avatar">{item.firstName.substr(0, 1) + item.lastName.substr(0, 1)}</div> :
                            <img src={item.photoUrl} alt="" className="mr-3"/>}
                          {item.telegramUsername ?
                            <div><a target="_blank" href={"http://t.me/" + item.telegramUsername}>
                              <p className="user-name">{item.fullName}</p></a></div> :
                            <div><p className="user-name">{item.fullName}</p></div>}
                        </Col>
                        <Col md="2" className="text-center">
                          {item.telegramUsername ?
                            <div><a target="_blank" href={"http://t.me/" + item.telegramUsername}>
                              <p className="user-name">{item.phoneNumber}</p></a></div> :
                            <div><p className="user-name">{item.phoneNumber}</p></div>}
                        </Col>
                        <Col md="3" className="text-center">
                          <p>{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(item.paySum))}</p>
                        </Col>
                        <Col md="2" className="text-center">
                          <p>{item.payTypeUz}</p>
                        </Col>
                        <Col className="text-center">
                          <p>{item.payTime ? item.payTime.substring(0, 10) + ' ' + new Date(item.payTime).toTimeString().substring(0, 5) : ''}</p>
                        </Col>
                      </Row>
                      <div className="line"/>
                    </div>
                  )}
                  <div className="col-md-3 offset-5 text-center my-3">
                    <Pagination aria-label="Page navigation example">
                      <PaginationItem disabled={page === 0}>
                        <PaginationLink first onClick={(e) => getPayments(e, 0)}/>
                      </PaginationItem>
                      <PaginationItem disabled={page === 0}>
                        <PaginationLink previous onClick={(e) => getPayments(e, page !== 0 ? page - 1 : 0)}/>
                      </PaginationItem>
                      {paginationList.map(i => <PaginationItem active={page === (i - 1)}
                                                               disabled={page === (i - 1) || i === '...'}>
                        <PaginationLink onClick={(e) => getPayments(e, i - 1)}>
                          {i}
                        </PaginationLink>
                      </PaginationItem>)}
                      <PaginationItem disabled={totalPages === page + 1}>
                        <PaginationLink next
                                        onClick={(e) => getPayments(e, totalPages >= (page + 1) ? (page + 1) : page)}/>
                      </PaginationItem>
                      <PaginationItem disabled={totalPages === page + 1}>
                        <PaginationLink last
                                        onClick={(e) => getPayments(e, page !== totalPages ? page + 1 : totalPages)}/>
                      </PaginationItem>
                    </Pagination>
                  </div>
                </div>
                :
                (
                  <div>
                    <h3 className="font-weight-bolder text-center margin-70">Hozirda to'lovlar mavjud emas</h3>
                    <Card onClick={openModal} className="client-add-btn">
                      <CardBody>
                        <p className="text-center mb-0"><span className="icon icon-user-add"/> To'lov qo'shish</p>
                      </CardBody>
                    </Card>
                  </div>
                )
              }
            </div>
            <Modal isOpen={modalOpen} toggle={openModal} className="event-client-modal">
              <ModalHeader toggle={openModal} className="border-0"/>
              <ModalBody className="pl-5 pr-5">
                <AvForm onValidSubmit={save}>
                  <h3>Mijoz qo'shish ()</h3>
                  {modalType === 'addClients' ?
                    <div>
                      <div className="row mt-5">
                        <div className="col-md-4">
                          <InputGroup>
                            <Input onChange={searchUser} value={inputValue} placeholder="Qidirish"/>
                            <button type="button" onClick={clearInput} className="clearButton">x</button>
                          </InputGroup>
                        </div>
                      </div>
                      <div className="row mt-5">
                        {searchUsers.map((item, i) => <div className="col-md-12">
                          <div className="card">
                            <div className="card-body">
                              <div className="row">
                                <div className="col-md-1 pt-2">
                                  <input type="radio" name="userId" onChange={() => getUserId(item.id)}
                                         className="form-control"/>
                                </div>
                                <div className="col-md-3 pt-2 pl-0">
                                  {/*<div className="user-avatar" style={{backgroundImage: url(${item.photoUrl})}}></div>*/}
                                  {item.fullName}
                                </div>
                                <div className="col-md-2 pt-2 text-center">
                                  {item.phoneNumber}
                                </div>
                                <div className="col-md-2 pt-2 text-center">
                                  {item.company} {item.positionUz ? ',' + item.positionUz : ''}
                                </div>
                                <div className="col-md-2 pt-2 text-center">
                                  {item.awareUz}
                                </div>
                                <div className="col-md-2 pt-2 text-center">
                                  {item.countVisit} marta
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>)}
                        {searchUsers.length === 0 ?
                          <p onClick={() => changeModalType('addNewClient')} className="mx-auto"><img
                            src="/assets/images/icon/plus-green.png" alt="a"/>Yangi mijoz qo'shish
                          </p> : ''
                        }
                      </div>
                    </div>
                    :

                    <div>
                      <div className="row mt-5">
                        <div className="col-md-4 offset-1">
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput name="firstName" required/>
                            <label>Ismi</label>
                          </AvGroup>
                        </div>
                        <div className="col-md-4 offset-2">
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput name="company" required/>
                            <label>Tashkilot nomi</label>
                          </AvGroup>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-4 offset-1">
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput name="lastName" required/>
                            <label>Familiyasi</label>
                          </AvGroup>
                        </div>
                        <div className="col-md-4 offset-2">
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput type="select" name="positionId" required>
                              <option>Lavozimni tanlang</option>
                              {positions.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                            </AvInput>
                            <label>Lavozimi</label>
                          </AvGroup>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-4 offset-1">
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput  name="phoneNumber" type="text" required/>
                            <label>Telefon raqami</label>
                          </AvGroup>
                        </div>
                        <div className="col-md-4 offset-2 pl-0">
                          <div className="form-group">
                            <AvGroup>
                              <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                              <DatePicker
                                name="birthDate"
                                selected={birthDate}
                                className="form-control"
                                onChange={changeBirthDate}
                                timeIntervals={30}
                                dateFormat="dd.MM.yyyy"
                                timeCaption="time"
                              />
                              <label className="label-date">Tug'ilgan sanasi</label>
                            </AvGroup>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-4 offset-1">
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput type="select" name="regionId" required>
                              <option>Viloyatni tanlang</option>
                              {regions.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                            </AvInput>
                            <label>Viloyati</label>
                          </AvGroup>
                        </div>
                        <div className="col-md-4 offset-2">
                          <AvGroup>
                            <AvInput type="select" name="awareId" required>
                              <option>Reklamani ko'rgan joyini tanlang</option>
                              {awares.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                            </AvInput>
                            <label>Reklama ko'rgan joyi</label>
                          </AvGroup>
                        </div>
                      </div>
                    </div>
                  }
                  <div>
                    <div className="col-md-12"><input type="radio" name="usd" onChange={usd}
                                                      className=" border-0 p-0 mb-2"/>
                      To'lov dollarda
                    </div>
                    <div className="col-md-12"><input type="radio" name="usd" checked={!isUsd} onChange={isNotUsd}
                                                      className=" border-0 p-0 mb-2"/> Dollarda emas
                    </div>
                    <div className="row mt-3">
                      <div className="col-md-4 offset-1">
                        <AvGroup>
                          <AvInput name="paySum" tag={CurrencyInput} precision="0" thousandSeparator=" "/>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <label>{isUsd ? "So'mdagi miqdori" : "To'langan pul miqdori"}</label>
                        </AvGroup>
                      </div>
                      <div className="col-md-4 offset-2">
                        {!isUsd ? <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <AvInput type="select" name="payTypeId" required>
                            <option>To'lov turi</option>
                            {payTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                          </AvInput>
                          <label>To'lov turi</label>
                        </AvGroup> : <AvGroup>
                          <DatePicker
                            selected={dateTime}
                            showTimeSelect
                            timeFormat="HH:mm"
                            className="form-control"
                            onChange={changeDateTime}
                            timeIntervals={30}
                            dateFormat="dd.MM.yyyy  hh:mm"
                            timeCaption="time"
                          />
                          <label className="label-date">To'lov vaqti</label>
                        </AvGroup>}
                      </div>
                      <div className="col-md-4 offset-1">
                        {isUsd ? <AvGroup>
                          <AvInput name="sumUsd" tag={CurrencyInput} precision="0" thousandSeparator=" "/>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <label>Dollardagi pul miqdori</label>
                        </AvGroup> : <AvGroup>
                          <AvInput name="details"/>
                          <label>Qo'shimcha ma'lumot</label>
                        </AvGroup>}
                      </div>
                      <div className="col-md-4 offset-2">
                        {!isUsd ? <AvGroup>
                          <DatePicker
                            selected={dateTime}
                            showTimeSelect
                            timeFormat="HH:mm"
                            className="form-control"
                            onChange={changeDateTime}
                            timeIntervals={30}
                            dateFormat="dd.MM.yyyy  hh:mm"
                            timeCaption="time"
                          />
                          <label className="label-date">To'lov vaqti</label>
                        </AvGroup> : <AvGroup>
                          <AvInput name="details"/>
                          <label>Qo'shimcha ma'lumot</label>
                        </AvGroup>}
                      </div>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <Col md={{size: 2, offset: 7}}>
                      {modalType !== 'addClients' ?
                        <button
                          type="button" className="btn-block btn-outline-secondary btn"
                          onClick={() => changeModalType('addClients')}>Bekor qilish</button>
                        : ""
                      }
                    </Col>
                    <div className="col-md-2">
                      <Button type="submit" bsStyle="success" className=" "
                              loading={isLoading}>Saqlash </Button>
                    </div>
                  </div>
                </AvForm>

              </ModalBody>
            </Modal>


          </section>
        </main>


      </div>
    );
  }
}

export default Payment;
