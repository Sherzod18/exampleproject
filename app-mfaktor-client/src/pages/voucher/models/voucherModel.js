import api from 'api'

const {getVouchers, addVoucher, editVoucher, deleteVoucher} = api;
export default ({
  namespace: 'voucherModel',
  state: {
    isModalShow: false,
    dateTime: new Date(),
    selectedVoucher:{},
    modalType: "new",
    randomId: ""
  },

  subscriptions: {

  },
  effects: {
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
