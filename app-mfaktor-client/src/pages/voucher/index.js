import React, {Component} from 'react';
import {
  Button,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  Modal,
  ModalBody,
  ModalHeader, PopoverBody, PopoverHeader,
  Row, UncontrolledPopover
} from "reactstrap";
import {Link} from 'umi';
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import DatePicker from "react-datepicker/es";
import {connect} from "react-redux";

@connect(({voucherModel, app}) => ({voucherModel, app}))
class Voucher extends Component {
  componentDidMount() {
    const {dispatch, app} = this.props;
    dispatch({
      type: 'app/getVouchers'
    });
    dispatch({
      type: 'app/getUsers'
    });
  }

  clickHiding = () => {
    document.getElementById("hr-search-icon").classList.add("aylan");
  };

  render() {
    const {dispatch, voucherModel, app} = this.props;
    const {isModalShow, dateTime, selectedVoucher, modalType, randomId} = voucherModel;
    const {users, vouchers} = app;

    const openModal = () => {
      dispatch({
        type: "voucherModel/updateState",
        payload: {
          isModalShow: !isModalShow,
          selectedVoucher: {},
          randomId: ""
        }
      });
    };
    const openEditModal = (voucher) => {
      dispatch({
        type: "voucherModel/updateState",
        payload: {
          isModalShow: !isModalShow,
          selectedVoucher: voucher,
          modalType: "edit",
          randomId: voucher.voucherId
        }
      });
    };
    const changeDateTime = (date) => {
      dispatch({
        type: 'voucherModel/updateState',
        payload: {
          dateTime: date
        }
      })
    };
    const getRandomId = () => {
      let random = Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5)
      dispatch({
        type: 'voucherModel/updateState',
        payload: {
          randomId: random
        }
      })
    }
    const save = (e, v) => {
      dispatch({
        type: 'app/addVoucher',
        payload: {
          ...v,
          id: selectedVoucher.id,
          deadline: dateTime
        }
      }).then(() => {
      dispatch({
        type: 'app/getVouchers'
      });
      })
    };
    const deleteVoucher = (voucher) => {
      dispatch({
        type: "app/deleteVoucher",
        payload: {
          id: voucher
        }
      })
    }
    return (
      <div className="voucher-page">
        <header className="border-bottom">
          <Container>
            <Row className="mt-5 mt-md-5 pt-md-5">
              <Col md={2}>
                <h3 className="header-title">Voucher</h3>
              </Col>
              <Col md={3} className="pl-md-0 pl-3">
                <InputGroup>
                  <InputGroupAddon onClick={() => this.clickHiding()} className="hr-search-icon"
                                   addonType="prepend"><span
                    className="icon icon-search"/></InputGroupAddon>
                  <Input className="hr-search-input" placeholder="Write name"/>
                </InputGroup>
              </Col>

              <Col md={7} className="hr-category mt-md-0 mt-2">
                <ul className="list-un-styled mb-0">
                  <li className="d-inline-block mt-md-0 mt-2">
                    <button type="button"
                            className="btn header-btn-light ">Barchasi
                    </button>
                  </li>
                  <Link to="/">
                    <li className="d-inline-block  ml-2 ml-md-4 mt-md-0 mt-2 ">
                      <button type="button"
                              className="btn header-btn-light">Nizom
                      </button>
                    </li>
                  </Link>
                </ul>
              </Col>
            </Row>
          </Container>
        </header>
        <section className="mb-5">
          <Container className="px-5">
            <div>
              <Row className="mt-4">
                <Col md="2" className="pl-4">
                  <h2 className="font-weight-bolder">ID</h2>
                </Col>
                <Col md="3">
                  <h2 className="font-weight-bolder">Mijoz</h2>
                </Col>
                <Col md="2">
                  <h2 className="font-weight-bolder">Summa</h2>
                </Col>
                <Col md="2">
                  <h2 className="font-weight-bolder">Amal qilish</h2>
                </Col>
                <Col className="2">
                  <h2 className="font-weight-bolder">Status</h2>
                </Col>
              </Row>
              {vouchers.map(voucher => (
                <div className="mt-3">
                  <div className="client-list-item pt-2">
                    <Row>
                      <Col md="2" className='pl-4'>
                        <p>{voucher.voucherId}</p>
                      </Col>
                      <Col md="3">
                        <p>{voucher.firstName + " " + voucher.lastName}</p>
                      </Col>
                      <Col md="2">
                        <p>{voucher.price}</p>
                      </Col>
                      <Col md="2">
                        <p>{voucher.deadline.substr(0, 10)}</p>
                      </Col>
                      <Col className="2">
                        <p>{voucher.status}</p>
                      </Col>
                    </Row>
                    <div className="line"/>
                    <button className="btn btn-edit-28 p-0"><span
                      className="icon icon-edit" onClick={() => openEditModal(voucher)}/></button>
                    <button className="btn btn-delete-28 p-0"  id={"popover" + voucher.id}><span
                      className="icon icon-delete"/></button>
                    <UncontrolledPopover trigger="focus" placement="bottom" target={"popover" + voucher.id}>
                      <PopoverHeader>O'chirmoqchisiz?</PopoverHeader>
                      <PopoverBody>
                        <Button color="danger" className="p-2" onClick={() => deleteVoucher(voucher.id)}>Ha</Button>
                        <Button className="p-2 d-inline-block float-right">Yo'q</Button>
                      </PopoverBody>
                    </UncontrolledPopover>
                  </div>
                </div>
              ))}
            </div>
          </Container>
          <Modal isOpen={isModalShow} toggle={openModal} className="custom-modal">
            <ModalHeader toggle={openModal} className="border-0"/>
            <ModalBody className="px-5 pb-5">
              <AvForm onValidSubmit={save}>
                <h3 className="font-weight-bolder">Vaucher qo'shish</h3>
                <Row className="mt-5">
                  <Col md={{size: 4, offset: 1}}>
                    <AvGroup>
                      <AvInput type="select" name="ownerId" value={selectedVoucher.ownerId} required>
                        {selectedVoucher.id ? "" : <option>Mijozni tanlang</option>}
                        {users.map((i) => <option value={i.id}>{i.fullName}</option>)}
                      </AvInput>
                      <label>Mijoz nomi</label>
                    </AvGroup>
                  </Col>
                  <Col md={{size: 4, offset: 2}}>
                    <AvGroup>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      <DatePicker
                        name="deadline"
                        selected={dateTime}
                        className="form-control"
                        onChange={changeDateTime}
                        timeIntervals={30}
                        dateFormat="dd.MM.yyyy"
                        timeCaption="time"
                      />
                      <label className="label-date">Tug'ilgan sanasi</label>
                    </AvGroup>
                  </Col>
                </Row>
                <Row className="mt-3">
                  <Col md={{size: 4, offset: 1}}>
                    <AvGroup>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      <AvInput name="price" type="number" value={selectedVoucher.price} required/>
                      <label>Summa</label>
                    </AvGroup>
                  </Col>
                  <Col md={{size: 4, offset: 2}}>
                    <AvGroup>
                      <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      <AvInput name="voucherId" value={randomId} type="voucherId" required/>
                      <label>Id kod</label>
                    </AvGroup>
                    <Button className="btn-reload" onClick={getRandomId}><img src="/assets/images/refresh-cw.png" alt=""/></Button>
                  </Col>
                </Row>
                {modalType === 'edit' ?
                  <Row className="mt-3">
                    <Col md={{size: 4, offset: 1}}>
                      <AvGroup>
                        <AvInput type="select" name="status" required>
                          <option value="ACTIVE">Aktiv</option>
                          <option value="USED">Ishlatilgan</option>
                          <option value="EXPIRED">Eskirgan</option>
                        </AvInput>
                        <label>Vaucher holati</label>
                      </AvGroup>
                    </Col>
                  </Row> : ""
                }
                <Row className="mb-5 mt-4">
                  <Col md={{size: 2, offset: 7}}>
                    <Button outline type="button" className="btn-sm btn-block" onClick={openModal}>Bekor
                      qilish</Button>
                  </Col>
                  <Col md={{size: 2}}>
                    <Button type="submit" color="success" className="btn-sm btn-block">Saqlash</Button>
                  </Col>
                </Row>
              </AvForm>
            </ModalBody>
          </Modal>
        </section>
      </div>
    );
  }
}

export default Voucher;
