import React, {Component} from 'react';
import "jquery/dist/jquery.min";
import {Link} from 'umi';
import "bootstrap/dist/js/bootstrap";
import {AvField, AvForm, AvGroup, AvFeedback, AvInput} from "availity-reactstrap-validation";
import {connect} from "react-redux";
import ModalVideo from 'react-modal-video';
import Loader from 'react-loader-spinner'
import {
  Card,
  CardBody,
  Col,
  Modal,
  CardImg,
  Container,
  Row,
  ModalHeader,
  ModalBody,
  ModalFooter,
  ButtonGroup,
  Button,
  Input, InputGroup, InputGroupAddon
} from 'reactstrap'

@connect(({addVideos}) => ({addVideos}))
class Videos extends Component {

  componentWillMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'addVideos/getVideoTypes'
    })
  }

  clickHiding = () => {
    let x = document.getElementById("ss");
    if (!x.classList.contains("inputQidirish")) {
      x.classList.add("inputQidirish");
    } else {
      const {dispatch} = this.props;
      dispatch({
        type: 'addVideos/searchByTitle',
        payload: {
          path: x.value
        }
      })
    }

  };

  render() {
    const {addVideos, dispatch} = this.props;
    const {
      isModalShow, loading, isModalShoww, iframe, videoId, videoIframes,
      vidTypeId, vidTypes, description, title, videoTypeName,
      activeCat, vtypeName, activeBarchasi, ifameUrl, ifameId, youtubeId,
      currentUser, size, page, isDeleteModal, isYanaYuklash, videotypeName
    } = addVideos;
    const modalShow = () => {
      dispatch({
        type: 'addVideos/updateState',
        payload: {
          isModalShow: !isModalShow,
          title: null,
          description: null,
          iframe: null
        }
      });
    };
    const modalShow2 = (youtubeId) => {
      dispatch({
        type: 'addVideos/getVideoIfameId',
        payload: {
          frameUrl: youtubeId
        }
      });
    };
    const modalShowDelete = (deleteVideoId) => {
      dispatch({
        type: 'addVideos/updateState',
        payload: {
          isDeleteModal: !isDeleteModal,
          videoId: deleteVideoId
        }
      });
    };
    const handleSubmit = (event, errors, values) => {
      dispatch({
        type: 'addVideos/saveVideoIframe',
        payload: {
          ...values
        }
      });
      dispatch({
        type: 'addVideos/updateState',
        payload: {
          isModalShow: !isModalShow,
          title: null,
          description: null,
        }
      });
    };
    const saveLink = (e) => {
      dispatch({
        type: 'addVideos/updateState',
        payload: {
          iframe: e.target.value
        }
      });
    };
    const saveTitle = (e) => {
      dispatch({
        type: 'addVideos/updateState',
        payload: {
          title: e.target.value
        }
      });
    };
    const saveDescription = (e) => {
      dispatch({
        type: 'addVideos/updateState',
        payload: {
          description: e.target.value
        }
      });
    };
    const chooseVideo = (videoId) => {
      dispatch({
          type: 'addVideos/deleteVideos',
          payload: {
            id: videoId
          }
        }
      );
      dispatch({
          type: 'addVideos/updateState',
          payload: {
            isDeleteModal: !isDeleteModal
          }
        }
      );
    };
    const getAllVideoTypes = () => {
      dispatch({
        type: 'addVideos/updateState',
        payload: {
          vtypeName: "Barchasi"
        }
      });
      dispatch({
        type: 'addVideos/getByVideoTypes',
        payload: {
          page: 0,
          size: 12
        }
      });
    };
    const yanaVideo = () => {
      {
        vtypeName === "videos" ?
          function () {
            dispatch({
              type: 'addVideos/getVideoIframes',
              payload: {
                page: page + 1,
                size: size
              }
            })
            dispatch({
              type: 'addVideos/updateState',
              payload: {
                isYanaYuklash: !isYanaYuklash
              }
            })
          }() :
          function () {
            dispatch({
              type: 'addVideos/getByVideoTypes',
              payload: {
                page: 0,
                size: size + 12,
                vidTypeNam: vtypeName
              }
            });
            dispatch({
              type: 'addVideos/updateState',
              payload: {
                isYanaYuklash: !isYanaYuklash
              }
            })
          }()
      }
      ;
    }
    const getPrakVideos = (vtypeName) => {
      dispatch({
        type: 'addByType/updateState',
        payload: {
          vtypeName: vtypeName
        }
      });
      dispatch({
          type: 'addByType/getByVideoTypes',
          payload: {
            vidTypeNam: vtypeName,
            page: 0,
            size: 12

          }
        }
      );
    };
    return (

      <div className="header-section">

        <header className="border-bottom">
          <Container>
            <Row className="mt-5 mt-md-5 mb-3 mb-md-0 pt-md-5">
              <Col md={2}>
                <h3 className="header-title">Videolar</h3>
              </Col>
              <Col md={3} className="pl-md-0 pl-3 pt-2">
                <InputGroup>
                  <InputGroupAddon id='hr-search-icon' onClick={() => this.clickHiding()}
                                   className="hr-search-icon mr-4"
                                   addonType="prepend"><span
                    className="icon icon-search"/></InputGroupAddon>
                  <AvForm onSubmit={() => this.clickHiding()}>
                    <input id="ss" className="hr-search-input inputKorinishi" placeholder="Video nomini kiriting"/>
                  </AvForm>
                </InputGroup>
              </Col>

              <Col md={7} className="hr-category mt-md-0 mt-2">
                <ul className="list-un-styled mb-0">

                  <li className="d-inline-block mt-md-0 mt-2">
                    <button type="button" onClick={getAllVideoTypes}
                            className={"btn header-btn-light " + activeBarchasi}>Barchasi
                    </button>
                  </li>

                  {vidTypes ?
                    vidTypes.map((i, a) =>
                      <Link to={"/videos/" + i.name}>
                        <li key={a} className="d-inline-block  ml-2 ml-md-4 mt-md-0 mt-2 ">
                          <button type="button" onClick={() => getPrakVideos(i.name)}
                                  className={vidTypeId === i.id ? "btn header-btn-light " + activeCat : "btn header-btn-light"}>{i.name}</button>
                        </li>
                      </Link>
                    )

                    : ''}

                  {currentUser.roles ? currentUser.roles.filter((i) =>
                    i.name === "ROLE_CASHIER" ||
                    i.name === "ROLE_MANAGER" ||
                    i.name === "ROLE_ADMIN" ||
                    i.name === "ROLE_DIRECTOR").length > 0 ?
                    <li className="d-inline-block  ml-2 ml-md-4 mt-md-0 mt-2">
                      <button type="button" className="btn header-btn-light float-right mt-3" onClick={modalShow}>+ Add
                      </button>
                    </li> : "" : ""}
                </ul>
              </Col>
            </Row>
          </Container>
        </header>
        <main>
          <Container>


            {loading ?
              <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px', marginBottom: '100px'}}>
                <Loader
                  type="Triangle"
                  color="#ED2939"
                  height="100"
                  width="100"
                /></div> :
              <section className="event-videos">

                {videoIframes ? <div>{videoIframes.length > 0 ? <Row className="py-4 mb-3 mb-md-0">
                  <Col md={6}>
                    <h6 className="title">{videotypeName}</h6>
                  </Col>
                </Row> : ''}</div> : ''}


                <Row>


                  {videoIframes ? videoIframes.map((i, b) =>
                    <Col md={6} key={b} className="col-lg-3 mb-4 mt-md-0 ">
                      <Card className="card-video-block">
                        <div className='blog-theme' dangerouslySetInnerHTML={{
                          __html: i.videoUrl
                        }}>
                        </div>
                        <CardBody className="px-2 py-3">
                          <a onClick={() => modalShow2(i.youtubeId)} href="#">
                            <h4 className="m-0 pb-3">{i.title}</h4>
                          </a>
                          {currentUser.roles ? currentUser.roles.filter((i) =>
                            i.name === "ROLE_CASHIER" ||
                            i.name === "ROLE_MANAGER" ||
                            i.name === "ROLE_ADMIN" ||
                            i.name === "ROLE_DIRECTOR").length > 0 ?
                            <button type="button" className="btn px-3 py-1
                   btn-danger float-right"
                                    onClick={() => modalShowDelete(i.id)}>Delete</button> : "" : ""}
                        </CardBody>
                      </Card>
                    </Col>
                  ) : ''}

                </Row>

                <Row className="mt-4 mt-md-5">
                  {isYanaYuklash ? <Col md={{size: 2, offset: '5'}} className="col-6 mb-5">
                    <Button onClick={yanaVideo} color="danger" className="btn-block">
                      Yana yuklash
                    </Button>
                  </Col> : <Col md={{size: 2, offset: '5'}} className="col-12 text-center">
                    <Row>
                      <Col md="4" className="text-right">
                        <Loader
                          type="ThreeDots"
                          color="#ED2939"
                          width="30px"
                        />
                      </Col>
                      <Col md="8" className="text-left" style={{paddingTop: '25px'}}>
                        <span>Yuklanmoqda</span>
                      </Col>
                    </Row>
                  </Col>}
                </Row>

              </section>
            }
          </Container>
        </main>
        <Modal isOpen={isModalShow} toggle={modalShow}>
          <ModalHeader>Add Video</ModalHeader>

          <AvForm onSubmit={handleSubmit}>
            <AvGroup>
              <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
              <AvInput value={title} name="title" onChange={saveTitle} required/>
              <label>Video sarlavhasini kiriting.</label>
            </AvGroup>
            <AvGroup>
              <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
              <AvInput value={iframe} name="videoUrl" onChange={saveLink} required/>
              <label>Video linkini kiriting.</label>
            </AvGroup>
            <AvGroup>
              <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>

              <AvInput type="select" className=" p-0 pl-2 mb-1"
                       name="videoTypeId"
                       id="videoTypeId"
                       value="0"
                       required>

                <option key="title" value='0'>Video turini tanlang</option>
                  {vidTypes.map((i,c) => <option key="i.id" value={i.id}>{i.name}</option>)}


              </AvInput>
              <label>Video turi</label>
            </AvGroup>
            <p className="text-right pr-1">
              <Button type="button" color="danger" className="mr-3" onClick={modalShow}>Cancel</Button>
              <Button type="submit" className="mr-2" color="success">Save Video</Button>
            </p>
          </AvForm>
        </Modal>
        <Modal isOpen={isDeleteModal} toggle={() => modalShowDelete(videoId)}>
          <ModalHeader>Delete Video</ModalHeader>

          <ModalFooter>
            <Button type="button" onClick={() => chooseVideo(videoId)} color="primary">Delete Video</Button>
            <Button type="button" color="secondary" onClick={() => modalShowDelete("")}>Cancel</Button>
          </ModalFooter>

        </Modal>
        <ModalVideo channel='youtube' isOpen={isModalShoww} videoId={youtubeId}
                    onClose={() => modalShow2("")}/>

      </div>
    );
  }
}

export default Videos;
