import api from 'api'
import router from "umi/router";

const {
  getAllVideos,
  saveVideo, deleteVideo, videoTypes, getPraktikums, userMe,
  searchByTitle
} = api;
export default ({
  namespace: "addVideos",
  state: {
    isYanaYuklash: true,
    isYanaYuklashByType: true,
    isModalShow: false,
    isModalShoww: false,
    isDeleteModal: false,
    iframe: null,
    videoIframes: [],
    videoFramesByTpe:[],
    videoId: null,
    vidTypeId: null,
    vidTypes: [],
    title: null,
    description: null,
    videoTypeName: null,
    activeCat: '',
    activeBarchasi: "active",
    ifameUrl: null,
    youtubeId: null,
    ifameId: null,
    vtypeName: "",
    videotypeName: "Barchasi",
    currentUser: {},
    page: 0,
    loading: true,
    size: 12,
    searchingVideos: []
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen(({pathname}) => {
        const str = pathname.substring(pathname.lastIndexOf("/") + 1);

        if (pathname === ("/videos")) {
          dispatch({
            type: 'getVideoTypes'
          });
          dispatch({
            type: 'userMe'
          });

          dispatch({
            type: 'getVideoIframes',
            payload: {
              page: 0,
              size: 12
            }
          });
          dispatch({
            type: 'updateState',
            payload: {
              vtypeName: "videos",
              activeBarchasi: "active",
              activeCat: '',
              videotypeName:"Barchasi"
            }
          })
        } else if (pathname.includes(str)) {
          dispatch({
            type: 'getVideoTypes'
          });
          dispatch({
            type: 'userMe'
          });
          dispatch({
              type: 'getByVideoTypes',
              payload: {
                vidTypeNam: str,
                page: 0,
                size: 12

              }
            }
          );
          dispatch({
            type: 'updateState',
            payload: {
              vtypeName: str,
              activeBarchasi: '',
              activeCat: "active",
              videotypeName:str
            }
          })
        }
      })
    }
  },
  effects: {
    * userMe({payload}, {call, put, select}) {
      try {
        const res = yield call(userMe);
        if (!res.success) {
          yield put({
            type: 'updateState',
            payload: {currentUser: {}}
          });


        } else {
          yield put({
            type: 'updateState',
            payload: {currentUser: res.object}
          })
        }
      } catch (error) {
        yield put({
          type: 'updateState',
          payload: {currentUser: {}}
        });
      }
    },

    * getVideoIframes({payload}, {call, put, select}) {
      const {isYanaYuklash}= yield select(_=>_.addVideos);
      const res = yield call(getAllVideos, payload);
      const {videoIframes} = yield select(_ => _.addVideos);
      let list;
      if (payload.page === 0) {
        list = res.object;
      } else
        list = videoIframes.concat(res.object);
      yield put({
        type: 'updateState',
        payload: {
          videoIframes: list,
          loading: false,
          activeBarchasi: "active",
          activeCat: '',
          page: payload.page,
          size: payload.size,
          isYanaYuklash: isYanaYuklash,
          videotypeName: "Barchasi"
        }
      })
    },

    * getByVideoTypes({payload}, {call, put, select}) {
      const res = yield call(getPraktikums, payload);
      const {isYanaYuklashByType}= yield select(_=>_.addVideos);
      const {videoFramesByTpe} = yield select(_ => _.addVideos);
      let list;
      if (payload.page === 0) {
        list = res.object;
      } else
        list = videoFramesByTpe.concat(res.object);
      yield put({
        type: 'updateState',
        payload: {
          activeBarchasi: '',
          activeCat: "active",
          videoFramesByTpe: list,
          page: payload.page,
          size: payload.size,
          videotypeName: payload.vidTypeNam,
          isYanaYuklashByType:!isYanaYuklashByType,


        }
      })
    },
    * getVideoTypes({payload}, {call, put, select}) {
      const res = yield call(videoTypes);
      yield put({
        type: 'updateState',
        payload: {
          vidTypes: res.object
        }
      })
    },
    * saveVideoIframe({payload}, {call, put, select}) {
      const res = yield call(saveVideo, {...payload});
      yield put({
        type: 'updateState',
        payload: {
          iframe: res.object.videoUrl
        }
      })
      yield put({
        type: 'getVideoIframes',
        payload: {
          page: 0,
          size: 12
        }
      });
    },
    * deleteVideos({payload}, {call, put, select}) {
      const res = yield call(deleteVideo, payload);
      yield put({
        type: 'getVideoIframes',
        payload: {
          page: 0,
          size: 12
        }
      });
    },
    * getVideoIfameId({payload}, {call, put, select}) {
      const isModalShoww = yield select(_ => _.addVideos);
      yield put({
        type: 'updateState',
        payload: {
          isModalShoww: !isModalShoww,
          youtubeId: payload.frameUrl
        }
      })
    },
    * searchByTitle({payload}, {call, put, select}) {
      const res = yield call(searchByTitle, payload);
      const {videoIframes} = yield select(_ => _.addVideos);
      const {activeCat} = yield select(_ => _.addVideos);
      if (activeCat === "active") {
        yield put({
          type: 'updateState',
          payload: {
            videoFramesByTpe: res.object
          }
        });
      } else {
        yield put({
          type: 'updateState',
          payload: {
            videoIframes: res.object
          }
        });
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
