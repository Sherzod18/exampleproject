import React, {Component} from 'react';

class Register extends Component {
  render() {
    return (
      <div>
        <main>
          <section className="user_form">
            <div className="container">
              <h2 className="title" style={{marginTop: '90px', marginBottom: 0, marginLeft: '10px'}}>Ro'yxatdan o'tish</h2>
              <div className="card my-5 border-0">
                <div className="card-body p-5">
                  <div className="row mt-2">
                    <div className="col-md-6 offset-3">
                      <div className="row mt-4">
                        <div className="col-md-12 mb-4">
                          <div className="input_wrap border pl-3">
                            <label htmlFor="name" className="mb-0 mt-1 ml-2">First name</label>
                            <input type="text" id="name" name="firstName" className="form-control border-0 p-0 pl-2 mb-2" />
                          </div>
                        </div>
                        <div className="col-md-12 mb-4">
                          <div className="input_wrap border pl-3">
                            <label htmlFor="company" className="mb-0 mt-1 ml-2">Last name</label>
                            <input type="text" name="lastName" id="company" className="form-control border-0 p-0 pl-2 mb-2" />
                          </div>
                        </div>
                        <div className="col-md-12 mb-4">
                          <div className="input_wrap border pl-3">
                            <label htmlFor="phoneNumber" className="mb-0 mt-1 ml-2">Phonenumber</label>
                            <input type="text" name="phoneNumber" id="phoneNumber" className="form-control border-0 p-0 pl-2 mb-2" />
                          </div>
                        </div>
                        <div className="col-md-12 mb-4">
                          <div className="input_wrap border pl-3">
                            <label htmlFor="password" className="mb-0 mt-1 ml-2">Password</label>
                            <input type="password" name="password" id="password" className="form-control border-0 p-0 pl-2 mb-2" />
                          </div>
                        </div>
                      </div>
                      <div className="row mt-3">
                        <div className="col-md-5 offset-7 mt-4">
                          <button type="submit" className="btn  py-2 px-5 float-right">Saqlash</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    );
  }
}

export default Register;
