import api from 'api'

const {addVideoType, editVideoType, getVideoTypes, getVideoType, deleteVideoType} = api;

export default ({
  namespace: 'videoType',
  state: {
    videoTypes: [],
    isModalShow: false,
    delModalShow: false,
    current:{},
    modalType:'new',
    deleteId: 0,
    pathname:''
  },
  subscriptions:{
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getVideoTypes({payload}, {put, call, select}) {
      const res = yield call(getVideoTypes);
      yield put({
        type: 'updateState',
        payload: {
          videoTypes: res._embedded.list
        },
      });
    },
    * save({payload}, {put, call, select}) {
      const res = yield call(addVideoType, payload);
      if (res.success){
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getVideoTypes'
        });
      }
    },
    * edit({payload}, {put, call, select}) {
      const res = yield call(editVideoType, {path:payload.id, ...payload});
      if (res.success){
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getVideoTypes'
        });
      }
    },
    * delete({payload}, {put, call, select}){
      const del = yield call(deleteVideoType, {id:payload.id});
      yield put({
        type:'getVideoTypes'
      });
    },

    * getVideoType ({payload}, {put, call, select}){
      const res = yield call(getVideoType, payload);
      res.id = payload.path;
      yield put({
        type:'updateState',
        payload:{
          current:res
        }
      })

    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
