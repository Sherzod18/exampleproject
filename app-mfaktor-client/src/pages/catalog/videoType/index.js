import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';
import {router} from "umi";
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({videoType}) =>  ({videoType}))
class VideoType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type:'videoType/getVideoTypes'
    })
  }

  render() {
    const {dispatch, videoType} = this.props;
    const {isModalShow, videoTypes, current, modalType,
      delModalShow, deleteId,pathname} = videoType;

    const modalShow = () => {
      dispatch({
        type:'videoType/updateState',
        payload:{
          modalType:'new'
        }
      })
      dispatch({
        type: 'videoType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };
    const deleteModalShow = (id) => {
      if (id !== -1){
        dispatch({
          type:'videoType/updateState',
          payload:{
            deleteId: id
          }
        });
      }
      dispatch({
        type:'videoType/updateState',
        payload:{
          delModalShow: !delModalShow
        }
      });
    };
    const handleSubmit = (event, errors, values) => {
      if (errors.length === 0) {
        if (modalType==='new') {
          dispatch({
            type:'videoType/save',
            payload: {
              ...values
            }
          });
        }else {
          dispatch({
            type:'videoType/edit',
            payload: {
              ...values,
              id:current.id
            }
          });
        }
      }
    };
    const deleteVideoType = () => {
      dispatch({
        type: "videoType/delete",
        payload: {
          id: deleteId
        }
      });
      deleteModalShow(-1);
    };
    const editVideoType = (id) => {
      dispatch({
        type:'videoType/updateState',
        payload:{
          modalType:'update'
        }
      })
      dispatch({
        type: 'videoType/getVideoType',
        payload:{
          path:id,
        }
      });
      dispatch({
        type: 'videoType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };

    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">

            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="2">
                    <h2 className="font-weight-bolder">T/r</h2>
                  </Col><Col md="8">
                    <h2 className="font-weight-bolder">O'zbek</h2>
                  </Col>
                </Row>
                {videoTypes !== undefined ? videoTypes.map((item,a) =>
                  <div key={a} className="client-list-item pt-2">
                    <Row>
                      <Col md="2" className="text-center">
                        <p>{a+1}</p>
                      </Col>
                      <Col md="8" className="text-center">
                        <p>{item.name}</p>
                      </Col>

                      <Col md="2 ml-auto">
                        <button className="btn btn-edit-28 p-0" onClick={()=> editVideoType(item.id)}><span className="icon icon-edit" /></button>
                        <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(item.id)}><span className="icon icon-delete" /></button>
                      </Col>
                    </Row>
                    <div className="line" />
                  </div>
                ) : ''}
                <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>
              </div>
            </section>
          </div>
        </div>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
          <ModalFooter>
            <Button className="btn-sm" color="primary" onClick={deleteVideoType}>Ha</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isModalShow} toggle={modalShow} className={this.props.className}>
          <ModalHeader>{modalType==='new'? 'Saqlash':'Video turlarini tahrirlash'}</ModalHeader>
          <AvForm  onSubmit={handleSubmit}>
            <ModalBody>
              <Row>
                <Col md={{size: 10, offset:1}}>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="name"  value={modalType==='new'? '' : current.name} type="text" required/>
                    <label>O'zbek</label>
                  </AvGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button className="btn-sm" color="primary">{modalType==='new'? 'Saqlash':'Tahrirlash'}</Button>
              <Button className="btn-sm" color="secondary" onClick={modalShow}>Bekor qilish</Button>
            </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

VideoType.propTypes = {
  dispatch: PropTypes.func
}

export default VideoType;
