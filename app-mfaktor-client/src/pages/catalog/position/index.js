import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';
import {router} from "umi";
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({position}) => ({position}))
class PositionType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type:'position/getPositions'
    })
  }

  render() {
    const {dispatch, position} = this.props;
    const {isModalShow, positions, currentPosition, modalType,
      delModalShow, deleteId,pathname} = position;

    const modalShow = () => {
      dispatch({
        type:'position/updateState',
        payload:{
          modalType:'new'
        }
      })
      dispatch({
        type: 'position/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };
    const deleteModalShow = (id) => {
      if (id !== -1){
        dispatch({
          type:'position/updateState',
          payload:{
            deleteId: id
          }
        });
      }
      dispatch({
        type:'position/updateState',
        payload:{
          delModalShow: !delModalShow
        }
      });
    };
    const handleSubmit = (event, errors, values) => {
      if (errors.length === 0) {
        if (modalType==='new') {
          dispatch({
            type:'position/savePosition',
            payload: {
              ...values
            }
          });
        }else {
          dispatch({
            type:'position/editPosition',
            payload: {
              ...values,
              id:currentPosition.id
            }
          });
        }
      }
    };
    const deleteEventType = () => {
      dispatch({
        type: "position/deletePosition",
        payload: {
          id: deleteId
        }
      });
      deleteModalShow(-1);
    };
    const editEventType = (id) => {
      dispatch({
        type:'position/updateState',
        payload:{
          modalType:'update'
        }
      })
      dispatch({
        type: 'position/getPosition',
        payload:{
          path:id,
        }
      });
      dispatch({
        type: 'position/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };

    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="5">
                    <h2 className="font-weight-bolder">O'zbek</h2>
                  </Col>
                  <Col md="5">
                    <h2 className="font-weight-bolder">Русский</h2>
                  </Col>
                </Row>
                {positions !== undefined ? positions.map((eType,a) =>
                  <div key={a} className="client-list-item pt-2">
                    <Row>
                      <Col md="5" className="text-center">
                        <p>{eType.nameUz}</p>
                      </Col>
                      <Col md="5" className="text-center">
                        <p>{eType.nameRu}</p>
                      </Col>
                      <Col md="2 ml-auto">
                        <button className="btn btn-edit-28 p-0" onClick={()=> editEventType(eType.id)}><span className="icon icon-edit" /></button>
                        <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(eType.id)}><span className="icon icon-delete" /></button>
                      </Col>
                    </Row>
                    <div className="line" />
                  </div>
                ) : ''}
                <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>
              </div>
            </section>
          </div>
        </div>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
          <ModalFooter>
            <Button className="btn-sm" color="primary" onClick={deleteEventType}>Ha</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isModalShow} toggle={modalShow} className={this.props.className}>
          <ModalHeader>{modalType==='new'? 'Lavozimni saqlash':'Lavozimni tahrirlash'}</ModalHeader>
          <AvForm  onSubmit={handleSubmit}>
          <ModalBody>
            <Row>
              <Col md={{size: 10, offset:1}}>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="nameUz"  value={modalType==='new'? '' : currentPosition.nameUz} type="text" required/>
                  <label>O'zbek</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="nameRu"  value={modalType==='new'? '' : currentPosition.nameRu} type="text" required/>
                  <label>Русский</label>
                </AvGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-sm" color="primary">{modalType==='new'? 'Saqlash':'Tahrirlash'}</Button>
            <Button className="btn-sm" color="secondary" onClick={modalShow}>Cancel</Button>
          </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

PositionType.propTypes = {
  dispatch: PropTypes.func
}

export default PositionType;
