import api from 'api'
import {toast} from 'react-toastify';

const {addPosition, editPosition, getPositions, getPosition, deletePosition} = api;

export default ({
  namespace: 'position',
  state: {
    positions: [],
    isModalShow: false,
    delModalShow: false,
    currentPosition:{},
    modalType:'new',
    deleteId: 0,
    pathname:''
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getPositions({payload}, {put, call, select}) {
      const res = yield call(getPositions);
      yield put({
        type: 'updateState',
        payload: {
          positions: res._embedded.list
        },
      });
    },
    * savePosition({payload}, {put, call, select}) {
      const res = yield call(addPosition, payload);
      if (res.success){
        toast.success("Saqlandi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getPositions'
        });
      }else{
        if (res.statusCode === 403){
          toast.error("Saqlashga ruxsatingiz yo'q!");
        } else {
          toast.error("Saqlashda xatolik!");
        }
      }
    },
    * editPosition({payload}, {put, call, select}) {
      const res = yield call(editPosition, {path:payload.id, ...payload});
      if (res.success){
        toast.success("O'zgartirildi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getPositions'
        });
      }else {
        if (res.statusCode === 403){
          toast.error("O'zgartirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'zgartirishda xatolik!");
        }
      }
    },
    * deletePosition({payload}, {put, call, select}){
      const del = yield call(deletePosition, {id:payload.id});
      if (del.success) {
        toast.success("O'chirildi!");
        yield put({
          type: 'getPositions'
        });
      }else {
        if (del.statusCode === 403){
          toast.error("O'chirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'chirishda xatolik!");
        }
      }
    },

    * getPosition ({payload}, {put, call, select}){
      const res = yield call(getPosition, payload);
      res.id = payload.path;
      yield put({
        type:'updateState',
        payload:{
          currentPosition:res
        }
      })

    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
