import api from 'api'

const {addCategory, editCategory, getCategories, getCategory, deleteCategory} = api;

export default ({
  namespace: 'category',
  state: {
    categories: [],
    isModalShow: false,
    delModalShow: false,
    current:{},
    modalType:'new',
    deleteId: 0,
    pathname:''
  },
  subscriptions:{
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getCategories({payload}, {put, call, select}) {
      const res = yield call(getCategories);
      yield put({
        type: 'updateState',
        payload: {
          categories: res._embedded.list
        },
      });
    },
    * save({payload}, {put, call, select}) {
      const res = yield call(addCategory, payload);
      if (res.success){
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getCategories'
        });
      }
    },
    * edit({payload}, {put, call, select}) {
      const res = yield call(editCategory, {path:payload.id, ...payload});
      if (res.success){
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getCategories'
        });
      }
    },
    * delete({payload}, {put, call, select}){
      const del = yield call(deleteCategory, {id:payload.id});
      yield put({
        type:'getCategories'
      });
    },

    * getCategory ({payload}, {put, call, select}){
      const res = yield call(getCategory, payload);
      res.id = payload.path;
      yield put({
        type:'updateState',
        payload:{
          current:res
        }
      })

    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
