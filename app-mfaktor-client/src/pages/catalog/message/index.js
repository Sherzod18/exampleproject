import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from 'reactstrap';
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({messageModel}) => ({messageModel}))
class PositionType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'messageModel/getMessages'
    });
    dispatch({
      type: 'messageModel/getEvents'
    })
  }

  render() {
    const {dispatch, messageModel} = this.props;
    const {
      isModalShow, messages, modalType,
      delModalShow, deleteId, pathname, events
    } = messageModel;

    const modalShow = () => {
      dispatch({
        type: 'messageModel/updateState',
        payload: {
          isModalShow:!isModalShow
        }
      });
    };
    const handleSubmit = (event, errors, values) => {
      if (errors.length === 0) {
        dispatch({
          type: 'messageModel/sendMessage',
          payload: {
            ...values
          }
        });
      }
    };

    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="2">
                    <h2 className="font-weight-bolder">Tr</h2>
                  </Col>
                  <Col md="5">
                    <h2 className="font-weight-bolder">Qaysi tadbirga?</h2>
                  </Col>
                  <Col md="5">
                    <h2 className="font-weight-bolder">Xabar matni</h2>
                  </Col>
                </Row>
                {messages.map((message, a) =>
                  <div key={a} className="client-list-item pt-2">
                    <Row>
                      <Col md="2">
                        <h2 className="font-weight-bolder">{a+1}</h2>
                      </Col>
                      <Col md="5" className="text-center">
                        <p>{message.eventName?message.eventName:'Barcha foydalanuvchilarga'}</p>
                      </Col>
                      <Col md="5" className="text-center">
                        <p>{message.text}</p>
                      </Col>
                    </Row>
                    <div className="line"/>
                  </div>
                )}
                <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>
              </div>
            </section>
          </div>
        </div>
        <Modal isOpen={isModalShow} toggle={modalShow} className={this.props.className}>
          <ModalHeader>Xabar yuborish</ModalHeader>
          <AvForm onSubmit={handleSubmit}>
            <ModalBody>
              <Row>
                <Col md={{size: 10, offset: 1}}>
                  <label>Matn</label>
                    <AvField name="text" type="textarea" cols={30} rows={6} placeholder="..."
                             required/>
                </Col>
              </Row>
              <Row>
                <Col md={{size: 10, offset: 1}}>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="eventId" type="select">
                      <option value="all">Barcha foydalanuvchilarga</option>
                      {events.map((item, i) =>
                        <option value={item.id}>{item.title}</option>
                      )}
                    </AvInput>
                    <label>Qaysi tadbirga?</label>
                  </AvGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button className="btn-sm" color="primary">Saqlash</Button>
              <Button className="btn-sm" color="secondary" onClick={modalShow}>Bekor qilish</Button>
            </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

PositionType.propTypes = {
  dispatch: PropTypes.func
}

export default PositionType;
