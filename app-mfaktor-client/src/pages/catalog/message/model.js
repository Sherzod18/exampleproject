import api from 'api'
import {toast} from 'react-toastify';

const {addMessage, getOpenEvents, getMessages} = api;

export default ({
  namespace: 'messageModel',
  state: {
    positions: [],
    isModalShow: false,
    deleteId: 0,
    pathname: '',
    events: [],
    messages: []
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getEvents({payload}, {put, call, select}) {
      const res = yield call(getOpenEvents);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            events: res.object
          },
        });
      }
    },
    * getMessages({payload}, {put, call, select}) {
      const res = yield call(getMessages);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            messages: res.object
          },
        });
      }
    },
    * sendMessage({payload}, {put, call, select}) {
      const res = yield call(addMessage, payload);
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            isModalShow: false
          }
        });
        yield put({
          type: 'getMessages'
        });
      } else {
        toast.error(res.message);
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
