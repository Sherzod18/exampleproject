import React, {Component} from 'react';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table,
  Pagination, PaginationItem, PaginationLink, InputGroup, InputGroupAddon, Input, Label, FormGroup
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvInput, AvFeedback} from 'availity-reactstrap-validation';
import {connect} from 'react-redux';
import {Editor} from '@tinymce/tinymce-react';
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({post}) => ({post}))
class Post extends Component {
  componentDidMount() {
    const {dispatch, post} = this.props;
    dispatch({
      type: 'post/getCategories'
    })
    dispatch({
      type: 'post/getArticles'
    })
  }

  render() {
    const {dispatch, post} = this.props;
    const {isModalShow, pathname, delModalShow, list, current, modalType, photoId, categories, description, paginations, page, deleteId} = post;
    const modalShow = () => {
      dispatch({
        type: 'post/updateState',
        payload: {
          modalType: 'new',
          photoId: '',
          isModalShow: !isModalShow
        }
      })
    }
    const handleSubmit = (e, er, v) => {
      if (modalType === 'edit') {
        v = {...v, id: current.id}
      }
      dispatch({
        type: 'post/save',
        payload: {
          ...v
        }
      })
    };
    const uploadAvatar = (e) => {
      dispatch({
        type: 'post/uploadAvatar',
        payload: {
          file: e.target.files,
          fileUpload: true,
          // type:'ARTICLE_BANNER'
        }
      })
    };
    const onChangeEditor = (e) => {
      dispatch({
        type: 'post/updateState',
        payload: {
          [e.target.id]: e.target.getContent()
        }
      })
    };
    const deleteArticle = () => {
      dispatch({
        type: 'post/deleteArticle',
        payload: {
          id: deleteId
        }
      });
      deleteModalShow(-1);
    }
    const editArticle = (item) => {
      dispatch({
        type: 'post/updateState',
        payload: {
          modalType: 'edit',
          isModalShow: !isModalShow,
          current: item,
          description: item.description,
          photoId: item.photo.id
        }
      })
    };
    const getArticles = (e, page) => {
      e.preventDefault();
      dispatch({
        type: 'post/getArticles',
        payload: {
          page
        }
      })
    };
    const deleteModalShow = (id) => {
      if (id !== -1) {
        dispatch({
          type: 'post/updateState',
          payload: {
            deleteId: id
          }
        });
      }
      dispatch({
        type: 'post/updateState',
        payload: {
          delModalShow: !delModalShow
        }
      });
    };
    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="3">
                    <h2 className="font-weight-bolder">Rasm</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Sarlavha</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Kategoriya</h2>
                  </Col>
                </Row>
                <main>
                  {list.map((item, a) =>
                    <div key={a} className="client-list-item pt-2">
                      <Row>
                        <Col md="3" className="text-center">
                          {item.photo === '' ?
                            <div className="client-avatar"></div> :
                            <img src={'/api/file/' + item.photo.id} alt="userlogo" className="mr-3"/>}

                        </Col>
                        <Col md="3" className="text-center">
                          <p>{item.title}</p>
                        </Col>
                        <Col md="3" className="text-center">
                          <p>{item.category.nameUz}</p>

                        </Col>
                        <Col md="3 ml-auto">
                          <button className="btn btn-edit-28 p-0" onClick={() => editArticle(item)}><span
                            className="icon icon-edit"/></button>
                          <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(item.id)}><span
                            className="icon icon-delete"/></button>
                        </Col>
                      </Row>
                      <div className="line"/>

                    </div>)}
                  <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>


                </main>
                <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
                  <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
                  <ModalFooter>

                    <Button className="btn-sm" color="primary" onClick={deleteArticle}>Ha</Button>
                    <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
                  </ModalFooter>
                </Modal>
                <Modal isOpen={isModalShow} toggle={modalShow} className="custom-modal">
                  <ModalHeader toggle={modalShow} className="border-0"></ModalHeader>
                  <ModalBody className="px-5">
                    <h3 className="font-weight-bolder">Yangi maqola qo'shish</h3>
                    <AvForm onSubmit={handleSubmit}>
                      <Row>

                        <div className="col-md-4">
                          <div className="speaker-image mt-3">
                            <label htmlFor="eventImg" style={{width: '100%', backgroundColor: 'unset'}}
                                   className="label-for-img">
                              <div className="row image-upload">
                                <img style={{width: '100%', borderRadius: '5px'}}
                                     src={photoId ? '/api/file/' + photoId : ''}
                                     width="100"/>
                                <div
                                  style={photoId ? {display: 'none'} : {display: 'block'}}
                                  className="col-md-12 upload-box text-center">
                                  <div className=" upload-button">
                                    <span className="icon icon-upload"/> Maqola uchun rasm
                                    <input className="d-none" name="attachment" id="eventImg"
                                           onChange={uploadAvatar}
                                           type="file" required/>
                                  </div>
                                </div>
                              </div>
                            </label>
                          </div>
                        </div>

                        <Col md="8">
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput name="title" id="title" type="text" required
                                     value={modalType === 'new' ? '' : current.title}/>
                            <label>Sarlavxa</label>
                          </AvGroup>
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput type="select" label="" name="categoryId"
                                     value={modalType === 'new' ? '' : current.category ? current.category.id : ''}
                                     required>
                              <option key="title" value=''>Kategoriyani tanlang</option>
                              {categories.map((c, b) => <option key={b} value={c.id}>{c.nameUz}</option>)}
                            </AvInput>
                            <label>Kategoriya</label>
                          </AvGroup>
                          <AvGroup>
                            <Editor
                              id="description"
                              initialValue={modalType === "new" ? "" : description}
                              onChange={onChangeEditor}
                              apiKey="nk94j5woa7gcg2ysyz6c7kqee1ahwmv854m5k61qke02boxk"
                              init={{
                                plugins: 'link code fullscreen',
                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code fullscreen'
                              }}
                            />
                          </AvGroup>
                        </Col>
                      </Row>
                      <div className="row mt-3">
                        <div className="col-md-3 offset-6">
                          <Button type="button" outline block color="secondary" onClick={modalShow}>Bekor
                            qilish</Button>
                        </div>
                        <div className="col-md-3">
                          <Button type="submit" block
                                  color="success">{modalType === 'new' ? 'Saqlash' : 'Tahrirlash'}</Button>
                        </div>
                      </div>
                    </AvForm>
                  </ModalBody>
                </Modal>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default Post;
