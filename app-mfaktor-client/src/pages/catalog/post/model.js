import api from 'api'
import {toast} from 'react-toastify';

const {savePost, uploadFile, getCategories, getArticles, deleteArticle, getTotalPages} = api;
function pagination(page, numPages) {
  var res = [];

  var from = 1;
  var to = numPages;
  if (numPages > 10) {
    from = Math.max(page - 2, 1);
    to = Math.max(Math.min(page + 2, numPages), 5);
    if (to > 5) {
      res.push(1);
      if (from > 2) res.push(2);
      if (from > 3) {
        if (from === 4) {
          res.push(3);
        } else {
          res.push("...");
        }
      }
    }
  }

  for (var i = from; i <= to; i++) {

    res.push(i);

  }

  if (numPages > 10) {
    if (to < (numPages - 2)) {
      if (to === 8) {
        res.push(9);
      } else {
        res.push("...");
      }
    }
    if (to < numPages)
      res.push(numPages - 1);
    if (to !== numPages)
      res.push(numPages);
  }

  return res;
}
export default ({
  namespace: "post",
  state: {
    list: [],
    current: {},
    isModalShow: false,
    delModalShow: false,
    modalType: 'new',
    photoId: '',
    deleteId: 0,
    categories: [],
    description: '',
    size: 10,
    page: 0,
    paginations: [],
    pathname: ''
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * uploadAvatar({payload}, {call, put, select}) {
      const res = yield call(uploadFile, {payload});
      if (res.success){
        yield put({
          type: 'updateState',
          payload: {
            photoId: res.object[0].fileId,
          }
        })
      }else {
        toast.error(res.message);
      }
    },
    * save({payload}, {call, put, select}){
      const {photoId, description} = yield select(_ => _.post);
      //var str = description.replace(/background-color: #ffffff;/g, '');
      const res = yield call(savePost, {...payload, photoId, description});
      if (res.success) {
        toast.success("Saqlandi!");
        yield put({
          type: 'updateState',
          payload: {isModalShow: false}
        })
        yield put({
          type: 'getArticles'
        })
      }else {
        toast.error("Saqlashda xatolik!")
      }
    },
    * deleteArticle({payload}, {call, put, select}){
      const res = yield call(deleteArticle, {id:payload.id});
      if (res.success){
        toast.success("O'chirildi!");
        yield put({
          type: 'getArticles'
        })
      }
      else {
        if (res.statusCode === 403){
          toast.error("O'chirishga ruxsatingiz yo'q");
        } else {
          toast.error("O'chirishda xatolik!");
        }
      }
    },
    * getArticles({payload}, {call, put, select}){
      let {size, page} = yield select(_ => _.post);
      if (payload) {
        page = payload.page
        yield put({
          type: 'updateState',
          payload: {page: payload.page}
        })
      }
      const res = yield call(getArticles, {size, page});
      const totalPages = yield call(getTotalPages, {size, page})
      yield put({
        type: 'updateState',
        payload: {
          paginations: pagination(page, totalPages.data),
          list: res.object
        }
      })
    },
    * getCategories({payload}, {call, put, select}){
      const res = yield call(getCategories);
      yield put({
        type: 'updateState',
        payload: {
          categories: res._embedded.list
        }
      })
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
