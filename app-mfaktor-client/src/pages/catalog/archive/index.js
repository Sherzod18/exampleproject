import React, {Component} from 'react';
import {Button, Card, CardFooter, CardImg, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import {Loader} from "react-loader-spinner";
import CatalogSidebar from "components/CatalogSidebar";
import {AvForm, AvInput} from "availity-reactstrap-validation";

@connect(({archiveModel}) => ({archiveModel}))
class Index extends Component {
  componentWillMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'archiveModel/getEvents'
    });
  }

  render() {
    const {dispatch, archiveModel} = this.props;
    const {events, loading, pathname, status, page, size} = archiveModel;

    const getEvents = (e, v) => {
      dispatch({
        type: 'archiveModel/updateState',
        payload: {
          status: v
        }
      });
      dispatch({
        type: 'archiveModel/getEvents',
      });

    };

    const moreEvent = () => {
      dispatch({
        type: 'archiveModel/updateState',
        payload: {
          page: page + 1,
          size: size
        }
      });
      dispatch({
        type: 'archiveModel/getEvents',
      })
    };


    return (
      <div>
        <div
          className="catalog">
          <CatalogSidebar
            pathname={pathname}
          />
          <div
            className="catalog-menu"
            style={
              {
                padding: '1rem 1rem'
              }
            }>
            <
              section>
              {loading ?
                <div
                  className="col-md-4 offset-4 text-center"
                  style={
                    {
                      paddingTop: '150px', marginBottom:
                        '150px'
                    }
                  }>
                  <
                    Loader
                    type="Triangle"
                    color="#ED2939"
                    height="100"
                    width="100"/></div> :
                <div><Row
                  className="ml-1 mb-3">
                  <AvForm>
                    <label> Tadbirlarni
                      saralash </label>
                    <AvInput
                      type="select"
                      value="0"
                      name="status"
                      onChange={getEvents}>
                      <option
                        value="0"
                        disabled> Tadbirlarni
                        saralash
                      </option>
                      <option
                        value="all"> Hammasi
                      </option>
                      <option
                        value="archive"> Zaxiraga
                        olingan
                      </option>
                      <option
                        value="cancel"> Bekor
                        qilingan
                      </option>
                    </AvInput>
                  </AvForm>
                </Row>

                  <Row
                    className="icon-row">
                    {
                      events.map((item, i) =>
                        <Col className="col-md-12 mb-4">
                          <Card className="card-list">
                            <div className="d-flex">
                              <div className="w-30">
                                <CardImg src={item.photoUrl} className="rounded-0"/>
                              </div>
                              <div className="w-40 py-4">
                                <h3 className="font-weight-light px-5"> {item.isOpen ?
                                  <Link to={"/event/clients/" + item.id}><h3
                                    className="event_name"> {item.title} </h3></Link>
                                  : <
                                    h3
                                    className="event_name"> {item.title} </h3>}</h3>
                                <div
                                  className="d-flex px-5 pt-4">
                                  <div
                                    className="w-50">
                                    <p
                                      className="m-0 text-muted small"> Spiker </p>
                                    <p
                                      className="m-0 font-weight-bold text-body"> {item.speakers} </p>
                                  </div>
                                  <div
                                    className="w-50">
                                    <p
                                      className="m-0 small text-muted"> Vaqti </p>
                                    <p
                                      className="m-0 font-weight-bold text-body">
                                      {
                                        item
                                          .startTime ? item.startTime.substring(0, 10) + ' ' + new Date(item.startTime).toTimeString().substring(0, 5) : ''
                                      } </p>
                                  </div>
                                </div>
                              </div>
                              <div
                                className="w-30 pt-4">
                                <CardFooter
                                  className="bg-transparent position-relative border-0">
                                  <div
                                    className="card-price">
                                    <div
                                      className="d-flex">

                                      <div
                                        className="w-33 text-center ">
                                        <h1
                                          className="m-0 font-weight-bold">
                                          {item.allSeat}
                                        </h1>
                                        <p
                                          className="small font-weight-light text-muted">
                                          Joylar
                                        </p>
                                      </div>
                                      <div
                                        className="w-33 text-center ">
                                        <h1
                                          className="m-0 font-weight-bold">
                                          {(item.bookedAndPartly)
                                          }
                                        </h1>
                                        <p className="small font-weight-light text-muted">
                                          Band
                                        </p>
                                      </div>
                                      <div
                                        className="w-33 text-center ">
                                        <h1
                                          className="m-0 font-weight-bold text-success">
                                          {(item.allSeat) - (item.emptySeat) - item.bookedAndPartly
                                          }
                                        </h1>
                                        <p
                                          className="small font-weight-light text-muted">
                                          Sotilgan
                                        </p>
                                      </div>
                                      <div
                                        className="w-33 text-center ">
                                        <h1
                                          className="m-0 font-weight-bold text-danger">
                                          {item.emptySeat}
                                        </h1>
                                        <p
                                          className="small font-weight-light text-muted">
                                          Sotilmagan
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                  <div
                                    className="card-price">
                                    <div
                                      className="d-flex">
                                      <div
                                        className="text-center d-flex pl-4">
                                        <div
                                          className="rate pt-4 pl-2">
                                          <p
                                            className="price-sum font-weight-bold">
 <span
   className="pr-3 "> <img
   className="eyes-img"
   src="/assets/images/eye.png"
   alt=""/> </span>
                                            {
                                              (new Intl.NumberFormat('fr-FR').format(item.sum))
                                            }
                                            <span> UZS </span></p>
                                          <p className="all-sum"> Umumiy
                                            yig’ilgan
                                            pul
                                            miqdori </p>
                                        </div>
                                      </div>

                                    </div>
                                  </div>
                                </CardFooter>
                              </div>
                            </div>
                          </Card>
                        </Col>)}
                  </Row>
                  <Row
                    className="mt-4 mt-md-5">
                    <Col md={{size: 2, offset: '5'}}
                         className="col-6 mb-5">
                      <Button onClick={moreEvent}
                        color="danger"
                        className="btn-block">
                        Yana
                        yuklash
                      </Button>
                    </Col>
                  </Row>
                </div>}
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default Index;
