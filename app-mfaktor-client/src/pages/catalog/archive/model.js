import api from "api";


const {getArchiveAndCancelEvents} = api;
export default ({
  namespace: 'archiveModel',
  state: {
    totalPages: 0,
    page: 0,
    size: 10,
    status: 'all',
    events: [],
    loading: false,
    pathname: ""
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getEvents({payload}, {call, put, select}) {
      let {page, size, status, events} = yield select(_ => _.archiveModel);
      console.log(events)
      payload = {page, size, status};
      const res = yield call(getArchiveAndCancelEvents, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            page,
            totalPages: res.object.totalPages,
            events: res.object.resEvents.concat(events),
            loading: false
          }
        })
      }
      console.log(events)
    },

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
