import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';
import {router} from "umi";
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({payType}) => ({payType}))
class PayType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type:'payType/getPayTypes'
    })
  }


  render() {
    const {dispatch, payType} = this.props;
    const {isModalShow, payTypes, currentPayType, modalType,
      delModalShow, deleteId, pathname} = payType;

    const modalShow = () => {
      dispatch({
        type:'payType/updateState',
        payload:{
          modalType:'new'
        }
      });
      dispatch({
        type: 'payType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };
    const deleteModalShow = (id) => {
      if (id !== -1){
        dispatch({
          type:'payType/updateState',
          payload:{
            deleteId: id
          }
        });
      }
      dispatch({
        type:'payType/updateState',
        payload:{
          delModalShow: !delModalShow
        }
      });
    };
    const handleSubmit = (payment, errors, values) => {
      if (errors.length === 0) {
        if (modalType==='new') {
          dispatch({
            type:'payType/savePayType',
            payload: {
              ...values
            }
          });
        }else {
          dispatch({
            type:'payType/editPayType',
            payload: {
              ...values,
              id:currentPayType.id
            }
          });
        }
      }
    };
    const deletePayType = () => {
      dispatch({
        type: "payType/deletePayType",
        payload: {
          payDel: deleteId
        }
      });
      deleteModalShow(-1);
    };
    const editPayType = (id) => {
      dispatch({
        type:'payType/updateState',
        payload:{
          modalType:'update'
        }
      });
      dispatch({
        type: 'payType/getPayType',
        payload:{
          path:id,
        }
      });
      dispatch({
        type: 'payType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };

    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="2">
                    <h2 className="font-weight-bolder">O'zbek</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Русский</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Rangi</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Izoh</h2>
                  </Col>
                </Row>
                {payTypes !== undefined ? payTypes.map((pType,a) =>
                  <div key={a} className="client-list-item pt-2">
                    <Row>
                      <Col md="2" className="text-center">
                        <p>{pType.nameUz}</p>
                      </Col>
                      <Col md="3" className="text-center">
                        <p>{pType.nameRu}</p>
                      </Col>
                      <Col md="2" className="text-center">
                        <p className="payColor" style={{background:pType.color}}/>
                      </Col>
                      <Col md="3" className="text-center">
                        <p>{pType.description}</p>
                      </Col>
                      <Col md="2 ml-auto">
                        <button className="btn btn-edit-28 p-0" style={{left:"10%"}} onClick={()=> editPayType(pType.id)}><span className="icon icon-edit" /></button>
                        <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(pType.id)}><span className="icon icon-delete" /></button>
                      </Col>
                    </Row>
                    <div className="line" />
                  </div>
                ) : ''}
                <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>
              </div>
            </section>
          </div>
        </div>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
          <ModalFooter>
            <Button className="btn-sm" color="primary" onClick={deletePayType}>Ha</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isModalShow} toggle={modalShow} className={this.props.className}>
          <ModalHeader>{modalType==='new'? 'To`lov turini saqlash':'To`lov turini tahrirlash'}</ModalHeader>
          <AvForm  onSubmit={handleSubmit}>
          <ModalBody>
            <Row>
              <Col md={{size: 10, offset:1}}>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="nameUz"  value={modalType==='new'? '' : currentPayType.nameUz} type="text" required/>
                  <label>O'zbek</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="nameRu"  value={modalType==='new'? '' : currentPayType.nameRu} type="text" required/>
                  <label>Русский</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="color"  value={modalType==='new'? '' : currentPayType.color} type="color" required/>
                  <label>Rangi</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="description"  value={modalType==='new'? '' : currentPayType.description} type="text" required/>
                  <label>Izoh</label>
                </AvGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-sm" color="primary">{modalType==='new'? 'Saqlash':'Tahrirlash'}</Button>
            <Button className="btn-sm" color="secondary" onClick={modalShow}>Bekor qilish</Button>
          </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

PayType.propTypes = {
  dispatch: PropTypes.func
}

export default PayType;
