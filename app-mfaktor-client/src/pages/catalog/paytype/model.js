import api from 'api'
import {toast} from 'react-toastify';

const {addPayType, getPayTypes, getPayType, editPayType, deletePayType} = api;

export default ({
  namespace: 'payType',
  state: {
    payTypes: [],
    isModalShow: false,
    delModalShow: false,
    currentPayType:{},
    modalType:'new',
    deleteId: 0,
    pathname: ""
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getPayTypes({payload}, {put, call, select}) {
      const res = yield call(getPayTypes);
      yield put({
        type: 'updateState',
        payload: {
          payTypes: res._embedded.list
        },
      });
    },
    * savePayType({payload}, {put, call, select}) {
      const res = yield call(addPayType, payload);
      if (res.success){
        toast.success("Saqlandi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getPayTypes'
        });
      }else{
        if (res.statusCode === 403){
          toast.error("Saqlashga ruxsatingiz yo'q!");
        } else {
          toast.error("Saqlashda xatolik!");
        }
      }
    },
    * editPayType({payload}, {put, call, select}) {
      const res = yield call(editPayType, {path:payload.id, ...payload});
      if (res.success){
        toast.success("O'zgartirildi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getPayTypes'
        });
      }else {
        if (res.statusCode === 403){
          toast.error("O'zgartirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'zgartirishda xatolik!");
        }
      }
    },
    * deletePayType({payload}, {put, call, select}){
      const del = yield call(deletePayType, {id:payload.payDel});
      if (del.success) {
        toast.success("O'chirildi!");
        yield put({
          type: 'getPayTypes'
        });
      }else {
        if (del.statusCode === 403){
          toast.error("O'chirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'chirishda xatolik!");
        }
      }
    },

    * getPayType ({payload}, {put, call, select}){
      const res = yield call(getPayType, payload);
      res.id = payload.path;
      yield put({
        type:'updateState',
        payload:{
          currentPayType:res
        }
      })

    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
