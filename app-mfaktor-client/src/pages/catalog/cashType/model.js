import api from 'api'
import {toast} from 'react-toastify';

const {addCashType, getCashTypes, getCashType, editCashType, deleteCashType} = api;

export default ({
  namespace: 'cashType',
  state: {
    cashTypes: [],
    isModalShow: false,
    delModalShow: false,
    currentCashType: {},
    modalType: 'new',
    deleteId: 0,
    pathname: ""
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getCashTypes({payload}, {put, call, select}) {
      const res = yield call(getCashTypes);
      yield put({
        type: 'updateState',
        payload: {
          cashTypes: res.object
        },
      });
    },
    * saveCashType({payload}, {put, call, select}) {
      const res = yield call(addCashType, payload);
      if (res.success) {
        toast.success("Saqlandi!");
        yield put({
          type: 'updateState',
          payload: {
            isModalShow: false
          }
        });
        yield put({
          type: 'getCashTypes'
        });
      } else {
        if (res.statusCode === 403) {
          toast.error("Saqlashga ruxsatingiz yo'q!");
        } else {
          toast.error("Saqlashda xatolik!");
        }
      }
    },
    * deleteCashType({payload}, {put, call, select}) {
      const del = yield call(deleteCashType, {id: payload.payDel});
      if (del.success) {
        toast.success("O'chirildi!");
        yield put({
          type: 'getCashTypes'
        });
      } else {
        if (del.statusCode === 403) {
          toast.error("O'chirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'chirishda xatolik!");
        }
      }
    },

    * getCashType({payload}, {put, call, select}) {
      const res = yield call(getCashType, payload);
      res.id = payload.path;
      yield put({
        type: 'updateState',
        payload: {
          currentCashType: res
        }
      })

    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
