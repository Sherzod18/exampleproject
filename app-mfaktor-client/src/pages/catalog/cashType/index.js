import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Button, Col, Modal, ModalBody, ModalFooter, ModalHeader, Row} from 'reactstrap';
import {AvFeedback, AvForm, AvGroup, AvInput} from 'availity-reactstrap-validation';
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({cashType, app}) => ({cashType, app}))
class CashType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'cashType/getCashTypes'
    });
    dispatch({
      type: 'app/getPayTypes',
    });
  }

  render() {
    const {dispatch, cashType, app} = this.props;
    const {
      isModalShow, cashTypes, currentCashType, modalType,
      delModalShow, deleteId, pathname
    } = cashType;
    const {payTypes} = app;

    const modalShow = () => {
      dispatch({
        type: 'cashType/updateState',
        payload: {
          modalType: 'new'
        }
      });
      dispatch({
        type: 'cashType/updateState',
        payload: {
          isModalShow: !isModalShow
        }
      });
    };

    const deleteModalShow = (id) => {
      if (id !== -1) {
        dispatch({
          type: 'cashType/updateState',
          payload: {
            deleteId: id
          }
        });
      }
      dispatch({
        type: 'cashType/updateState',
        payload: {
          delModalShow: !delModalShow
        }
      });
    };

    const handleSubmit = (payment, errors, values) => {
      if (errors.length === 0) {
        if (modalType === 'new' || modalType === 'edit') {
          dispatch({
            type: 'cashType/saveCashType',
            payload: {
              ...values,
              id: currentCashType.id
            }
          });
        }
      }
    };

    const deleteCashType = () => {
      dispatch({
        type: "cashType/deleteCashType",
        payload: {
          payDel: deleteId
        }
      });
      deleteModalShow(-1);
    };

    const editCashType = (item) => {
      dispatch({
        type: 'cashType/updateState',
        payload: {
          currentCashType: item,
          modalType: 'edit'
        }
      });
      dispatch({
        type: 'cashType/updateState',
        payload: {
          isModalShow: !isModalShow
        }
      });
    };

    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="2">
                    <h2 className="font-weight-bolder">O'zbek</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Русский</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">To'lov turlari</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Rangi</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Izoh</h2>
                  </Col>
                </Row>
                {cashTypes !== undefined ? cashTypes.map((item, a) =>
                  <div key={a} className="client-list-item pt-2">
                    <Row>
                      <Col md="2" className="text-center">
                        <p>{item.nameUz}</p>
                      </Col>
                      <Col md="3" className="text-center">
                        <p>{item.nameRu}</p>
                      </Col>
                      <Col md="2" className="text-center">
                        <p>{item.payTypes}</p>
                      </Col>
                      <Col md="2" className="text-center">
                        <p className="payColor" style={{background: item.color}}/>
                      </Col>
                      <Col md="3" className="text-center">
                        <p>{item.description}</p>
                      </Col>
                      <Col md="2 ml-auto">
                        <button className="btn btn-edit-28 p-0" style={{left: "10%"}}
                                onClick={() => editCashType(item)}><span className="icon icon-edit"/></button>
                        <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(item.id)}><span
                          className="icon icon-delete"/></button>
                      </Col>
                    </Row>
                    <div className="line"/>
                  </div>
                ) : ''}
                <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>
              </div>
            </section>
          </div>
        </div>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
          <ModalFooter>
            <Button className="btn-sm" color="primary" onClick={deleteCashType}>Ha</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isModalShow} toggle={modalShow} className={this.props.className}>
          <ModalHeader>{modalType === 'new' ? 'To`lov turini saqlash' : 'To`lov turini tahrirlash'}</ModalHeader>
          <AvForm onSubmit={handleSubmit}>
            <ModalBody>
              <Row>
                <Col md={{size: 10, offset: 1}}>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="nameUz" value={modalType === 'new' ? '' : currentCashType.nameUz} type="text"
                             required/>
                    <label>O'zbek</label>
                  </AvGroup>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="nameRu" value={modalType === 'new' ? '' : currentCashType.nameRu} type="text"
                             required/>
                    <label>Русский</label>
                  </AvGroup>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="color" value={modalType === 'new' ? '' : currentCashType.color} type="color"/>
                    <label>Rangi</label>
                  </AvGroup>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="description" value={modalType === 'new' ? '' : currentCashType.description}
                             type="text" required/>
                    <label>Izoh</label>
                  </AvGroup>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput type="select" value={modalType === 'new' ? '' : currentCashType.payTypeIds} multiple
                             name="payTypeIds">
                      {payTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                    </AvInput>
                    <label>To'lov turi</label>
                  </AvGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button className="btn-sm" color="primary">{modalType === 'new' ? 'Saqlash' : 'Tahrirlash'}</Button>
              <Button className="btn-sm" color="secondary" onClick={modalShow}>Bekor qilish</Button>
            </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

CashType.propTypes = {
  dispatch: PropTypes.func
};

export default CashType;
