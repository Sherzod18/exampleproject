import api from 'api'
import {toast} from 'react-toastify';

const {addExpenseType, getExpenseTypes, getExpenseType, editExpenseType, deleteExpenseType} = api;

export default ({
  namespace: 'expenseType',
  state: {
    expenseTypes: [],
    isModalShow: false,
    delModalShow: false,
    currentExpenseType:{},
    modalType:'new',
    deleteId: 0,
    pathname:""
  },
  subscriptions: { setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }},
  effects: {
    * saveExpenseType({payload}, {put, call, select}) {
      const res = yield call(addExpenseType, payload);
      if (res.success){
        toast.success("Saqlandi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getExpenseTypes'
        });
      }else{
        if (res.statusCode === 403){
          toast.error("Saqlashga ruxsatingiz yo'q!");
        } else {
          toast.error("Saqlashda xatolik!");
        }
      }
    },
    * editExpenseType({payload}, {put, call, select}) {
      const res = yield call(editExpenseType, {path:payload.id, ...payload});
      if (res.success){
        toast.success("O'zgartirildi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getExpenseTypes'
        });
      }else {
        if (res.statusCode === 403){
          toast.error("O'zgartirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'zgartirishda xatolik!");
        }
      }
    },
    * deleteExpenseType({payload}, {put, call, select}){
      const del = yield call(deleteExpenseType, {id:payload.expenceDel});
      if (del.success) {
        toast.success("O'chirildi!");
        yield put({
          type: 'getExpenceTypes'
        });
      }else {
        if (del.statusCode === 403){
          toast.error("O'chirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'chirishda xatolik!");
        }
      }
    },
    * getExpenseType ({payload}, {put, call, select}){
      const res = yield call(getExpenseType, payload);
      res.id = payload.path;
      yield put({
        type:'updateState',
        payload:{
          currentExpenseType:res
        }
      })
    },
    * getExpenseTypes({payload}, {put, call, select}) {
      const res = yield call(getExpenseTypes);
      yield put({
        type: 'updateState',
        payload: {
          expenseTypes: res._embedded.list
        },
      });
    },


  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
