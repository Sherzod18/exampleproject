import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table
} from 'reactstrap';
import {AvForm, AvField, AvFeedback, AvInput, AvGroup} from 'availity-reactstrap-validation';
import {router} from "umi";
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({expenseType}) => ({expenseType}))
class ExpenseType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'expenseType/getExpenseTypes'
    })
  }

  render() {
    const {dispatch, expenseType} = this.props;
    const {
      isModalShow, expenseTypes, currentExpenseType, modalType,
      delModalShow, deleteId, pathname
    } = expenseType;

    const modalShow = () => {
      dispatch({
        type: 'expenseType/updateState',
        payload: {
          modalType: 'new'
        }
      })
      dispatch({
        type: 'expenseType/updateState',
        payload: {
          isModalShow: !isModalShow
        }
      });
    };
    const deleteModalShow = (id) => {
      if (id !== -1) {
        dispatch({
          type: 'expenseType/updateState',
          payload: {
            deleteId: id
          }
        });
      }
      dispatch({
        type: 'expenseType/updateState',
        payload: {
          delModalShow: !delModalShow
        }
      });
    };
    const handleSubmit = (payment, errors, values) => {
      if (errors.length === 0) {
        if (modalType === 'new') {
          dispatch({
            type: 'expenseType/saveExpenseType',
            payload: {
              ...values
            }
          });
        } else {
          dispatch({
            type: 'expenseType/editExpenseType',
            payload: {
              ...values,
              id: currentExpenseType.id
            }
          });
        }
      }
    };
    const deleteExpenseType = () => {
      dispatch({
        type: "expenseType/deleteExpenseType",
        payload: {
          expenseDel: deleteId
        }
      });
      deleteModalShow(-1);
    };
    const editExpenseType = (id) => {
      dispatch({
        type: 'expenseType/updateState',
        payload: {
          modalType: 'update'
        }
      })
      dispatch({
        type: 'expenseType/getExpenseType',
        payload: {
          path: id,
        }
      });
      dispatch({
        type: 'expenseType/updateState',
        payload: {
          isModalShow: !isModalShow
        }
      });
    };
    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="3">
                    <h2 className="font-weight-bolder">O'zbek</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Русский</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Izox</h2>
                  </Col>
                </Row>
                {expenseTypes !== undefined ? expenseTypes.map((expenseType) =>
                  <div className="client-list-item pt-2">
                    <Row>
                      <Col md="3" className="text-center">
                        <p>{expenseType.nameUz}</p>
                      </Col>
                      <Col md="3" className="text-center">
                        <p>{expenseType.nameRu}</p>
                      </Col>
                      <Col md="3" className="text-center">
                        <p>{expenseType.description}</p>
                      </Col>
                      <Col md="2 ml-auto">
                        <button className="btn btn-edit-28 p-0" onClick={() => editExpenseType(expenseType.id)}><span
                          className="icon icon-edit"/></button>
                        <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(expenseType.id)}><span
                          className="icon icon-delete"/></button>
                      </Col>
                    </Row>
                    <div className="line"/>
                  </div>
                ) : ''}
                <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>
              </div>
            </section>
          </div>
        </div>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
          <ModalFooter>
            <Button className="btn-sm" color="primary" onClick={deleteExpenseType}>Ha</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isModalShow} toggle={modalShow} className={this.props.className}>
          <ModalHeader>{modalType === 'new' ? 'Xarajat turlari' : 'Xarajatlarni Tahrirlash'}</ModalHeader>
          <AvForm onSubmit={handleSubmit}>
            <ModalBody>
              <Row>
                <Col md={{size: 10, offset: 1}}>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="nameUz" value={modalType === 'new' ? '' : currentExpenseType.nameUz} type="text"
                             required/>
                    <label>O'zbek</label>
                  </AvGroup>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="nameRu" value={modalType === 'new' ? '' : currentExpenseType.nameRu} type="text"
                             required/>
                    <label>Русский</label>
                  </AvGroup>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="description" value={modalType === 'new' ? '' : currentExpenseType.description}
                             type="text" required/>
                    <label>Izoh</label>
                  </AvGroup>
                </Col>
              </Row>


            </ModalBody>
            <ModalFooter>
              <Button className="btn-sm" color="primary">{modalType === 'new' ? 'Saqlash' : 'Tahrirlash'}</Button>
              <Button className="btn-sm" color="secondary" onClick={modalShow}>Bekor qilish</Button>
            </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

ExpenseType.propTypes = {
  dispatch: PropTypes.func
};

export default ExpenseType;
