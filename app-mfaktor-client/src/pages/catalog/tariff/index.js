import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';
import {router} from "umi";
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({tariffType}) => ({tariffType}))
class AwareType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type:'tariffType/getTariffs'
    });
    dispatch({
      type:'tariffType/getEvents'
    });
  }

  render() {
    const {dispatch, tariffType} = this.props;
    const {isModalShow, tariffTypes, currentTariff, modalType, eventTypes,
      delModalShow, deleteId,pathname} = tariffType;

    const modalShow = () => {
      dispatch({
        type:'tariffType/updateState',
        payload:{
          modalType:'new'
        }
      });
      dispatch({
        type: 'tariffType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };
    const deleteModalShow = (id) => {
      if (id !== -1){
        dispatch({
          type:'tariffType/updateState',
          payload:{
            deleteId: id
          }
        });
      }
      dispatch({
        type:'tariffType/updateState',
        payload:{
          delModalShow: !delModalShow
        }
      });
    };
    const handleSubmit = (event, errors, values) => {
      values.eventType = "/api/eventType/" + values.eventType;
      if (errors.length === 0) {
        if (modalType==='new') {
          dispatch({
            type:'tariffType/saveTariffs',
            payload: {
              ...values
            }
          });
        }else {
          dispatch({
            type:'tariffType/editTariff',
            payload: {
              ...values,
              id:currentTariff.id
            }
          });
        }
      }
    };
    const deleteTariff = () => {
        dispatch({
          type: "tariffType/deleteTariff",
          payload: {
            id: deleteId
          }
        });
        deleteModalShow(-1);
    };
    const editTariffType = (id) => {
      dispatch({
        type:'tariffType/updateState',
        payload:{
          modalType:'update'
        }
      })
      dispatch({
        type: 'tariffType/get',
        payload:{
          path:id,
        }
      });
      dispatch({
        type: 'tariffType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };

    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="2">
                    <h2 className="font-weight-bolder">O'zbek</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Русский</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Narx</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Ticketlar soni</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Tadbir turi</h2>
                  </Col>
                </Row>
                {tariffTypes !== undefined ? tariffTypes.map((item) =>
                  <div className="client-list-item pt-2">
                    <Row>
                      <Col md="2" className="text-center">
                        <p>{item.nameUz}</p>
                      </Col>
                      <Col md="2" className="text-center">
                        <p>{item.nameRu}</p>
                      </Col>
                      <Col md="2" className="text-center">
                        <p>{item.price}</p>
                      </Col>
                      <Col md="2" className="text-center">
                        <p>{item.ticketCount}</p>
                      </Col>
                      <Col md="2" className="text-center">
                        <p>{item.eventTypeUz}</p>
                      </Col>
                      <Col md="2 ml-auto">
                        <button className="btn btn-edit-28 p-0" onClick={()=> editTariffType(item.id)}><span className="icon icon-edit" /></button>
                        <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(item.id)}><span className="icon icon-delete" /></button>
                      </Col>
                    </Row>
                    <div className="line" />
                  </div>
                ) : ''}
                <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>
              </div>
            </section>
          </div>
        </div>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
          <ModalFooter>
            <Button className="btn-sm" color="primary" onClick={deleteTariff}>Ha</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isModalShow} toggle={modalShow}>
          <ModalHeader>{modalType==='new'? 'Tarifni saqlash':'Tarifni tahrirlash'}</ModalHeader>
          <AvForm  onSubmit={handleSubmit}>
          <ModalBody >

            <Row>
              <Col md={{size: 10, offset:1}}>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="nameUz"  value={modalType==='new'? '' : currentTariff.nameUz} type="text" required/>
                  <label>O'zbek</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="nameRu"  value={modalType==='new'? '' : currentTariff.nameRu} type="text" required/>
                  <label>Русский</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="price"  value={modalType==='new'? '' : currentTariff.price} type="number" required/>
                  <label>Narx</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="ticketCount"  value={modalType==='new'? '' : currentTariff.ticketCount} type="number" required/>
                  <label>Ticketlar soni</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>

                  <AvInput name="eventType"  type="select"
                           value={modalType !== 'new' && currentTariff._embedded ? currentTariff._embedded.eventType.id : ''} required>
                  <option key="title" value=''>Tadbir turini tanlang</option>
                  {eventTypes !== undefined ? eventTypes.map((item) =>
                    <option key={item.id} value={item.id}>{item.nameUz}</option>
                  ) :''}
                  </AvInput>
                  <label>Tadbir turi</label>
                </AvGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-sm" color="primary">{modalType==='new'? 'Saqlash':'Tahrirlash'}</Button>
            <Button className="btn-sm" color="secondary" onClick={modalShow}>Bekor qilish</Button>
          </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

AwareType.propTypes = {
  dispatch: PropTypes.func
}

export default AwareType;
