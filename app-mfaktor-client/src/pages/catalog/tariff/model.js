import api from 'api'
import {toast} from 'react-toastify';

const {addTariff, editTariff, getTariffs, getTariff, deleteTariff, getEventTypes} = api;

export default ({
  namespace: 'tariffType',
  state: {
    tariffTypes: [],
    isModalShow: false,
    delModalShow: false,
    currentTariff:{},
    modalType:'new',
    eventTypes:[],
    deleteId: 0,
    pathname:''
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getTariffs({payload}, {put, call, select}) {
      const res = yield call(getTariffs);
      yield put({
        type: 'updateState',
        payload: {
          tariffTypes: res._embedded.list
        },
      });
    },
    * saveTariffs({payload}, {put, call, select}) {
      const res = yield call(addTariff, payload);
      if (res.success){
        toast.success("Saqlandi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getTariffs'
        });
      }else{
        if (res.statusCode === 403){
          toast.error("Saqlashga ruxsatingiz yo'q!");
        } else {
          toast.error("Saqlashda xatolik!");
        }
      }
    },
    * editTariff({payload}, {put, call, select}) {
      const res = yield call(editTariff, {path:payload.id, ...payload});
      if (res.success){
        toast.success("O'zgartirildi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getTariffs'
        });
      }else {
        if (res.statusCode === 403){
          toast.error("O'zgartirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'zgartirishda xatolik!");
        }
      }
    },
    * deleteTariff({payload}, {put, call, select}){
      const del = yield call(deleteTariff, {id:payload.id});
      if (del.success) {
        toast.success("O'chirildi!");
        yield put({
          type: 'getTariffs'
        });
      }else {
        if (del.statusCode === 403){
          toast.error("O'chirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'chirishda xatolik!");
        }
      }
    },

    * get({payload}, {put, call, select}){
      const res = yield call(getTariff, payload);
      res.id = payload.path;
      yield put({
        type:'updateState',
        payload:{
          currentTariff:res
        }
      })

    },
    * getEvents({payload}, {put, call, select}) {
      const res = yield call(getEventTypes);
      yield put({
        type: 'updateState',
        payload: {
          eventTypes: res._embedded.eventTypes
        },
      });
    }

    },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
