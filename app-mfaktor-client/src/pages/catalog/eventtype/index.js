import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';
import {router} from "umi";
import CatalogSidebar from "../../../components/CatalogSidebar";
import FooterAdmin from "../../../components/Layout/FooterAdmin";

@connect(({eventType}) => ({eventType}))
class EventType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type:'eventType/getEventTypes'
    })
  }

  render() {
    const {dispatch, eventType} = this.props;
    const {isModalShow, eventTypes, currentEventType, modalType,
      delModalShow, deleteId,pathname} = eventType;

    const modalShow = () => {
      dispatch({
        type:'eventType/updateState',
        payload:{
          modalType:'new'
        }
      })
      dispatch({
        type: 'eventType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };
    const deleteModalShow = (id) => {
      if (id !== -1){
        dispatch({
          type:'eventType/updateState',
          payload:{
            deleteId: id
          }
        });
      }
      dispatch({
        type:'eventType/updateState',
        payload:{
          delModalShow: !delModalShow
        }
      });
    };
    const handleSubmit = (event, errors, values) => {
      if (errors.length === 0) {
        if (modalType==='new') {
          dispatch({
            type:'eventType/saveEventType',
            payload: {
              ...values
            }
          });
        }else {
          dispatch({
            type:'eventType/editEventType',
            payload: {
              ...values,
              id:currentEventType.id
            }
          });
        }
      }
    };
    const deleteEventType = () => {
      dispatch({
        type: "eventType/deleteEventType",
        payload: {
          eventDel: deleteId
        }
      });
      deleteModalShow(-1);
    };
    const editEventType = (id) => {
      dispatch({
        type:'eventType/updateState',
        payload:{
          modalType:'update'
        }
      });
      dispatch({
        type: 'eventType/getEventType',
        payload:{
          path:id,
        }
      });
      dispatch({
        type: 'eventType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };

    return (
    <div className="catalog">
      <CatalogSidebar pathname={pathname}/>
      <div className="catalog-menu">
        <section className="client_page">
          <div className="container">
            <Row className="mt-4">
              <Col md="3">
                <h2 className="font-weight-bolder">O'zbek</h2>
              </Col>
              <Col md="3">
                <h2 className="font-weight-bolder">Русский</h2>
              </Col>
              <Col md="3">
                <h2 className="font-weight-bolder">Izox</h2>
              </Col>
            </Row>
            {eventTypes !== undefined ? eventTypes.map((item,a) =>
              <div key={a} className="client-list-item pt-2">
                <Row>
                  <Col md="3" className="text-center">
                    <p>{item.nameUz}</p>
                  </Col>
                  <Col md="3" className="text-center">
                    <p>{item.nameRu}</p>
                  </Col>
                  <Col md="3" className="text-center">
                    <p>{item.description}</p>
                  </Col>
                  <Col md="2 ml-auto">
                    <button className="btn btn-edit-28 p-0" onClick={()=> editEventType(item.id)}><span className="icon icon-edit" /></button>
                    <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(item.id)}><span className="icon icon-delete" /></button>
                  </Col>
                </Row>
                <div className="line" />
              </div>
            ) : ''}
            <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>
          </div>
        </section>
      </div>
      <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
        <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
        <ModalFooter>
          <Button className="btn-sm" color="primary" onClick={deleteEventType}>Ha</Button>
          <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
        </ModalFooter>
      </Modal>
      <Modal isOpen={isModalShow} toggle={modalShow} className={this.props.className}>
        <ModalHeader>{modalType==='new'? 'Tadbir turini saqlash':'Tadbir turini tahrirlash'}</ModalHeader>
        <AvForm  onSubmit={handleSubmit}>
          <ModalBody>
            <Row>
              <Col md={{size: 10, offset:1}}>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="nameUz"  value={modalType==='new'? '' : currentEventType.nameUz} type="text" required/>
                  <label>O'zbek</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="nameRu"  value={modalType==='new'? '' : currentEventType.nameRu} type="text" required/>
                  <label>Русский</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="description"  value={modalType==='new'? '' : currentEventType.description} type="text" required/>
                  <label>Izoh</label>
                </AvGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-sm" color="primary">{modalType==='new'? 'Saqlash':'Tahrirlash'}</Button>
            <Button className="btn-sm" color="secondary" onClick={modalShow}>Bekor qilish</Button>
          </ModalFooter>
        </AvForm>
      </Modal>
    </div>
    );
  }
}

EventType.propTypes = {
  dispatch: PropTypes.func
}

export default EventType;
