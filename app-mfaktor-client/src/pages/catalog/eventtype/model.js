import api from 'api'
import {toast} from 'react-toastify';

const {getEventTypes, addEventType,deleteEventType,getEventType,editEventType} = api;

export default ({
  namespace: 'eventType',
  state: {
    eventTypes: [],
    isModalShow: false,
    delModalShow: false,
    currentEventType:{},
    modalType:'new',
    deleteId: 0,
    pathname:''
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getEventTypes({payload}, {put, call, select}) {
      const res = yield call(getEventTypes);
      yield put({
        type: 'updateState',
        payload: {
          eventTypes: res._embedded.eventTypes
        },
      });
    },
    * saveEventType({payload}, {put, call, select}) {
      const res = yield call(addEventType, payload);
      if (res.success){
        toast.success("Saqlandi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getEventTypes'
        });
      }else{
        if (res.statusCode === 403){
          toast.error("Saqlashga ruxsatingiz yo'q!");
        } else {
          toast.error("Saqlashda xatolik!");
        }
      }
    },
    * editEventType({payload}, {put, call, select}) {
      const res = yield call(editEventType, {path:payload.id, ...payload});
      if (res.success){
        toast.success("O'zgartirildi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getEventTypes'
        });
      }else {
        if (res.statusCode === 403){
          toast.error("O'zgartirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'zgartirishda xatolik!");
        }
      }
    },
    * deleteEventType({payload}, {put, call, select}){
      const del = yield call(deleteEventType, {id:payload.eventDel});
      if (del.success) {
        toast.success("O'chirildi!");
        yield put({
          type: 'getEventTypes'
        });
      }else {
        if (del.statusCode === 403){
          toast.error("O'chirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'chirishda xatolik!");
        }
      }
    },
    * getEventType ({payload}, {put, call, select}){
      const res = yield call(getEventType, payload);
      res.id = payload.path;
      yield put({
        type:'updateState',
        payload:{
          currentEventType:res
        }
      })

    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
