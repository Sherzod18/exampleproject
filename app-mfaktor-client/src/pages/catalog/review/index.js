import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Col,
  Row,
  UncontrolledCollapse,
  CustomInput,
  PaginationItem,
  PaginationLink, Pagination
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';
import {router} from "umi";
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({review}) => ({review}))
class Review extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'review/getReviews'
    });
  }

  render() {
    const {dispatch, review} = this.props;
    const {currentReview, reviews, page, paginations,pathname} = review;

    const changePublished = (id) => {
      dispatch({
        type: 'review/changePublished',
        payload: {
          path: id,
        }
      })
    };

    const getReviews = (e, page) => {
      e.preventDefault();
      dispatch({
        type: 'review/getReviews',
        payload: {
          page
        }
      })
    };

    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="2">
                    <h2 className="font-weight-bolder">Rasmi</h2>
                  </Col>
                  <Col md="4">
                    <h2 className="font-weight-bolder">Ismi</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Familyasi</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Text</h2>
                  </Col>
                  <Col md="1">
                    <h2 className="font-weight-bolder">Saytda ko'rinishi</h2>
                  </Col>
                </Row>
                {reviews !== undefined ? reviews.map((item,a) =>
                  <div key={a} className="client-list-item pt-2">
                    <Row className="review-info">
                      <Col md={11} id={"text" + item.id} onClick={this.toggle}>
                        <Row>
                          <Col md="3">
                            <img src={item.photoUrl ? item.photoUrl : '/assets/images/icon/user.svg'}
                                 className="img-review"
                                 alt=""/>
                          </Col>
                          <Col md="3" className="text-center">
                            <p>{item.firstName}</p>
                          </Col>
                          <Col md="3" className="text-center">
                            <p>{item.lastName}</p>
                          </Col>
                          <Col md="3" className="text-center">
                            ...
                          </Col>
                          <Col md={12}>
                            <UncontrolledCollapse toggler={"#text" + item.id}>
                              <div className="pl-4 pr-4 pt-2">
                                <div className="font-weight-bold">Comment</div>
                                {item.text}
                              </div>
                            </UncontrolledCollapse>
                          </Col>
                        </Row>
                      </Col>
                      <Col md={1}>
                        <CustomInput type="checkbox" onClick={() => changePublished(item.id)} checked={item.published}
                                     id={"exampleCustomCheckbox" + item.id} label=""/>
                      </Col>
                    </Row>
                    <div className="line"/>
                  </div>
                ) : ''}
                <Pagination aria-label="Page navigation example">
                  {paginations.map(i => <PaginationItem active={page === (i - 1)}
                                                        disabled={page === (i - 1) || i === '...'}>
                    <PaginationLink onClick={(e) => getReviews(e, i - 1)}>
                      {i}
                    </PaginationLink>
                  </PaginationItem>)}
                </Pagination>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

Review.propTypes = {
  dispatch: PropTypes.func
}

export default Review;
