import api from 'api';
import {toast} from 'react-toastify';

const {getAllReviews, changePublished, getReviewTotalPages} = api;
function pagination(page, numPages) {
  var res = [];

  var from = 1;
  var to = numPages;
  if (numPages > 10) {
    from = Math.max(page - 2, 1);
    to = Math.max(Math.min(page + 2, numPages), 5);
    if (to > 5) {
      res.push(1);
      if (from > 2) res.push(2);
      if (from > 3) {
        if (from === 4) {
          res.push(3);
        } else {
          res.push("...");
        }
      }
    }
  }

  for (var i = from; i <= to; i++) {

    res.push(i);

  }

  if (numPages > 10) {
    if (to < (numPages - 2)) {
      if (to === 8) {
        res.push(9);
      } else {
        res.push("...");
      }
    }
    if (to < numPages)
      res.push(numPages - 1);
    if (to !== numPages)
      res.push(numPages);
  }

  return res;
}

export default ({
  namespace: 'review',
  state: {
    reviews: [],
    size: 10,
    page: 0,
    paginations: [],
    pathname:''
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {

    *changePublished({payload},{call,put,select}){

      const data = yield call(changePublished,payload)
      if(data.success)
      {
        yield put({
          type:'getReviews'
        })
      }

      toast.success(data.message);

    },

    * getReviews({payload}, {put, call, select}) {
      let {size, page} = yield select(_ => _.review);
      if (payload) {
        page = payload.page;
        yield put({
          type: 'updateState',
          payload: {page: payload.page}
        })
      }
      const res = yield call(getAllReviews, {size, page});
      const totalPages = yield call(getReviewTotalPages, {size, page});
      yield put({
        type: 'updateState',
        payload: {
          reviews: res.object,
          paginations: pagination(page, totalPages.data),
        },
      });
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
