import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CatalogSidebar from "../../../components/CatalogSidebar";
import {connect} from "react-redux";
import {Col, Row} from "reactstrap";

@connect(({transfer}) => ({transfer}))
class Transfer extends Component {
  render() {
    const {dispatch, transfer} = this.props;

    const {
      isModalShow, usdObj, modalType,
      pathname
    } = transfer;
    return (
      <div className="catalog">
      <CatalogSidebar pathname={pathname}/>
        <div className="catalog-menu">
          <section className="client_page">
            <div className="container">

              <Row className="mt-4">
                <Col md="12">
                  <h2 className="text-center">Pul ko'chirish</h2>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col md="1">
                  <h2 className="font-weight-bolder">T/r</h2>
                </Col>
                <Col md="3">
                  <h2 className="font-weight-bolder">Qaysi kassadan</h2>
                </Col>
                <Col md="3">
                  <h2 className="font-weight-bolder">Qaysi kassaga</h2>
                </Col>
                <Col md="2">
                  <h2 className="font-weight-bolder">Miqdori</h2>
                </Col>
                <Col md="2">
                  <h2 className="font-weight-bolder">Vaqti</h2>
                </Col>
              </Row>
              <div className="client-list-item pt-2">
                <Row>
                  <Col md="1" className="text-center">
                    <p>1</p>
                  </Col>
                  <Col md="3" className="text-center">
                    <p>Dollarning miqdori</p>
                  </Col>
                  <Col md="3" className="text-center">
                    <p>{usdObj}</p>
                  </Col>
                  <Col md="2" className="text-center">
                    <p>ssdsd</p>
                  </Col>
                  <Col md="2" className="text-center">
                    <p>ssdsd</p>
                  </Col>
                  {/*<Col md="2 ml-auto">*/}
                    {/*<button className="btn btn-edit-28 p-0" onClick={() => modalShow}><span*/}
                      {/*className="icon icon-edit"/></button>*/}
                  {/*</Col>*/}
                </Row>
                <div className="line"/>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

Transfer.propTypes = {};

export default Transfer;
