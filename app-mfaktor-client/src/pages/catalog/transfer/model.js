import api from 'api'
import {toast} from 'react-toastify';

const {getUsd, editUsd} = api;

export default ({
  namespace: 'transfer',
  state: {
    usdObj: 0,
    modalType: '',
    isModalShow: false
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getUsd({payload}, {put, call, select}) {
      const res = yield call(getUsd);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            usdObj: res.object
          },
        });
      }
    },
    * editUsd({payload}, {put, call, select}) {
      const res = yield call(editUsd, payload);
      if (res.success) {
        toast.success("O'zgartirildi!");
        yield put({
          type: 'updateState',
          payload: {
            isModalShow: false
          }
        });
        yield put({
          type: 'getUsd'
        });
      } else {
        if (res.statusCode === 403) {
          toast.error("O'zgartirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'zgartirishda xatolik!");
        }
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
