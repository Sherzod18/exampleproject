import api from 'api'
import {toast} from 'react-toastify';

const {addAware, editAware, getAwares, getAware, deleteAware} = api;

export default ({
  namespace: 'aware',
  state: {
    awares: [],
    isModalShow: false,
    delModalShow: false,
    currentAware:{},
    modalType:'new',
    popoverOpen: 0,
    deleteId: 0,
    pathname:''
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getAwares({payload}, {put, call, select}) {
      const res = yield call(getAwares);
      yield put({
        type: 'updateState',
        payload: {
          awares: res._embedded.awares
        },
      });
    },
    * saveAware({payload}, {put, call, select}) {
      const res = yield call(addAware, payload);
      if (res.success){
        toast.success("Saqlandi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getAwares'
        });
      }else{
        if (res.statusCode === 403){
          toast.error("Saqlashga ruxsatingiz yo'q!");
        } else {
          toast.error("Saqlashda xatolik!");
        }
      }
    },
    * editAware({payload}, {put, call, select}) {
      const res = yield call(editAware, {path:payload.id, ...payload});
      if (res.success){
        toast.success("O'zgartirildi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getAwares'
        });
      }else {
        if (res.statusCode === 403){
          toast.error("O'zgartirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'zgartirishda xatolik!");
        }
      }
    },
    * deleteAware({payload}, {put, call, select}){
      const del = yield call(deleteAware, {id:payload.id});
      if (del.success){
        toast.success("O'chirildi!");
        yield put({
          type:'getAwares'
        });
      } else {
        if (del.statusCode === 403){
          toast.error("O'chirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'chirishda xatolik!");
        }
      }
    },

    * getAware ({payload}, {put, call, select}){
      const res = yield call(getAware, payload);
      res.id = payload.path;
      yield put({
        type:'updateState',
        payload:{
          currentAware:res
        }
      })

    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
