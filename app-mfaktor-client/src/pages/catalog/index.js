import Redirect from 'umi/redirect'

export default () => <Redirect to="/catalog/eventtype" />
