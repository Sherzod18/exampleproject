import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';
import {router} from "umi";
import CatalogSidebar from "../../../components/CatalogSidebar";
import NavigationAdmin from "../../../components/Layout/NavigationAdmin";
import Footer from "../../../components/Layout/Footer";

@connect(({attachmentType}) => ({attachmentType}))
class PayType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type:'attachmentType/getAttachmentTypes'
    })
  }


  render() {
    const {dispatch, attachmentType} = this.props;
    const {isModalShow, attachmentTypes, currentAttachmentType, modalType,
      delModalShow, deleteId,pathname} = attachmentType;

    const modalShow = () => {
      dispatch({
        type:'attachmentType/updateState',
        payload:{
          modalType:'new'
        }
      });
      dispatch({
        type: 'attachmentType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };
    const deleteModalShow = (id) => {
      if (id !== -1){
        dispatch({
          type:'attachmentType/updateState',
          payload:{
            deleteId: id
          }
        });
      }
      dispatch({
        type:'attachmentType/updateState',
        payload:{
          delModalShow: !delModalShow
        }
      });
    };
    const handleSubmit = (payment, errors, values) => {
      if (errors.length === 0) {
        if (modalType==='new') {
          dispatch({
            type:'attachmentType/saveattachmentType',
            payload: {
              ...values
            }
          });
        }else {
          dispatch({
            type:'attachmentType/editAttachmentType',
            payload: {
              ...values,
              id:currentAttachmentType.id
            }
          });
        }
      }
    };
    const deletePayType = () => {
      dispatch({
        type: "attachmentType/deleteAttachmentType",
        payload: {
          payDel: deleteId
        }
      });
      deleteModalShow(-1);

    };
    const editPayType = (id) => {
      dispatch({
        type:'attachmentType/updateState',
        payload:{
          modalType:'update'
        }
      });
      dispatch({
        type: 'attachmentType/getAttachmentType',
        payload:{
          path:id,
        }
      });
      dispatch({
        type: 'attachmentType/updateState',
        payload:{
          isModalShow:!isModalShow
        }
      });
    };

    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="3">
                    <h2 className="font-weight-bolder">Fayl tipi</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Kengligi</h2>
                  </Col>
                  <Col md="3">
                    <h2 className="font-weight-bolder">Balandligi</h2>
                  </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Tipi</h2>
                  </Col>
                </Row>
                {attachmentTypes !== undefined ? attachmentTypes.map((item,a) =>
                  <div key={a} className="client-list-item pt-2">
                    <Row>
                      <Col md="3" className="text-center">
                        <p>{item.contentTypes}</p>
                      </Col>
                      <Col md="2" className="text-center">
                        <p>{item.width}</p>
                      </Col>
                      <Col md="3" className="text-center">
                        <p>{item.height}</p>
                      </Col>
                      <Col md="2" className="text-center">
                        <p>{item.type}</p>
                      </Col>
                      <Col md="2 ml-auto">
                        <button className="btn btn-edit-28 p-0" onClick={()=> editPayType(item.id)}><span className="icon icon-edit" /></button>
                        <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(item.id)}><span className="icon icon-delete" /></button>
                      </Col>
                    </Row>
                    <div className="line" />
                  </div>
                ) : ''}
                <button className="btn btn-success px-4 py-2 mt-5" onClick={modalShow}>Qo'shish</button>
              </div>
            </section>
          </div>
        </div>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>Do you want to delete this item?</ModalHeader>
          <ModalFooter>
            <Button className="btn-sm" color="primary" onClick={deletePayType}>Yes</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>No</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isModalShow} toggle={modalShow} className={this.props.className}>
          <ModalHeader>{modalType==='new'? 'Add Attachment Type':'Edit Attachment Type'}</ModalHeader>
          <AvForm  onSubmit={handleSubmit}>
          <ModalBody>

            <Row>
              <Col md={{size: 10, offset:1}}>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="contentTypes"  value={modalType==='new'? '' : currentAttachmentType.contentTypes} type="text" required/>
                  <label>Content Type</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="width"  value={modalType==='new'? '' : currentAttachmentType.width} type="number" required/>
                  <label>Width</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="height"  value={modalType==='new'? '' : currentAttachmentType.height} type="number" required/>
                  <label>Height</label>
                </AvGroup>
                <AvGroup>
                  <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                  <AvInput name="type"  value={modalType==='new'? '' : currentAttachmentType.type} type="select" required>
                    <option key="title" value=''>Select Attachment Type</option>
                    <option key="1" value='SPEAKER_AVATAR'>SPEAKER AVATAR</option>
                    <option key="1" value='COMPANY_LOGO'>COMPANY LOGO</option>
                    <option key="2" value='EVENT_BANNER'>EVENT BANNER</option>
                    <option key="3" value='USER_AVATAR'>USER AVATAR</option>
                    <option key="4" value='ARTICLE_BANNER'>ARTICLE BANNER</option>
                  </AvInput>
                  <label>Attachment type</label>
                </AvGroup>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-sm" color="primary">{modalType==='new'? 'Save Attachment Type':'Edit Attachment Type'}</Button>
            <Button className="btn-sm" color="secondary" onClick={modalShow}>Cancel</Button>
          </ModalFooter>
          </AvForm>
        </Modal>
        <Footer/>
      </div>
    );
  }
}

PayType.propTypes = {
  dispatch: PropTypes.func
}

export default PayType;
