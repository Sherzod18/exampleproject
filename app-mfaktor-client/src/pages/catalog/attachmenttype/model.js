import api from 'api'
import {toast} from 'react-toastify';

const {addAttachmentType, editAttachmentType, getAttachmentTypes, getAttachmentType,
  deleteAttachmentType} = api;

export default ({
  namespace: 'attachmentType',
  state: {
    attachmentTypes: [],
    isModalShow: false,
    delModalShow:false,
    currentAttachmentType:{},
    modalType:'new',
    deleteId:0,
    pathname: ""
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
    * getAttachmentTypes({payload}, {put, call, select}) {
      const res = yield call(getAttachmentTypes);
      yield put({
        type: 'updateState',
        payload: {
          attachmentTypes: res._embedded.list
        },
      });
    },
    * saveattachmentType({payload}, {put, call, select}) {
      const res = yield call(addAttachmentType, payload);
      if (res.success){
        toast.success("Saqlandi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getAttachmentTypes'
        });
      }else{
        if (res.statusCode === 403){
          toast.error("Saqlashga ruxsatingiz yo'q!");
        } else {
          toast.error("Saqlashda xatolik!");
        }
      }
    },
    * editAttachmentType({payload}, {put, call, select}) {
      const res = yield call(editAttachmentType, {path:payload.id, ...payload});
      if (res.success){
        toast.success("O'zgartirildi!");
        yield put({
          type:'updateState',
          payload:{
            isModalShow: false
          }
        });
        yield put({
          type:'getAttachmentTypes'
        });
      }else {
        if (res.statusCode === 403){
          toast.error("O'zgartirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'zgartirishda xatolik!");
        }
      }
    },
    * deleteAttachmentType({payload}, {put, call, select}){
      const del = yield call(deleteAttachmentType, {id:payload.payDel});
      if (del.success) {
        toast.success("O'chirildi!");
        yield put({
          type: 'getAttachmentTypes'
        });
      }else {
        if (del.statusCode === 403){
          toast.error("O'chirishga ruxsatingiz yo'q!");
        } else {
          toast.error("O'chirishda xatolik!");
        }
      }
    },
    * getAttachmentType({payload}, {put, call, select}){
      const res = yield call(getAttachmentType, payload);
      res.id = payload.path;
      yield put({
        type:'updateState',
        payload:{
          currentAttachmentType:res
        }
      })

    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
