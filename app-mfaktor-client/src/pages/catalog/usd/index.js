import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';
import {router} from "umi";
import CatalogSidebar from "../../../components/CatalogSidebar";
import FooterAdmin from "../../../components/Layout/FooterAdmin";

@connect(({usd}) => ({usd}))
class EventType extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'usd/getUsd'
    })
  }

  render() {
    const {dispatch, usd} = this.props;
    const {
      isModalShow, usdObj, modalType,
      pathname
    } = usd;

    const modalShow = () => {
      dispatch({
        type: 'usd/updateState',
        payload: {
          modalType: 'new'
        }
      });
      dispatch({
        type: 'usd/updateState',
        payload: {
          isModalShow: !isModalShow
        }
      });
    };
    const handleSubmit = (event, errors, values) => {
      if (errors.length === 0) {
        dispatch({
          type: 'usd/editUsd',
          payload: {
            ...values,
          }
        });
      }
    };

    const editUsd=(id)=>{

    };

    return (
      <div className="catalog">
        <CatalogSidebar pathname={pathname}/>
        <div className="catalog-menu">
          <section className="client_page">
            <div className="container">
              <Row className="mt-4">
              <Col md="3">
                <h2 className="font-weight-bolder">T/r</h2>
              </Col>
              <Col md="3">
                <h2 className="font-weight-bolder">Dollar</h2>
              </Col>
              <Col md="3">
                <h2 className="font-weight-bolder">Miqdori</h2>
              </Col>
            </Row>
              <div className="client-list-item pt-2">
                <Row>
                  <Col md="3" className="text-center">
                    <p>1</p>
                  </Col>
                  <Col md="3" className="text-center">
                    <p>Dollarning miqdori</p>
                  </Col>
                  <Col md="3" className="text-center">
                    <p>{usdObj}</p>
                  </Col>
                  <Col md="2 ml-auto">
                    <button className="btn btn-edit-28 p-0" onClick={() => modalShow}><span
                      className="icon icon-edit"/></button>
                  </Col>
                </Row>
                <div className="line"/>
              </div>
            </div>
          </section>
        </div>
        <Modal isOpen={isModalShow} toggle={modalShow} className={this.props.className}>
          <ModalHeader>Dollar kursini tahrirlash</ModalHeader>
          <AvForm onSubmit={handleSubmit}>
            <ModalBody>
              <Row>
                <Col md={{size: 10, offset: 1}}>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="rate" value={usdObj} type="number" required/>
                    <label>Qiymatni kiriting</label>
                  </AvGroup>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button className="btn-sm" color="primary">{modalType === 'new' ? 'Saqlash' : 'Tahrirlash'}</Button>
              <Button className="btn-sm" color="secondary" onClick={modalShow}>Bekor qilish</Button>
            </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    );
  }
}

EventType.propTypes = {
  dispatch: PropTypes.func
}

export default EventType;
