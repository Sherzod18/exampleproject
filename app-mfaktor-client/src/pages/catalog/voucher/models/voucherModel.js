import api from 'api'

const {getVouchers, addVoucher, editVoucher, deleteVoucher} = api;
export default ({
  namespace: 'voucherModel',
  state: {
    isModalShow: false,
    delModalShow: false,
    dateTime: new Date(),
    selectedVoucher:{},
    modalType: "new",
    randomId: "",
    deleteId:"",
    pathname:''
  },

  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
      })
    }
  },
  effects: {
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
