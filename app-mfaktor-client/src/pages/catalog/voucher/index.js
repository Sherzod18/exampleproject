import React, {Component} from 'react';
import {
  Button,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  Modal,
  ModalBody, ModalFooter,
  ModalHeader, PopoverBody, PopoverHeader,
  Row, UncontrolledPopover
} from "reactstrap";
import {Link} from 'umi';
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import DatePicker from "react-datepicker/es";
import {connect} from "react-redux";
import CatalogSidebar from "../../../components/CatalogSidebar";

@connect(({voucherModel, app}) => ({voucherModel, app}))
class Voucher extends Component {
  componentDidMount() {
    const {dispatch, app} = this.props;
    dispatch({
      type: 'app/getVouchers'
    });
    dispatch({
      type: 'app/getUsers'
    });
  }

  clickHiding = () => {
    document.getElementById("hr-search-icon").classList.add("aylan");
  };

  render() {
    const {dispatch, voucherModel, app} = this.props;
    const {isModalShow,delModalShow,pathname,deleteId, dateTime, selectedVoucher, modalType, randomId} = voucherModel;
    const {users, vouchers} = app;

    const deleteModalShow = (id) => {
      if (id !== -1){
        dispatch({
          type:'voucherModel/updateState',
          payload:{
            deleteId: id
          }
        });
      }
      dispatch({
        type:'voucherModel/updateState',
        payload:{
          delModalShow: !delModalShow
        }
      });
    };
    const openModal = () => {
      dispatch({
        type: "voucherModel/updateState",
        payload: {
          isModalShow: !isModalShow,
          selectedVoucher: {},
          randomId: ""
        }
      });
    };
    const openEditModal = (voucher) => {
      dispatch({
        type: "voucherModel/updateState",
        payload: {
          isModalShow: !isModalShow,
          selectedVoucher: voucher,
          modalType: "edit",
          randomId: voucher.voucherId
        }
      });
    };
    const changeDateTime = (date) => {
      dispatch({
        type: 'voucherModel/updateState',
        payload: {
          dateTime: date
        }
      })
    };
    const getRandomId = () => {
      let random = Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5)
      dispatch({
        type: 'voucherModel/updateState',
        payload: {
          randomId: random
        }
      })
    }
    const save = (e, v) => {
      dispatch({
        type: 'app/addVoucher',
        payload: {
          ...v,
          id: selectedVoucher.id,
          deadline: dateTime
        }
      }).then(() => {
        dispatch({
          type: 'app/getVouchers'
        });
      })
    };
    const deleteVoucher = () => {
      dispatch({
        type: "app/deleteVoucher",
        payload:{
          id:deleteId
        }
      })
    }
    return (
      <div>
        <div className="catalog">
          <CatalogSidebar pathname={pathname}/>
          <div className="catalog-menu">
            <section className="client_page">
              <div className="container">
                <Row className="mt-4">
                  <Col md="2">
                    <h2 className="font-weight-bolder">ID</h2>
                  </Col>
                  <Col md="2">
                  <h2 className="font-weight-bolder">Mijoz</h2>
                </Col>
                  <Col md="2">
                  <h2 className="font-weight-bolder">Summa</h2>
                </Col>
                  <Col md="2">
                  <h2 className="font-weight-bolder">Amal qilish</h2>
                </Col>
                  <Col md="2">
                    <h2 className="font-weight-bolder">Status</h2>
                  </Col>
                </Row>
              </div>
            </section>
            <main>
              {vouchers.map((voucher,a) => (
                <div key={a} className="mt-3">
                  <div className="client-list-item pt-2">
                    <Row className="pl-3">
                      <Col md="2" className='pl-4'>
                        <p>{voucher.voucherId}</p>
                      </Col>
                      <Col md="2">
                        <p>{voucher.firstName + " " + voucher.lastName}</p>
                      </Col>
                      <Col md="2">
                        <p>{voucher.price}</p>
                      </Col>
                      <Col md="2">
                        <p>{voucher.deadline.substr(0, 10)}</p>
                      </Col>
                      <Col md="2">
                        <p>{voucher.status}</p>
                      </Col>
                      <Col md="2 ml-auto">
                        <button className="btn btn-edit-28 p-0" onClick={()=> openEditModal(voucher)}><span className="icon icon-edit" /></button>
                        <button className="btn btn-delete-28 p-0" onClick={() => deleteModalShow(voucher.id)}><span className="icon icon-delete" /></button>
                      </Col>
                    </Row>
                    <div className="line"/>

                  </div>
                </div>
              ))}
              <button className="btn btn-success px-4 py-2 mt-5" onClick={openModal}>Qo'shish</button>
            </main>
          </div>
        </div>
        <Modal isOpen={delModalShow} toggle={() => deleteModalShow(false)}>
          <ModalHeader>O'chirishni istaysizmi?</ModalHeader>
          <ModalFooter>

            <Button className="btn-sm" color="primary" onClick={deleteVoucher}>Ha</Button>
            <Button className="btn-sm" color="secondary" onClick={() => deleteModalShow(-1)}>Yo'q</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={isModalShow} toggle={openModal} className="custom-modal">
          <ModalHeader toggle={openModal} className="border-0"/>
          <ModalBody className="px-5 pb-5">
            <AvForm onValidSubmit={save}>
              <h3 className="font-weight-bolder">Vaucher qo'shish</h3>
              <Row className="mt-5">
                <Col md={{size: 4, offset: 1}}>
                  <AvGroup>
                    <AvInput type="select" name="ownerId" value={selectedVoucher.ownerId} required>
                      {selectedVoucher.id ? "" : <option>Mijozni tanlang</option>}
                      {users.map((i,b) => <option key={b} value={i.id}>{i.fullName}</option>)}
                    </AvInput>
                    <label>Mijoz nomi</label>
                  </AvGroup>
                </Col>
                <Col md={{size: 4, offset: 2}}>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <DatePicker
                      name="deadline"
                      selected={dateTime}
                      className="form-control"
                      onChange={changeDateTime}
                      timeIntervals={30}
                      dateFormat="dd.MM.yyyy"
                      timeCaption="time"
                    />
                    <label className="label-date">Amal qilish muddati</label>
                  </AvGroup>
                </Col>
              </Row>
              <Row className="mt-3">
                <Col md={{size: 4, offset: 1}}>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="price" type="number" value={selectedVoucher.price} required/>
                    <label>Summa</label>
                  </AvGroup>
                </Col>
                <Col md={{size: 4, offset: 2}}>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="voucherId" value={randomId} type="voucherId" required/>
                    <label>Id kod</label>

                  </AvGroup>
                  <Button className="btn-reload" onClick={getRandomId}><img src="/assets/images/refresh-cw.png" alt=""/></Button>
                </Col>
              </Row>
              {modalType === 'edit' ?
                <Row className="mt-3">
                  <Col md={{size: 4, offset: 1}}>
                    <AvGroup>
                      <AvInput type="select" name="status" required>
                        <option value="ACTIVE">Aktiv</option>
                        <option value="USED">Ishlatilgan</option>
                        <option value="EXPIRED">Eskirgan</option>
                      </AvInput>
                      <label>Vaucher holati</label>
                    </AvGroup>
                  </Col>
                </Row> : ""
              }
              <Row className="mb-5 mt-4">
                <Col md={{size: 2, offset: 7}}>
                  <Button outline type="button" className="btn-sm btn-block" onClick={openModal}>Bekor
                    qilish</Button>
                </Col>
                <Col md={{size: 2}}>
                  <Button type="submit" color="success" className="btn-block">Saqlash</Button>
                </Col>
              </Row>
            </AvForm>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

export default Voucher;
