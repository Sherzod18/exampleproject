import React, {Component} from 'react';
import {connect} from "react-redux";
import Loader from "react-loader-spinner";
import Button from 'react-bootstrap-button-loader';
import DatePicker from "react-datepicker/es";
import "react-datepicker/dist/react-datepicker.css";
import {
  Card,
  CardBody,
  Col,
  Modal,
  ModalBody,
  ModalHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
  Row
} from "reactstrap";
import {AvFeedback, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import CurrencyInput from "react-currency-input";
import {toast} from "react-toastify";

@connect(({expenseModel, app}) => ({expenseModel, app}))
class MyComponent extends Component {
  componentWillMount() {
    const {dispatch, expenseModel, app} = this.props;
    dispatch({
      type: 'expenseModel/getExpenses'
    });
    dispatch({
      type: 'app/getCashTypes'
    });
    dispatch({
      type: 'app/getExpenseTypes'
    })

  }

  render() {
    const {dispatch, expenseModel, app} = this.props;
    const {
      expenses, loading, page, size, totalPages,
      modalOpen, date, isLoading, id, paginationList, currentExpense,eventId,
    } = expenseModel;
    const {cashTypes, expenseTypes} = app;

    const openModal = () => {
      dispatch({
        type: 'expenseModel/updateState',
        payload: {
          modalOpen: !modalOpen,
          id: '',
          currentExpense: {},
          date: new Date()
        }
      })
    };
    const changeDateTime = (date) => {
      dispatch({
        type: 'expenseModel/updateState',
        payload: {
          date
        }
      })
    };

    const getExpenses = (e, page) => {
      dispatch({
        type: 'expenseModel/getExpenses',
        payload: {
          page
        }
      })
    };

    const edit = (expense) => {
      dispatch({
        type: 'expenseModel/updateState',
        payload: {
          modalOpen: !modalOpen,
          id: expense.id,
          eventId:expense.eventId,
          currentExpense: expense,
          date: new Date(expense.date)
        }
      });
    };

    const save = (e, v) => {
      dispatch({
        type: 'expenseModel/updateState',
        payload: {
          isLoading:true
        }
      });
      v.sum = v.sum.replace(/ /g, '');
      if (v.sum > 0) {
        dispatch({
          type: 'expenseModel/addExpense',
          payload: {
            id,
            ...v,
            date
          }
        })
      } else {
        toast.error("Pul miqdorini kiritish majburiy");
        dispatch({
          type: 'expenseModel/updateState',
          payload: {
            isLoading:false
          }
        });
      }
    };


    return (
      <div>

        <main>
          <section className="expense_page margin-70">
            <div className="container">
              {loading ?
                <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px'}}><Loader
                  type="Triangle"
                  color="#ED2939"
                  height="100"
                  width="100"
                /></div>

                : ''}

              {expenses.length !== 0 ?
                <div>
                  <Row className="pt-2 pt-md-5">
                    <Col md={2}>
                      <h3 className="header-title">Xarajatlar</h3>
                    </Col>
                    <Col md={2} className="offset-8">
                      <button className="btn btn-success px-4 py-2 float-right" onClick={openModal}>Qo'shish</button>
                    </Col>
                  </Row>
                  <Row className="mt-4">
                    <Col md="2">
                      <h2 className="font-weight-bolder">Xarajat turi</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">Miqdori</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">To'lov turi</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">Vaqti</h2>
                    </Col>
                    <Col md="2">
                      <h2 className="font-weight-bolder">Tadbirga?</h2>
                    </Col>
                    <Col md="1">
                      <h2 className="font-weight-bolder">Izoh</h2>
                    </Col>
                  </Row>
                  {expenses.map((item, i) =>
                    <div className="client-list-item pt-2 pb-2">
                      <Row key={item.id}>
                        <Col md="2" className="text-center">
                          <p>{item.expenseTypeNameUz}</p>
                        </Col>
                        <Col md="2" className="text-center">
                          <p>{(new Intl.NumberFormat('fr-FR', {currency: 'UZS'}).format(item.sum))}</p>
                        </Col>
                        <Col md="2" className="text-center">
                          <p>{item.cashTypeUz}</p>
                        </Col>
                        <Col md="2" className="text-center">
                          <p>{item.date ? item.date.substring(0, 10) + ' ' + new Date(item.date).toTimeString().substring(0, 5) : ''}</p>
                        </Col>
                        <Col md="2" className="text-center">
                          <p>{item.eventName}</p>
                        </Col>
                        <Col md="1" className="text-center">
                          <p>{item.comment}</p>
                        </Col>
                      </Row>
                      {item.canEdit ? <button className="btn btn-edit-28 p-0" onClick={() => edit(item)}><span
                        className="icon icon-edit"/></button> : ""}
                    </div>
                  )}
                  <div className="col-md-3 offset-5 text-center my-3">
                    <Pagination aria-label="Page navigation example">
                      <PaginationItem disabled={page === 0}>
                        <PaginationLink first onClick={(e) => getExpenses(e, 0)}/>
                      </PaginationItem>
                      <PaginationItem disabled={page === 0}>
                        <PaginationLink previous onClick={(e) => getExpenses(e, page !== 0 ? page - 1 : 0)}/>
                      </PaginationItem>
                      {paginationList.map(i => <PaginationItem active={page === (i - 1)}
                                                               disabled={page === (i - 1) || i === '...'}>
                        <PaginationLink onClick={(e) => getExpenses(e, i - 1)}>
                          {i}
                        </PaginationLink>
                      </PaginationItem>)}
                      <PaginationItem disabled={totalPages === page + 1}>
                        <PaginationLink next
                                        onClick={(e) => getExpenses(e, totalPages >= (page + 1) ? (page + 1) : page)}/>
                      </PaginationItem>
                      <PaginationItem disabled={totalPages === page + 1}>
                        <PaginationLink last
                                        onClick={(e) => getExpenses(e, page !== totalPages ? page + 1 : totalPages)}/>
                      </PaginationItem>
                    </Pagination>
                  </div>
                </div>
                :
                (
                  <div>
                    <h3 className="font-weight-bolder text-center margin-70">Hozirda xarajatlar mavjud emas</h3>
                    <Card onClick={openModal} className="client-add-btn">
                      <CardBody>
                        <p className="text-center mb-0"><span className="icon icon-user-add"/> Xarajat qo'shish</p>
                      </CardBody>
                    </Card>
                  </div>
                )
              }
            </div>
            <Modal isOpen={modalOpen} toggle={openModal} className="event-client-modal">
              <ModalHeader toggle={openModal} className="border-0">
              </ModalHeader>
              <ModalBody className="pl-5 pr-5">
                <AvForm onValidSubmit={save}>
                  <h3>Xarajat qo'shish</h3>
                  <div className="row mt-5">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput type="select" name="expenseTypeId" value={currentExpense.expenseTypeId} required>
                          <option disabled>Xarajat turini tanlang</option>
                          {expenseTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                        </AvInput>
                        <label>Xarajat turi</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-5 offset-2 pl-0">
                      <div className="form-group">
                        <AvGroup>
                          <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                          <DatePicker
                            selected={date}
                            showTimeSelect
                            timeFormat="HH:mm"
                            className="form-control"
                            onChange={changeDateTime}
                            timeIntervals={30}
                            dateFormat="dd.MM.yyyy  HH:mm"
                            timeCaption="time"
                          />
                          <label className="label-date">Xarajat qilingan vaqti</label>
                        </AvGroup>
                      </div>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvInput name="sum" tag={CurrencyInput} precision="0" thousandSeparator=" "
                                 value={('' + currentExpense.sum + '')} required/>
                        <label>To'langan pul miqdori</label>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                      </AvGroup>
                    </div>
                    <div className="col-md-4 offset-2">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput type="select" name="cashTypeId" required value={currentExpense.cashTypeId}>
                          <option>To'lov turi</option>
                          {cashTypes.map((i) => <option value={i.id}>{i.nameUz}</option>)}
                        </AvInput>
                        <label>To'lov turi</label>
                      </AvGroup>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-md-4 offset-1">
                      <AvGroup>
                        <AvInput name="comment" value={currentExpense.comment}/>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <label>Qo'shimcha ma'lumot</label>
                      </AvGroup>
                    </div>
                  </div>
                  <div className="row mt-3">
                    <div className="col-md-5 offset-8">
                      <button type="button" onClick={openModal} className="btn-outline-secondary btn mx-2">Bekor qilish
                      </button>
                      <Button type="submit" bsStyle="success"
                              loading={isLoading}>Saqlash </Button>
                    </div>
                  </div>
                </AvForm>
              </ModalBody>
            </Modal>
          </section>
        </main>
      </div>
    );
  }
}

MyComponent.propTypes = {};

export default MyComponent;
