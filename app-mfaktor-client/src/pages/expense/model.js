import api from 'api'
// import {routerRedux} from 'dva/router'
import {parsePhoneNumber} from 'libphonenumber-js'
import {STORAGE_NAME} from 'utils/constant';
import {toast} from "react-toastify";

function pagination(page, numPages) {
  var res = [];
  var from = 1;
  var to = numPages;
  if (numPages > 10) {
    from = Math.max(page - 2, 1);
    to = Math.max(Math.min(page + 2, numPages), 5);
    if (to > 5) {
      res.push(1);
      if (from > 2) res.push(2);
      if (from > 3) {
        if (from === 4) {
          res.push(3);
        } else {
          res.push("...");
        }
      }
    }
  }
  for (var i = from; i <= to; i++) {
    res.push(i);
  }

  if (numPages > 10) {
    if (to < (numPages - 2)) {
      if (to === 8) {
        res.push(9);
      } else {
        res.push("...");
      }
    }
    if (to < numPages)
      res.push(numPages - 1);
    if (to !== numPages)
      res.push(numPages);
  }
  return res;
}

const {getExpenses, addExpense, getExpense, editExpense, deleteExpense} = api;
export default ({
  namespace: "expenseModel",
  state: {
    eventId: '',
    expenses: [],
    loading: true,
    totalPages: 0,
    page: 0,
    size: 10,
    modalOpen: false,
    date: new Date(),
    isLoading: false,
    expense: {},
    id: '',
    paginationList: [],
    currentExpense: {}
  },
  subscriptions: {},
  effects: {
    * getExpenses({payload}, {call, put, select}) {
      let {page, size} = yield select(_ => _.expenseModel);
      if (payload) {
        page = payload.page;
        payload = {size, page};
      } else {
        payload = {page, size};
      }
      const res = yield call(getExpenses, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            page,
            totalPages: res.object.totalPages,
            expenses: res.object.resExpenses,
            paginationList: pagination(page, res.object.totalPages),
            loading: false
          }
        })
      }
    },
    * addExpense({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoading: true
        }
      });
      let {eventId} = yield select(_ => _.expenseModel);
      const res = yield call(addExpense, {...payload,eventId:eventId});
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            modalOpen: false
          }
        });
        yield put({
          type: 'getExpenses'
        })
      } else {
        toast.error(res.message);
      }
      yield put({
        type: 'updateState',
        payload: {
          isLoading: false
        }
      });
    },
    * editExpense({payload}, {call, put, select}) {
      const res = yield call(editExpense, payload);
      if (res.success) {
        toast.success(res.message);
        yield put({
          type: 'updateState',
          payload: {
            modalOpen: false
          }
        });
        yield put({
          type: 'getExpenses',
        });
      } else {
        toast.error(res.message);
      }
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
