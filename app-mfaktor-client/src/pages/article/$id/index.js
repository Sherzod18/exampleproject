import React, {Component} from 'react';
import "jquery/dist/jquery.min"
import "bootstrap/dist/js/bootstrap"
import {connect} from 'dva';
import {Link} from 'umi';
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import {toast} from "react-toastify";

@connect(({articleByUrlModel}) => ({articleByUrlModel}))
class Index extends Component {

  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'articleByUrlModel/getLatestArticles'
    });
  }

  render() {
    const {dispatch, articleByUrlModel} = this.props;
    const {item, latestArticles, comments, text} = articleByUrlModel;
    const getText = (e) => {
      dispatch({
        type: 'articleByUrlModel/updateState',
        payload: {
          text:e.target.value
        }
      })
    };
    const sendComment = (e, v) => {
      if (v.text.length > 0) {
        dispatch({
          type: 'articleByUrlModel/addComment',
          payload: {
            ...v,
            articleId: item.id
          }
        })
      } else {
        toast.error("Izoh qo'shishda xatolik")
      }
    };
    return (<div>
      <main id="client">
        <div className="container">
          <section className="mb-5 article">
            <div className="row mb-4">
              <div className="col-md-8 pr-0">
                <img src={item.photo ? ('/api/file/' + item.photo.id) : ''} alt="" className="img-fluid"/>
              </div>
              <div className="col-md-4 pl-0">
                <div className="card" style={{height:"100%"}}>
                  <div className="card-body" style={{paddingTop:"120px"}}>
                    <h4 className="card-title" style={{fontSize:"26px"}}>

                      {item.title}
                    </h4>
                    <p className="mt-2">{item.createdAt ? item.createdAt.substring(0, 10) : ''}</p>
                  </div>
                </div>
              </div>
            </div>

            <div className="row">
              <div className="col-md-8 py-4">
                <div className="article-text" dangerouslySetInnerHTML={{
                  __html: item.description
                }}/>
                <hr/>

                <div className="post-block mt-5">
                  {comments.map((comment, i) =>
                    <div className="row post my-4">
                      <div className="col-md-1">
                        <img src={comment.photoUrl} className="img-comment" style={{borderRadius:"50%"}}/>
                      </div>
                      <div className="col-md-8 pl-0 mb-0">
                        <h6 className="mb-0 ">{comment.fullName}</h6>
                        <p
                          className="date" style={{fontSize:"12px"}}>{comment.commentTime ? comment.commentTime.substring(0, 10) + ' ' + new Date(comment.commentTime).toTimeString().substring(0, 5) : ''}</p>
                        <p>{comment.text}</p>
                      </div>
                    </div>
                  )}


                  <div className="row">
                    <div className="col-md-9">
                      <AvForm onValidSubmit={sendComment} >
                        <div className="input-group mt-2" >
                          <AvGroup style={{width:"100%"}}>

                            <AvInput type="textarea" name="text" value={text} onChange={getText}
                                     className="form-control back-img" placeholder="Sharhingiz"/>

                            {/*<div className="input-group-prepend">*/}
                            {/*  <span className="icon icon-chat"/>*/}
                            {/*</div>*/}
                          </AvGroup>
                        </div>
                        <button type="submit" className="btn active float-right send-button mt-1">Jo'natish</button>
                      </AvForm>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-4 right-sidebar pl-5">
                <div className="row border-bottom">
                  <div className="col-md-12 py-3 pl-4">
                    <h2 className="title">{item.category ? item.category.nameUz : ''}</h2>
                  </div>
                </div>
                <div className="row my-3">
                  <div className="col-md-12">
                    <h4 className="type">So'nggi yangiliklar</h4>
                  </div>
                </div>
                {latestArticles.map(i => <div className="row pl-2 mb-4 new-block">
                  <div className="col-md-3">
                    <img src={i.photo ? '/api/file/' + i.photo.id : ''} alt="" className="img-fluid"/>
                  </div>
                  <div className="col-md-9 pl-0">
                    <h4>
                      <Link to={'/article/' + i.url}>{i.title}</Link>
                    </h4>
                    <p className="date">{i.createdAt.substring(0, 10)}</p>
                  </div>
                </div>)}

              </div>
            </div>
          </section>
        </div>
      </main>
    </div>)
  }
}

export default Index;
