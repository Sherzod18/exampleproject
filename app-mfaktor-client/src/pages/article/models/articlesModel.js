import api from 'api'

const {getArticlesByActive, getArticlesByCategory, getCategories, searchArticle} = api;
export default ({
  namespace: 'articles',
  state: {
    categoryId: '',
    list: [],
    categories: [],
    size: 6,
    page: 0,
    totalPages: 0,
    isBtnVisible: false,
    isLoading: false,
    loading: false,
    searchValue: '',
  },

  effects: {
    * getArticles({payload}, {call, put, select}) {
      const {categoryId, size, list} = yield select(_ => _.articles);
      let page = 0;
      if (payload) {
        page = payload.page
      }
      yield put({
        type: 'updateState',
        payload: {page}
      });
      let res;
      if (categoryId) {
        res = yield call(getArticlesByCategory, {size, page, categoryId})
      } else {
        res = yield call(getArticlesByActive, {size, page})
      }
      const articles = page === 0 ? res.object.articles : list.concat(res.object.articles);
      yield put({
        type: 'updateState',
        payload: {
          list: articles,
          totalPages: res.object.totalPages
        }
      });

      yield put({
        type: 'updateState',
        payload: {
          isBtnVisible: !(res.object.totalPages === (page + 1) || articles.length === 0),
          isLoading: false,
          loading: false,
          searchValue: '',
        }
      });
    },

    * getCategories({payload}, {call, put, select}) {
      const res = yield call(getCategories);
      yield put({
        type: 'updateState',
        payload: {
          categories: res._embedded.list
        }
      })
    },
    * changeCategory({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          page: 0,
          categoryId: payload.categoryId
        }
      });
      yield put({
        type: 'getArticles'
      })
    },
    * searchArticle({payload}, {call, put, select}) {
      const res = yield call(searchArticle, payload);
      yield put({
        type: 'updateState',
        payload: {
          list: res.object
        }
      })
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
