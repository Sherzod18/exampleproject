import api from 'api';
import {toast} from "react-toastify";
import router from "../../.umi/router";

const {articleByUrl, getLatestArticles, addComment, getComments} = api;
export default ({
  namespace: 'articleByUrlModel',
  state: {
    item: {},
    latestArticles: [],
    comments: [],
    text:''
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen(({pathname}) => {
        if (pathname.includes("/article/")) {
          const url = pathname.split('/')[2]
          dispatch({
            type: 'byUrl',
            payload: {
              url
            }
          });
          window.scrollTo(0, 0)
        }
      })
    }
  },
  effects: {
    * addComment({payload}, {call, put, select}) {
      const res = yield call(addComment, payload)
      if (res.success){
        toast.success(res.message);
        let {item} = yield select(_ => _.articleByUrlModel);
        yield put({
          type: 'getComments',
          payload: {
            path: item.id
          }
        });
        yield put({
          type: 'updateState',
          payload: {
            text:''
          }
        })
      }else {
        toast.error(res.message)
      }

    },
    * getComments({payload}, {call, put, select}) {
      const res = yield call(getComments,{path:payload.path});
      if (res.success) {
        yield put({
          type: "updateState",
          payload: {
            comments: res.object
          }
        })
      }
    },
    * byUrl({payload}, {call, put, select}) {
      const res = yield call(articleByUrl, payload);
      yield put({
        type: 'updateState',
        payload: {item: res}
      });
      let {item} = yield select(_ => _.articleByUrlModel);
      yield put({
        type: 'getComments',
        payload: {
          path: item.id
        }
      })
    },
    * getLatestArticles({payload}, {call, put, select}) {
      const res = yield call(getLatestArticles);
      yield put({
        type: 'updateState',
        payload: {
          latestArticles: res.object
        }
      })
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
