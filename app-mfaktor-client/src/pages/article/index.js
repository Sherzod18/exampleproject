import React, {Component} from 'react';
import "jquery/dist/jquery.min"
import "bootstrap/dist/js/bootstrap"
import {Link} from "umi";
import {connect} from 'dva';
import Container from "reactstrap/es/Container";
import {Button, Card, Col, Input, InputGroup, InputGroupAddon, Row} from "reactstrap";
import CardImg from "reactstrap/es/CardImg";
import CardBody from "reactstrap/es/CardBody";
import Loader from "react-loader-spinner";
import {AvForm} from "availity-reactstrap-validation";

@connect(({articles}) => ({articles}))
class Article extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'articles/getCategories'
    });
    dispatch({
      type: 'articles/getArticles',
      loading: false
    })
  }

  clickHiding = () => {
    let x = document.getElementById("ss");
    if (!x.classList.contains("inputQidirish")) {
      x.classList.add("inputQidirish");
    } else {
      const {dispatch} = this.props;
      dispatch({
        type: 'addVideos/searchByTitle',
        payload: {
          path: x.value
        }
      })
    }

  };

  render() {
    const {dispatch, articles} = this.props;
    const {categories, categoryId, list, page, totalPages, loading, isBtnVisible, isLoading, searchValue} = articles;
    const selectCategory = (id) => {
      dispatch({
        type: 'articles/changeCategory',
        payload: {
          categoryId: id
        }
      })
    };
    const getNextArticles = () => {
      dispatch({
        type: 'articles/getArticles',
        payload: {
          page: page + 1
        }
      });
      dispatch({
        type: 'articles/updateState',
        payload: {
          isLoading: true
        }
      })
    };
    const handleKeyDown = (e) => {
      if (e.key === 'Enter') {
        dispatch({
          type: 'articles/searchArticle',
          payload: {
            word: searchValue,
          }
        })
      }
    };
    const handleValue = (event) => {
      dispatch({
        type: 'articles/updateState',
        payload: {
          searchValue: event.target.value
        }
      })
    };
    return (
      <div>
        <header className="border-bottom">
          <Container>
            <Row className="mt-5 mt-md-5 mb-3 mb-md-0 pt-md-5">
              {/*<Col md={2}>*/}
                {/*<h3 className="header-title">Maqolalar</h3>*/}
              {/*</Col>*/}

              <Col md={3} className="pl-md-0 pl-3 pt-2">
                <InputGroup>
                  <InputGroupAddon id='hr-search-icon' onClick={() => this.clickHiding()}
                                   className="hr-search-icon mr-4"
                                   addonType="prepend"><span
                    className="icon icon-search"/></InputGroupAddon>
                  <AvForm onSubmit={() => this.clickHiding()}>
                    <input value={searchValue} onChange={handleValue}
                           onKeyDown={handleKeyDown} id="ss" className="hr-search-input inputKorinishi"
                           placeholder="Maqola nomini kiriting"/>
                  </AvForm>
                </InputGroup>
              </Col>

              <Col md={7} className="hr-category mt-md-0 mt-2">
                <ul className="list-un-styled mb-0">
                  <li className="d-inline-block mt-md-0 mt-2">
                    <button onClick={() => selectCategory('')}
                            className={"btn header-btn-light " + (!categoryId ? 'active' : '')}>Barchasi
                    </button>
                  </li>
                  {categories.map((c, a) =>
                    <li key={a} className="d-inline-block  ml-2 ml-md-4 mt-md-0 mt-2">
                      <button onClick={() => selectCategory(c.id)}
                              className={"btn header-btn-light " + (categoryId === c.id ? 'active' : '')}> {c.nameUz}</button>
                    </li>)}
                </ul>
              </Col>
            </Row>
          </Container>
        </header>
        <Container className="mt-5">

          {loading ? <div className="col-md-4 offset-4 text-center" style={{paddingTop: '150px'}}><Loader
            type="Triangle"
            color="#ED2939"
            height="100"
            width="100"
          /></div> : <div>
            <Row>
              {list.length > 0 ? list.map((i, b) => <Col key={b} md="4" className="mb-3">
                  <Card className="card-article article-card">
                    <CardImg style={{"height":"250px"}} src={'/api/file/' + i.photo.id}/>
                    <CardBody>
                      <p className="mb-3 text-muted border-left border-danger p-0 pl-2">{i.category.nameUz}</p>
                      <Link to={"/article/" + i.url}><h4>{i.title}</h4></Link>
                      <div className="d-flex m-0 text-muted pt-3">
                        <div className="w-40">{i.createdAt.substring(0, 10)}</div>
                        <div className="w-25"><span className="icon icon-chat text-muted small"/> 4</div>
                      </div>
                    </CardBody>
                  </Card>
                </Col>) :
                <div className='col-md-4 offset-md-4 col-12 text-center'>
                  <h3>Maqolalar mavjud emas</h3>
                </div>}
            </Row>
            <Row className="mt-4 mt-md-5">
              {isBtnVisible && <Col md={{size: 2, offset: '5'}} className="col-6 mb-5">
                <Button type="button" onClick={getNextArticles} className="btn btn-block btn-red btn-sm">
                  Yana yuklash
                </Button>
              </Col>}
              {isLoading && <Col md={{size: 2, offset: '5'}} className="col-12 text-center">
                <Row className="min-height">
                  <Col md="4" className="text-right">
                    <Loader
                      type="ThreeDots"
                      color="#ED2939"
                      // height="100"
                      width="30px"
                    />
                  </Col>
                  <Col md="8" className="text-left" style={{paddingTop: '25px'}}>
                    <span>Yuklanmoqda</span>
                  </Col>
                </Row>
              </Col>}
            </Row>
          </div>}

        </Container>
      </div>
    );
  }
}

export default Article;
