import api from 'api'

const {getFaqs, updateFaq, deleteFaq, createFaq} = api;

export default ({
  namespace: 'auth',
  state: {
    list: [],
    photoId: '',
    selectedItem: {},
    modalType: 'new',
    isModalShow: false,
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen((location) => {
        if (location.pathname === '/faq') {

        }
      });
    }
  },
  effects: {
    * getFaqs({payload}, {call, put, select}) {
      const res = yield call(getFaqs);
      yield put({
        type: 'updateState',
        payload: {
          list: res._embedded.list
        }
      })
    },
    * createFaq({payload}, {call, put, select}){
      const res = yield call(createFaq, payload);
      if (res.success) {
        yield put({
          type: 'getFaqs'
        })
      }
    },
    * updateFaq({payload}, {call, put, select}){
      const res = yield call(updateFaq, payload);
      if (res.success) {
        yield put({
          type: 'getFaqs'
        })
      }
    },
    * deleteFaq({payload}, {call, put, select}){
      const res = yield call(deleteFaq, payload);
      if (res.success) {
        yield put({
          type: 'getFaqs'
        })
      }
    },

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
