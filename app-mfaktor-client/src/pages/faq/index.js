import React, {Component} from 'react';
import {connect} from 'dva';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row,
  Card, CardHeader, CardBody, ListGroup, ListGroupItem, Table
} from 'reactstrap';
import {AvForm, AvField, AvGroup, AvFeedback, AvInput} from 'availity-reactstrap-validation';

@connect(({faq}) => ({faq}))
class Faq extends Component {
  render() {
    const {dispatch, faq} = this.props;
    const {list, photoId, isModalShow, modalType, selectedItem} = faq;
    const uploadFile = (e) => {
      dispatch({
        type: 'app/uploadAvatar',
        payload: {
          file: e.target.files,
          fileUpload: true
        }
      })
    };
    const modalShow = () => {
      dispatch({
        type: 'app/updateState',
        payload: {
          photoId: ''
        }
      })
      dispatch({
        type: 'faq/updateState',
        payload: {
          modalType: 'new',
          isModalShow: !isModalShow
        }
      })
    }
    const handleSubmit = (event, values) => {
      dispatch({
        type: 'faq/' + (modalType === 'new' ? 'createFaq' : 'updateFaq'),
        payload: {
          ...values,
          photoId
        }
      });
    };
    const editItem = (i) => {
      dispatch({
        type: 'faq/updateState',
        payload: {
          selectedItem: i,
          modalType: 'update',
          isModalShow: true,
        }
      })
    }
    const deleteItem = (id) => {
      dispatch({
        type: 'faq/deleteFaq',
        payload: {
          id
        }
      })
    }
    return (
      <div>
        <Table>
          <thead>
          <tr>
            <th>Photo</th>
            <th>Question</th>
            <th>Answer</th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          {list.map(i => <tr>
            <td>{i.attachment ? <img src={'/api/file/' + i.attachment} width="100"/> : ''}</td>
            <td>{i.question}</td>
            <td>{i.answer}</td>
            <td>
              <Button color="danger" onClick={() => deleteItem(i.id)}>Delete</Button>{' '}
              <Button color="primary" onClick={() => editItem(i)}>Edit</Button>
            </td>
          </tr>)}
          </tbody>
        </Table>
        <Modal isOpen={isModalShow} toggle={modalShow}>
          <ModalHeader>{modalType === 'new' ? 'Add Attachment Type' : 'Edit Attachment Type'}</ModalHeader>
          <AvForm onValidSubmit={handleSubmit}>
            <ModalBody>

              <Row>
                <Col md={{size: 10, offset: 1}}>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="question" value={modalType === 'new' ? '' : selectedItem.question} type="text"
                             required/>
                    <label>Question</label>
                  </AvGroup>
                  <AvGroup>
                    <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                    <AvInput name="answer" value={modalType === 'new' ? '' : selectedItem.answer} type="text"
                             required/>
                    <label>Answer</label>
                  </AvGroup>
                  <input type="file" name="file" onChange={uploadFile}/>
                  {photoId ? <img src={'/api/file/' + photoId} width="100"/> : ''}
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button className="btn-sm"
                      color="primary">{modalType === 'new' ? 'Save Attachment Type' : 'Edit Attachment Type'}</Button>
              <Button className="btn-sm" color="secondary" onClick={modalShow}>Cancel</Button>
            </ModalFooter>
          </AvForm>
        </Modal>
      </div>
    )
  }
}
