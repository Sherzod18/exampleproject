import api from 'api'


const {lastVideos,anonsType, getLatest3Articles, getOwnReview, getUpcomingEvents} = api;

export default ({
  namespace: 'landingPage',
  state: {
    reviewList: [],
    latest3: [],
    ytubeId: '',
    isModalShoww: false,
    anonsVideoType:'',
    upcomingEvents:[],
    lasVid:[]
  },
  subscriptions: {
    setup({dispatch, history}) {
      history.listen(({pathname}) => {
        if (pathname === ("/")) {
          dispatch({
            type: 'getPublishedReviews',
          });
        }
      })
    }
  },
  effects: {
    * getLastVides({payload}, {call, put, select}) {
      const res = yield call(lastVideos);
      yield put({
        type: 'updateState',
        payload: {
          lasVid:res.object
        }
      })
    },
    * getPublishedReviews({payload}, {call, put, select}) {
      const res = yield call(getOwnReview);
      yield put({
        type: 'updateState',
        payload: {
          reviewList: res.object
        }
      })
    },
    * getUpcomingEvents({payload}, {call, put, select}) {
      const res = yield call(getUpcomingEvents, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            upcomingEvents: res.object
          }
        })
      }
    },
    * get3Article({payload}, {call, put, select}) {
      const res = yield call(getLatest3Articles);
      yield put({
        type: 'updateState',
        payload: {
          latest3: res.object
        }
      })
    },
    * getVideoIfameIds({payload}, {call, put, select}) {
      const {isModalShoww} = yield select(_ => _.landingPage);
      yield put({
        type: 'updateState',
        payload: {
          isModalShoww: !isModalShoww,
          ytubeId: payload.frameUrl
        }
      })
    },
    * getAnonsType({payload},{call,put,select}){
      const res=yield call(anonsType);
      yield put({
        type: 'updateState',
        payload: {
          anonsVideoType:res.object.name
        }
      })
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
