import api from 'api'
import {config} from 'utils';
// import {getDistricts, getGenders, getRegions, userMe, getBusinessType, deleteEdu, uploadFile,login} from 'services';
import router from 'umi/router';
import {toast} from "react-toastify";
import {routerRedux} from "dva";
// import {eduList} from "../pages/cabinet/edu/service";

const {
  uploadFile, userMe, getPositions, getRegions,
  getAwares, getSpeakers, getUsers, getUsersBySort, register, editUser, getPayTypes, getCashTypes,
  editSpeaker, addSpeaker, getUser, getSpeakersPagination,
  getVouchers, addVoucher, editVoucher, deleteVoucher, getLanguages, getExpenseTypes
} = api;

function pagination(page, numPages) {
  var res = [];
  var from = 1;
  var to = numPages;
  if (numPages > 10) {
    from = Math.max(page - 2, 1);
    to = Math.max(Math.min(page + 2, numPages), 5);
    if (to > 5) {
      res.push(1);
      if (from > 2) res.push(2);
      if (from > 3) {
        if (from === 4) {
          res.push(3);
        } else {
          res.push("...");
        }
      }
    }
  }
  for (var i = from; i <= to; i++) {
    res.push(i);
  }

  if (numPages > 10) {
    if (to < (numPages - 2)) {
      if (to === 8) {
        res.push(9);
      } else {
        res.push("...");
      }
    }
    if (to < numPages)
      res.push(numPages - 1);
    if (to !== numPages)
      res.push(numPages);
  }
  return res;
}

export default {
  namespace: 'app',
  state: {
    isAdmin: false,
    isSort: false,
    photoId: '',
    eventBannerId: '',
    positions: [],
    regions: [],
    awares: [],
    speakers: [],
    currentUser: {},
    payTypes: [],
    cashTypes: [],
    pathname: '',
    users: [],
    isModalShow: false,
    isModalPlaceShow: false,
    logosId: [],
    vouchers: [],
    isLoading: false,
    loading: true,
    speakerSize: 1,
    balance: 0,
    currentItem: {},
    languages: [],
    totalPages: 0,
    page: 0,
    size: 10,
    paginations: [],
    search: '',
    expenseTypes: []
  },
  subscriptions: {
    setupHistory({dispatch, history}) {
      history.listen((location) => {
        dispatch({
          type: 'updateState',
          payload: {
            pathname: location.pathname,
          },
        });
        // dispatch({
        //   type: 'userMe',
        //   payload: {
        //     pathname: location.pathname
        //   }
        // })
      })
    },
    setup({dispatch, history}) {
      history.listen(({pathname}) => {
        if (!config.openPages.includes(pathname) && !pathname.includes('/article/') && !pathname.includes('/videos/')) {
          dispatch({
            type: 'userMe',
            payload: {
              pathname
            }
          })
        }
        dispatch({
          type: 'userMe2',
          payload: {
            pathname
          }
        })
      })
    }
  },
  effects: {
    * userMe2({payload}, {call, put, select}) {
      try {
        const res = yield call(userMe);
        if (!res.success) {
          yield put({
            type: 'updateState',
            payload: {currentUser: {}}
          });
          localStorage.removeItem('token');
        } else {
          yield put({
            type: 'updateState',
            payload: {
              currentUser: res.object,
            }
          })
        }
      } catch (error) {
        yield put({
          type: 'updateState',
          payload: {currentUser: {}}
        });
        localStorage.removeItem('token');
      }
    },
    * userMe({payload}, {call, put, select}) {
      try {
        const res = yield call(userMe);
        if (!res.success) {
          yield put({
            type: 'updateState',
            payload: {currentUser: {}}
          });
          localStorage.removeItem('token');
          router.push('/login');
          if (payload) {
            if (!config.openPages.includes(payload.pathname) && !payload.pathname.includes('/article/') && !payload.pathname.includes('/videos/')) {
              localStorage.removeItem('token');
              router.push('/login')
            }
          } else {
            localStorage.removeItem('token');
            router.push('/login')
          }
        } else {
          yield put({
            type: 'updateState',
            payload: {currentUser: res.object}
          })
        }
      } catch (error) {
        yield put({
          type: 'updateState',
          payload: {currentUser: {}}
        });
        localStorage.removeItem('token');
        router.push('/login');
        if (payload) {
          if (!config.openPages.includes(payload.pathname) && !payload.pathname.includes('/article/') && !payload.pathname.includes('/videos/')) {
            localStorage.removeItem('token');
            router.push('/login')
          }
        } else {
          localStorage.removeItem('token');
          router.push('/login')
        }
      }
    },
    * uploadAvatar({payload}, {call, put, select}) {
      const res = yield call(uploadFile, {payload});
      if (!res.success) {
        toast.error(res.message)
      }
      yield put({
        type: 'updateState',
        payload: {
          photoId: res.object[0].fileId,
        }
      })
    },
    * uploadEventBanner({payload}, {call, put, select}) {
      const res = yield call(uploadFile, {payload});
      if (!res.success) {
        toast.error(res.message)
      }
      yield put({
        type: 'updateState',
        payload: {
          eventBannerId: res.object[0].fileId,
        }
      })
    },
    * getPosition({}, {call, put, select}) {
      const res = yield call(getPositions);
      yield put({
        type: 'updateState',
        payload: {
          positions: res._embedded.list
        }
      })
    },
    * getLanguages({}, {call, put, select}) {
      const res = yield call(getLanguages);
      yield put({
        type: 'updateState',
        payload: {
          languages: res._embedded.list
        }
      })
    },
    * getBalance({payload}, {call, put, select}) {
      const res = yield call(getUsers, payload);
      yield put({
        type: 'updateState',
        payload: {
          balance: res.object.balance
        }
      })
    },
    * getRegions({}, {call, put, select}) {
      const res = yield call(getRegions);
      yield put({
        type: 'updateState',
        payload: {
          regions: res._embedded.list
        }
      })
    },
    * getAwares({payload}, {call, put, select}) {
      const res = yield call(getAwares);
      yield put({
        type: 'updateState',
        payload: {
          awares: res._embedded.awares
        }
      })
    },
    * getSpeakers({payload}, {call, put, select}) {
      const res = yield call(getSpeakers);
      yield put({
        type: 'updateState',
        payload: {
          speakers: res.object,
          isLoading: false,
          loading: false,
        }
      })
    },
    * getSpeakersPagination({payload}, {call, put, select}) {
      const res = yield call(getSpeakersPagination, payload);
      yield put({
        type: 'updateState',
        payload: {
          speakers: res.object,
          isLoading: false,
          loading: false,
        }
      })
    },
    * getUsers({payload}, {call, put, select}) {
      let {page, isSort, size, totalPages, search} = yield select(_ => _.app);
      if (payload) {
        page = payload.page;
        payload = {size, page, ...payload};
        if (payload.search) {
          search = payload.search;
        }
      } else {
        payload = {page, size}
      }
      let res = [];
      if (isSort) {
        res = yield call(getUsersBySort, payload);
      } else {
        res = yield call(getUsers, payload);
      }
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            page,
            size,
            search,
            users: res.object.users,
            totalPages: res.object.totalPages,
            paginations: pagination(page, res.object.totalPages),
            loading: false,
            totalElements: res.object.totalElements
          }
        })
      }
    },
    * getUsersBySort({payload}, {call, put, select}) {
      let {page, size, totalPages} = yield select(_ => _.app);
      payload = {page, size};
      const res = yield call(getUsersBySort, payload);
      yield put({
        type: 'updateState',
        payload: {
          page,
          size,
          users: res.object.users,
          totalPages: res.object.totalPages,
          paginations: pagination(page, res.object.totalPages),
          loading: false
        }
      })
    },
    * registerClient({payload}, {call, put, select}) {
      try {
        if (payload.id) {
          const res1 = yield call(editUser, payload);
          if (res1.success) {
            toast.success("O'zgartirildi");
            yield put({
              type: 'getUsers'
            });
            yield put({
              type: "clientModel/updateState",
              payload: {
                isModalShow: false,
                selectedUser: {}
              }
            })
          }
        } else {
          const res = yield call(register, payload);
          if (res.statusCode === 201) {
            toast.success("Qo'shildi");
            yield put({
              type: 'getUsers'
            });
            yield put(routerRedux.push('/client'));
            yield put({
              type: "clientModel/updateState",
              payload: {
                isModalShow: false,
                selectedUser: {}
              }
            })
          }
        }
      } catch (e) {
        toast.error("Xatolik");
      }
    },
    * getPayTypes({payload}, {put, call, select}) {
      const res = yield call(getPayTypes);
      yield put({
        type: 'updateState',
        payload: {
          payTypes: res._embedded.list
        },
      });
    },
    * getCashTypes({payload}, {put, call, select}) {
      const res = yield call(getCashTypes);
      console.log(res)
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            cashTypes: res.object
          },
        });
      }
    },
    * uploadLogo({payload}, {call, put, select}) {
      const res = yield call(uploadFile, {payload});
      if (res.success) {
        let {logosId} = yield select(_ => _.app);
        logosId = logosId.concat(res.object.map(i => i.fileId));
        yield put({
          type: 'updateState',
          payload: {
            logosId,
          }
        })
      } else {
        if (!res.success) {
          toast.error(res.message)
        }
      }
    },
    * registerSpeaker({payload}, {call, put, select}) {
      yield put({
        type: 'updateState',
        payload: {
          isLoading: true,
          loading: false,
        }
      });
      let {logosId, photoId} = yield select(_ => _.app);
      if (payload.id) {
        const res1 = yield call(editSpeaker, {...payload, photoId, logoIds: logosId === "" ? [] : logosId});
        if (res1.success) {
          toast.success("O'zgartirildi");
          yield put({
            type: 'getSpeakers'
          });
          yield put({
            type: 'updateState',
            payload: {
              photoId: '',
              logosId: '',
              isModalShow: false,
              isLoading: false,
            }
          });
        } else {
          toast.error("Ma'lumotlar to'liq kiritilmadi. Rasm va logotipni kiritish majburiy");
          yield put({
            type: 'updateState',
            payload: {
              isLoading: false,
            }
          });
        }
      } else {
        const res = yield call(addSpeaker, {...payload, photoId, logoIds: logosId ? logosId : []});
        if (res.success) {
          toast.success("Qo'shildi");
          yield put({
            type: 'getSpeakers'
          });
          yield put({
            type: 'updateState',
            payload: {
              photoId: '',
              isModalShow: false,
              logosId: '',
              isLoading: false
            }
          })
        } else {
          toast.error(res.message);
        }
        yield put({
          type: 'updateState',
          payload: {
            isLoading: false,
          }
        });
      }
    },
    * getVouchers({}, {call, put, select}) {
      const res = yield call(getVouchers);
      yield put({
        type: 'updateState',
        payload: {
          vouchers: res.object
        }
      })
    },
    * addVoucher({payload}, {call, put, select}) {
      if (payload.id) {
        const res1 = yield call(editVoucher, payload);
        if (res1.success) {
          toast.success("O'zgartirildi");
          // yield put(routerRedux.push('/catalog/voucher'));
          yield put({
            type: "voucherModel/updateState",
            payload: {
              isModalShow: false,
              selectedVoucher: {}
            }
          })
        }
      } else {
        const res = yield call(addVoucher, payload);
        if (res.statusCode === 201) {
          toast.success("Qo'shildi");
          // yield put(routerRedux.push('/voucher'));
          yield put({
            type: "voucherModel/updateState",
            payload: {
              isModalShow: false,
              selectedVoucher: {}
            }
          })
        }
      }
    },
    * deleteVoucher({payload}, {call, put, select}) {
      const res = yield call(deleteVoucher, payload);
      yield put({
        type: 'voucherModel/updateState',
        payload: {
          delModalShow: false,
        }
      });
      if (res.success) {
        yield put({
          type: 'getVouchers'
        })
      }
    },
    * getUser({payload}, {call, put, select}) {
      const res = yield call(getUser, payload);
      yield put({
        type: 'updateState',
        payload: {
          user: res.object
        }
      })
    },
    * getExpenseTypes({payload}, {put, call, select}) {
      const res = yield call(getExpenseTypes);
      yield put({
        type: 'updateState',
        payload: {
          expenseTypes: res._embedded.list
        },
      });
    },
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
