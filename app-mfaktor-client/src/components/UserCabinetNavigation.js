import React, {Component} from 'react';
import {
  Collapse,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  UncontrolledDropdown
} from "reactstrap";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import router from "umi/router";

@connect(({user, app}) => ({user, app}))
class UserCabinetNavigation extends Component {
  componentDidMount() {
    const {dispatch, app} = this.props;
    dispatch({
      type: 'app/getBalance',
      payload: {
        path: app.currentUser.id
      }
    });
  }

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    const {app} = this.props;
    const {balance, currentUser} = app;
    const userEdit = (e) => {
      e.preventDefault();
      router.push('/user/edit');
    }
    const userEditPwd = (e) => {
      e.preventDefault();
      router.push('/user/editPassword');
    };
    const goCabinet = (e) => {
      e.preventDefault();
      router.push('/user/' + app.currentUser.id);
    };

    const logout = (e) => {
      e.preventDefault();
      localStorage.removeItem('token');
      router.push('/login')
    }
    return (
      <div className="mf-navbaruser bg-light">
        <Container>
          <Navbar color="light" light expand="md" className="p-0">
            <NavbarBrand href="/">
              <img src="/assets/images/logoNav.png" className="d-inline-block ml-4"/>
            </NavbarBrand>
            <NavbarToggler onClick={this.toggle}/>
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav navbar className="d-flex align-items-center">
                <NavItem className="mx-4">
                  <Link to={"/user/" + currentUser.id} className="nav-link active nl_2">
                    <span>Mening tadbirlarim</span>
                  </Link>
                </NavItem>
                <NavItem className="mx-4">
                  <Link to="/events" className="nav-link nl_2">
                    <span>Tadbirlar</span>
                  </Link>
                </NavItem>
              </Nav>
              <Nav navbar className="align-items-center ml-auto mr-4">
                <NavItem className="nav-item mr-4">
                  <UncontrolledDropdown setActiveFromChild>
                    <DropdownToggle tag="a" className="nav-link">
                      <div className="d-flex align-items-center justify-content-end">
                        <img className="img-fluid user_photo"
                             src={currentUser.photo == null ? "/assets/images/man.png" : ('/api/file/' + currentUser.photo.id)}
                             alt="user"/>
                        <div>
                          <span>{currentUser.firstName ? currentUser.firstName : ''}</span>
                          <span className=" d-block">{currentUser.lastName ? currentUser.lastName : ''}</span>
                        </div>
                      </div>
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem>
                        <div className="balance text-white">
                          <p className="small">Sizning xisobingiz:</p>
                          <h5 className="text-right text-white">{balance}
                            <small>UZS</small>
                          </h5>
                        </div>
                      </DropdownItem>
                      <DropdownItem onClick={(e) => goCabinet(e)}>
                        <span className="icon icon-dashboard mr-2"></span>Kabinet
                      </DropdownItem>
                      <DropdownItem tag="a" href="/blah" onClick={(e) => userEdit(e)}>
                        <span className="icon icon-setting mr-2"></span>Sozlamalar
                      </DropdownItem>
                      <DropdownItem tag="a" href="/blah" onClick={(e) => userEditPwd(e)}>
                        <span className="icon icon-key mr-2"></span>Parol o'zgartirish
                      </DropdownItem>
                      <DropdownItem tag="a" onClick={(e) => logout(e)}>
                        <span className="icon icon-log-out mr-2"></span>Chiqish
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
        </Container>
      </div>
    );
  }
}

UserCabinetNavigation.propTypes = {};

export default UserCabinetNavigation;
