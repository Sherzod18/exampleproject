import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";
import Button from 'react-bootstrap-button-loader';
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import MaskedInput from 'react-text-mask'
class SpeakerModal extends Component {
  render() {

    const {openModal,save,currentItem,photoId,logosId,
      positions,isModalShow,logosSpeaker,uploadAvatarSpeaker,isLoading}=this.props;

    return (
        <Modal isOpen={isModalShow} toggle={openModal} className="custom-modal py-4">
          <ModalHeader toggle={openModal} className="border-0 pl-5">
            <h3 className="font-weight-bolder">Yangi spiker ro'yxatga olish</h3>
          </ModalHeader>
          <ModalBody className="px-5">
            <main>
              <Row className="mt-3">

                <Col md={3}>
                  <div className="speaker-image mt-3">
                    <label htmlFor="speakerImgId" style={{width: '100%', backgroundColor: 'unset'}}
                           className="label-for-img">
                      <div className="row image-upload">
                        <img style={{width: '100%', borderRadius: '5px'}}
                             src={photoId ? '/api/file/' + photoId : ''}
                             width="100"/>
                        <div
                          style={photoId ? {display: 'none'} : {display: 'block'}}
                          className="col-md-12 upload-box text-center">
                          <div className=" upload-button">
                            <span className="icon icon-upload"/> Spiker rasmi
                            <input className="d-none" name="attachment" id="speakerImgId"
                                   onChange={uploadAvatarSpeaker}
                                   type="file" required/>
                          </div>
                        </div>
                      </div>
                    </label>
                  </div>
                </Col>

                <Col md="9">
                  <AvForm onValidSubmit={save} className="row">

                    <div className="col-md-12 mt-md-0 mt-3">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput name="company" id="company" type="text"
                                 value={currentItem !== {} ? currentItem.company : ''}
                                 required/>
                        <label>Tashkilot nomi</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-6 mt-md-0 mt-3">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput name="firstName" id="name" type="text"
                                 value={currentItem !== {} ? currentItem.name : ''}
                                 required/>
                        <label>Ismi</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-6 mt-md-0 mt-3">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput name="lastName" id="lastName" type="text"
                                 value={currentItem !== {} ? currentItem.lastName : ''}
                                 required/>
                        <label>Familiyasi</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-6 mt-md-0 mt-3">
                      <div><AvGroup>
                        <AvInput type="select" name="positionId"
                                 value={currentItem !== {} ? currentItem.positionId : ''} required>
                          <option>[Lavozimni tanlang]</option>
                          {positions.map((i,a) => <option key={a} value={i.id}>{i.nameUz}</option>)}
                        </AvInput>
                        <label>Lavozimi</label>
                      </AvGroup>
                      </div>
                    </div>
                    <div className="col-md-6 mt-md-0 mt-3">
                      <AvGroup>
                        <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                        <AvInput name="phoneNumber" id="phoneNumber"
                                 type="text"
                                 value={currentItem !== {} ? currentItem.phoneNumber : ''}
                                 required
                                 tag={MaskedInput}
                                 mask={["+", "9", "9", "8", /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
                        />
                        <label>Telefon Raqami</label>
                      </AvGroup>
                    </div>
                    <div className="col-md-12 mt-3">
                      <AvField placeholder="BIO" type="textarea"
                               value={currentItem !== {} ? currentItem.about : ''}
                               name="about" className="form-textarea"/>
                    </div>
                    <div className="col-md-12 mt-3">
                      <h4 className="title ">Tashkilot logotiplarini yuklash</h4>
                      <div className="clearfix">
                        <div className="speaker-image speaker-logo md-4">
                          <label htmlFor="logoImg" className="text-center logoImg">
                            <img src="/assets/images/img/upload.png" className="img-fluid"/> Logotip yuklash
                          </label>
                        </div>
                        <input type="file" multiple name="file" className="d-none" id="logoImg"
                               onChange={logosSpeaker}/>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="row mt-4 px-4 speaker">
                        <label className="text-center logoImg"></label>
                        {logosId.length>0?logosId.map((id,b) =>
                          <div key={b} className="col-md-3 mb-4">
                            <img className="logo img-fluid" src={"/api/file/" + id}/>
                          </div>):''}
                      </div>
                    </div>
                    <div className="col-md-3 offset-md-8 mt-4">
                      <Button type="submit" bsStyle="success" className="btn-block" loading={isLoading}>Saqlash</Button>
                    </div>
                  </AvForm>
                </Col>
              </Row>
            </main>
          </ModalBody>
        </Modal>
    );
  }
}

SpeakerModal.propTypes = {
  openModal: PropTypes.func,
  save: PropTypes.func,
  logosSpeaker: PropTypes.func,
  uploadAvatarSpeaker: PropTypes.func,

};

export default SpeakerModal;
