import React, {Component} from 'react';
import {Container, Row, Col} from 'reactstrap';
import {Link} from "react-router-dom";

class FooterAdmin extends Component {
  render() {
    return (
      <div className="mf-footer-admin">
        <footer className="bg-white">
          <Container>
            <Col md={12} className="text-center py-3">
              <a href="https://pdp.uz/home" className="text-dark">©2019 | "Personal Development Process" MCHJ</a>
            </Col>
          </Container>
        </footer>
      </div>
    );
  }
}

export default FooterAdmin;
