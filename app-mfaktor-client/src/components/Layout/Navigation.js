import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {
  Button,
  Collapse,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  UncontrolledDropdown
} from 'reactstrap';
import {router} from "umi";
import {connect} from "react-redux";

@connect(({app}) => ({app}))
class Navigation extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    const {app} = this.props;
    const logout = (e) => {
      e.preventDefault();
      localStorage.removeItem('token');
      router.push('/login')
    };
    const userEditPwd = (e) => {
      e.preventDefault();
      router.push('/user/editPassword');
    };
    const userEdit = (e) => {
      e.preventDefault();
      router.push('/user/edit');
    };
    const goCabinet = (e) => {
      e.preventDefault();
      router.push('/cabinet');
    };
    const goCabinetUser = (e) => {
      e.preventDefault();
      router.push('/user/'+app.currentUser.id);
    };

    const {user, path} = this.props;

    return (
      <div className="mf-navbar">
        <Container>
          <Navbar color="transparent" light expand="md">
            <NavbarBrand tag={Link} to="/"><img src="/assets/images/logoNav.png" className="d-inline-block"/></NavbarBrand>
            <NavbarToggler onClick={this.toggle}/>
            <Collapse isOpen={this.state.isOpen} navbar>

              <Nav navbar>
                <NavItem className="mx-3">
                  <Link to="/events" className={path === "/events" ? "nav-link active" : "nav-link"}>
                    <span>Tadbirlar</span>
                  </Link>
                </NavItem>

                <NavItem className="mx-3 px-1">
                  <Link to="/speakers" className={path === "/speakers" ? "nav-link active" : "nav-link"}>
                    <span>Spikerlar</span>
                  </Link>
                </NavItem>

                {/*<NavItem className="mx-3 px-1">*/}
                  {/*<Link to="/article" className={path === "/article" ? "nav-link active" : "nav-link"}>*/}
                    {/*<span>Maqolalar</span>*/}
                  {/*</Link>*/}
                {/*</NavItem>*/}

                <NavItem className="mx-3 px-1">
                  <Link to="/videos" className={path === "/videos" ? "nav-link active" : "nav-link"}>
                    <span>Videolar</span>
                  </Link>
                </NavItem>
              </Nav>

              <Nav navbar className="align-items-center ml-auto">
                <NavItem className="nav-item mr-4">
                  <span>(99) </span>
                  <h4 className="font-weight-bold d-inline">802-00-88</h4>
                </NavItem>

                <NavItem className="nav-item mr-4">
                  <UncontrolledDropdown setActiveFromChild>
                    <DropdownToggle tag="a" className="nav-link">
                      Uz <span className="icon icon-vector"/>
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem tag="a" href="/#" >UZ</DropdownItem>
                      <DropdownItem tag="a" href="/#" >RU</DropdownItem>
                      <DropdownItem tag="a" href="/#" >EN</DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </NavItem>

                <NavItem className="nav-item mr-4 pb-2 pb-md-0">
                  {user.id ? <div className="d-flex  align-items-center">

                      {/*<div className="img-block text-right"*/}
                      {/**/}
                      {/*style={{backgroundImage: 'url("'+ user.photo === null ? '/assets/images/user.png' : ('/api/file/' + user.photo.id) +'")',*/}
                      {/*backgroundSize: user.photo === null ? '60%' : '100%'}}>*/}
                      {/**/}
                      {/*</div>*/}
                      <div className="img-block text-right"
                           style={{
                             backgroundImage: user.photo === null ? 'url("/assets/images/user.png")' : '',
                             backgroundSize: user.photo === null ? '60%' : '100%'
                           }}>
                        {user.photo !== null ? <img style={{
                          objectFit: 'cover',
                          borderRadius: '50%',
                          width: '40px',
                          height: '40px'
                        }} src={"/api/file/" + user.photo.id} alt=""/> : ""}
                      </div>
                      <div className="user-info">
                        <UncontrolledDropdown nav inNavbar>
                          <DropdownToggle nav>
                            <h6 className="mb-0">{user.lastName + ' ' + user.firstName}</h6>
                            <p
                              className="text-muted mb-0">{user.roles.filter(i => i.name === 'ROLE_ADMIN').length > 0 ? 'Administrator' : ''}</p>
                          </DropdownToggle>
                          <DropdownMenu>
                            {user.roles.filter(i => i.name === 'ROLE_ADMIN').length > 0 ?
                              <DropdownItem onClick={(e) => goCabinet(e)}>
                                <span className="icon icon-dashboard mr-2"></span>Kabinet
                              </DropdownItem> : ''}
                            {user.roles.filter(i => i.name === 'ROLE_USER').length > 0 ?
                              <DropdownItem onClick={(e) => goCabinetUser(e)}>
                                <span className="icon icon-dashboard mr-2"></span>Kabinet
                              </DropdownItem> : ''}
                            <DropdownItem onClick={(e) => userEdit(e)}>
                              <span className="icon icon-setting mr-2"></span>Sozlamalar
                            </DropdownItem>
                            <DropdownItem onClick={(e) => userEditPwd(e)}>
                              <span className="icon icon-key mr-2"></span>Parol o'zgartirish
                            </DropdownItem>
                            <DropdownItem onClick={(e) => logout(e)}>
                              <span className="icon icon-log-out mr-2"></span>Chiqish
                            </DropdownItem>
                          </DropdownMenu>
                        </UncontrolledDropdown>
                      </div>
                    </div> :
                    <Link to="/login">
                      <Button color="outline-dark wont-weight-bold" className="btn-sm px-4">
                        Kirish
                      </Button>
                    </Link>
                  }
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
        </Container>
      </div>
    );
  }
}

Navigation.propTypes = {};

export default Navigation;
