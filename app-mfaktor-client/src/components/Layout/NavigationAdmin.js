import React, {Component} from 'react';
import Container from "reactstrap/es/Container";
import {
  Button,
  Collapse, DropdownItem, DropdownMenu,
  DropdownToggle,
  Nav,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  NavItem,
  UncontrolledDropdown
} from "reactstrap";
import {Link} from "umi";
import {router} from 'umi'

class NavigationAdmin extends Component {

  state = {
    isOpen: false
  };

  toggle = () => {
    this.setState({isOpen: !this.state.isOpen})
  };

  render() {
    const {modalShow, user, path} = this.props;

    const logout = (e) => {
      e.preventDefault();
      localStorage.removeItem('token');
      router.push('/login')
    };

    const userEditPwd = (e) => {
      e.preventDefault();
      router.push('/user/editPassword');
    };

    const userEdit = (e) => {
      e.preventDefault();
      router.push('/user/edit');
    };

    const goCabinet = (e) => {
      e.preventDefault();
      router.push('/cabinet');
    };

    return (
      <div className="mf-navbar-admin">
        <Container>
          <Navbar color="transparent" light expand="md">
            <NavbarBrand tag={Link} to='/'><img src="/assets/images/logoNav.png"
                                                className="d-inline-block"/></NavbarBrand>
            <NavbarToggler onClick={this.toggle}/>
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav navbar>
                <NavItem className="mx-3">
                  <Link to="/event" className={path === "/event" ? "nav-link active" : "nav-link"}>
                    <span>Tadbirlar</span>
                  </Link>
                </NavItem>
                <NavItem className="mx-3 px-1">
                  <Link to="/speaker" className={path === "/speaker" ? "nav-link active" : "nav-link"}>
                    <span>Spikerlar</span>
                  </Link>
                </NavItem>
                <NavItem className="mx-3 px-1">
                  <Link to="/client" className={path === "/client" ? "nav-link active" : "nav-link"}>
                    <span>Mijozlar</span>
                  </Link>
                </NavItem>
                <NavItem className="mx-3 px-1">
                  <Link to="/payment" className={path === "/payment" ? "nav-link active" : "nav-link"}>
                    <span>To’lovlar</span>
                  </Link>
                </NavItem>
                <NavItem className="mx-3 px-1">
                  <Link to="/expense" className={path === "/expense" ? "nav-link active" : "nav-link"}>
                    <span>Xarajatlar</span>
                  </Link>
                </NavItem>
                <NavItem className="mx-3 px-1">
                  <Link to="/waitingExpense" className={path === "/waitingExpense" ? "nav-link active" : "nav-link"}>
                    <span>Kutilayotgan xarajatlar</span>
                  </Link>
                </NavItem>
                <NavItem className="mx-3 px-1">
                  <Link to="/catalog" className={path.includes("/catalog") ? "nav-link active" : "nav-link"}>
                    <span>Katalog</span>
                  </Link>
                </NavItem>
              </Nav>
              <Nav navbar className="align-items-center ml-auto">
                <div className="d-flex  align-items-center">
                  <div className="img-block text-right"
                       style={{
                         backgroundImage: user.photo === null ? 'url("/assets/images/user.png")' : '',
                         backgroundSize: user.photo === null ? '60%' : '100%'
                       }}>
                    {user.photo !== null ? <img style={{
                      objectFit: 'cover',
                      borderRadius: '50%',
                      width: '40px',
                      height: '40px'
                    }} src={"/api/file/" + user.photo.id} alt=""/> : ""}
                  </div>
                  <div className="user-info">
                    <UncontrolledDropdown nav inNavbar>
                      <DropdownToggle nav caret>
                        <h6 className="mb-0">{user.lastName + ' ' + user.firstName}</h6>
                        <p
                          className="text-muted mb-0">{user.roles.filter(i => i.name === 'ROLE_ADMIN').length > 0 ? 'Administrator' : ''}</p>
                      </DropdownToggle>
                      <DropdownMenu>
                        {user.roles.filter(i => i.name === 'ROLE_ADMIN').length > 0 ?
                          <DropdownItem onClick={(e) => goCabinet(e)}>
                            <span className="icon icon-dashboard mr-2"></span>Kabinet
                          </DropdownItem> : ''}
                        <DropdownItem onClick={(e) => userEdit(e)}>
                          <span className="icon icon-setting mr-2"></span>Sozlamalar
                        </DropdownItem>
                        <DropdownItem onClick={(e) => userEditPwd(e)}>
                          <span className="icon icon-key mr-2"></span>Parol o'zgartirish
                        </DropdownItem>
                        <DropdownItem onClick={(e) => logout(e)}>
                          <span className="icon icon-log-out mr-2"></span>Chiqish
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </div>
                </div>
              </Nav>
            </Collapse>
          </Navbar>
        </Container>
      </div>
    );
  }
}

export default NavigationAdmin;
