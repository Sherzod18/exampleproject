import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {Col, Container, Nav, Navbar, NavItem, Row} from 'reactstrap';

class Footer extends Component {
  render() {
    return (
      <div id="contact" className="mf-footer py-2">
        <Container>
          <Row className="align-items-center">
            <Col md={2} className="text-center">
              <Link to="/"><img src="/assets/images/logoFooter.png" className="img-fluid" alt=""/></Link>
            </Col>
            <Col md={7}>
              <Navbar>
                <Nav>
                  <NavItem>
                    <Link to="/events" className="nav-link text-muted">
                      <span>Tadbirlar</span>
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link to="/speakers" className="nav-link text-muted">
                      <span>Spikerlar</span>
                    </Link>
                  </NavItem>
                  {/*<NavItem>*/}
                  {/*<Link to="/article" className="nav-link text-muted">*/}
                  {/*<span>Maqolalar</span>*/}
                  {/*</Link>*/}
                  {/*</NavItem>*/}
                  <NavItem>
                    <Link to="/videos" className="nav-link text-muted">
                      <span>Videolar</span>
                    </Link>
                  </NavItem>
                  <NavItem>
                    <Link to="#contact" className="nav-link text-muted">
                      <span>Kontakt</span>
                    </Link>
                  </NavItem>
                </Nav>
              </Navbar>
              <a href="https://pdp.uz/home" className="m-0 pl-4 text-muted">
                © 2019 Personal Development Process
              </a>
            </Col>
            <Col md={3}>
              <Navbar>
                <Nav navbar>
                  <NavItem className="nav-item mr-4">
                    <span><span className="icon icon-phone"/> (99) </span>
                    <h4 className="font-weight-bold d-inline">802-00-88</h4>
                  </NavItem>
                </Nav>
                <Nav className="nav-footer">
                  <NavItem className="nav-item">
                    <a href="https://www.youtube.com/channel/UCOH0xOR3ZUiqFVd6cf2djiQ" className="nav-link nav-footer">
                      <span className="icon icon-youtube"/>
                    </a>
                  </NavItem>
                  <NavItem className="nav-item">
                    <a href="https://t.me/MFaktorUz" to="#" className="nav-link nav-footer">
                      <span className="icon icon-telegram"/>
                    </a>
                  </NavItem>
                  <NavItem className="nav-item">
                    <a href="https://www.facebook.com/MFaktorUz/" className="nav-link nav-footer">
                      <span className="icon icon-facebook"/>
                    </a>
                  </NavItem>
                  <NavItem className="nav-item">
                    <a href="https://www.instagram.com/mfaktoruz/" className="nav-link nav-footer">
                      <span className="icon icon-instagram"/>
                    </a>
                  </NavItem>
                </Nav>
              </Navbar>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Footer.propTypes = {};

export default Footer;
