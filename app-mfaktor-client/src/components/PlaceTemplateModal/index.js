import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Col, Modal, ModalBody, ModalHeader, Badge, Row} from "reactstrap";
import {AvFeedback, AvField, AvForm, AvGroup, AvInput} from "availity-reactstrap-validation";
import Button from 'react-bootstrap-button-loader';

class PlaceTemplateModal extends Component {

  render() {
    const {
      openModal, isModalShow, getName, place, reqChairs,
      addRowChairs, addChair, saveChairs, deleteChair, closeModal, isLoading
    } = this.props;

    return (
      <Modal isOpen={isModalShow} toggle={openModal} className="custom-modal">
        <ModalHeader toggle={openModal} className="border-0"/>
        <ModalBody className="px-5">
          <div className="container">
            <h2 className="title pt-5">Stollar joylashuvi qo'shish</h2>
            <div className="row m-0">
              <div className="col-md-12 set-card bg-white">
                <AvForm>

                  <div className="row mb-2">
                    <div className="col-md-12">
                      <div className="row">
                        <div className="col-md-4 wrap_input px-3">
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput onChange={getName} id="place_name" name="place_name" type="text" value={place.name || ''}
                                     required/>
                            <label>Joylashuv nomi</label>
                          </AvGroup>
                        </div>
                        <div className="col-md-3 d-flex wrap_input ml-md-4 ml-0 mt-md-0 mt-3 px-3">
                          <label htmlFor="place_count">O'rinlar soni</label>
                          <h1><Badge
                            color="danger" className="ml-3">{reqChairs.reduce((res, item) => res + item.length, 0)}</Badge></h1>
                        </div>

                      </div>
                    </div>
                  </div>

                  {reqChairs ? <div>{reqChairs.length > 0 ? <div className="row">
                      <div className="col-md-4 wrap_input ml-3">
                        <label htmlFor="place_name">Qator narxi</label>
                      </div>
                    </div>
                    : ''}</div> : ''}


                  {reqChairs.map((row, index) => <div className="row mt-2 mb-2 pt-3 mr-3 ml-3 set-list">
                    <Col md={2} className="pl-0">
                      <div>
                        <h3><Badge
                          color="danger" style={{lineHeight: '45px'}}>
                          {row.length !== 0 ? row[0].price : ''}
                        </Badge></h3>
                      </div>
                    </Col>
                    <Col md={10}>
                      <Row className="set-list">

                        {row.map(i => <AvField className="border-0 set-name"
                                               value={i.name} disabled
                                               name="setName" type="text"/>)}
                        {reqChairs[index].length !== 0 ? <div>
                            <div className="form-group-btn">
                              <Button className="add-button border-0 mr-2" onClick={() => addChair(index)}>
                                +
                              </Button>
                            </div>
                          </div>
                          : ''}
                        {reqChairs[index].length !== 0 ? <div>
                            <div className="form-group-btn">
                              <Button className="add-button btn-danger border-0" onClick={() => deleteChair(index)}>
                                -
                              </Button>
                            </div>
                          </div>
                          : ''}
                      </Row>
                    </Col>
                  </div>)}
                  <div className="row">
                    <div className="col-md-12 pl-0">
                      <div className="float-right d-md-flex d-block">
                        <div className="wrap_input mx-md-2 mx-0 mt-md-0 mt-3">

                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput  id="place_price" onChange={getName} name="place_price" type="text" value={place.name || ''}
                                     required/>
                            <label>Qator narxi</label>
                          </AvGroup>
                        </div>
                        <div className="wrap_input mx-md-2 mx-0 mt-md-0 mt-3">
                          <AvGroup>
                            <AvFeedback>Bu joy to'ldirilishi shart</AvFeedback>
                            <AvInput onChange={getName} name="place_line_count" id="place_line_count" type="text" value={place.name || ''}
                                     required/>
                            <label>Qatordagi o'rinlar soni</label>
                          </AvGroup>
                        </div>
                        <div className="wrap_place mb_btn mx-md-2 mx-0" style={{marginTop: '6px'}}>
                          <button className="btn btn-success mt-0" type="button" onClick={addRowChairs}>+</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row mt-4">
                    <div className="col-md-12">
                      <div className="wrap_place  float-right ">
                        <button className="btn px-4 mr-2 btn-outline-secondary" onClick={closeModal} type="button">Bekor qilish
                        </button>


                        <Button className="btn btn-success btn-save px-4 " bsStyle="success" loading={isLoading}
                                onClick={saveChairs}
                                type="submit">Saqlash
                        </Button>
                      </div>
                    </div>
                  </div>
                </AvForm>
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

PlaceTemplateModal.propTypes = {
  openModal: PropTypes.func,
  addRowChairs: PropTypes.func,
  addChair: PropTypes.func,
  saveChairs: PropTypes.func,

};

export default PlaceTemplateModal;
