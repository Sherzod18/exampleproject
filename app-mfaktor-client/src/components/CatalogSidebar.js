import React, {Component} from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import {Link} from "react-router-dom";
class CatalogSidebar extends Component {
  render() {
    return (
      <div className="catalog-sidebar">
        <h6 className="title font-weight-bold">Kataloglar ro’yxati</h6>
        <ListGroup>
          <ListGroupItem className={this.props.pathname === "/catalog/eventtype" ? "active-catalog" : ""}><Link to="/catalog/eventtype" className="nav-link">Tadbir turi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/paytype" ? "active-catalog" : ""}><Link to="/catalog/paytype" className="nav-link">To'lov turi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/cashType" ? "active-catalog" : ""}><Link to="/catalog/cashType" className="nav-link">Kassa turi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/expencetype" ? "active-catalog" : ""}><Link to="/catalog/expencetype" className="nav-link">Xarajat turi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/position" ? "active-catalog" : ""}><Link to="/catalog/position" className="nav-link">Lavozim turi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/aware" ? "active-catalog" : ""}><Link to="/catalog/aware" className="nav-link">Reklama manbalari</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/tariff" ? "active-catalog" : ""}><Link to="/catalog/tariff" className="nav-link">Tarif turi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/category" ? "active-catalog" : ""}><Link to="/catalog/category" className="nav-link">Maqola Kategoriyasi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/videoType" ? "active-catalog" : ""}><Link to="/catalog/videoType" className="nav-link">Video turi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/attachmenttype" ? "active-catalog" : ""}><Link to="/catalog/attachmenttype" className="nav-link">Banner o'lchamlari</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/review" ? "active-catalog" : ""}><Link to="/catalog/review" className="nav-link">Izohlar</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/post" ? "active-catalog" : ""}><Link to="/catalog/post" className="nav-link">Maqola</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/voucher" ? "active-catalog" : ""}><Link to="/catalog/voucher" className="nav-link">Vaucher</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/archive" ? "active-catalog" : ""}><Link to="/catalog/archive" className="nav-link">Archive</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/transfer" ? "active-catalog" : ""}><Link to="/catalog/transfer" className="nav-link">Pul ko'chirish</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/event/places" ? "active-catalog" : ""}><Link to="/event/places" className="nav-link">Stollar joylashuvi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/videos" ? "active-catalog" : ""}><Link to="/videos" className="nav-link">Videolar</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/usd" ? "active-catalog" : ""}><Link to="/catalog/usd" className="nav-link">Dollar kursi</Link></ListGroupItem>
          <ListGroupItem className={this.props.pathname === "/catalog/message" ? "active-catalog" : ""}><Link to="/catalog/message" className="nav-link">Xabar jo'natish</Link></ListGroupItem>
        </ListGroup>
      </div>
    );
  }
}

export default CatalogSidebar;
