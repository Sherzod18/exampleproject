import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import {Button, Col, Container, Row,Card,CardBody,CardImg} from "reactstrap";
import ModalVideo from 'react-modal-video';
import {connect} from "react-redux";
@connect(({landingPage}) => ({landingPage}))
class Header extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'landingPage/getAnonsType'
    })
  }

  constructor() {
    super();
    this.state = {
      isOpen: false
    };
    this.openModal = this.openModal.bind(this)
  }

  openModal() {
    this.setState({isOpen: true})
  }

  render() {
    const {dispatch,landingPage}=this.props;
    const {anonsVideoType}=landingPage;
    const getAnonsType = () =>{
      dispatch({
        type: 'landingPage/getAnonsType'
      })
    };
    return (
      <div className="mf-header py-md-5 py-0">
        <Container className="pt-4">
          <Row>
            <Col md={6}>
              <h1 className="font-weight-bold">
                MFaktor-muvaffaqiyat va mag’lubiyat omillari
              </h1>
              <p className="py-4 text-md-left text-justify">
                O’zbekistonning eng muvaffaqiyatli
                tadbirkorlaridan biznes saboqlari
                va mahorat darslarini olish imkoniyatini
                qo’ldan boy bermang. Biznesingizni
                MFaktor yordamida rivojlantiring.
              </p>
              <p className="py-4">
                <Link onClick={getAnonsType} to={"/videos/"+anonsVideoType} className="btn btn-outline-dark btn-lg px-5 media-anons">
                  Anonslar
                </Link>
                <Link to="/login" className="btn btn-danger ml-md-5 ml-0 btn-lg register-media">
                  Ro’yxatdan o’tish
                </Link>
              </p>
            </Col>
          </Row>
        </Container>
        <div className="mf-video-block">
          {/*<img src="/assets/images/landing-back.png" className="img-fluid" alt=""/>*/}
          <Button onClick={this.openModal}  color="link">
              <span className="icon icon-play"/>
          </Button>
        </div>
        <ModalVideo channel='youtube' isOpen={this.state.isOpen} videoId='6rEnqE2jQoU'
                    onClose={() => this.setState({isOpen: false})}/>
      </div>
    );
  }
}

Header.propTypes = {};

export default Header;
